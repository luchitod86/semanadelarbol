﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class UsuarioActividad : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioActividad()
        {
        }

        #endregion

        #region Propiedades

        private Usuario _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual Usuario Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private string _Descripcion;
        /// <summary>
        /// Descripcion
        /// </summary>
        public virtual string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        private DateTime _FechaYHora;
        /// <summary>
        /// Fecha y Hora
        /// </summary>
        public virtual DateTime FechaYHora
        {
            get { return _FechaYHora; }
            set { _FechaYHora = value; }
        }

        private float _Latitud;
        /// <summary>
        /// Latitud
        /// </summary>
        public virtual float Latitud
        {
            get { return _Latitud; }
            set { _Latitud = value; }
        }

        private float _Longitud;
        /// <summary>
        /// Longitud
        /// </summary>
        public virtual float Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private Provincia _Provincia;
        /// <summary>
        /// Provincia
        /// </summary>
        public virtual Provincia Provincia
        {
            get { return _Provincia; }
            set { _Provincia = value; }
        }

        private string _Comentarios;
        /// <summary>
        /// Comentarios
        /// </summary>
        public virtual string Comentarios
        {
            get { return _Comentarios; }
            set { _Comentarios = value; }
        }

        private bool _EsPublica;
        /// <summary>
        /// Es Pública?
        /// </summary>
        public virtual bool EsPublica
        {
            get { return _EsPublica; }
            set { _EsPublica = value; }
        }

        #endregion
    }
}
