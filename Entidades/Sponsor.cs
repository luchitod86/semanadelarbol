﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Sponsor : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Sponsor()
        {
        }

        #endregion

        #region Propiedades
        
        private string _Imagen;
        /// <summary>
        /// Imagen
        /// </summary>
        public virtual string Imagen
        {
            get { return _Imagen; }
            set { _Imagen = value; }
        }

        private string _Web;
        /// <summary>
        /// Web
        /// </summary>
        public virtual string Web
        {
            get { return _Web; }
            set { _Web = value; }
        }

        private SponsorTipo _Tipo;
        /// <summary>
        /// Tipo
        /// </summary>
        public virtual SponsorTipo Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// País
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
