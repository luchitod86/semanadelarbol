﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Noticia : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Noticia()
        {
        }

        #endregion

        #region Propiedades

        private string _Titulo;
        /// <summary>
        /// Titulo
        /// </summary>
        public virtual string Titulo
        {
            get { return _Titulo; }
            set { _Titulo = value; }
        }

        private string _Resumen;
        /// <summary>
        /// Resumen
        /// </summary>
        public virtual string Resumen
        {
            get { return _Resumen; }
            set { _Resumen = value; }
        }
        private string _Texto;
        /// <summary>
        /// Texto
        /// </summary>
        public virtual string Texto
        {
            get { return _Texto; }
            set { _Texto = value; }
        }

        private DateTime _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        private IList<NoticiaImagen> _Imagenes;
        /// <summary>
        /// Imagenes para la Noticia
        /// </summary>
        public virtual IList<NoticiaImagen> Imagenes
        {
            get { return _Imagenes; }
            set { _Imagenes = value; }
        }
      
        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
