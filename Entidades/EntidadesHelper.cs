﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class EntidadesHelper
    {
        public static string ObtenerNombre(Usuario usuario)
        {
            var nombre      = usuario.Nombre;
            var apellido    = String.IsNullOrEmpty(usuario.Apellido) ? String.Empty : " " + usuario.Apellido;

            return nombre + apellido;
        }
    }
}
