﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    /// <summary>
    /// Clase de Configuracion de Email
    /// </summary>
    public class ConfiguracionEmail : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public ConfiguracionEmail()
        {

        }

        #endregion

        #region Propiedades

        #region Emisor

        private string _EmisorAlias;
        /// <summary>
        /// Alias del Emisor
        /// </summary>
        public virtual string EmisorAlias
        {
            get { return _EmisorAlias; }
            set { _EmisorAlias = value; }
        }

        private string _EmisorUsuario;
        /// <summary>
        /// Nombre de Usuario del Emisor
        /// </summary>
        public virtual string EmisorUsuario
        {
            get { return _EmisorUsuario; }
            set { _EmisorUsuario = value; }
        }

        private string _EmisorDireccion;
        /// <summary>
        /// Direccion del Emisor
        /// </summary>
        public virtual string EmisorDireccion
        {
            get { return _EmisorDireccion; }
            set { _EmisorDireccion = value; }
        }

        private string _EmisorPassword;
        /// <summary>
        /// Password del Emisor
        /// </summary>
        public virtual string EmisorPassword
        {
            get { return _EmisorPassword; }
            set { _EmisorPassword = value; }
        }

        #endregion

        #region Servidor SMTP

        private string _SMTPServidor;
        /// <summary>
        /// Direccion del Servidor SMTP
        /// </summary>
        public virtual string SMTPServidor
        {
            get { return _SMTPServidor; }
            set { _SMTPServidor = value; }
        }

        private int _SMTPPuerto;
        /// <summary>
        /// Puerto del Servidor SMTP
        /// </summary>
        public virtual int SMTPPuerto
        {
            get { return _SMTPPuerto; }
            set { _SMTPPuerto = value; }
        }

        private bool _SMTPHabilitarSSL;
        /// <summary>
        /// Habilitar SSL ?
        /// </summary>
        public virtual bool SMTPHabilitarSSL
        {
            get { return _SMTPHabilitarSSL; }
            set { _SMTPHabilitarSSL = value; }
        }

        #endregion

        #endregion
    }
}
