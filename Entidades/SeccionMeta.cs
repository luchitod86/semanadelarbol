﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class SeccionMeta : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SeccionMeta()
        {
        }

        #endregion

        #region Propiedades

        private Seccion _Seccion;
        /// <summary>
        /// Sección
        /// </summary>
        public virtual Seccion Seccion
        {
            get { return _Seccion; }
            set { _Seccion = value; }
        }

        private string _Keywords;
        /// <summary>
        /// Keywords
        /// </summary>
        public virtual string Keywords
        {
            get { return _Keywords; }
            set { _Keywords = value; }
        }

        private string _Description;
        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        #endregion

    }
}
