﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Seccion : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Seccion()
        {
        }

        #endregion

        #region Propiedades

        /// <summary>
        /// Tipo de Sección
        /// </summary>
        private SeccionTipo _Tipo;
        public virtual SeccionTipo Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        /// <summary>
        /// Descripción
        /// </summary>
        private string _Descripcion;
        public virtual string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        private IList<SeccionContenido> _Contenidos;
        /// <summary>
        /// Contenidos de la Sección
        /// </summary>
        public virtual IList<SeccionContenido> Contenidos
        {
            get { return _Contenidos; }
            set { _Contenidos = value; }
        }

        private bool _Privada;
        /// <summary>
        /// Privada
        /// </summary>
        public virtual bool Privada
        {
            get { return _Privada; }
            set { _Privada = value; }
        }

        #endregion
    }
}
