﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Pais : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Pais()
        {
        }

        #endregion

        #region Propiedades

        private Continente _Continente;
        /// <summary>
        /// Continente
        /// </summary>
        public virtual Continente Continente
        {
            get { return _Continente; }
            set { _Continente = value; }
        }

        private string _Bandera;
        /// <summary>
        /// Bandera
        /// </summary>
        public virtual string Bandera
        {
            get { return _Bandera; }
            set { _Bandera = value; }
        }

        private string _Lenguaje;
        /// <summary>
        /// Lenguaje
        /// </summary>
        public virtual string Lenguaje
        {
            get { return _Lenguaje; }
            set { _Lenguaje = value; }
        }

        private bool _Activo;
        /// <summary>
        /// Activo
        /// </summary>
        public virtual bool Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }

        #endregion
    }
}
