﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class UsuarioAdopcion : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioAdopcion()
        {
        }

        #endregion

        #region Propiedades

        private Usuario _Usuario;
        /// <summary>
        /// Donante
        /// </summary>
        public virtual Usuario Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private Usuario _Donante;
        /// <summary>
        /// Donante
        /// </summary>
        public virtual Usuario Donante
        {
            get { return _Donante; }
            set { _Donante = value; }
        }

        private Especie _Arbol;
        /// <summary>
        /// Arbol
        /// </summary>
        public virtual Especie Arbol
        {
            get { return _Arbol; }
            set { _Arbol = value; }
        }

        private int _Cantidad;
        /// <summary>
        /// Cantidad
        /// </summary>
        public virtual int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }

        private DateTime _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
