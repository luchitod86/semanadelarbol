﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Email : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Email()
        {
        }

        /// <summary>
        /// Construye un objeto Email a partir de un nombre, mail y comentarios
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="mail"></param>
        /// <param name="comentarios"></param>
        public Email(string nombre, string email, string comentarios)
        {
            this.Nombre         = nombre;
            this.Mail          = email;
            this.Comentarios    = comentarios;
            this.Fecha          = DateTime.Now;
        }

        #endregion

        #region Propiedades

        private string _Mail;
        /// <summary>
        /// Email
        /// </summary>
        public virtual string Mail
        {
            get { return _Mail; }
            set { _Mail = value; }
        }

        private string _Comentarios;
        /// <summary>
        /// Comentarios
        /// </summary>
        public virtual string Comentarios
        {
            get { return _Comentarios; }
            set { _Comentarios = value; }
        }

        private DateTime _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        #endregion
    }
}
