﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Vivero : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Vivero()
        {
        }

        #endregion

        #region Propiedades

        private Usuario _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual Usuario Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private string _Direccion;
        /// <summary>
        /// Direccion
        /// </summary>
        public virtual string Direccion
        {
            get { return _Direccion; }
            set { _Direccion = value; }
        }

        private string _Telefono;
        /// <summary>
        /// Telefono
        /// </summary>
        public virtual string Telefono
        {
            get { return _Telefono; }
            set { _Telefono = value; }
        }

        private string _Email;
        /// <summary>
        /// Email
        /// </summary>
        public virtual string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _Web;
        /// <summary>
        /// Web
        /// </summary>
        public virtual string Web
        {
            get { return _Web; }
            set { _Web = value; }
        }

        private byte[] _Imagen;
        /// <summary>
        /// Imagen
        /// </summary>
        public virtual byte[] Imagen
        {
            get { return _Imagen; }
            set { _Imagen = value; }
        }

        #endregion
    }
}
