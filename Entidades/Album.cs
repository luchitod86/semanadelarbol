﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Album : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Album()
        {
        }

        #endregion

        #region Propiedades

        private string _IdEntidad;
        /// <summary>
        /// Id Entidad
        /// </summary>
        public virtual string IdEntidad
        {
            get { return _IdEntidad; }
            set { _IdEntidad = value; }
        }

        private string _IdAlbum;
        /// <summary>
        /// Id Album
        /// </summary>
        public virtual string IdAlbum
        {
            get { return _IdAlbum; }
            set { _IdAlbum = value; }
        }

        private string _Url;
        /// <summary>
        /// Url
        /// </summary>
        public virtual string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private DateTime _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        #endregion
    }
}
