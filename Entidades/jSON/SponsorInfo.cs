﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// 
    /// </summary>
    public class SponsorInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SponsorInfo()
        {
        }

        public SponsorInfo(Sponsor sponsor)
        {
            this.Id         = sponsor.Id;
            this.Nombre     = sponsor.Nombre;
            this.Imagen     = sponsor.Imagen;
            this.Web        = sponsor.Web;
            this.IdTipo     = sponsor.Tipo.Id;
            this.Tipo       = sponsor.Tipo.Nombre;
            this.IdPais     = sponsor.Pais.Id;
            this.Pais       = sponsor.Pais.Nombre;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Imagen;
        /// <summary>
        /// Imagen
        /// </summary>
        public virtual string Imagen
        {
            get { return _Imagen; }
            set { _Imagen = value; }
        }

        private string _Web;
        /// <summary>
        /// Web
        /// </summary>
        public virtual string Web
        {
            get { return _Web; }
            set { _Web = value; }
        }

        private int _IdTipo;
        /// <summary>
        /// Id de Tipo
        /// </summary>
        public virtual int IdTipo
        {
            get { return _IdTipo; }
            set { _IdTipo = value; }
        }

        private string _Tipo;
        /// <summary>
        /// Tipo
        /// </summary>
        public virtual string Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }
        private int _IdPais;
        /// <summary>
        /// Id de País
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        private string _Pais;
        /// <summary>
        /// País
        /// </summary>
        public virtual string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
