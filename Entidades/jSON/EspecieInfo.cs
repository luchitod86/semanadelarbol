﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    public class EspecieInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspecieInfo()
        {
        }

        public EspecieInfo(Especie especie, bool infoCompleta = false)
        {
            this.Id                 = especie.Id;
            this.Nombre             = especie.Nombre.NullToString();
            this.FactorCO2          = especie.FactorCO2;
            this.NombreCientifico   = especie.NombreCientifico.NullToString();
            this.EsExotico = especie.EsExotico.GetValueOrDefault(false);
            this.EsNativo = especie.EsNativo.GetValueOrDefault(false);
            int? nullableint = null;
            this.IdPais = especie.Pais != null ? especie.Pais.Id : nullableint;
            this.NombrePais = especie.Pais != null ? especie.Pais.Nombre : string.Empty;
            if (infoCompleta == true){
                this.DesdeWiki          = especie.DesdeWiki;
                this.Aprobado           = especie.Aprobado;
                this.Origen             = especie.Origen.NullToString();
                this.Ecorregion         = especie.Ecorregion.NullToString();
                this.Altura             = especie.Altura.NullToString();
                this.Follaje            = especie.Follaje.NullToString();
                this.Hojas              = especie.Hojas.NullToString();
                this.Flores             = especie.Flores.NullToString();
                this.Frutos             = especie.Frutos.NullToString();
                this.MasInfo            = especie.MasInfo.NullToString();
                this.AptoCantero        = especie.AptoCantero.GetValueOrDefault(false);
                this.AptoPlaza          = especie.AptoPlaza.GetValueOrDefault(false);
                this.AptoVereda         = especie.AptoVereda.GetValueOrDefault(false);
            
                this.Foto1              = especie.Imagenes.Count > 0 ? especie.Imagenes[0].URLImagen : null;
                this.Foto2              = especie.Imagenes.Count > 1 ? especie.Imagenes[1].URLImagen : null;
                this.Foto3              = especie.Imagenes.Count > 2 ?especie.Imagenes[2].URLImagen : null;
                this.Foto4              = especie.Imagenes.Count > 3 ? especie.Imagenes[3].URLImagen : null;
                this.Foto5              = especie.Imagenes.Count > 4 ? especie.Imagenes[4].URLImagen : null;
                this.Foto6              = especie.Imagenes.Count > 5 ? especie.Imagenes[5].URLImagen : null;
            }
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _NombreCientifico;
        /// <summary>
        /// Nombre Científico
        /// </summary>
        public virtual string NombreCientifico
        {
            get { return _NombreCientifico; }
            set { _NombreCientifico = value; }
        }

        private double _FactorCO2;
        /// <summary>
        /// Factor CO2
        /// </summary>
        public virtual double FactorCO2
        {
            get { return _FactorCO2; }
            set { _FactorCO2 = value; }
        }

        private string _MasInfo;
        /// <summary>
        /// MasInfo
        /// </summary>
        public virtual string MasInfo
        {
            get { return _MasInfo; }
            set { _MasInfo = value; }
        }

        private bool? _DesdeWiki;
        /// <summary>
        /// DesdeWiki
        /// </summary>
        public virtual bool? DesdeWiki
        {
            get { return _DesdeWiki; }
            set { _DesdeWiki = value; }
        }

        private bool? _Aprobado;
        /// <summary>
        /// Aprobado
        /// </summary>
        public virtual bool? Aprobado
        {
            get { return _Aprobado; }
            set { _Aprobado = value; }
        }

        private bool _EsNativo;
        /// <summary>
        /// EsNativo
        /// </summary>
        public virtual bool EsNativo
        {
            get { return _EsNativo; }
            set { _EsNativo = value; }
        }

        private bool _EsExotico;
        /// <summary>
        /// EsExotico
        /// </summary>
        public virtual bool EsExotico
        {
            get { return _EsExotico; }
            set { _EsExotico = value; }
        }

        private string _Origen;
        /// <summary>
        /// Origen
        /// </summary>
        public virtual string Origen
        {
            get { return _Origen; }
            set { _Origen = value; }
        }

        private string _Ecorregion;
        /// <summary>
        /// Ecorregion
        /// </summary>
        public virtual string Ecorregion
        {
            get { return _Ecorregion; }
            set { _Ecorregion = value; }
        }

        private string _Altura;
        /// <summary>
        /// Altura
        /// </summary>
        public virtual string Altura
        {
            get { return _Altura; }
            set { _Altura = value; }
        }

        private string _Follaje;
        /// <summary>
        /// Follaje
        /// </summary>
        public virtual string Follaje
        {
            get { return _Follaje; }
            set { _Follaje = value; }
        }

        private string _Hojas;
        /// <summary>
        /// Hojas
        /// </summary>
        public virtual string Hojas
        {
            get { return _Hojas; }
            set { _Hojas = value; }
        }

        private string _Flores;
        /// <summary>
        /// Flores
        /// </summary>
        public virtual string Flores
        {
            get { return _Flores; }
            set { _Flores = value; }
        }

        private string _Frutos;
        /// <summary>
        /// Frutos
        /// </summary>
        public virtual string Frutos
        {
            get { return _Frutos; }
            set { _Frutos = value; }
        }

        private bool _AptoVereda;
        /// <summary>
        /// AptoVereda
        /// </summary>
        public virtual bool AptoVereda
        {
            get { return _AptoVereda; }
            set { _AptoVereda = value; }
        }

        private bool _AptoCantero;
        /// <summary>
        /// AptoCantero
        /// </summary>
        public virtual bool AptoCantero
        {
            get { return _AptoCantero; }
            set { _AptoCantero = value; }
        }

        private bool _AptoPlaza;
        /// <summary>
        /// AptoPlaza
        /// </summary>
        public virtual bool AptoPlaza
        {
            get { return _AptoPlaza; }
            set { _AptoPlaza = value; }
        }

        private int? _IdPais;
        /// <summary>
        /// IdPais
        /// </summary>
        public virtual int? IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        private string _NombrePais;
        /// <summary>
        /// NombrePais
        /// </summary>
        public virtual string NombrePais
        {
            get { return _NombrePais; }
            set { _NombrePais = value; }
        }

        private string _Foto1;
        /// <summary>
        /// Foto1
        /// </summary>
        public virtual string Foto1
        {
            get { return _Foto1; }
            set { _Foto1 = value; }
        }

        private string _Foto2;
        /// <summary>
        /// Foto2
        /// </summary>
        public virtual string Foto2
        {
            get { return _Foto2; }
            set { _Foto2 = value; }
        }

        private string _Foto3;
        /// <summary>
        /// Foto4
        /// </summary>
        public virtual string Foto3
        {
            get { return _Foto3; }
            set { _Foto3 = value; }
        }

        private string _Foto4;
        /// <summary>
        /// Foto4
        /// </summary>
        public virtual string Foto4
        {
            get { return _Foto4; }
            set { _Foto4 = value; }
        }

        private string _Foto5;
        /// <summary>
        /// Foto5
        /// </summary>
        public virtual string Foto5
        {
            get { return _Foto5; }
            set { _Foto5 = value; }
        }

        private string _Foto6;
        /// <summary>
        /// Foto6
        /// </summary>
        public virtual string Foto6
        {
            get { return _Foto6; }
            set { _Foto6 = value; }
        }     

        #endregion
    }
}
