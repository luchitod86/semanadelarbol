﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades.jSON.Sistema;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase para mapear el contenido de un objeto Usuario
    /// </summary>
    public class UsuarioInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public UsuarioInfo()
        {
        }

        /// <summary>
        /// Construye un objeto UsuarioInfo a partir de un Usuario
        /// </summary>
        /// <param name="usuario"></param>
        public UsuarioInfo(Usuario usuario)
        {
            this.Id = usuario.Id;

            #region Web
            this.NombreUsuario              = usuario.NombreUsuario;
            this.Password                   = usuario.Password;
            this.NombreUsuarioFacebook      = usuario.NombreUsuarioFacebook;
            this.NombreUsuarioTwitter       = usuario.NombreUsuarioTwitter;
            this.NombreUsuarioGooglePlus    = usuario.NombreUsuarioGooglePlus;
            this.FechaCreacion              = usuario.FechaCreacion;

            #endregion

            this.Nombre = usuario.Nombre;
            this.Apellido = string.IsNullOrEmpty(usuario.Apellido) ? String.Empty : usuario.Apellido;

            this.Web = String.IsNullOrEmpty(usuario.Web) ? String.Empty : usuario.Web;
            this.Email = usuario.Email;
            this.Telefono = usuario.Telefono;

            this.IdTipo     = usuario.Tipo.Id;
            this.Tipo       = usuario.Tipo.Nombre;
            this.IdPerfil   = usuario.Perfil.Id;
            this.Perfil     = usuario.Perfil.Nombre;

            #region Ubicacion
            if ( usuario.Pais != null )
            {
                this.IdPais = usuario.Pais.Id;
                this.Pais = usuario.Pais.Nombre;
            }
            if ( usuario.Provincia != null )
            {
                this.IdProvincia = usuario.Provincia.Id;
                this.Provincia = usuario.Provincia.Nombre;
            }
            this.Ciudad = usuario.Ciudad;
            #endregion

            #region Donante
            this.Latitud    = usuario.Latitud;
            this.Longitud   = usuario.Longitud;
            #endregion
        }

        #endregion

        #region Propiedades
        
        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private int _IdTipo;
        /// <summary>
        /// Id del Tipo de Usuario
        /// </summary>
        public virtual int IdTipo
        {
            get { return _IdTipo; }
            set { _IdTipo = value; }
        }

        private string _Tipo;
        /// <summary>
        /// Tipo de Usuario
        /// </summary>
        public virtual string Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        private int _IdPerfil;
        /// <summary>
        /// Id del Perfil de Usuario
        /// </summary>
        public virtual int IdPerfil
        {
            get { return _IdPerfil; }
            set { _IdPerfil = value; }
        }

        private string _Perfil;
        /// <summary>
        /// Perfil de Usuario
        /// </summary>
        public virtual string Perfil
        {
            get { return _Perfil; }
            set { _Perfil = value; }
        }

        #region Web

        private string _NombreUsuario;
        /// <summary>
        /// Nombre de Usuario
        /// </summary>
        public string NombreUsuario
        {
            get { return _NombreUsuario; }
            set { _NombreUsuario = value; }
        }

        private string _Password;
        /// <summary>
        /// Password
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        private bool _Admin;
        /// <summary>
        /// Admin
        /// </summary>
        public bool Admin
        {
            get { return _Admin; }
            set { _Admin = value; }
        }

        private string _NombreUsuarioFacebook;
        /// <summary>
        /// Nombre de Usuario de Facebook
        /// </summary>
        public virtual string NombreUsuarioFacebook
        {
            get { return _NombreUsuarioFacebook; }
            set { _NombreUsuarioFacebook = value; }
        }

        private string _NombreUsuarioTwitter;
        /// <summary>
        /// Nombre de Usuario de Twitter
        /// </summary>
        public virtual string NombreUsuarioTwitter
        {
            get { return _NombreUsuarioTwitter; }
            set { _NombreUsuarioTwitter = value; }
        }

        private string _NombreUsuarioGooglePlus;
        /// <summary>
        /// Nombre de Usuario de Google+
        /// </summary>
        public virtual string NombreUsuarioGooglePlus
        {
            get { return _NombreUsuarioGooglePlus; }
            set { _NombreUsuarioGooglePlus = value; }
        }

        private DateTime _FechaCreacion;
        /// <summary>
        /// Fecha Creación
        /// </summary>
        public virtual DateTime FechaCreacion
        {
            get { return _FechaCreacion; }
            set { _FechaCreacion = value; }
        }

        #endregion

        #region Individuo / Institucion / Municipio

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Apellido;
        /// <summary>
        /// Apellido
        /// </summary>
        public string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }

        private string _Telefono;
        /// <summary>
        /// _
        /// </summary>
        public string Telefono
        {
            get { return _Telefono; }
            set { _Telefono = value; }
        }

        private string _Email;
        /// <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _Web;
        /// <summary>
        /// Web
        /// </summary>
        public string Web
        {
            get { return _Web; }
            set { _Web = value; }
        }

        #endregion

        #region Ubicacion

        private int _IdPais;
        /// <summary>
        /// Id del País
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        private string _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private int _IdProvincia;
        /// <summary>
        /// Id de la Provincia
        /// </summary>
        public virtual int IdProvincia
        {
            get { return _IdProvincia; }
            set { _IdProvincia = value; }
        }
        private string _Provincia;

        /// <summary>
        /// Provincia
        /// </summary>
        public virtual string Provincia
        {
            get { return _Provincia; }
            set { _Provincia = value; }
        }

        private string _Ciudad;
        /// <summary>
        /// Ciudad
        /// </summary>
        public virtual string Ciudad
        {
            get { return _Ciudad; }
            set { _Ciudad = value; }
        }

        #endregion

        #region Donante

        private float _Latitud;
        /// <summary>
        /// Latitud
        /// </summary>
        public virtual float Latitud
        {
            get { return _Latitud; }
            set { _Latitud = value; }
        }

        private float _Longitud;
        /// <summary>
        /// Longitud
        /// </summary>
        public virtual float Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }
        #endregion

        #endregion
    }
}
