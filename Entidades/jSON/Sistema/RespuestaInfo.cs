﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON.Sistema
{
    /// <summary>
    /// Objeto a mapear como respuesta a cualquier invocación de un PageMethod
    /// </summary>
    public class RespuestaInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public RespuestaInfo()
        {
            _Resultado = true;
            _Mensaje = string.Empty;
            _Valor = null;
        }
        
        #endregion

        #region Propiedades

        private bool _Resultado;
        /// <summary>
        /// Resultado
        /// </summary>
        public bool Resultado
        {
            get { return _Resultado; }
            set { _Resultado = value; }
        }

        private string _Mensaje;
        /// <summary>
        /// Mensaje
        /// </summary>
        public string Mensaje
        {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }

        private object _Valor;
        /// <summary>
        /// Valor
        /// </summary>
        public object Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}