﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON.Sistema
{
    /// <summary>
    /// Clase para mapear el contenido de los filtros
    /// </summary>
    public class FiltroInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public FiltroInfo()
        {
        }

        #endregion

        #region Propiedades

        private int _Periodo;
        /// <summary>
        /// Período
        /// </summary>
        public int Periodo
        {
            get { return _Periodo; }
            set { _Periodo = value; }
        }

        private string _Pais;
        /// <summary>
        /// País
        /// </summary>
        public string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private int _IdPais;
        /// <summary>
        /// Id del País
        /// </summary>
        public int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
