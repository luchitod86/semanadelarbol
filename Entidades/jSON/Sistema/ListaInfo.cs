﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON.Sistema
{
    /// <summary>
    /// Clase para mapear el contenido de un objeto que herede de IEntidad
    /// </summary>
    public class ListaInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public ListaInfo()
        {
        }

        /// <summary>
        /// Construye un objeto MensajeInfo a partir de un objeto Mensaje
        /// </summary>
        /// <param name="mensaje"></param>
        /// <param name="valor"></param>
        public ListaInfo(Entidad entidad, object valor)
        {
            this.Id     = entidad.Id;
            this.Texto  = entidad.Nombre;
            this.Valor  = valor;
        }

        #endregion

        #region Propiedades
        
        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private string _Texto;
        /// <summary>
        /// Texto
        /// </summary>
        public string Texto
        {
            get { return _Texto; }
            set { _Texto = value; }
        }

        private object _Valor;
        /// <summary>
        /// Valor
        /// </summary>
        public object Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
