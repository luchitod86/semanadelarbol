﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON.Sistema
{
    /// <summary>
    /// Clase para mapear el contenido de los totales de los indicadores
    /// </summary>
    public class TotalesInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public TotalesInfo()
        {
        }

        /// <summary>
        /// Construye una objeto TotalesInfo a partir de los valores de las listas asociadas a un Usuario
        /// </summary>
        public TotalesInfo(IList<UsuarioPlantacion> plantaciones, IList<UsuarioDonacion> donaciones, IList<UsuarioAdopcion> adopciones)
        {
            int cantidadPlantaciones    = 0;
            int cantidadDonaciones      = 0;
            int cantidadAdopciones      = 0;
            double cantidadCO2Capturado = 0;

            double factorCO2 = 0;

            if ( plantaciones != null && plantaciones.Count() > 0 )
            {
                cantidadPlantaciones = plantaciones.Count();
                factorCO2 = plantaciones[0].Arbol.FactorCO2;
                cantidadCO2Capturado = cantidadPlantaciones * factorCO2;
            }
            if ( plantaciones != null && donaciones.Count() > 0 )
                cantidadDonaciones = donaciones.Count();
            if ( adopciones != null && adopciones.Count() > 0 )
                cantidadAdopciones = adopciones.Count();

            this.Plantaciones   = cantidadPlantaciones;
            this.Donaciones     = cantidadDonaciones;
            this.Adopciones     = cantidadAdopciones;
            this.CO2Capturado   = cantidadCO2Capturado;
        }

        /// <summary>
        /// Construye un objeto TotalesInfo a partir de los valores totales
        /// </summary>
        /// <param name="plantaciones"></param>
        /// <param name="donaciones"></param>
        /// <param name="adopciones"></param>
        /// <param name="individuos"></param>
        /// <param name="instituciones"></param>
        /// <param name="municipios"></param>
        /// <param name="CO2capturado"></param>
        public TotalesInfo(int plantaciones, int donaciones, int adopciones, int individuos, int instituciones, int municipios, double co2capturado)
        {
            this.Plantaciones   = plantaciones;
            this.Donaciones     = donaciones;
            this.Adopciones     = adopciones;
            this.Individuos     = individuos;
            this.Instituciones  = instituciones;
            this.Municipios     = municipios;
            this.CO2Capturado   = co2capturado;
        }

        #endregion

        #region Propiedades
        
        //private int _IdUsuario;
        ///// <summary>
        ///// Id Usuario
        ///// </summary>
        //public virtual int IdUsuario
        //{
        //    get { return _IdUsuario; }
        //    set { _IdUsuario = value; }
        //}

        private int _Plantaciones;
        /// <summary>
        /// Cantidad de Plantaciones
        /// </summary>
        public int Plantaciones
        {
            get { return _Plantaciones; }
            set { _Plantaciones = value; }
        }

        private int _Donaciones;
        /// <summary>
        /// Cantidad de Donaciones
        /// </summary>
        public int Donaciones
        {
            get { return _Donaciones; }
            set { _Donaciones = value; }
        }

        private int _Adopciones;
        /// <summary>
        /// Cantidad de Adopciones
        /// </summary>
        public int Adopciones
        {
            get { return _Adopciones; }
            set { _Adopciones = value; }
        }

        private double _CO2Capturado;
        /// <summary>
        /// Cantidad de CO2 capturado
        /// </summary>
        public double CO2Capturado
        {
            get { return _CO2Capturado; }
            set { _CO2Capturado = value; }
        }

        private int _Individuos;
        /// <summary>
        /// Cantidad de Individuos
        /// </summary>
        public int Individuos
        {
            get { return _Individuos; }
            set { _Individuos = value; }
        }

        private int _Instituciones;
        /// <summary>
        /// Cantidad de Instituciones
        /// </summary>
        public int Instituciones
        {
            get { return _Instituciones; }
            set { _Instituciones = value; }
        }

        private int _Municipios;
        /// <summary>
        /// Cantidad de Municipios
        /// </summary>
        public int Municipios
        {
            get { return _Municipios; }
            set { _Municipios = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
