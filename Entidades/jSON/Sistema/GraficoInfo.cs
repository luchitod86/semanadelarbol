﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON.Sistema
{
    public class GraficoInfo
    {
        public GraficoInfo()
        {
            this.Datos = new List<Item>();
        }

        public GraficoInfo(string titulo)
        {
            this.Titulo = titulo;
            this.Datos = new List<Item>();
        }

        private string _Titulo;
        /// <summary>
        /// Titulo
        /// </summary>
        public string Titulo
        {
            get { return _Titulo; }
            set { _Titulo = value; }
        }

        public class Item
        {
            public Item()
            {
            }

            public Item(string texto, object valor)
            {
                this.Texto = texto;
                this.Valor = valor;
            }

            private string _Texto;
            /// <summary>
            /// Texto
            /// </summary>
            public string Texto
            {
                get { return _Texto; }
                set { _Texto = value; }
            }

            private object _Valor;
            /// <summary>
            /// Valor
            /// </summary>
            public object Valor
            {
                get { return _Valor; }
                set { _Valor = value; }
            }
        }

        private List<Item> _Datos;
        /// <summary>
        /// Datos
        /// </summary>
        public List<Item> Datos
        {
            get { return _Datos; }
            set { _Datos = value; }
        }
    }
}
