﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON.Sistema
{
    /// <summary>
    /// Clase para almacenar una colección de listas (ListaInfo)
    /// </summary>
    public class ColeccionInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public ColeccionInfo()
        {
        }

        #endregion

        #region Propiedades
        
        private string _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual string Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private List<ListaInfo> _Lista;
        /// <summary>
        /// Lista
        /// </summary>
        public List<ListaInfo> Lista
        {
            get { return _Lista; }
            set { _Lista = value; }
        }

        #endregion

        #region Métodos

        #endregion

    }
}
