﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de un Meta Tag de una Sección
    /// </summary>
    public class SeccionMetaInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SeccionMetaInfo()
        {
        }

        public SeccionMetaInfo(SeccionMeta seccionMeta)
        {
            this.Id                 = seccionMeta.Id;
            this.IdSeccion          = seccionMeta.Seccion.Id;
            this.Seccion            = seccionMeta.Seccion.Nombre;
            this.SeccionDescripcion = seccionMeta.Seccion.Descripcion;
            this.Keywords           = seccionMeta.Keywords != null ? seccionMeta.Keywords : String.Empty;
            this.Description        = seccionMeta.Description != null ? seccionMeta.Description : String.Empty;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private int _IdSeccion;
        /// <summary>
        /// Id de Sección
        /// </summary>
        public virtual int IdSeccion
        {
            get { return _IdSeccion; }
            set { _IdSeccion = value; }
        }

        private string _Seccion;
        /// <summary>
        /// Sección
        /// </summary>
        public virtual string Seccion
        {
            get { return _Seccion; }
            set { _Seccion = value; }
        }

        private string _SeccionDescripcion;
        /// <summary>
        /// Descripción de Sección
        /// </summary>
        public virtual string SeccionDescripcion
        {
            get { return _SeccionDescripcion; }
            set { _SeccionDescripcion = value; }
        }

        private string _Keywords;
        /// <summary>
        /// Keywords
        /// </summary>
        public virtual string Keywords
        {
            get { return _Keywords; }
            set { _Keywords = value; }
        }

        private string _Description;
        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        #endregion
    }
}
