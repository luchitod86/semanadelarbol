﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase para mapear el contenido en JSON de la clase ConfiguracionEmail
    /// </summary>
    public class ConfiguracionEmailInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ConfiguracionEmailInfo()
        {
        }
        
        /// <summary>
        /// Crea una instancia de ConfiguracionEmailInfo en base a un objeto ConfiguracionEmail
        /// </summary>
        /// <param name="configSocial"></param>
        public ConfiguracionEmailInfo(ConfiguracionEmail configEmail)
        {
            this.Id = configEmail.Id;
        
            this.EmisorAlias = configEmail.EmisorAlias;
            this.EmisorDireccion = configEmail.EmisorDireccion;
            this.EmisorUsuario = configEmail.EmisorUsuario;
            this.EmisorPassword = configEmail.EmisorPassword;

            this.SMTPServidor = configEmail.SMTPServidor;
            this.SMTPPuerto = configEmail.SMTPPuerto;
            this.SMTPHabilitarSSL = configEmail.SMTPHabilitarSSL;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public int Id
        {
            get { return _Id; }
            set {  _Id = value; }
        }

        #region Emisor

        private string _EmisorAlias;
        /// <summary>
        /// Alias del Emisor
        /// </summary>
        public string EmisorAlias
        {
            get { return _EmisorAlias; }
            set { _EmisorAlias = value; }
        }

        private string _EmisorUsuario;
        /// <summary>
        /// Nombre de Usuario del Emisor
        /// </summary>
        public string EmisorUsuario
        {
            get { return _EmisorUsuario; }
            set { _EmisorUsuario = value; }
        }

        private string _EmisorDireccion;
        /// <summary>
        /// Direccion del Emisor
        /// </summary>
        public string EmisorDireccion
        {
            get { return _EmisorDireccion; }
            set { _EmisorDireccion = value; }
        }

        private string _EmisorPassword;
        /// <summary>
        /// Password del Emisor
        /// </summary>
        public string EmisorPassword
        {
            get { return _EmisorPassword; }
            set { _EmisorPassword = value; }
        }

        #endregion

        #region Servidor SMTP

        private string _SMTPServidor;
        /// <summary>
        /// Direccion del Servidor SMTP
        /// </summary>
        public string SMTPServidor
        {
            get { return _SMTPServidor; }
            set { _SMTPServidor = value; }
        }

        private int _SMTPPuerto;
        /// <summary>
        /// Puerto del Servidor SMTP
        /// </summary>
        public int SMTPPuerto
        {
            get { return _SMTPPuerto; }
            set { _SMTPPuerto = value; }
        }

        private bool _SMTPHabilitarSSL;
        /// <summary>
        /// Habilitar SSL ?
        /// </summary>
        public bool SMTPHabilitarSSL
        {
            get { return _SMTPHabilitarSSL; }
            set { _SMTPHabilitarSSL = value; }
        }

        #endregion

        #endregion
    }
}
