﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de un Arbol plantado por un Usuario
    /// </summary>
    public class PlantacionInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public PlantacionInfo()
        {
        }

        public PlantacionInfo(UsuarioPlantacion plantacion, bool incluirFoto = false)
        {
            this.Id             = plantacion.Id;
            this.IdUsuario      = plantacion.Usuario.Id;
            this.Usuario        = EntidadesHelper.ObtenerNombre(plantacion.Usuario);
            this.TipoUsuario    = plantacion.Usuario.Tipo.Nombre;
            this.Cantidad       = plantacion.Cantidad;
            this.Latitud        = plantacion.Latitud;
            this.Longitud       = plantacion.Longitud;
            this.IdEspecie      = plantacion.Arbol.Id;
            this.Especie        = plantacion.Arbol.Nombre;
            this.Fecha          = plantacion.Fecha.ToShortDateString();
            this.TieneFoto      = plantacion.TieneFoto;
            
            if (incluirFoto && this.TieneFoto )
                this.Foto   = Comun.Formateador.Base64.ConvertirEnBase64(plantacion.Foto);
            this.IdPais     = plantacion.Pais != null ? plantacion.Pais.Id : -1;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private int _IdUsuario;
        /// <summary>
        /// Id Usuario
        /// </summary>
        public virtual int IdUsuario
        {
            get { return _IdUsuario; }
            set { _IdUsuario = value; }
        }

        private string _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual string Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private string _TipoUsuario;
        /// <summary>
        /// Tipo de Usuario
        /// </summary>
        public virtual string TipoUsuario
        {
            get { return _TipoUsuario; }
            set { _TipoUsuario = value; }
        }

        private int _IdEspecie;
        /// <summary>
        /// Id Especie
        /// </summary>
        public virtual int IdEspecie
        {
            get { return _IdEspecie; }
            set { _IdEspecie = value; }
        }

        private string _Especie;
        /// <summary>
        /// Especie
        /// </summary>
        public virtual string Especie
        {
            get { return _Especie; }
            set { _Especie = value; }
        }

        private int _Cantidad;
        /// <summary>
        /// Cantidad
        /// </summary>
        public virtual int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }

        private string _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual string Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        private float _Latitud;
        /// <summary>
        /// Latitud
        /// </summary>
        public virtual float Latitud
        {
            get { return _Latitud; }
            set { _Latitud = value; }
        }

        private float _Longitud;
        /// <summary>
        /// Longitud
        /// </summary>
        public virtual float Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        private string _Foto;
        /// <summary>
        /// Foto
        /// </summary>
        public virtual string Foto
        {
            get { return _Foto; }
            set { _Foto = value; }
        }

        private bool _TieneFoto;
        /// <summary>
        /// Tiene Foto ?
        /// </summary>
        public virtual bool TieneFoto
        {
            get { return _TieneFoto; }
            set { _TieneFoto = value; }
        }

        private int _IdPais;
        /// <summary>
        /// Id del País
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
