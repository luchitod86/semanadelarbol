﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase para mapear el contenido en JSON de la clase ConfiguracionSocial
    /// </summary>
    public class ConfiguracionSocialInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ConfiguracionSocialInfo()
        {
        }
        
        /// <summary>
        /// Crea una instancia de ConfiguracionSocialInfo en base a un objeto ConfiguracionSocial
        /// </summary>
        /// <param name="configSocial"></param>
        public ConfiguracionSocialInfo(ConfiguracionSocial configSocial)
        {
            this.Id = configSocial.Id;
            this.Facebook = configSocial.Facebook;
            this.Twitter = configSocial.Twitter;
            this.YouTube = configSocial.YouTube;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public int Id
        {
            get { return _Id; }
            set {  _Id = value; }
        }

        private string _Facebook;
        /// <summary>
        /// Facebook
        /// </summary>
        public string Facebook
        {
            get { return _Facebook; }
            set { _Facebook = value; }
        }

        private string _Twitter;
        /// <summary>
        /// Twitter
        /// </summary>
        public string Twitter
        {
            get { return _Twitter; }
            set { _Twitter = value; }
        }

        private string _YouTube;
        /// <summary>
        /// YouTube
        /// </summary>
        public string YouTube
        {
            get { return _YouTube; }
            set { _YouTube = value; }
        }

        #endregion
    }
}
