﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    public class EspeciePaisInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspeciePaisInfo()
        {
        }

        public EspeciePaisInfo(EspeciePais especiePais)
        {
            this.Id         = especiePais.Id;
            this.Nombre     = especiePais.Nombre;
            this.IdPais     = especiePais.Pais.Id;
            this.Pais       = especiePais.Pais.Nombre;
            this.IdEspecie  = especiePais.Especie.Id;
            this.Especie    = especiePais.Especie.Nombre;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private int _IdEspecie;
        /// <summary>
        /// Id Especie
        /// </summary>
        public virtual int IdEspecie
        {
            get { return _IdEspecie; }
            set
            {
                _IdEspecie = value;
            }
        }

        private string _Especie;
        /// <summary>
        /// Especie
        /// </summary>
        public virtual string Especie
        {
            get { return _Especie; }
            set { _Especie = value; }
        }

        private int _IdPais;
        /// <summary>
        /// Id Pais
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set
            {
                _IdPais = value;
            }
        }

        private string _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}

