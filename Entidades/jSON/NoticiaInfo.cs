﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de una Noticia
    /// </summary>
    public class NoticiaInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public NoticiaInfo()
        {
        }

        public NoticiaInfo(Noticia noticia, bool incluirFoto = false)
        {
            this.Id             = noticia.Id;
            this.Titulo         = noticia.Titulo;
            this.Resumen        = noticia.Resumen;
            this.Texto          = noticia.Texto;
            this.Fecha          = noticia.Fecha.ToShortDateString();
            this.Foto1          = noticia.Imagenes.Count > 0 ? noticia.Imagenes[0].URLImagen : string.Empty;
            this.Foto2          = noticia.Imagenes.Count > 1 ? noticia.Imagenes[1].URLImagen : string.Empty;
            this.Foto3          = noticia.Imagenes.Count > 2 ? noticia.Imagenes[2].URLImagen : string.Empty;
            this.Foto4          = noticia.Imagenes.Count > 3 ? noticia.Imagenes[3].URLImagen : string.Empty;
            this.Foto5          = noticia.Imagenes.Count > 4 ? noticia.Imagenes[4].URLImagen : string.Empty;
            this.Foto6          = noticia.Imagenes.Count > 5 ? noticia.Imagenes[5].URLImagen : string.Empty;
            
            this.IdPais         = noticia.Pais != null ? noticia.Pais.Id : -1;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private string _Titulo;
        /// <summary>
        /// Titulo
        /// </summary>
        public virtual string Titulo
        {
            get { return _Titulo; }
            set { _Titulo = value; }
        }   
        
        private string _Resumen;
        /// <summary>
        /// Resumen
        /// </summary>
        public virtual string Resumen
        {
            get { return _Resumen; }
            set { _Resumen = value; }
        }

        private string _Texto;
        /// <summary>
        /// Texto
        /// </summary>
        public virtual string Texto
        {
            get { return _Texto; }
            set { _Texto = value; }
        }

        private string _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual string Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        private string _Foto1;
        /// <summary>
        /// Foto1
        /// </summary>
        public virtual string Foto1
        {
            get { return _Foto1; }
            set { _Foto1 = value; }
        }

        private string _Foto2;
        /// <summary>
        /// Foto2
        /// </summary>
        public virtual string Foto2
        {
            get { return _Foto2; }
            set { _Foto2 = value; }
        }

        private string _Foto3;
        /// <summary>
        /// Foto4
        /// </summary>
        public virtual string Foto3
        {
            get { return _Foto3; }
            set { _Foto3 = value; }
        }

        private string _Foto4;
        /// <summary>
        /// Foto4
        /// </summary>
        public virtual string Foto4
        {
            get { return _Foto4; }
            set { _Foto4 = value; }
        }

        private string _Foto5;
        /// <summary>
        /// Foto5
        /// </summary>
        public virtual string Foto5
        {
            get { return _Foto5; }
            set { _Foto5 = value; }
        }

        private string _Foto6;
        /// <summary>
        /// Foto6
        /// </summary>
        public virtual string Foto6
        {
            get { return _Foto6; }
            set { _Foto6 = value; }
        }     

        private int _IdPais;
        /// <summary>
        /// Id del País
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
