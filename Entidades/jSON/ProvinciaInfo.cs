﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de una Provincia
    /// </summary>
    public class ProvinciaInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ProvinciaInfo()
        {
        }

        public ProvinciaInfo(Provincia provincia)
        {
            this.Id = provincia.Id;
            this.Nombre = provincia.Nombre;
            this.Pais = provincia.Pais.Nombre;
            this.PaisId = provincia.Pais.Id;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private int _PaisId;
        /// <summary>
        /// Id País
        /// </summary>
        public virtual int PaisId
        {
            get { return _PaisId; }
            set
            {
                _PaisId = value;
            }
        }

        private string _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
