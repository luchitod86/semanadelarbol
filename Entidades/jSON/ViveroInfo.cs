﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de un Vivero
    /// </summary>
    public class ViveroInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ViveroInfo()
        {
        }

        public ViveroInfo(Vivero vivero)
        {
            this.Id             = vivero.Id;
            this.Direccion      = vivero.Direccion;
            this.Email          = vivero.Email;
            this.IdPais         = vivero.Usuario.Pais != null ? vivero.Usuario.Pais.Id : -1;
            this.IdProvincia    = vivero.Usuario.Provincia != null ? vivero.Usuario.Provincia.Id : -1;
            this.IdUsuario      = vivero.Usuario != null ? vivero.Usuario.Id : -1;
            this.Imagen         = Comun.Formateador.Base64.ConvertirEnBase64(vivero.Imagen);
            this.Pais           = vivero.Usuario.Pais != null ? vivero.Usuario.Pais.Nombre : string.Empty;
            this.Provincia      = vivero.Usuario.Provincia != null ? vivero.Usuario.Provincia.Nombre : string.Empty;
            this.Telefono       = vivero.Telefono;
            this.Usuario        = vivero.Usuario != null ? vivero.Usuario.Nombre + " " + vivero.Usuario.Apellido : string.Empty;
            this.Web            = vivero.Web;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private int _IdUsuario;
        /// <summary>
        /// Id Usuario
        /// </summary>
        public virtual int IdUsuario
        {
            get { return _IdUsuario; }
            set { _IdUsuario = value; }
        }

        private string _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual string Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private int _IdPais;
        /// <summary>
        /// Id de Pais
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        private string _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private int _IdProvincia;
        /// <summary>
        /// Id de Provincia
        /// </summary>
        public virtual int IdProvincia
        {
            get { return _IdProvincia; }
            set { _IdProvincia = value; }
        }

        private string _Provincia;
        /// <summary>
        /// Provincia
        /// </summary>
        public virtual string Provincia
        {
            get { return _Provincia; }
            set { _Provincia = value; }
        }

        private string _Direccion;
        /// <summary>
        /// Direccion
        /// </summary>
        public virtual string Direccion
        {
            get { return _Direccion; }
            set { _Direccion = value; }
        }

        private string _Telefono;
        /// <summary>
        /// Telefono
        /// </summary>
        public virtual string Telefono
        {
            get { return _Telefono; }
            set { _Telefono = value; }
        }

        private string _Email;
        /// <summary>
        /// Email
        /// </summary>
        public virtual string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _Web;
        /// <summary>
        /// Web
        /// </summary>
        public virtual string Web
        {
            get { return _Web; }
            set { _Web = value; }
        }

        private string _Imagen;
        /// <summary>
        /// Imagen
        /// </summary>
        public virtual string Imagen
        {
            get { return _Imagen; }
            set { _Imagen = value; }
        }

        #endregion
    }
}
