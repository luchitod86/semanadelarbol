﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de un Recurso
    /// </summary>
    public class RecursoInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public RecursoInfo()
        {
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Ruta;
        /// <summary>
        /// Ruta
        /// </summary>
        public virtual string Ruta
        {
            get { return _Ruta; }
            set { _Ruta = value; }
        }

        private PaisInfo _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual PaisInfo Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        //private string _Pais;
        ///// <summary>
        ///// Pais
        ///// </summary>
        //public virtual string Pais
        //{
        //    get { return _Pais; }
        //    set { _Pais = value; }
        //}

        //private string _Bandera;
        ///// <summary>
        ///// Bandera
        ///// </summary>
        //public virtual string Bandera
        //{
        //    get { return _Bandera; }
        //    set { _Bandera = value; }
        //}

        //private string _Continente;
        ///// <summary>
        ///// Continente
        ///// </summary>
        //public virtual string Continente
        //{
        //    get { return _Continente; }
        //    set { _Continente = value; }
        //}

        //private string _Lenguaje;
        ///// <summary>
        ///// Lenguaje
        ///// </summary>
        //public virtual string Lenguaje
        //{
        //    get { return _Lenguaje; }
        //    set { _Lenguaje = value; }
        //}

        #endregion
    }
}
