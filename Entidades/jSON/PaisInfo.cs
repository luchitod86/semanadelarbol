﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de un País
    /// </summary>
    public class PaisInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public PaisInfo()
        {
        }

        public PaisInfo(Pais pais)
        {
            this.Id             = pais.Id;
            this.Nombre         = pais.Nombre;
            this.Bandera        = pais.Bandera;
            this.IdContinente   = pais.Continente.Id;
            this.Continente     = pais.Continente.Nombre;
            this.Lenguaje       = pais.Lenguaje;
            this.Activo         = pais.Activo;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _Bandera;
        /// <summary>
        /// Bandera
        /// </summary>
        public virtual string Bandera
        {
            get { return _Bandera; }
            set { _Bandera = value; }
        }

        private int _IdContinente;
        /// <summary>
        /// Id del Continente
        /// </summary>
        public virtual int IdContinente
        {
            get { return _IdContinente; }
            set { _IdContinente = value; }
        }

        private string _Continente;
        /// <summary>
        /// Continente
        /// </summary>
        public virtual string Continente
        {
            get { return _Continente; }
            set { _Continente = value; }
        }

        private string _Lenguaje;
        /// <summary>
        /// Lenguaje
        /// </summary>
        public virtual string Lenguaje
        {
            get { return _Lenguaje; }
            set { _Lenguaje = value; }
        }

        private bool _Activo;
        /// <summary>
        /// Activo
        /// </summary>
        public virtual bool Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }

        #endregion
    }
}
