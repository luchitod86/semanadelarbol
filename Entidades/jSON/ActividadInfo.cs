﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de una Actividad
    /// </summary>
    public class ActividadInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ActividadInfo()
        {
        }

        public ActividadInfo(UsuarioActividad actividad)
        {
            this.Id             = actividad.Id;
            this.IdUsuario      = actividad.Usuario.Id;
            this.Usuario        = EntidadesHelper.ObtenerNombre(actividad.Usuario);
            this.TipoUsuario    = actividad.Usuario.Tipo.Nombre;
            this.Comentarios    = actividad.Comentarios;
            this.Descripcion    = actividad.Descripcion;
            this.FechaYHora     = actividad.FechaYHora.ToShortDateString() + " " + actividad.FechaYHora.ToShortTimeString();
            this.Latitud        = actividad.Latitud;
            this.Longitud       = actividad.Longitud;
            this.IdPais         = actividad.Pais != null ? actividad.Pais.Id : -1;
            this.Pais           = actividad.Pais != null ? actividad.Pais.Nombre : string.Empty;
            this.IdProvincia    = actividad.Provincia != null ? actividad.Provincia.Id : -1;
            this.Provincia      = actividad.Provincia != null ? actividad.Provincia.Nombre : string.Empty;
            this.EsPublica      = actividad.EsPublica;
            this.FechaTexto     = actividad.FechaYHora.ToString("dddd d");
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private int _IdUsuario;
        /// <summary>
        /// Id Usuario
        /// </summary>
        public virtual int IdUsuario
        {
            get { return _IdUsuario; }
            set { _IdUsuario = value; }
        }

        private string _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual string Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private string _TipoUsuario;
        /// <summary>
        /// Tipo de Usuario
        /// </summary>
        public virtual string TipoUsuario
        {
            get { return _TipoUsuario; }
            set { _TipoUsuario = value; }
        }

        private float _Latitud;
        /// <summary>
        /// Latitud
        /// </summary>
        public virtual float Latitud
        {
            get { return _Latitud; }
            set { _Latitud = value; }
        }

        private float _Longitud;
        /// <summary>
        /// Longitud
        /// </summary>
        public virtual float Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        private int _IdPais;
        /// <summary>
        /// Id Pais
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        private string _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private int _IdProvincia;
        /// <summary>
        /// Id de la Provincia
        /// </summary>
        public virtual int IdProvincia
        {
            get { return _IdProvincia; }
            set { _IdProvincia = value; }
        }
        private string _Provincia;

        /// <summary>
        /// Provincia
        /// </summary>
        public virtual string Provincia
        {
            get { return _Provincia; }
            set { _Provincia = value; }
        }

        private string _Ciudad;
        /// <summary>
        /// Ciudad
        /// </summary>
        public virtual string Ciudad
        {
            get { return _Ciudad; }
            set { _Ciudad = value; }
        }

        private string _Descripcion;
        /// <summary>
        /// Descripcion
        /// </summary>
        public virtual string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        private string _FechaYHora;
        /// <summary>
        /// Fecha y Hora
        /// </summary>
        public virtual string FechaYHora
        {
            get { return _FechaYHora; }
            set { _FechaYHora = value; }
        }

        private string _Comentarios;
        /// <summary>
        /// Comentarios
        /// </summary>
        public virtual string Comentarios
        {
            get { return _Comentarios; }
            set { _Comentarios = value; }
        }

        private bool _EsPublica;
        /// <summary>
        /// Es Pública?
        /// </summary>
        public virtual bool EsPublica
        {
            get { return _EsPublica; }
            set { _EsPublica = value; }
        }

        private string _FechaTexto;
        /// <summary>
        /// FechaTexto?
        /// </summary>
        public virtual string FechaTexto
        {
            get { return _FechaTexto; }
            set { _FechaTexto = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
