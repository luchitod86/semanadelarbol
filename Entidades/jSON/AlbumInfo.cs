﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    public class AlbumInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AlbumInfo()
        {
        }

        public AlbumInfo(Album album)
        {
            this.Id         = album.Id;
            this.Fecha      = album.Fecha;
            this.IdAlbum    = album.IdAlbum;
            this.IdEntidad  = album.IdEntidad;
            this.IdPais     = album.Pais != null ? album.Pais.Id : -1;
            this.Nombre     = album.Nombre;
            this.Url        = album.Url;
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _IdEntidad;
        /// <summary>
        /// Id Entidad
        /// </summary>
        public virtual string IdEntidad
        {
            get { return _IdEntidad; }
            set { _IdEntidad = value; }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        private string _IdAlbum;
        /// <summary>
        /// Id Album
        /// </summary>
        public virtual string IdAlbum
        {
            get { return _IdAlbum; }
            set { _IdAlbum = value; }
        }

        private string _Url;
        /// <summary>
        /// Url
        /// </summary>
        public virtual string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        private int _IdPais;
        /// <summary>
        /// Id Pais
        /// </summary>
        public virtual int IdPais
        {
            get { return _IdPais; }
            set { _IdPais = value; }
        }

        private DateTime _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        #endregion
    }
}
