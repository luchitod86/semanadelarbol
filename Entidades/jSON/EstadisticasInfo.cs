﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    public class EstadisticasInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EstadisticasInfo()
        {
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Pais;
        /// <summary>
        /// País
        /// </summary>
        public virtual string Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private int _Usuarios;
        /// <summary>
        /// Usuarios
        /// </summary>
        public virtual int Usuarios
        {
            get { return _Usuarios; }
            set { _Usuarios = value; }
        }

        private int _Plantaciones;
        /// <summary>
        /// Plantaciones
        /// </summary>
        public virtual int Plantaciones
        {
            get { return _Plantaciones; }
            set { _Plantaciones = value; }
        }

        private int _Adopciones;
        /// <summary>
        /// Adopciones
        /// </summary>
        public virtual int Adopciones
        {
            get { return _Adopciones; }
            set { _Adopciones = value; }
        }

        private int _Donaciones;
        /// <summary>
        /// Donaciones
        /// </summary>
        public virtual int Donaciones
        {
            get { return _Donaciones; }
            set { _Donaciones = value; }
        }

        private int _Donantes;
        /// <summary>
        /// Donantes
        /// </summary>
        public virtual int Donantes
        {
            get { return _Donantes; }
            set { _Donantes = value; }
        }

        private int _Actividades;
        /// <summary>
        /// Actividades
        /// </summary>
        public virtual int Actividades
        {
            get { return _Actividades; }
            set { _Actividades = value; }
        }

        #endregion
    }
}
