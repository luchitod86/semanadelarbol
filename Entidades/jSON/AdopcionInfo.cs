﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de un Arbol adoptado por un Usuario
    /// </summary>
    public class AdopcionInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AdopcionInfo()
        {
        }

        public AdopcionInfo(UsuarioAdopcion arbol)
        {
            this.Id             = arbol.Id;
            this.IdUsuario      = arbol.Usuario.Id;
            this.Usuario        = EntidadesHelper.ObtenerNombre(arbol.Usuario);
            this.TipoUsuario    = arbol.Usuario.Tipo.Nombre;
            this.IdArbol        = arbol.Arbol.Id;
            this.Arbol          = arbol.Arbol.Nombre;
            this.IdDonante      = arbol.Donante.Id;
            this.Donante        = EntidadesHelper.ObtenerNombre(arbol.Donante);
            this.TipoDonante    = arbol.Donante.Tipo.Nombre;
            this.Cantidad       = arbol.Cantidad;
            this.Fecha          = arbol.Fecha.ToShortDateString();
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private int _IdUsuario;
        /// <summary>
        /// Id Usuario
        /// </summary>
        public virtual int IdUsuario
        {
            get { return _IdUsuario; }
            set { _IdUsuario = value; }
        }

        private string _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual string Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private string _TipoUsuario;
        /// <summary>
        /// Tipo de Usuario
        /// </summary>
        public virtual string TipoUsuario
        {
            get { return _TipoUsuario; }
            set { _TipoUsuario = value; }
        }

        private int _IdDonante;
        /// <summary>
        /// Id Donante
        /// </summary>
        public virtual int IdDonante
        {
            get { return _IdDonante; }
            set { _IdDonante = value; }
        }

        private string _Donante;
        /// <summary>
        /// Donante
        /// </summary>
        public virtual string Donante
        {
            get { return _Donante; }
            set { _Donante = value; }
        }

        private string _TipoDonante;
        /// <summary>
        /// Tipo de Donante
        /// </summary>
        public virtual string TipoDonante
        {
            get { return _TipoDonante; }
            set { _TipoDonante = value; }
        }

        private int _IdArbol;
        /// <summary>
        /// Id Arbol
        /// </summary>
        public virtual int IdArbol
        {
            get { return _IdArbol; }
            set { _IdArbol = value; }
        }

        private string _Arbol;
        /// <summary>
        /// Arbol
        /// </summary>
        public virtual string Arbol
        {
            get { return _Arbol; }
            set { _Arbol = value; }
        }

        private int _Cantidad;
        /// <summary>
        /// Cantidad
        /// </summary>
        public virtual int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }

        private string _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual string Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
