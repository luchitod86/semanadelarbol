﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades.jSON
{
    /// <summary>
    /// Clase que contiene la informacion de un Arbol donado por un Usuario
    /// </summary>
    public class DonacionInfo
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public DonacionInfo()
        {
        }

        public DonacionInfo(UsuarioDonacion arbol)
        {
            this.Id             = arbol.Id;
            this.IdEspecie      = arbol.Arbol.Id;
            this.Especie        = arbol.Arbol.Nombre;
            this.IdDonante      = arbol.Donante.Id;
            this.TipoDonante    = arbol.Donante.Tipo.Nombre;
            this.Donante        = EntidadesHelper.ObtenerNombre(arbol.Donante);
            this.Adoptados      = arbol.Adoptados;
            this.Cantidad       = arbol.Cantidad;
            this.Disponibles    = arbol.Disponibles;
            this.Fecha          = arbol.Fecha.ToShortDateString();
        }

        #endregion

        #region Propiedades

        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set
            {
                _Id = value;
            }
        }

        private int _IdDonante;
        /// <summary>
        /// Id Donante
        /// </summary>
        public virtual int IdDonante
        {
            get { return _IdDonante; }
            set { _IdDonante = value; }
        }

        private string _Donante;
        /// <summary>
        /// Donante
        /// </summary>
        public virtual string Donante
        {
            get { return _Donante; }
            set { _Donante = value; }
        }

        private string _TipoDonante;
        /// <summary>
        /// Tipo de Donante
        /// </summary>
        public virtual string TipoDonante
        {
            get { return _TipoDonante; }
            set { _TipoDonante = value; }
        }

        private int _IdEspecie;
        /// <summary>
        /// Id Especie
        /// </summary>
        public virtual int IdEspecie
        {
            get { return _IdEspecie; }
            set { _IdEspecie = value; }
        }

        private string _Especie;
        /// <summary>
        /// Especie
        /// </summary>
        public virtual string Especie
        {
            get { return _Especie; }
            set { _Especie = value; }
        }

        private int _Cantidad;
        /// <summary>
        /// Cantidad
        /// </summary>
        public virtual int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }

        private int _Disponibles;
        /// <summary>
        /// Disponibles
        /// </summary>
        public virtual int Disponibles
        {
            get { return _Disponibles; }
            set { _Disponibles = value; }
        }

        private int _Adoptados;
        /// <summary>
        /// Adoptados
        /// </summary>
        public virtual int Adoptados
        {
            get { return _Adoptados; }
            set { _Adoptados = value; }
        }

        private string _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual string Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
