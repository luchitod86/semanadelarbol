﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class EspecieImagen : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspecieImagen()
        {
        }

        #endregion

        #region Propiedades

        private Especie _Especie;
        /// <summary>
        /// Noticia
        /// </summary>
        public virtual Especie Especie
        {
            get { return _Especie; }
            set { _Especie = value; }
        }       

        private string _URLImagen;
        /// <summary>
        /// URLImagen
        /// </summary>
        public virtual string URLImagen
        {
            get { return _URLImagen; }
            set { _URLImagen = value; }
        }            

        #endregion
    }
}
