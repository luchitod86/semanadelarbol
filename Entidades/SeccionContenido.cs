﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class SeccionContenido : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SeccionContenido()
        {
        }

        #endregion

        #region Propiedades

        private Seccion _Seccion;
        /// <summary>
        /// Sección
        /// </summary>
        public virtual Seccion Seccion
        {
            get { return _Seccion; }
            set { _Seccion = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private string _Campo;
        /// <summary>
        /// Campo
        /// </summary>
        public virtual string Campo
        {
            get { return _Campo; }
            set { _Campo = value; }
        }

        private string _Contenido;
        /// <summary>
        /// Contenido
        /// </summary>
        public virtual string Contenido
        {
            get { return _Contenido; }
            set { _Contenido = value; }
        }

        private string _Valor;
        /// <summary>
        /// Valor
        /// </summary>
        public virtual string Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        #endregion
    }
}
