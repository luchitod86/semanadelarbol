﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class UsuarioPreferencia : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioPreferencia()
        {
        }

        #endregion

        #region Propiedades

        private object _Valor;
        /// <summary>
        /// Valor
        /// </summary>
        public virtual object Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        #endregion
    }
}