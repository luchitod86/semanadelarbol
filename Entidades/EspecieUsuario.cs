﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class EspecieUsuario : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspecieUsuario()
        {
        }

        #endregion

        #region Propiedades

        private Usuario _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual Usuario Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private Especie _Especie;
        /// <summary>
        /// Especie
        /// </summary>
        public virtual Especie Especie
        {
            get { return _Especie; }
            set { _Especie = value; }
        }        

        #endregion
    }
}
