﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    /// <summary>
    /// Clase de Configuracion de Sociales
    /// </summary>
    public class ConfiguracionSocial : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public ConfiguracionSocial()
        {

        }

        #endregion

        #region Propiedades


        private string _Facebook;
        /// <summary>
        /// Facebook
        /// </summary>
        public virtual string Facebook
        {
            get { return _Facebook; }
            set { _Facebook = value; }
        }

        private string _Twitter;
        /// <summary>
        /// Twitter
        /// </summary>
        public virtual string Twitter
        {
            get { return _Twitter; }
            set { _Twitter = value; }
        }

        private string _YouTube;
        /// <summary>
        /// YouTube
        /// </summary>
        public virtual string YouTube
        {
            get { return _YouTube; }
            set { _YouTube = value; }
        }

        #endregion

        #region Métodos

        #endregion
    }
}
