﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class UsuarioPlantacion : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioPlantacion()
        {
        }

        #endregion

        #region Propiedades

        private Usuario _Usuario;
        /// <summary>
        /// Usuario
        /// </summary>
        public virtual Usuario Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        private Especie _Arbol;
        /// <summary>
        /// Arbol
        /// </summary>
        public virtual Especie Arbol
        {
            get { return _Arbol; }
            set { _Arbol = value; }
        }

        private float _Latitud;
        /// <summary>
        /// Latitud
        /// </summary>
        public virtual float Latitud
        {
            get { return _Latitud; }
            set { _Latitud = value; }
        }

        private float _Longitud;
        /// <summary>
        /// Longitud
        /// </summary>
        public virtual float Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        private int _Cantidad;
        /// <summary>
        /// Cantidad
        /// </summary>
        public virtual int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }

        private DateTime _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        private byte[] _Foto;
        /// <summary>
        /// Foto
        /// </summary>
        public virtual byte[] Foto
        {
            get { return _Foto; }
            set { _Foto = value; }
        }

        private bool _TieneFoto;
        /// <summary>
        /// Tiene Foto?
        /// </summary>
        public virtual bool TieneFoto
        {
            get { return _TieneFoto; }
            set { _TieneFoto = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
