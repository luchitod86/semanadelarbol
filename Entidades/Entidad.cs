﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    [Serializable]
    public abstract class Entidad
    {
        private int _Id;
        /// <summary>
        /// Id (PK)
        /// </summary>
        public virtual int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        private string _Nombre;
        /// <summary>
        /// Nombre
        /// </summary>
        public virtual string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
    }
}
