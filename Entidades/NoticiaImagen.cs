﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class NoticiaImagen : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public NoticiaImagen()
        {
        }

        #endregion

        #region Propiedades

        private Noticia _Noticia;
        /// <summary>
        /// Noticia
        /// </summary>
        public virtual Noticia Noticia
        {
            get { return _Noticia; }
            set { _Noticia = value; }
        }

        private string _URLImagen;
        /// <summary>
        /// URLImagen
        /// </summary>
        public virtual string URLImagen
        {
            get { return _URLImagen; }
            set { _URLImagen = value; }
        }            

        #endregion
    }
}
