﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    /// <summary>
    /// Clase de Configuracion
    /// </summary>
    public class Configuracion : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por default
        /// </summary>
        public Configuracion()
        {

        }

        #endregion

        #region Propiedades

        private ConfiguracionEmail _Email;
        /// <summary>
        /// Email
        /// </summary>
        /// 
        public virtual ConfiguracionEmail Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private ConfiguracionSocial _Social;
        /// <summary>
        /// Social
        /// </summary>
        /// 
        public virtual ConfiguracionSocial Social
        {
            get { return _Social; }
            set { _Social = value; }
        }

        #endregion
    }
}
