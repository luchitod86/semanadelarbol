﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    /// <summary>
    /// Clase que representa un Mensaje
    /// </summary>
    public class Mensaje : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Mensaje()
        {
        }

        #endregion

        #region Propiedades

        private string _Texto;
        /// <summary>
        /// Texto
        /// </summary>
        public virtual string Texto
        {
            get { return _Texto; }
            set
            {
                _Texto = value;
                base.Nombre = value;
            }
        }

        #endregion

        #region Métodos

        #endregion

    }

}
