﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Log : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Log()
        {
        }

        #endregion

        #region Propiedades

        private LogTipo _Tipo;
        /// <summary>
        /// Tipo de Log
        /// </summary>
        public virtual LogTipo Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        private DateTime _FechaYHora;
        /// <summary>
        /// Fecha y Hora
        /// </summary>
        public virtual DateTime FechaYHora
        {
            get { return _FechaYHora; }
            set { _FechaYHora = value; }
        }

        private Seccion _Seccion;
        /// <summary>
        /// Seccion
        /// </summary>
        public virtual Seccion Seccion
        {
            get { return _Seccion; }
            set { _Seccion = value; }
        }

        private string _Texto;
        /// <summary>
        /// Texto
        /// </summary>
        public virtual string Texto
        {
            get { return _Texto; }
            set
            {
                _Texto = value;
                base.Nombre = value;
            }
        }

        private string _Detalle;
        /// <summary>
        /// Detalle
        /// </summary>
        public virtual string Detalle
        {
            get { return _Detalle; }
            set { _Detalle = value; }
        }

        #endregion
    }
}
