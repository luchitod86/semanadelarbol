﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class UsuarioTipo : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioTipo()
        {
        }

        #endregion

        #region Propiedades

        private string _Descripcion;
        /// <summary>
        /// Descripción
        /// </summary>
        public virtual string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        #endregion
    }
}
