﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Usuario : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Usuario()
        {
        }

        #endregion

        #region Propiedades

        #region Web

        private string _NombreUsuario;
        /// <summary>
        /// Nombre de Usuario
        /// </summary>
        public virtual string NombreUsuario
        {
            get { return _NombreUsuario; }
            set { _NombreUsuario = value; }
        }

        private string _Password;
        /// <summary>
        /// Password
        /// </summary>
        public virtual string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        private string _NombreUsuarioFacebook;
        /// <summary>
        /// Nombre de Usuario de Facebook
        /// </summary>
        public virtual string NombreUsuarioFacebook
        {
            get { return _NombreUsuarioFacebook; }
            set { _NombreUsuarioFacebook = value; }
        }

        private string _NombreUsuarioTwitter;
        /// <summary>
        /// Nombre de Usuario de Twitter
        /// </summary>
        public virtual string NombreUsuarioTwitter
        {
            get { return _NombreUsuarioTwitter; }
            set { _NombreUsuarioTwitter = value; }
        }

        private string _NombreUsuarioGooglePlus;
        /// <summary>
        /// Nombre de Usuario de Google+
        /// </summary>
        public virtual string NombreUsuarioGooglePlus
        {
            get { return _NombreUsuarioGooglePlus; }
            set { _NombreUsuarioGooglePlus = value; }
        }

        private UsuarioPerfil _Perfil;
        /// <summary>
        /// Perfil del Usuario
        /// </summary>
        public virtual UsuarioPerfil Perfil
        {
            get { return _Perfil; }
            set { _Perfil = value; }
        }

        private UsuarioTipo _Tipo;
        /// <summary>
        /// Tipo del Usuario
        /// </summary>
        public virtual UsuarioTipo Tipo
        {
            get { return _Tipo; }
            set { _Tipo = value; }
        }

        private DateTime _FechaCreacion;
        /// <summary>
        /// Fecha Creación
        /// </summary>
        public virtual DateTime FechaCreacion
        {
            get { return _FechaCreacion; }
            set { _FechaCreacion = value; }
        }

        #endregion

        #region Individuo / Institucion / Municipio
        
        private string _Apellido;
        /// <summary>
        /// Apellido
        /// </summary>
        public virtual string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }

        private string _Telefono;
        /// <summary>
        /// Telefono
        /// </summary>
        public virtual string Telefono
        {
            get { return _Telefono; }
            set { _Telefono = value; }
        }

        private string _Email;
        /// <summary>
        /// Email
        /// </summary>
        public virtual string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _Web;
        /// <summary>
        /// Web
        /// </summary>
        public virtual string Web
        {
            get { return _Web; }
            set { _Web = value; }
        }

        #endregion

        #region Ubicacion

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private Provincia _Provincia;
        /// <summary>
        /// Provincia
        /// </summary>
        public virtual Provincia Provincia
        {
            get { return _Provincia; }
            set { _Provincia = value; }
        }

        private string _Ciudad;
        /// <summary>
        /// Ciudad
        /// </summary>
        public virtual string Ciudad
        {
            get { return _Ciudad; }
            set { _Ciudad = value; }
        }

        #endregion

        #region Datos Donante

        private float _Latitud;
        /// <summary>
        /// Latitud
        /// </summary>
        public virtual float Latitud
        {
            get { return _Latitud; }
            set { _Latitud = value; }
        }

        private float _Longitud;
        /// <summary>
        /// Longitud
        /// </summary>
        public virtual float Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        #endregion

        #region Arboles

        private IList<UsuarioPlantacion> _Plantaciones;
        /// <summary>
        /// Arboles plantados por el Usuario
        /// </summary>
        public virtual IList<UsuarioPlantacion> Plantaciones
        {
            get { return _Plantaciones; }
            set { _Plantaciones = value; }
        }

        private IList<UsuarioAdopcion> _Adopciones;
        /// <summary>
        /// Arboles adoptados por el Usuario
        /// </summary>
        public virtual IList<UsuarioAdopcion> Adopciones
        {
            get { return _Adopciones; }
            set { _Adopciones = value; }
        }

        private IList<UsuarioDonacion> _Donaciones;
        /// <summary>
        /// Arboles donados por el Usuario
        /// </summary>
        public virtual IList<UsuarioDonacion> Donaciones
        {
            get { return _Donaciones; }
            set { _Donaciones = value; }
        }

        #endregion

        private IList<UsuarioActividad> _Actividades;
        /// <summary>
        /// Actividades agendadas por el Usuario
        /// </summary>
        public virtual IList<UsuarioActividad> Actividades
        {
            get { return _Actividades; }
            set { _Actividades = value; }
        }



        #endregion
    }
}
