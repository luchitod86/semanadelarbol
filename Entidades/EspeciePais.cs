﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class EspeciePais : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspeciePais()
        {
        }

        #endregion

        #region Propiedades

        private Especie _Especie;
        /// <summary>
        /// Especie
        /// </summary>
        public virtual Especie Especie
        {
            get { return _Especie; }
            set { _Especie = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
