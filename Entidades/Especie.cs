﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Especie : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Especie()
        {
        }

        #endregion

        #region Propiedades

        private string _NombreCientifico;
        /// <summary>
        /// Nombre Científico
        /// </summary>
        public virtual string NombreCientifico
        {
            get { return _NombreCientifico; }
            set { _NombreCientifico = value; }
        }

        private double _FactorCO2;
        /// <summary>
        /// Factor CO2
        /// </summary>
        public virtual double FactorCO2
        {
            get { return _FactorCO2; }
            set { _FactorCO2 = value; }
        }

        private bool? _DesdeWiki;
        /// <summary>
        /// Desde Wiki?
        /// </summary>
        public virtual bool? DesdeWiki
        {
            get { return _DesdeWiki; }
            set { _DesdeWiki = value; }
        }

        private bool? _Aprobado;
        /// <summary>
        /// Aprobado?
        /// </summary>
        public virtual bool? Aprobado
        {
            get { return _Aprobado; }
            set { _Aprobado = value; }
        }

        private bool? _EsNativo;
        /// <summary>
        /// EsNativo
        /// </summary>
        public virtual bool? EsNativo
        {
            get { return _EsNativo; }
            set { _EsNativo = value; }
        }

        private bool? _EsExotico;
        /// <summary>
        /// EsExotico
        /// </summary>
        public virtual bool? EsExotico
        {
            get { return _EsExotico; }
            set { _EsExotico = value; }
        }

        private string _MasInfo;
        /// <summary>
        /// MasInfo
        /// </summary>
        public virtual string MasInfo
        {
            get { return _MasInfo; }
            set { _MasInfo = value; }
        }

        private string _Origen;
        /// <summary>
        /// Origen
        /// </summary>
        public virtual string Origen
        {
            get { return _Origen; }
            set { _Origen = value; }
        }

        private string _Ecorregion;
        /// <summary>
        /// Ecorregion
        /// </summary>
        public virtual string Ecorregion
        {
            get { return _Ecorregion; }
            set { _Ecorregion = value; }
        }

        private string _Altura;
        /// <summary>
        /// Altura
        /// </summary>
        public virtual string Altura
        {
            get { return _Altura; }
            set { _Altura = value; }
        }

        private string _Follaje;
        /// <summary>
        /// Follaje
        /// </summary>
        public virtual string Follaje
        {
            get { return _Follaje; }
            set { _Follaje = value; }
        }

        private string _Hojas;
        /// <summary>
        /// Hojas
        /// </summary>
        public virtual string Hojas
        {
            get { return _Hojas; }
            set { _Hojas = value; }
        }

        private string _Flores;
        /// <summary>
        /// Flores
        /// </summary>
        public virtual string Flores
        {
            get { return _Flores; }
            set { _Flores = value; }
        }

        private string _Frutos;
        /// <summary>
        /// Frutos
        /// </summary>
        public virtual string Frutos
        {
            get { return _Frutos; }
            set { _Frutos = value; }
        }

        private bool? _AptoVereda;
        /// <summary>
        /// AptoVereda
        /// </summary>
        public virtual bool? AptoVereda
        {
            get { return _AptoVereda; }
            set { _AptoVereda = value; }
        }

        private bool? _AptoCantero;
        /// <summary>
        /// AptoCantero
        /// </summary>
        public virtual bool? AptoCantero
        {
            get { return _AptoCantero; }
            set { _AptoCantero = value; }
        }

        private bool? _AptoPlaza;
        /// <summary>
        /// AptoPlaza
        /// </summary>
        public virtual bool? AptoPlaza
        {
            get { return _AptoPlaza; }
            set { _AptoPlaza = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        private IList<EspeciePais> _Localizaciones;
        /// <summary>
        /// Localizaciones
        /// </summary>
        public virtual IList<EspeciePais> Localizaciones
        {
            get { return _Localizaciones; }
            set { _Localizaciones = value; }
        }

        private IList<EspeciePaisWiki> _Paises;
        /// <summary>
        /// Paises
        /// </summary>
        public virtual IList<EspeciePaisWiki> Paises
        {
            get { return _Paises; }
            set { _Paises = value; }
        }

        private IList<EspecieImagen> _Imagenes;
        /// <summary>
        /// Imagenes para la Especie
        /// </summary>
        public virtual IList<EspecieImagen> Imagenes
        {
            get { return _Imagenes; }
            set { _Imagenes = value; }
        }

        private IList<EspecieUsuario> _Usuarios;
        /// <summary>
        /// Localizaciones
        /// </summary>
        public virtual IList<EspecieUsuario> Usuarios
        {
            get { return _Usuarios; }
            set { _Usuarios = value; }
        }

        #endregion
    }
}
