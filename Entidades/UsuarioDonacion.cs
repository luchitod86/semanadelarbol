﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class UsuarioDonacion : Entidad
    {
        #region Constructores

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioDonacion()
        {
        }

        #endregion

        #region Propiedades

        private Usuario _Donante;
        /// <summary>
        /// Donante
        /// </summary>
        public virtual Usuario Donante
        {
            get { return _Donante; }
            set { _Donante = value; }
        }

        private Especie _Arbol;
        /// <summary>
        /// Arbol
        /// </summary>
        public virtual Especie Arbol
        {
            get { return _Arbol; }
            set { _Arbol = value; }
        }

        private int _Cantidad;
        /// <summary>
        /// Cantidad
        /// </summary>
        public virtual int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }

        private int _Disponibles;
        /// <summary>
        /// Disponibles
        /// </summary>
        public virtual int Disponibles
        {
            get { return _Disponibles; }
            set { _Disponibles = value; }
        }

        private int _Adoptados;
        /// <summary>
        /// Adoptados
        /// </summary>
        public virtual int Adoptados
        {
            get { return _Adoptados; }
            set { _Adoptados = value; }
        }

        private DateTime _Fecha;
        /// <summary>
        /// Fecha
        /// </summary>
        public virtual DateTime Fecha
        {
            get { return _Fecha; }
            set { _Fecha = value; }
        }

        private Pais _Pais;
        /// <summary>
        /// Pais
        /// </summary>
        public virtual Pais Pais
        {
            get { return _Pais; }
            set { _Pais = value; }
        }

        #endregion
    }
}
