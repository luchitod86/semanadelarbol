﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;
using Persistencia;

namespace Controladores
{
    public class EmailControlador
    {
        EmailRepositorio _emailRepositorio = new EmailRepositorio();

        /// <summary>
        /// Guardar un Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool Guardar(Email email)
        {
            return _emailRepositorio.Guardar(email);
        }

    }
}
