﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class PaisControlador
    {
        PaisRepositorio _paisRepositorio = new PaisRepositorio();

        /// <summary>
        /// Devuelve un Pais según un Id
        /// </summary>
        /// <param name="Id_Pais"></param>
        /// <returns></returns>
        public Pais ObtenerPorId(int Id_Pais)
        {
            return _paisRepositorio.Obtener(Id_Pais);
        }

        /// <summary>
        /// Devuelve un Pais según un Nombre
        /// </summary>
        /// <param name="Nombre_Pais"></param>
        /// <returns></returns>
        public Pais ObtenerPorNombre(string Nombre_Pais)
        {
            return _paisRepositorio.ObtenerPorNombre(Nombre_Pais);
        }

        /// <summary>
        /// Devuelve todos los Países
        /// </summary>
        /// <param name="incluirInactivos"></param>
        /// <returns></returns>
        public List<Pais> ObtenerTodos(bool incluirInactivos = false)
        {
            return _paisRepositorio.ObtenerTodos(incluirInactivos);
        }
    }
}
