﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class LogControlador
    {
        LogRepositorio logRepositorio = new LogRepositorio();

        /// <summary>
        /// Devuelve un Log según un Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Log ObtenerPorId(int id)
        {
            return logRepositorio.Obtener(id);
        }

        /// <summary>
        /// Devuelve un Tipo de Log, según su Nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public LogTipo ObtenerTipo(string nombre)
        {
            return logRepositorio.ObtenerTipo(nombre);
        }

        /// <summary>
        /// Devuelve todos los Logs
        /// </summary>
        /// <param name="fechaDesde"></param>
        /// <param name="fechaHasta"></param>
        /// <param name="idTipo"></param>
        /// <param name="seccion"></param>
        /// <returns></returns>
        public List<Log> ObtenerTodos(Nullable<DateTime> fechaDesde, Nullable<DateTime> fechaHasta, Nullable<int> idTipo, string seccion = null)
        {
            return logRepositorio.ObtenerTodos(fechaDesde, fechaHasta, idTipo, seccion);
        }

        /// <summary>
        /// Guardar un Log
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public bool Guardar(Log log)
        {
            return logRepositorio.Guardar(log);
        }
    }
}
