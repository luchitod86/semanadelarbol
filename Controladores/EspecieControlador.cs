﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class EspecieControlador
    {
        EspecieRepositorio _especieRepositorio = new EspecieRepositorio();

        /// <summary>
        /// Devuelve una especie de Árbol según un Id
        /// </summary>
        /// <param name="Id_Especie"></param>
        /// <returns></returns>
        public Especie ObtenerPorId(int Id_Especie)
        {
            return _especieRepositorio.Obtener(Id_Especie);
        }

        /// <summary>
        /// Devuelve todas las Especies
        /// </summary>
        /// <returns></returns>
        public List<Especie> ObtenerTodas()
        {
            return _especieRepositorio.ObtenerTodas();
        }

        /// <summary>
        /// Devuelve todas las Especies de un pais y las especies comunes
        /// </summary>
        /// <returns></returns>
        public List<Especie> ObtenerTodasPorPais(int idPais)
        {
            return _especieRepositorio.ObtenerTodasPorPais(idPais);
        }

        /// <summary>
        /// Devuelve todas las Localizaciones de una Especie
        /// </summary>
        /// <returns></returns>
        public List<EspeciePais> ObtenerLocalizaciones(int idEspecie)
        {
            return _especieRepositorio.ObtenerLocalizaciones(idEspecie);
        }

        /// <summary>
        /// Devuelve las últimas Noticias paginadas
        /// </summary>
        /// <returns></returns>
        public List<Especie> ObtenerTodasWiki()
        {
            return _especieRepositorio.ObtenerTodasWiki();
        }

        /// <summary>
        /// Devuelve la cantidad total de especies de la Wiki
        /// </summary>
        /// <returns></returns>
        public int ObtenerTotalEspeciesWiki()
        {
            return _especieRepositorio.ObtenerTotalEspeciesWiki();
        }

        /// <summary>
        /// Devuelve todas los usuarios que aportaron en la info de una Especie
        /// </summary>
        /// <returns></returns>
        public List<Usuario> ObtenerUsuariosAporteWiki(int idEspecie)
        {
            return _especieRepositorio.ObtenerUsuariosAporteWiki(idEspecie);
        }

        /// <summary>
        /// Devuelve todas los paises asociados a una Especie
        /// </summary>
        /// <returns></returns>
        public List<Pais> ObtenerPaisesEspecieWiki(int idEspecie)
        {
            return _especieRepositorio.ObtenerPaisesEspecieWiki(idEspecie);
        }


        public bool GuardarEspecie(Especie especie){
            return _especieRepositorio.Guardar(especie);
        }
    }
}
