﻿using Entidades;
using Persistencia.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladores
{
    public class ControladorBase<T> where T : Entidad
    {
        RepositorioBase<T> _repositorioBase = new RepositorioBase<T>();

        /// <summary>
        /// Devuelve todas las Entidades
        /// </summary>
        /// <returns></returns>
        public List<T> ObtenerEntidades()
        {
            try
            {
                return _repositorioBase.ObtenerEntidades();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Devuelve todas las Entidades, según un filtro
        /// </summary>
        /// <returns></returns>
        public List<T> ObtenerEntidades(string filtro)
        {
            try
            {
                return _repositorioBase.ObtenerEntidades(filtro);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Devuelve una Entidad, dado su Id (PK)
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public T ObtenerEntidad(int id)
        {
            try
            {
                return _repositorioBase.ObtenerEntidad(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Devuelve una Entidad, según un filtro
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public T ObtenerEntidad(string filtro)
        {
            try
            {
                return _repositorioBase.ObtenerEntidad(filtro);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Guarda una Entidad
        /// <param name="entidad"></param>
        /// </summary>
        /// <returns></returns>
        public bool GuardarEntidad(Entidad entidad)
        {
            return _repositorioBase.GuardarEntidad(entidad);
        }

        /// <summary>
        /// Elimina una Entidad
        /// </summary>
        /// <param name="entidad"></param>
        /// <returns></returns>
        public bool EliminarEntidad(Entidad entidad)
        {
            return _repositorioBase.EliminarEntidad(entidad);
        }
    }

}
