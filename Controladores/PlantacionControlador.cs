﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class PlantacionControlador
    {
        PlantacionRepositorio _plantacionRepositorio = new PlantacionRepositorio();

        /// <summary>
        /// Devuelve una plantación de usuario, según un Id
        /// </summary>
        /// <param name="Id_Plantacion"></param>
        /// <returns></returns>
        public UsuarioPlantacion ObtenerPorId(int Id_Plantacion)
        {
            return _plantacionRepositorio.Obtener(Id_Plantacion);
        }

        /// <summary>
        /// Devuelve el total de las plantaciones de usuarios
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public int ObtenerTotalPlantaciones(int idPais = -1, int periodo = -1, int idUsuario = -1)
        {
            return _plantacionRepositorio.ObtenerTotalPlantaciones(idPais, periodo, idUsuario);
        }

        /// <summary>
        /// Devuelve todas las plantaciones de usuarios
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <param name="soloConFoto"></param>
        /// <returns></returns>
        public List<UsuarioPlantacion> ObtenerTodas(int idUsuario = -1, string pais = "", int periodo = -1, bool soloConFoto = false)
        {
            return _plantacionRepositorio.ObtenerTodas(idUsuario, pais, periodo, soloConFoto);
        }

        /// <summary>
        /// Devuelve todas las plantaciones de usuarios
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="soloConFoto"></param>
        /// <returns></returns>
        public List<UsuarioPlantacion> ObtenerTodas(int idUsuario = -1, int idPais = Comun.Constantes.Defaults.Pais.ID, int periodo = -1, bool soloConFoto = false)
        {
            return _plantacionRepositorio.ObtenerTodas(idUsuario, idPais, periodo, soloConFoto);
        }

        /// <summary>
        /// Devuelve las últimas Plantaciones, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioPlantacion> ObtenerUltimasPlantaciones(int cantidad, string pais = "", int periodo = -1)
        {
            return _plantacionRepositorio.ObtenerUltimasPlantaciones(cantidad, pais, periodo);
        }

    }
}
