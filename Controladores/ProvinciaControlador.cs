﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class ProvinciaControlador
    {
        ProvinciaRepositorio _provinciaRepositorio = new ProvinciaRepositorio();

        /// <summary>
        /// Devuelve una Provincia según un Id
        /// </summary>
        /// <param name="Id_Provincia"></param>
        /// <returns></returns>
        public Provincia ObtenerPorId(int Id_Provincia)
        {
            return _provinciaRepositorio.Obtener(Id_Provincia);
        }

        /// <summary>
        /// Devuelve todas las Provincias de un País
        /// </summary>
        /// <param name="Id_Pais"></param>
        /// <returns></returns>
        public List<Provincia> ObtenerTodas(int Id_Pais)
        {
            return _provinciaRepositorio.ObtenerTodas(Id_Pais);
        }

    }
}
