﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia.Repositorios;
using Entidades;

namespace Controladores
{
    public class AdopcionControlador
    {
        AdopcionRepositorio _adopcionRepositorio = new AdopcionRepositorio();

        /// <summary>
        /// Devuelve todas las Adopciones
        /// </summary>
        /// <param name="idDonante"></param>
        /// <param name="idEspecie"></param>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioAdopcion> ObtenerTodas(int idDonante = -1, int idEspecie = -1, int idPais = Comun.Constantes.Defaults.Pais.ID, int periodo = -1)
        {
            return _adopcionRepositorio.ObtenerTodas(idDonante, idEspecie, idPais, periodo);
        }

        /// <summary>
        /// Devuelve la cantidad total de Adopciones
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idDonante"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idEspecie"></param>
        /// <returns></returns>
        public int ObtenerTotalAdopciones(int idPais = -1, int periodo = -1, int idDonante = -1, int idUsuario = -1, int idEspecie = -1)
        {
            try
            {
                return _adopcionRepositorio.ObtenerTotalAdopciones(idPais, periodo, idDonante, idUsuario, idEspecie);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve las últimas Adopciones, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioAdopcion> ObtenerUltimasAdopciones(int cantidad, string pais = "", int periodo = -1)
        {
            return _adopcionRepositorio.ObtenerUltimasAdopciones(cantidad, pais, periodo);
        }

    }
}
