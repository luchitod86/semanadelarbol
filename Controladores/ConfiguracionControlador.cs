﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class ConfiguracionControlador
    {
        ConfiguracionRepositorio _configuracionRepositorio = new ConfiguracionRepositorio();

        /// <summary>
        /// Devuelve la Configuración del sistema
        /// </summary>
        /// <returns></returns>
        public Configuracion ObtenerConfiguracion()
        {
            return _configuracionRepositorio.ObtenerConfiguracion();
        }

        /// <summary>
        /// Devuelve la Configuración de Email
        /// </summary>
        /// <returns></returns>
        public ConfiguracionEmail ObtenerConfiguracionEmail()
        {
            return _configuracionRepositorio.ObtenerConfiguracionEmail();
        }
        
        /// <summary>
        /// Guarda una Configuracion
        /// </summary>
        /// <param name="configuracion"></param>
        /// <returns></returns>
        public bool Guardar(Configuracion configuracion)
        {
            return _configuracionRepositorio.Guardar(configuracion);
        }
    
    }
}