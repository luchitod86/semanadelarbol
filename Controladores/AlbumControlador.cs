﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class AlbumControlador
    {
        AlbumRepositorio _albumRepositorio = new AlbumRepositorio();

        /// <summary>
        /// Devuelve un Album según un Id
        /// </summary>
        /// <param name="Id_Provincia"></param>
        /// <returns></returns>
        public Album ObtenerPorId(int id)
        {
            return _albumRepositorio.Obtener(id);
        }

        /// <summary>
        /// Devuelve todos los Albums de una entidad
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="idEntidad"></param>
        /// <returns></returns>
        public List<Album> ObtenerTodos(int periodo = -1, string pais = "", string idEntidad = "")
        {
            return _albumRepositorio.ObtenerTodos(periodo, pais, idEntidad);
        }

        /// <summary>
        /// Guardar un Album
        /// </summary>
        /// <param name="album"></param>
        /// <returns></returns>
        public bool Guardar(Album album)
        {
            return _albumRepositorio.Guardar(album);
        }

        /// <summary>
        /// Elimina un Album
        /// </summary>
        /// <param name="album"></param>
        /// <returns></returns>
        public bool Eliminar(Album album)
        {
            return _albumRepositorio.Eliminar(album);
        }

    }
}
