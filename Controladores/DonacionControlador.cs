﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class DonacionControlador
    {
        DonacionRepositorio _donacionRepositorio = new DonacionRepositorio();

        /// <summary>
        /// Devuelve una donación de usuario, según un Id
        /// </summary>
        /// <param name="Id_Donacion"></param>
        /// <returns></returns>
        public UsuarioDonacion ObtenerPorId(int Id_Donacion)
        {
            return _donacionRepositorio.Obtener(Id_Donacion);
        }

        /// <summary>
        /// Devuelve todas las donaciones de un Donante
        /// </summary>
        /// <param name="idDonante"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioDonacion> ObtenerTodas(int idDonante = -1, string pais = "", int periodo = -1)
        {
            return _donacionRepositorio.ObtenerTodas(idDonante, pais, periodo);
        }

        /// <summary>
        /// Devuelve todas las donaciones de un Donante
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioDonacion> ObtenerTodas(int idDonante = -1, int idPais = Comun.Constantes.Defaults.Pais.ID, int periodo = -1)
        {
            return _donacionRepositorio.ObtenerTodas(idDonante, idPais, periodo);
        }

        /// <summary>
        /// Devuelve la cantidad total de Donaciones
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public int ObtenerTotalDonaciones(int idPais = -1, int periodo = -1, int idUsuario = -1)
        {
            return _donacionRepositorio.ObtenerTotalDonaciones(idPais, periodo, idUsuario);
        }

        /// <summary>
        /// Devuelve el total de todos los Donantes
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public int ObtenerTotalDonantes(int idPais = -1, int periodo = -1)
        {
            return _donacionRepositorio.ObtenerTotalDonantes(idPais, periodo);
        }

        /// <summary>
        /// Devuelve las últimas Donaciones, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioDonacion> ObtenerUltimasDonaciones(int cantidad, string pais = "", int periodo = -1)
        {
            return _donacionRepositorio.ObtenerUltimasDonaciones(cantidad, pais, periodo);
        }    

    }
}
