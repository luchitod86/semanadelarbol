﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Persistencia;
using Entidades;
using Comun;

namespace Controladores
{
    public class UsuarioControlador
    {
        UsuarioRepositorio _usuarioRepositorio = new UsuarioRepositorio();

        /// <summary>
        /// Devuelve un Usuario según un Id
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public Usuario ObtenerPorId(int Id_Usuario)
        {
            return _usuarioRepositorio.Obtener(Id_Usuario);
        }

        /// <summary>
        /// Obtiene el Tipo de Usuario
        /// </summary>
        /// <param name="Id_Tipo"></param>
        /// <returns></returns>
        public UsuarioTipo ObtenerTipoUsuario(int Id_Tipo)
        {
            return _usuarioRepositorio.ObtenerTipoUsuario(Id_Tipo);
        }

        /// <summary>
        /// Obtiene el Perfil de Usuario
        /// </summary>
        /// <param name="Id_Perfil"></param>
        /// <returns></returns>
        public UsuarioPerfil ObtenerPerfilUsuario(int Id_Perfil)
        {
            return _usuarioRepositorio.ObtenerPerfilUsuario(Id_Perfil);
        }

        #region Totales

        /// <summary>
        /// Devuelve el total de Usuarios
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public int ObtenerTotal(string filtro)
        {
            try
            {
                return _usuarioRepositorio.ObtenerTotal(filtro);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve la cantidad de Usuarios, según un Tipo
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idUsuario"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public int ObtenerTotalUsuarios(int idPais = -1, int periodo = -1, int idUsuario = -1, string tipo = "")
        {
            try
            {
                return _usuarioRepositorio.ObtenerTotalUsuarios(idPais, periodo, idUsuario, tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve la cantidad de Usuarios, según un pais
        /// </summary>
        /// <param name="pais"></param>
        /// <returns></returns>
        public int ObtenerTotalUsuarios(string pais = "")
        {
            try
            {
                return _usuarioRepositorio.ObtenerTotalUsuarios(pais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        /// <summary>
        /// Devuelve todos los Donantes
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<Usuario> ObtenerDonantes(string pais = "", int periodo = -1)
        {
            return _usuarioRepositorio.ObtenerDonantes(pais, periodo);
        }

        /// <summary>
        /// Devuelve todos los Donantes, según un Pais y Provincia
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="idPais"></param>
        /// <param name="idProvincia"></param>
        /// <param name="ciudad"></param>
        /// <returns></returns>
        public List<Usuario> ObtenerDonantes(int periodo, int idPais, int idProvincia, string ciudad)
        {
            return _usuarioRepositorio.ObtenerDonantes(periodo, idPais, idProvincia, ciudad);
        }

        /// <summary>
        /// Devuelve todos los Usuarios
        /// </summary>
        /// <returns></returns>
        public List<Usuario> ObtenerTodos()
        {
            return _usuarioRepositorio.ObtenerTodos();
        }

        /// <summary>
        /// Devuelve todos los Usuarios
        /// </summary>
        /// <returns></returns>
        public List<Usuario> ObtenerTodos(int periodo = -1, string pais = "")
        {
            return _usuarioRepositorio.ObtenerTodos(periodo, pais);
        }

        /// <summary>
        /// Devuelve todos los usuarios
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<Usuario> ObtenerTodos(int periodo = -1, int idPais = Comun.Constantes.Defaults.Pais.ID)
        {
            return _usuarioRepositorio.ObtenerTodos(periodo, idPais);
        }

        /// <summary>
        /// Devuelve todos los usuarios en una lista paginada
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<Usuario> ObtenerTodos_Paginado(int periodo = -1, string pais = "", int pagina = 1, int cantidad = 10)
        {
            return _usuarioRepositorio.ObtenerTodos_Paginado(periodo, pais, pagina, cantidad);
        }

        /// <summary>
        /// Obtiene un Usuario según un Nombre de Usuario y Password
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Usuario ObtenerPorUsuarioYPassword(string nombreUsuario, string password)
        {
            return _usuarioRepositorio.ObtenerPorUsuarioYPassword(nombreUsuario, password);
        }

        /// <summary>
        /// Obtiene un Usuario según un Nombre de Usuario
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <returns></returns>
        public Usuario ObtenerPorUsuario(string nombreUsuario)
        {
            return _usuarioRepositorio.ObtenerPorUsuario(nombreUsuario);
        }

        /// <summary>
        /// Obtiene un Usuario según un Nombre de Usuario y Red Social
        /// </summary>
        /// <param name="redSocial"></param>
        /// <param name="nombreUsuario"></param>
        /// <returns></returns>
        public Usuario ObtenerPorUsuarioRedSocial(string redSocial, string nombreUsuario)
        {
            return _usuarioRepositorio.ObtenerPorUsuarioRedSocial(redSocial, nombreUsuario);
        }

        /// <summary>
        /// Obtiene un Usuario según un Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Usuario ObtenerPorEmail(string email)
        {
            return _usuarioRepositorio.ObtenerPorEmail(email);
        }

        /// <summary>
        /// Guardar un Usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool Guardar(Usuario usuario)
        {
            return _usuarioRepositorio.Guardar(usuario);
        }

        /// <summary>
        /// Elimina un Usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool Eliminar(Usuario usuario)
        {
            return _usuarioRepositorio.Eliminar(usuario);
        }

    }
}
