﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class MensajeControlador
    {
        MensajeRepositorio _mensajeRepositorio = new MensajeRepositorio();

        /// <summary>
        /// Devuelve un Mensaje según un Id
        /// </summary>
        /// <param name="Id_Mensaje"></param>
        /// <returns></returns>
        public Mensaje ObtenerPorId(int Id_Mensaje)
        {
            return _mensajeRepositorio.Obtener(Id_Mensaje);
        }

        /// <summary>
        /// Devuelve todas los Mensajes
        /// </summary>
        /// <returns></returns>
        public List<Mensaje> ObtenerTodos()
        {
            return _mensajeRepositorio.ObtenerTodos();
        }

        /// <summary>
        /// Devuelve el total de Mensajes
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public int ObtenerTotal(string filtro)
        {
            try
            {
                return _mensajeRepositorio.ObtenerTotal(filtro);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Guarda un Mensaje
        /// </summary>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        public bool Guardar(Mensaje mensaje)
        {
            return _mensajeRepositorio.Guardar(mensaje);
        }

        /// <summary>
        /// Elimina un Mensaje
        /// </summary>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        public bool Eliminar(Mensaje mensaje)
        {
            return _mensajeRepositorio.Eliminar(mensaje);
        }

    }

}
