﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class ActividadControlador
    {
        ActividadRepositorio _actividadRepositorio = new ActividadRepositorio();

        /// <summary>
        /// Devuelve una Actividad según su Id
        /// </summary>
        /// <param name="Id_Actividad"></param>
        /// <returns></returns>
        public UsuarioActividad ObtenerPorId(int Id_Actividad)
        {
            return _actividadRepositorio.Obtener(Id_Actividad);
        }

        /// <summary>
        /// Devuelve todas las Actividades
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="mes"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        public List<UsuarioActividad> ObtenerTodas(int idPais = -1, int periodo = -1, int mes = -1, int cantidad = -1)
        {
            return _actividadRepositorio.ObtenerTodas(idPais, periodo, mes, cantidad);
        }

        /// <summary>
        /// Devuelve el total de las Actividades
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public int ObtenerTotalActividades(int idPais = -1, int periodo = -1)
        {
            return _actividadRepositorio.ObtenerTotalActividades(idPais, periodo);
        }

        /// <summary>
        /// Devuelve todas las Actividades de un período
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<UsuarioActividad> ObtenerTodasPorFecha(DateTime fecha, string pais = "")
        {
            return _actividadRepositorio.ObtenerTodasPorFecha(fecha, pais);
        }

        /// <summary>
        /// Devuelve las últimas Actividades, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioActividad> ObtenerUltimasActividades(int cantidad, string pais = "", int periodo = -1)
        {
            return _actividadRepositorio.ObtenerUltimasActividades(cantidad, pais, periodo);
        }

    }
}
