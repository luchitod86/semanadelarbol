﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class ViveroControlador
    {
        ViveroRepositorio _viveroRepositorio = new ViveroRepositorio();

        /// <summary>
        /// Devuelve una Provincia según un Id
        /// </summary>
        /// <param name="Id_Provincia"></param>
        /// <returns></returns>
        public Vivero ObtenerPorId(int id)
        {
            return _viveroRepositorio.Obtener(id);
        }

        /// <summary>
        /// Devuelve todas los Viveros de un País
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<Vivero> ObtenerTodos(int periodo = -1, string pais = "")
        {
            return _viveroRepositorio.ObtenerTodos(periodo, pais);
        }

        /// <summary>
        /// Guardar un Vivero
        /// </summary>
        /// <param name="vivero"></param>
        /// <returns></returns>
        public bool Guardar(Vivero vivero)
        {
            return _viveroRepositorio.Guardar(vivero);
        }

        /// <summary>
        /// Elimina un Vivero
        /// </summary>
        /// <param name="vivero"></param>
        /// <returns></returns>
        public bool Eliminar(Vivero vivero)
        {
            return _viveroRepositorio.Eliminar(vivero);
        }

    }
}
