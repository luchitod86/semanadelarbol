﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class SponsorControlador
    {
        SponsorRepositorio _sponsorRepositorio = new SponsorRepositorio();

        /// <summary>
        /// Devuelve una Provincia según un Id
        /// </summary>
        /// <param name="Id_Provincia"></param>
        /// <returns></returns>
        public Sponsor ObtenerPorId(int id)
        {
            return _sponsorRepositorio.Obtener(id);
        }

        /// <summary>
        /// Devuelve todas los Sponsors de un País
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="idPais"></param>
        /// <returns></returns>
        public List<Sponsor> ObtenerTodos(int periodo = -1, string pais = "", int idPais = -1)
        {
            return _sponsorRepositorio.ObtenerTodos(periodo, pais, idPais);
        }

        /// <summary>
        /// Guardar un Sponsor
        /// </summary>
        /// <param name="sponsor"></param>
        /// <returns></returns>
        public bool Guardar(Sponsor sponsor)
        {
            return _sponsorRepositorio.Guardar(sponsor);
        }

        /// <summary>
        /// Elimina un Sponsor
        /// </summary>
        /// <param name="sponsor"></param>
        /// <returns></returns>
        public bool Eliminar(Sponsor sponsor)
        {
            return _sponsorRepositorio.Eliminar(sponsor);
        }

    }
}
