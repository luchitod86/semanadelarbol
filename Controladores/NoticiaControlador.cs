﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using Persistencia.Repositorios;

namespace Controladores
{
    public class NoticiaControlador
    {
        NoticiaRepositorio _noticiaRepositorio = new NoticiaRepositorio();

        /// <summary>
        /// Devuelve una noticia, según un Id
        /// </summary>
        /// <param name="Id_Noticia"></param>
        /// <returns></returns>
        public Noticia ObtenerPorId(int Id_Noticia)
        {
            return _noticiaRepositorio.Obtener(Id_Noticia);
        }

        /// <summary>
        /// Devuelve el total de noticias de un pais
        /// </summary>
        /// <param name="pais"></param>
        /// <returns></returns>
        public int ObtenerTotalPlantaciones(string pais = "")
        {
            return _noticiaRepositorio.ObtenerTotalNoticias(pais);
        }

        /// <summary>
        /// Devuelve todas las plantaciones de noticias
        /// </summary>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<Noticia> ObtenerTodas(string pais = "")
        {
            return _noticiaRepositorio.ObtenerTodas(pais);
        }

        /// <summary>
        /// Devuelve las últimas Noticias, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<Noticia> ObtenerUltimasNoticias(int cantidad, string pais = "")
        {
            return _noticiaRepositorio.ObtenerUltimasNoticias(cantidad, pais);
        }

        /// <summary>
        /// Devuelve las últimas Noticias paginadas
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="pagina"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        public List<Noticia> ObtenerTodas_Paginado(string pais = "", int pagina = 1, int cantidad = 10)
        {
            return _noticiaRepositorio.ObtenerTodas_Paginado( pais, pagina, cantidad);
        }

        /// <summary>
        /// Devuelve la cantidad total de noticias
        /// </summary>
        /// <param name="pais"></param>
        /// <returns></returns>
        public int ObtenerTotalNoticias(string pais = "")
        {
            return _noticiaRepositorio.ObtenerTotalNoticias(pais);
        }

        /// <summary>
        /// Guardar una Noticia
        /// </summary>
        /// <param name="noticia"></param>
        /// <returns></returns>
        public bool Guardar(Noticia noticia)
        {
            return _noticiaRepositorio.Guardar(noticia);
        }
    }
}
