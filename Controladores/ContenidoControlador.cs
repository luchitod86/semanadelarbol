﻿using Entidades;
using Persistencia.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controladores
{
    public class ContenidoControlador
    {
        ContenidoRepositorio _contenidoRepositorio = new ContenidoRepositorio();

        #region SeccionContenido

        /// <summary>
        /// Devuelve una SeccionContenido, según un Id
        /// </summary>
        /// <param name="idSeccionContenido"></param>
        /// <returns></returns>
        public SeccionContenido ObtenerSeccionContenido(int idSeccionContenido)
        {
            return _contenidoRepositorio.ObtenerSeccionContenido(idSeccionContenido);
        }

        /// <summary>
        /// Devuelve una SeccionContenido de un Campo, correspondiente a una Sección, de un País
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="seccion"></param>
        /// <param name="campo"></param>
        /// <returns></returns>
        public SeccionContenido Obtener(int idPais, string seccion, string campo)
        {
            return _contenidoRepositorio.ObtenerSeccionContenido(idPais, seccion, campo);
        }

        /// <summary>
        /// Devuelve todos los Contenidos de una Sección, de un País
        /// </summary>
        /// <returns></returns>
        public List<SeccionContenido> ObtenerTodosLosSeccionContenido(int idPais = -1)
        {
            return _contenidoRepositorio.ObtenerTodosLosSeccionContenido(idPais);
        }

        /// <summary>
        /// Guardar un SeccionContenido
        /// </summary>
        /// <param name="seccionContenido"></param>
        /// <returns></returns>
        public bool Guardar(SeccionContenido seccionContenido)
        {
            return _contenidoRepositorio.GuardarSeccionContenido(seccionContenido);
        }

        #endregion

        #region Seccion

        /// <summary>
        /// Devuelve una Seccion, según un Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Seccion ObtenerSeccion(int id)
        {
            return _contenidoRepositorio.ObtenerSeccion(id);
        }

        /// <summary>
        /// Devuelve una Seccion, según su Nombre
        /// </summary>nombre"></param>
        /// <returns></returns>
        public Seccion ObtenerSeccion(string nombre)
        {
            return _contenidoRepositorio.ObtenerSeccion(nombre);
        }

        #endregion

    }
}
