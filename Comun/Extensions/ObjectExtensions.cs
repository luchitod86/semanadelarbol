﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class ObjectExtensions
{
    public static bool IsNull(this object source)
    {
        return source == null;
    }

    public static bool IsNotNull(this object source)
    {
        return source != null;
    }
}
