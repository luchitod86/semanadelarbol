﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class StringExtensions
{
    public static bool IsNullOrEmpty(this string source)
    {
        return source == null && source.Length == 0;
    }

    public static bool IsNotNullOrEmpty(this string source)
    {
        return source != null || source.Length > 0;
    }

    public static string NullToString(this string source)
    {
        return source.IsNull() ? String.Empty : source;
    }
}
