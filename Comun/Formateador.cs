﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
//using ImageMagick;

namespace Comun
{
    public class Formateador
    {
        #region jSON

        /// <summary>
        /// Clase para conversiones del formato jSON
        /// </summary>
        public static class jSON
        {
            /// <summary>
            /// Devuelve una serialización en formato jSON
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public static string convertToJson(object obj)
            {
                string jSON = string.Empty;
                try
                {
                    jSON = JsonConvert.SerializeObject(obj);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return jSON;
            }

            /// <summary>
            /// Deserializa un objeto a partir de una cadena en formato jSON
            /// </summary>
            /// <param name="jSON"></param>
            /// <returns></returns>
            public static T convertFromJson<T>(string jSON)
            {
                T obj;
                try
                {
                    obj = JsonConvert.DeserializeObject<T>(jSON);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return obj;
            }

        }

        #endregion

        #region Imágenes

        /// <summary>
        /// Clase para conversiones de array de Bytes
        /// </summary>
        public static class ArrayDeBytes
        {
            /// <summary>
            /// Convierte una imagen en un array de bytes
            /// </summary>
            /// <param name="image"></param>
            /// <param name="format"></param>
            /// <returns></returns>
            public static byte[] ConvertirEnByteArray(Image image, ImageFormat format)
            {
                MemoryStream ms = new MemoryStream();
                image.Save(ms, format);
                return ms.ToArray();
            }

            /// <summary>
            /// Convierte un array de bytes en una imagen
            /// </summary>
            /// <param name="array"></param>
            /// <returns></returns>
            public static Image ConvertirEnImagen(byte[] array)
            {
                MemoryStream ms = new MemoryStream(array);
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
        }

        /// <summary>
        /// Clase para conversiones del formato Base64
        /// </summary>
        public static class Base64
        {
            /// <summary>
            /// Convierte un array de Bytes en su representación en Base64
            /// </summary>
            /// <param name="image"></param>
            /// <returns></returns>
            public static string ConvertirEnBase64(byte[] array)
            {
                string base64 = string.Empty;
                if (array != null)
                {
                    base64 = Convert.ToBase64String(array);
                }
                return base64;
            }

            /// <summary>
            /// Convierte una representación en Base64 a un array de bytes
            /// </summary>
            /// <param name="base64Encoding"></param>
            /// <returns></returns>
            public static byte[] ConvertirEnArrayDeBytes(string base64)
            {
                byte[] imagen = new byte[] { };
                if (base64 != null)
                {
                    imagen = Convert.FromBase64String(base64);
                }
                return imagen;
            }
        }

        public static class Imagen
        {
            public static MemoryStream Formatear(byte[] image, int width, int height, ImageFormat format)
            {
                Image archivo = Image.FromStream( new MemoryStream(image) );
                return Formatear(archivo, width, height, format);
            }
            public static MemoryStream Formatear(byte[] image, decimal ratio, ImageFormat format)
            {
                Image archivo = Image.FromStream( new MemoryStream(image) );
                return Formatear(archivo, ratio, format);
            }
            public static MemoryStream Formatear(Image image, int width, int height, ImageFormat format)
            {
                Bitmap newImage = new Bitmap(image, width, height);
                return FormatearImagen(newImage, format);
            }
            public static MemoryStream Formatear(Image image, decimal ratio, ImageFormat format)
            {
                // Get the image's original width and height
                int originalWidth   = image.Width;
                int originalHeight  = image.Height;

                int newWidth        = originalWidth;
                int newHeight       = originalHeight;
                if ( ratio < 1 )
                {
                    // New width and height based on aspect ratio
                    newWidth = (int)(originalWidth * ratio);
                    newHeight = (int)(originalHeight * ratio);
                }

                Bitmap newImage = new Bitmap(image, newWidth, newHeight);
                return FormatearImagen(newImage, format);
            }

            #region Private
            private static MemoryStream FormatearImagen(Bitmap image, ImageFormat format)
            {
                // Draws the image in the specified size with quality mode set to HighQuality
                using (Graphics graphics = Graphics.FromImage(image))
                {
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode  = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode      = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode    = PixelOffsetMode.HighQuality;
                    graphics.DrawImage(image, 0, 0, image.Width, image.Height);
                }

                // Get an ImageCodecInfo object that represents the JPEG codec.
                ImageCodecInfo imageCodecInfo = GetEncoderInfo(format);

                // Create an Encoder object for the Quality parameter.
                System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;

                // Create an EncoderParameters object. 
                EncoderParameters encoderParameters = new EncoderParameters(1);
                EncoderParameter encoderParameter = new EncoderParameter(encoder, 100L);
                encoderParameters.Param[0] = encoderParameter;
            
                MemoryStream stream = new MemoryStream();
                image.Save(stream, imageCodecInfo, encoderParameters);
            
                return stream;
            }
            private static ImageCodecInfo GetEncoderInfo(ImageFormat format)
            {
                return ImageCodecInfo.GetImageDecoders().SingleOrDefault(c => c.FormatID == format.Guid);
            }
            #endregion
        }

        #endregion

        #region Streams

        /// <summary>
        /// Clase para conversiones de Streams
        /// </summary>
        public static class IO_Stream
        {
            /// <summary>
            /// Convierte el contenido de un Stream a un array de bytes
            /// </summary>
            /// <param name="input"></param>
            /// <returns></returns>
            public static byte[] ConvertirEnArray(Stream stream)
            {
                byte[] buffer = new byte[stream.Length];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    return ms.ToArray();
                }
            }
        }
        #endregion

        #region Texto
        public static string ReducirTexto(string texto, int longitud)
        {
            if ( texto.Length > longitud )
                texto = texto.Substring(0, longitud) + "...";
            return texto;
        }
        #endregion

        #region ToString

        /// <summary>
        /// Clase para conversiones a String
        /// </summary>
        public static class ToString
        {
            public static string ToCurrency(object value)
            {
                return String.Format("{0:C}", value);
            }

            public static string ToDateTime(DateTime value, string format)
            {
                string _format = "dd/MM/yyyy";
                if (!string.IsNullOrEmpty(format))
                    _format = format;
                string _timeFormat = " HH:mm:ss";
                return String.Format("{0:" + _format + _timeFormat + "}", value);
            }

            public static string ToDate(DateTime value, string format)
            {
                string _format = "dd/MM/yyyy";
                if (!string.IsNullOrEmpty(format))
                    _format = format;
                return String.Format("{0:" + _format + "}", value);
            }

            public static string ToTime(DateTime value)
            {
                return String.Format("{0:HH:mm:ss}", value);
            }

            public static string RemoveNonNumeric(string input)
            {
                //exclude any character that is not a comma ",", a period ".", or a number within range 0 to 9..
                var regex = new Regex("[^.,0-9]", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
                return regex.Replace(input, String.Empty);
            }

            public static string RemoveSpecialCharacters(string input)
            {
                //exclude any character that is not a blank space, or within ranges a to z, 0 to 9..
                var regex = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
                return regex.Replace(input, String.Empty);
            }
        }

        #endregion

    }
}
