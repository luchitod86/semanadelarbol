﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Comun
{
    public class Constantes
    {
        /// <summary>
        /// FILTRO
        /// </summary>
        public const string FILTRO = "FILTRO";

        public class RedSocial
        {
            /// <summary>
            /// Facebook
            /// </summary>
            public const string FACEBOOK = "Facebook";

            /// <summary>
            /// Twitter
            /// </summary>
            public const string TWITTER = "Twitter";

            /// <summary>
            /// Google+
            /// </summary>
            public const string GOOGLE_PLUS = "GooglePlus";
        }

        public class SessionKeys
        {
            /// <summary>
            /// Usuario logueado
            /// </summary>
            public const string USUARIO_LOGUEADO = "USUARIO_LOGUEADO";

            /// <summary>
            /// Preferencias del Usuario logueado
            /// </summary>
            public const string USUARIO_PREFERENCIAS = "USUARIO_PREFERENCIAS";
        }

        public class CacheKeys
        {
            /// <summary>
            /// Configuración
            /// </summary>
            public const string CONFIGURACION = "CONFIGURACION";
            
            /// <summary>
            /// Lista de Perfiles de Usuario
            /// </summary>
            public const string LISTA_USUARIO_PERFILES = "LISTA_USUARIO_PERFILES";
            
            /// <summary>
            /// Lista de Tipos de Usuario
            /// </summary>
            public const string LISTA_USUARIO_TIPOS = "LISTA_USUARIO_TIPOS";

            /// <summary>
            /// Lista de Sponsors
            /// </summary>
            public const string LISTA_SPONSORS = "LISTA_SPONSORS";

            /// <summary>
            /// Lista de Tipos de Sponsor
            /// </summary>
            public const string LISTA_SPONSOR_TIPOS = "LISTA_SPONSOR_TIPOS";
            
            /// <summary>
            /// Lista de Viveros
            /// </summary>
            public const string LISTA_VIVEROS = "LISTA_VIVEROS";

            /// <summary>
            /// Contenidos de Sección
            /// </summary>
            public const string SECCION_CONTENIDO = "SECCION_CONTENIDO";

            /// <summary>
            /// Lista de Países
            /// </summary>
            public const string LISTA_PAISES = "Lista_Paises";

            /// <summary>
            /// Lista de Provincias
            /// </summary>
            public const string LISTA_PROVINCIAS = "Lista_Provincias";

            /// <summary>
            /// Lista de Especies
            /// </summary>
            public const string LISTA_ESPECIES = "LISTA_ESPECIEs";

            /// <summary>
            /// Meta Tags
            /// </summary>
            public const string META_TAGS = "META_TAGS";
        }

        public class Secciones
        {
            /// <summary>
            /// HEADER
            /// </summary>
            public const string HEADER = "Header";

            /// <summary>
            /// FOOTER
            /// </summary>
            public const string FOOTER = "Footer";

            /// <summary>
            /// MENU_ACCIONES
            /// </summary>
            public const string MENU_ACCIONES = "MenuAcciones";

            /// <summary>
            /// AGENDA
            /// </summary>
            public const string AGENDA = "Agenda";

            /// <summary>
            /// MIS_DATOS
            /// </summary>
            public const string MIS_DATOS = "MisDatos";

            /// <summary>
            /// MAPA
            /// </summary>
            public const string MAPA = "Mapa";

            /// <summary>
            /// NOTIFICACIONES
            /// </summary>
            public const string NOTIFICACIONES = "Notificaciones";

            /// <summary>
            /// DEFAULT
            /// </summary>
            public const string DEFAULT = "Default";

            /// <summary>
            /// ADMINISTRACION
            /// </summary>
            public const string ADOPTAR = "Adoptar";

            /// <summary>
            /// ADMINISTRACION
            /// </summary>
            public const string ADMINISTRACION = "Administracion";

            /// <summary>
            /// COMO_PLANTAR_UN_ARBOL
            /// </summary>
            public const string COMO_PLANTAR_UN_ARBOL = "ComoPlantarUnArbol";

            /// <summary>
            /// DETALLE_USUARIO
            /// </summary>
            public const string DETALLE_USUARIO = "DetalleUsuario";

            /// <summary>
            /// DONAR
            /// </summary>
            public const string DONAR = "Donar";

            /// <summary>
            /// ENTIDADES
            /// </summary>
            public const string ENTIDADES = "Entidades";

            /// <summary>
            /// GALERIA
            /// </summary>
            public const string GALERIA = "Galeria";

            /// <summary>
            /// NUEVA_ACTIVIDAD
            /// </summary>
            public const string NUEVA_ACTIVIDAD = "NuevaActividad";

            /// <summary>
            /// PAISES
            /// </summary>
            public const string PAISES = "Paises";

            /// <summary>
            /// Provincias
            /// </summary>
            public const string PROVINCIAS = "Provincias";

            /// <summary>
            /// PARTICIPAR
            /// </summary>
            public const string PARTICIPAR = "Participar";

            /// <summary>
            /// PLANTAR
            /// </summary>
            public const string PLANTAR = "Plantar";

            /// <summary>
            /// QUIENES_SOMOS
            /// </summary>
            public const string QUIENES_SOMOS = "QuienesSomos";

            /// <summary>
            /// QUIENES_SOMOS
            /// </summary>
            public const string CONTACTO = "Contacto";

            /// <summary>
            /// REGISTRARSE
            /// </summary>
            public const string REGISTRARSE = "Registrarse";

            /// <summary>
            /// REPORTES
            /// </summary>
            public const string REPORTES = "Reportes";

            /// <summary>
            /// SEMANA_DEL_ARBOL
            /// </summary>
            public const string SEMANA_DEL_ARBOL = "SemanaDelArbol";

            /// <summary>
            /// SPONSORS
            /// </summary>
            public const string SPONSORS = "Sponsors";

            /// <summary>
            /// VIVEROS
            /// </summary>
            public const string VIVEROS = "Viveros";

            /// <summary>
            /// Configuración Email
            /// </summary>
            public const string CONFIGURACION_EMAIL = "ConfiguracionEmail";

            /// <summary>
            /// Meta Tags
            /// </summary>
            public const string META_TAGS = "MetaTags";

            /// <summary>
            /// Localización
            /// </summary>
            public const string LOCALIZACION = "Localizacion";

            /// <summary>
            /// Especies
            /// </summary>
            public const string ESPECIES = "Especies";

            /// <summary>
            /// Usuarios
            /// </summary>
            public const string USUARIOS = "Usuarios";

            /// <summary>
            /// Noticias
            /// </summary>
            public const string NOTICIAS = "Noticias";

            /// <summary>
            /// AdminNoticias
            /// </summary>
            public const string ADMIN_NOTICIAS = "AdminNoticias";

            /// <summary>
            /// DetalleNoticia
            /// </summary>
            public const string DETALLE_NOTICIA = "DetalleNoticia";

            /// <summary>
            /// AgregarNoticia
            /// </summary>
            public const string AGREGAR_NOTICIA = "AgregarNoticia";

            /// <summary>
            /// VerNoticia
            /// </summary>
            public const string VER_NOTICIA = "VerNoticia";

            /// <summary>
            /// WikiArbol
            /// </summary>
            public const string WIKI_ARBOL = "WikiArbol";

            /// <summary>
            /// AgregarEspecie
            /// </summary>
            public const string AGREGAR_ESPECIE = "AgregarEspecie";

            /// <summary>
            /// EditarEspecie
            /// </summary>
            public const string EDITAR_ESPECIE = "EditarEspecie";
        }

        public class SeccionTipos
        {
            /// <summary>
            /// Página
            /// </summary>
            public const string PAGINA = "Pagina";

            /// <summary>
            /// Id Página
            /// </summary>
            public const int ID_PAGINA = 1;

            /// <summary>
            /// Componente
            /// </summary>
            public const string COMPONENTE = "Componente";

            /// <summary>
            /// Id Componente
            /// </summary>
            public const int ID_COMPONENTE = 2;
        }

        public class UsuarioPerfiles
        {
            /// <summary>
            /// Común
            /// </summary>
            public const string Comun = "Común";

            /// <summary>
            /// Administrador
            /// </summary>
            public const string Administrador = "Admin";

            /// <summary>
            /// Editor
            /// </summary>
            public const string Editor = "Editor";
        }

        public class UsuarioTipos
        {
            /// <summary>
            /// Individuo
            /// </summary>
            public const string Individuo = "Individuo";

            /// <summary>
            /// Institucion
            /// </summary>
            public const string Institucion = "Institucion";

            /// <summary>
            /// Municipio
            /// </summary>
            public const string Municipio = "Municipio";
        }

        public class LogTipos
        {
            /// <summary>
            /// Error
            /// </summary>
            public const string ERROR = "Error";
        }

        public class SponsorTipos
        {
            /// <summary>
            /// Primario
            /// </summary>
            public const string PRIMARIO = "Primario";

            /// <summary>
            /// Secundario
            /// </summary>
            public const string SECUNDARIO = "Secundario";

            /// <summary>
            /// Terciario
            /// </summary>
            public const string TERCIARIO = "Terciario";
        }

        public class Defaults
        {
            public class Pais
            {
                /// <summary>
                /// ID
                /// </summary>
                public const int ID = 1;

                /// <summary>
                /// ID
                /// </summary>
                public const string NOMBRE = "Argentina";
            }
        }

    }
}
