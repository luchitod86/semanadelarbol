﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace Comun
{
    public class Email
    {
        #region internal classes
        public class EmailSender
        {
            public EmailSender(string name, string address, string password)
            {
                Name = name;
                Address = address;
                Password = password;
            }

            private string _Name;
            public virtual string Name
            {
                get { return _Name; }
                set { _Name = value; }
            }

            private string _Address;
            public virtual string Address
            {
                get { return _Address; }
                set { _Address = value; }
            }

            private string _Password;
            public virtual string Password
            {
                get { return _Password; }
                set { _Password = value; }
            }
        }
        public class EmailServer
        {
            public EmailServer(string smtpAddress, int portNumber, bool enableSSL)
            {
                SMTP = smtpAddress;
                PortNumber = portNumber;
                EnableSSL = enableSSL;
            }

            private string _SMTP;
            public virtual string SMTP
            {
                get { return _SMTP; }
                set { _SMTP = value; }
            }

            private int _PortNumber;
            public virtual int PortNumber
            {
                get { return _PortNumber; }
                set { _PortNumber = value; }
            }

            private bool _EnableSSL;
            public virtual bool EnableSSL
            {
                get { return _EnableSSL; }
                set { _EnableSSL = value; }
            }
        }
        public class EmailMessage
        {
            public EmailMessage(Dictionary<string, string> recipients, string subject, string body, Encoding encoding)
            {
                Recipients = recipients;
                Subject = subject;
                Body = body;
                Encoding = encoding;
            }

            private Dictionary<string, string> _Recipients;
            public virtual Dictionary<string, string> Recipients
            {
                get { return _Recipients; }
                set { _Recipients = value; }
            }

            private string _Subject;
            public virtual string Subject
            {
                get { return _Subject; }
                set { _Subject = value; }
            }

            private string _Body;
            public virtual string Body
            {
                get { return _Body; }
                set { _Body = value; }
            }

            private System.Text.Encoding _Encoding;
            public virtual System.Text.Encoding Encoding
            {
                get { return _Encoding; }
                set { _Encoding = value; }
            }
        }
        #endregion

        public Email()
        {
        }

        /// <summary>
        /// Enviar mail
        /// </summary>
        /// <param name="destinatarios"></param>
        /// <param name="asunto"></param>
        /// <param name="arrCuerpo"></param>
        /// <param name="encoding"></param>
        /// <param name="html"></param>
        public void EnviarMailContacto(string emisor, string direccion, string contraseña, string smtp, int puerto, bool activarSSL, Dictionary<string, string> destinatarios, string asunto, string[] arrCuerpo, Encoding encoding, bool html)
        {
            try 
	        {
                string nombre = arrCuerpo[0];
                string email = arrCuerpo[1];
                string comentarios = arrCuerpo[2];

                StringBuilder   sb = new StringBuilder();
                                sb.Append("<div style='border:0px solid Transparent; outline:none;' >");
                                sb.Append("<fieldset style='-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -khtml-border-radius: 5px; border: 1px solid #A6C9E2; background: #FCFDFD; color: #222; font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;' >");
                                sb.Append("<legend style='border:1px solid #686970; background-color: #959AC2; color: white; font-weight: bold;' >Contacto</legend>");
                                sb.Append("<table border='0' cellpadding='0' cellspacing='0' width='99%' >");
                                sb.Append("<colgroup><col width='100%' /></colgroup>");
                                sb.Append("<tr><td><label>Nombre:</label></td></tr>");
                                sb.Append("<tr><td><input type='text' style='width:98%' readonly='readonly' value='" + nombre + "' /></td></tr>");
                                sb.Append("<tr><td><label>E-mail:</label></td></tr><tr>");
                                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + email + "' /></td></tr>");
                                sb.Append("<tr><td><label>Comentarios:</label></td></tr>");
                                sb.Append("<tr><td><textarea style='width:99%; resize:vertical; font-family: Lucida Grande, Lucida Sans, Arial, sans-serif; font-size: 1.1em;' readonly='readonly' >" + comentarios + "</textarea></td></tr>");
                                sb.Append("</table>");
                                sb.Append("</fieldset>");
                                sb.Append("</div>");
                string cuerpo = sb.ToString();

                SendMail(emisor, direccion, contraseña, smtp, puerto, activarSSL, destinatarios, asunto, cuerpo, encoding, html);
            }
            catch (Exception)
            {
	            throw;
            }
        }

        /// <summary>
        /// Enviar mail de Adopción
        /// </summary>
        /// <param name="destinatarios"></param>
        /// <param name="asunto"></param>
        /// <param name="arrCuerpo"></param>
        /// <param name="encoding"></param>
        /// <param name="html"></param>
        public void EnviarMailAdopcion(string emisor, string direccion, string contraseña, string smtp, int puerto, bool activarSSL, Dictionary<string, string> destinatarios, string asunto, string[] arrCuerpo, Encoding encoding, bool html)
        {
            try 
	        {	        
                string usuario  = arrCuerpo[0];
                string especie  = arrCuerpo[1];
                string cantidad = arrCuerpo[2];
                string email    = arrCuerpo[3];

                StringBuilder   sb = new StringBuilder();
                                sb.Append("<div style='border:0px solid Transparent; outline:none;' >");
                                sb.Append("<fieldset style='-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -khtml-border-radius: 5px; border: 1px solid #A6C9E2; background: #FCFDFD; color: #222; font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;' >");
                                sb.Append("<legend style='border:1px solid #686970; background-color: #959AC2; color: white; font-weight: bold;' >Contacto</legend>");
                                sb.Append("<table border='0' cellpadding='0' cellspacing='0' width='99%' >");
                                sb.Append("<colgroup><col width='100%' /></colgroup>");
                                sb.Append("<tr><td><label>Usuario:</label></td></tr>");
                                sb.Append("<tr><td><input type='text' style='width:98%' readonly='readonly' value='" + usuario + "' /></td></tr>");
                                sb.Append("<tr><td><label>Especie:</label></td></tr>");
                                sb.Append("<tr><td><input type='text' style='width:98%' readonly='readonly' value='" + especie + "' /></td></tr>");
                                sb.Append("<tr><td><label>Cantidad:</label></td></tr>");
                                sb.Append("<tr><td><input type='text' style='width:98%' readonly='readonly' value='" + cantidad + "' /></td></tr>");
                                sb.Append("<tr><td><label>E-mail:</label></td></tr><tr>");
                                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + email + "' /></td></tr>");
                                sb.Append("</table>");
                                sb.Append("</fieldset>");
                                sb.Append("</div>");
                string cuerpo = sb.ToString();

                SendMail(emisor, direccion, contraseña, smtp, puerto, activarSSL, destinatarios, asunto, cuerpo, encoding, html);
            }
            catch (Exception)
            {
	            throw;
            }
        }

        /// <summary>
        /// Enviar mail de recuperacion de contraseña
        /// </summary>
        /// <param name="destinatarios"></param>
        /// <param name="asunto"></param>
        /// <param name="arrCuerpo"></param>
        /// <param name="encoding"></param>
        /// <param name="html"></param>
        public void EnviarMailRecuperacionPassword(string emisor, string direccion, string contraseña, string smtp, int puerto, bool activarSSL, Dictionary<string, string> destinatarios, string asunto, string[] arrCuerpo, Encoding encoding, bool html)
        {
            try 
	        {	        
                string usuario  = arrCuerpo[0];
                string password = arrCuerpo[1];
                string email    = arrCuerpo[2];

                StringBuilder   sb = new StringBuilder();
                                sb.Append("<div style='width:99%; border:0px solid Transparent; outline:none;' >");
                                sb.Append("<fieldset style='-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -khtml-border-radius: 5px; border: 1px solid #A6C9E2; background: #FCFDFD; color: #222; font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;' >");
                                sb.Append("<legend style='border:1px solid #686970; background-color: #959AC2; color: white; font-weight: bold;' >Datos del Usuario</legend>");
                                sb.Append("<table border='0' cellpadding='0' cellspacing='0' width='99%' >");
                                sb.Append("<colgroup><col width='100%' /></colgroup>");
                                sb.Append("<tr><td><label>Usuario:</label></td></tr>");
                                sb.Append("<tr><td><input type='text' style='width:98%' readonly='readonly' value='" + usuario + "' /></td></tr>");
                                sb.Append("<tr><td><label>Contraseña:</label></td></tr><tr>");
                                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + password + "' /></td></tr>");
                                sb.Append("<tr><td><label>E-mail:</label></td></tr><tr>");
                                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + email + "' /></td></tr>");
                                sb.Append("</table>");
                                sb.Append("</fieldset>");
                                sb.Append("</div>");
                string cuerpo = sb.ToString();

                SendMail(emisor, direccion, contraseña, smtp, puerto, activarSSL, destinatarios, asunto, cuerpo, encoding, html);
            }
            catch (Exception)
            {
	            throw;
            }
        }

        /// <summary>
        /// Enviar mail de participación en Actividad
        /// </summary>
        public void EnviarMailParticipacionEnActividad(string emisor, string direccion, string contraseña, string smtp, int puerto, bool activarSSL, Dictionary<string, string> destinatarios, string asunto, string[] arrCuerpo, Encoding encoding, bool html)
        {
            try 
	        {	        
                string usuario  = arrCuerpo[0];
                string telefono = arrCuerpo[1];
                string email    = arrCuerpo[2];

                StringBuilder   sb = new StringBuilder();
                                sb.Append("<div style='width:99%; border:0px solid Transparent; outline:none;' >");
                                sb.Append("<fieldset style='-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -khtml-border-radius: 5px; border: 1px solid #A6C9E2; background: #FCFDFD; color: #222; font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;' >");
                                sb.Append("<legend style='border:1px solid #686970; background-color: #959AC2; color: white; font-weight: bold;' >Datos del Usuario</legend>");
                                sb.Append("<table border='0' cellpadding='0' cellspacing='0' width='99%' >");
                                sb.Append("<colgroup><col width='100%' /></colgroup>");
                                sb.Append("<tr><td><label>Usuario:</label></td></tr>");
                                sb.Append("<tr><td><input type='text' style='width:98%' readonly='readonly' value='" + usuario + "' /></td></tr>");
                                sb.Append("<tr><td><label>Teléfono:</label></td></tr><tr>");
                                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + telefono + "' /></td></tr>");
                                sb.Append("<tr><td><label>E-mail:</label></td></tr><tr>");
                                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + email + "' /></td></tr>");
                                sb.Append("</table>");
                                sb.Append("</fieldset>");
                                sb.Append("</div>");
                string cuerpo = sb.ToString();
               
                SendMail(emisor, direccion, contraseña, smtp, puerto, activarSSL, destinatarios, asunto, cuerpo, encoding, html);
            }
            catch (Exception)
            {
	            throw;
            }
        }

        /// <summary>
        /// Enviar mail de solicitud de país
        /// </summary>
        /// <param name="destinatarios"></param>
        /// <param name="asunto"></param>
        /// <param name="arrCuerpo"></param>
        /// <param name="encoding"></param>
        /// <param name="html"></param>
        public void EnviarMailSolicitudPais(string emisor, string direccion, string contraseña, string smtp, int puerto, bool activarSSL, Dictionary<string, string> destinatarios, string asunto, string[] arrCuerpo, Encoding encoding, bool html)
        {
            try
            {
                string pais = arrCuerpo[0];
                string email = arrCuerpo[1];
                string organizacion = arrCuerpo[2];
                string paginaWeb = arrCuerpo[3];
                string comentarios = arrCuerpo[4];

                StringBuilder sb = new StringBuilder();
                sb.Append("<div style='width:99%; border:0px solid Transparent; outline:none;' >");
                sb.Append("<fieldset style='-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -khtml-border-radius: 5px; border: 1px solid #A6C9E2; background: #FCFDFD; color: #222; font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;' >");
                sb.Append("<legend style='border:1px solid #686970; background-color: #959AC2; color: white; font-weight: bold;' >Solicitud de país</legend>");
                sb.Append("<table border='0' cellpadding='0' cellspacing='0' width='99%' >");
                sb.Append("<colgroup><col width='100%' /></colgroup>");
                sb.Append("<tr><td><label>País:</label></td></tr>");
                sb.Append("<tr><td><input type='text' style='width:98%' readonly='readonly' value='" + pais + "' /></td></tr>");
                sb.Append("<tr><td><label>Email:</label></td></tr><tr>");
                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + email + "' /></td></tr>");
                sb.Append("<tr><td><label>Organización/Institución:</label></td></tr><tr>");
                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + organizacion + "' /></td></tr>");
                sb.Append("<tr><td><label>Página web:</label></td></tr><tr>");
                sb.Append("<td><input type='text' style='width:98%' readonly='readonly' value='" + paginaWeb + "' /></td></tr>");
                sb.Append("<tr><td><label>Comentarios:</label></td></tr>");
                sb.Append("<tr><td><textarea style='width:99%; resize:vertical; font-family: Lucida Grande, Lucida Sans, Arial, sans-serif; font-size: 1.1em;' readonly='readonly' >" + comentarios + "</textarea></td></tr>");
                sb.Append("</table>");
                sb.Append("</fieldset>");
                sb.Append("</div>");
                string cuerpo = sb.ToString();

                SendMail(emisor, direccion, contraseña, smtp, puerto, activarSSL, destinatarios, asunto, cuerpo, encoding, html);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Private Methods

        private void SendMail(string emisor, string direccion, string contraseña, string smtp, int puerto, bool activarSSL, Dictionary<string, string> recipients, string subject, string body, Encoding encoding, bool html)
        {
            if (recipients.Count > 0)
            {
                var sender = new EmailSender(emisor, direccion, contraseña);
                var server = new EmailServer(smtp, puerto, activarSSL);
                //Message = new EmailMessage(null, string.Empty, string.Empty, null);

                MailMessage mail = new MailMessage();
                SmtpClient SMTP = new SmtpClient();

                #region Sender
                mail.From = new MailAddress(sender.Address, sender.Name, encoding);
                #endregion

                #region Server
                SMTP.UseDefaultCredentials = false;
                SMTP.Credentials = new System.Net.NetworkCredential(sender.Address, sender.Password);
                SMTP.Host = server.SMTP;
                SMTP.Port = server.PortNumber;
                SMTP.EnableSsl = server.EnableSSL;
                #endregion

                #region Message
                var message = new EmailMessage(recipients, subject, body, encoding);
                mail.Subject = message.Subject;
                mail.SubjectEncoding = message.Encoding;
                mail.IsBodyHtml = html;
                mail.Body = message.Body;
                foreach (string recipientMail in recipients.Keys)
                {
                    mail.To.Add(new MailAddress(recipients[recipientMail], recipientMail, encoding));
                }
                #endregion

                //ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                SMTP.Send(mail);
            }
        }

        #endregion

    }
}
