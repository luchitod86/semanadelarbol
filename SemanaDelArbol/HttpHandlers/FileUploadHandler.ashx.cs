﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entidades.jSON.Sistema;
using Comun;
using SemanaDelArbol.Pages;

namespace SemanaDelArbol.HttpHandlers
{
    /// <summary>
    /// Summary description for FileUploadHandler
    /// </summary>
    public class FileUploadHandler : IHttpHandler
    {
        //public void ProcessRequest(HttpContext context)
        //{
        //    context.Response.ContentType = "text/plain";
        //    context.Response.Write("Hello World");
        //}
        
        /// <summary>
        /// Processes Http Request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            RespuestaInfo respuestaInfo = new RespuestaInfo();
            try
            {
                if ( context.Request.Files.Count > 0 )
                {
                    for(int i=0; i < context.Request.Files.Count; i++)
                    {
                        #region Process file
                        //HttpPostedFile file = context.Request.Files[i];
                        //strFileName = Path.GetFileName(file.FileName);
                        //string strExtension = Path.GetExtension(file.FileName).ToLower();
                        //switch (strExtension)
                        //{
                        //    case ".gif":
                        //        strMIMEType = "image/gif";
                        //        break;

                        //    case ".jpg":
                        //        strMIMEType = "image/jpeg";
                        //        break;

                        //    case ".png":
                        //        strMIMEType = "image/png";
                        //        break;

                        //    case ".mp3":
                        //        strMIMEType = "audio/mp3";
                        //        break;

                        //    default:
                        //        strMIMEType = string.Empty;
                        //        return;
                        //}
                        #endregion

                        //byte[] fileBytes = new byte[file.InputStream.Length];
                        //file.InputStream.Read(fileBytes, 0, fileBytes.Length);

                        byte[] fileBytes = new byte[context.Request.Files[0].InputStream.Length];
                        context.Request.Files[0].InputStream.Read(fileBytes, 0, fileBytes.Length);

                        respuestaInfo.Mensaje   = "Archivo subido exitosamente.";
                        respuestaInfo.Resultado = true;
                        respuestaInfo.Valor     = Formateador.Base64.ConvertirEnBase64(fileBytes);
                    }
                }
            }
            catch (Exception ex)
            {
                respuestaInfo.Resultado = false;
                respuestaInfo.Mensaje   = ex.Message;
                respuestaInfo.Valor     = null;
            }
            finally
            {
                context.Response.Write(Formateador.jSON.convertToJson(respuestaInfo));
            }
        }

        /// <summary>
        /// Is Reusable?
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
