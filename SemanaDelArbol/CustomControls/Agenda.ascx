﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Agenda.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Agenda" %>

    <!-- Agenda Styles -->
    <style type="text/css">
        #dtpActividades .ui-datepicker-inline
        {
            width: 100%;
            font-size: 14px !important;
        }
        #dtpActividades .ui-datepicker-header
        {
            font-size: 12px !important;
        }
        #listaActividades
        {
            overflow: hidden;
            height: 115px;
            float: left;
            width: 100%;        
        }
    </style>

    <script type="text/javascript" >
        $(document).ready(function ()
        {
            inicializarDialogosAgenda();
            obtenerActividadesDelMes(Helpers.DateTimeHelper.GetCurrentDate().getMonth() + 1);

            $("#btnAgregarNuevaActividad").click(function() { agregarNuevaActividad(); } );

            mostrarActividadesAgendadas = _preferencias.MostrarActividadesAgendadas.Valor;

            actualizarPreferencia("pnlActividades", "chkPanelActividades", _preferencias.MostrarActividadesAgendadas);
        });
        function inicializarDialogosAgenda()
        {
        /*    var actividadPorFechaOptions =
            {
                title: "Actividades agendadas",
                width: _dialogos.ActividadesPorFechaInfo.Width,
                height: _dialogos.ActividadesPorFechaInfo.Height
                /*,
                buttons:
                [
                    { id: "btnConfirmActividadesPorFechaDialog", text: "Aceptar", click: guardarActividadInfo }
                ]
                */
          /*  };
            Helpers.DialogHelper.initializeDialog(_dialogos.ActividadesPorFechaInfo.Id, actividadPorFechaOptions);
            */
            var colaboracionActividadOptions =
            {
                title: "Poniendo en contacto a Organizador y Participante..",
                width: _dialogos.ColaboracionActividad.Width,
                height: _dialogos.ColaboracionActividad.Height
                /*,
                buttons:
                [
                    { id: "btnConfirmActividadesPorFechaDialog", text: "Aceptar", click: guardarActividadInfo }
                ]
                */
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.ColaboracionActividad.Id, colaboracionActividadOptions);
        }
    </script>

    <!-- Diálogos -->
    <script type="text/javascript" >
        function abrirActividadesPorFechaDialog() {
            limpiarDatosActividadesPorFechaDialog();
          //  Helpers.DialogHelper.openDialog(_dialogos.ActividadesPorFechaInfo.Id);
        }
        function cerrarActividadesPorFechaDialog() {
            Helpers.DialogHelper.closeDialog(_dialogos.ActividadesPorFechaInfo.Id);
        }
        function limpiarDatosActividadesPorFechaDialog() {
            
        }
        function abrirColaboracionActividadDialog() {
            limpiarDatosColaboracionActividadDialogo();
            Helpers.DialogHelper.openDialog(_dialogos.ColaboracionActividad.Id);
        }
        function cerrarColaboracionActividadDialog() {
            Helpers.DialogHelper.closeDialog(_dialogos.ColaboracionActividad.Id);
        }
        function limpiarDatosColaboracionActividadDialogo() {
            $("#imgMailOrganizador").attr("src", "../Images/Procesando.gif");
            $("#lblMailOrganizador").val("Enviando email al Organizador..");
            $("#imgMailParticipante").val("src", "../Images/Procesando.gif");
            $("#lblMailParticipante").val("Enviando email al Participante..");
        }
    </script>
    <!-- Fin de [Diálogos] -->

    <!-- Actividades -->
    <script type="text/javascript" >
        function agregarNuevaActividad()
        {
            if (verificarUsuarioLogueado())
                abrirPagina(_secciones.NuevaActividad);
            else
                Helpers.NotificationHelper.showWarning("Antes debe loguearse para realizar esta acción.", "Acceso restringido");
        }
        function obtenerActividadesDelMes(mes)
        {
            var params          = new Object();
                params.idPais   = _filtroActual.Pais.Id;
                params.periodo  = _filtroActual.Periodo;
                params.mes      = mes;

            var jsonParams  = JSON.stringify(params);

            var data        = new Object();
                data.mes    = mes;

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerActividadesDelMes", jsonParams, obtenerActividadesDelMes_Respuesta, data);
        }
        function obtenerActividadesDelMes_Respuesta(data, datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var dias = [];
                var actividades = respuestaInfo.Valor;
                for (var i=0; i < actividades.length; i++)
                {
                    var exists = jQuery.inArray( actividades[i].FechaYHora, dias )
                    if ( exists == -1 )
                    {
                        var day     = actividades[i].FechaYHora.substring(0,2);
                        var month   = actividades[i].FechaYHora.substring(3,5);
                        var year    = actividades[i].FechaYHora.substring(6,10);
                        dias.push( new Date(year, month -1, day) );
                    }
                }

                var _currentMonth   = data.mes;
                var _defaultDate    = new Date(_filtroActual.Periodo, (_currentMonth-1), 1);
                var onBeforeShowDay = overloadFunction(enableDates, dias);
                
                $("#dtpActividades").datepicker( "destroy" );
                var dt = $("#dtpActividades").datepicker
                    ({
                        defaultDate: _defaultDate,
                        beforeShowDay: onBeforeShowDay,
                        onSelect: function(dateText, inst)
                        {
                            var day     = dateText.substring(0,2);
                            var month   = dateText.substring(3,5);
                            var year    = dateText.substring(6,10);
                            var date    = new Date(year, month -1, day);
                            obtenerActividadesPorFecha(date);
                        },
                        onChangeMonthYear: function(year, month, inst)
                        {
                            obtenerActividadesDelMes(month);
                        }
                    });
            }
        }
        function enableDates(dates, currentDate)
        {
            for( var i = 0; i < dates.length; i++ )
            {
                if ( dates[i].getFullYear() == currentDate.getFullYear() && dates[i].getMonth()+1 == currentDate.getMonth()+1 && dates[i].getDate() == currentDate.getDate() )
                    return [true];
            }
            return [false];
        }
        function disableDates(dates, currentDate)
        {
            for( var i = 0; i < dates.length; i++ )
            {
                if ( dates[i].getFullYear() == currentDate.getFullYear() && dates[i].getMonth()+1 == currentDate.getMonth()+1 && dates[i].getDate() == currentDate.getDate() )
                    return [false];
            }
            return [true];
        }
        function obtenerActividadesPorFecha(fecha, inst)
        {

        //    abrirActividadesPorFechaDialog();

        //    Helpers.showLoadingMessage();

            var params = new Object();
            params.fecha    = fecha;
            params.pais     = _filtroActual.Pais.Nombre;
            var jsonParams = JSON.stringify(params);

            var data = new Object();
            data.IdTable = "";

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerActividadesPorFecha", jsonParams, obtenerActividadesPorFecha_Respuesta, data);
        }
        function obtenerActividadesPorFecha_Respuesta(data, datos)
        {
            Helpers.hideLoadingMessage();

            var container = $("#pnlActividadesPorFecha");
                container.empty();

            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var actividades = respuestaInfo.Valor;

                container.append('<div class="' + UI_States.Valid + ' header tooltipLeft" style="font-size: 18px;" ><div class="actividad-dia">' + actividades[0].FechaTexto + '</div><div class="num-eventos">' + actividades.length + ' Evento/s</div></div>')

                var actividadesContainer = '';
                for(var i=0; i < actividades.length; i++)
                {                   

                    var actividadInfo = actividades[i];

                    var onUserClick     = 'javascript:verInfoUsuario(' + actividadInfo.IdUsuario + ')';
                    var onTitleClick    = 'javascript:verDetalleActividad(' + actividadInfo.Id + ')';
                    var onJoinClick     = 'javascript:participarEnActividad(' + actividadInfo.Id + ')';
                    var content = '<div >';
                    
                        content += '<input id="hdnActividadPorFechaInfo_' + i + '" type="hidden" value="' + actividadInfo.Id + '" />';
                        content += '<div class="cuerpo"><div><b style="color: #89CE61;font-weight:bold;">' + i + '.</b> ' + actividadInfo.Descripcion + '</div>';
                        content += '<div><b style="color: #89CE61;font-weight:bold;">Organizador</b>: <a href="' + onUserClick + '">' + actividadInfo.Usuario + '</a></div>';
                        content += '<div><b style="color: #89CE61;font-weight:bold;">Lugar</b>: ' + actividadInfo.Pais + (actividadInfo.Provincia.length > 0 ? ', ' + actividadInfo.Provincia : '') + '</div>';
                        content += '<div><b style="color: #89CE61;font-weight:bold;">Fecha y Hora</b>: ' + actividadInfo.FechaYHora + '</div></div>';
                    if ( actividadInfo.EsPublica ) 
                        content += '<div class="' + UI_States.Highlight + ' btnParticiparEnActividad" >Actividad abierta al público >><span><a href="' + onJoinClick + '" class="tooltipRight" title="Haga click si desea participar como colaborador en esta Actividad" >Participar</a></span></div>';
                    content += '</div><hr/>';

                    actividadesContainer += content;
                }
                container.append('<div class="contenido-actividades">' +actividadesContainer+'</div>');
            }

        //    activarMensajesDeAyuda(".tooltipLeft", "Mensaje de Ayuda", null, "left");
        //    activarMensajesDeAyuda(".tooltipRight", "Mensaje de Ayuda", null, "right");
        }
        function participarEnActividad(idActividad)
        {
            if ( !verificarUsuarioLogueado() )
            {
                Helpers.NotificationHelper.showWarning("Primero tiene que loguearse para realizar esta acción.");
                return;
            }

            $("#hdnActividadPorFechaInfoId").val(idActividad);
            Helpers.NotificationHelper.showConfirm("Se le enviará un email al organizador de la Actividad notificándole de su deseo de colaborar. ¿Está de acuerdo?", "Confirmación", "../Images/Contact_Email.png", confirmarParticipacionEnActividad_Confirmacion, null);
        }
        function confirmarParticipacionEnActividad_Confirmacion()
        {
            var idActividad = $("#hdnActividadPorFechaInfoId").val();
            $("#hdnActividadPorFechaInfoId").val("");

            abrirColaboracionActividadDialog();

            enviarEmailParticipacionEnActividad( idActividad, _usuarioLogueado.Id, "Organizador" );
            enviarEmailParticipacionEnActividad( idActividad, _usuarioLogueado.Id, "Participante" );
        }
        function enviarEmailParticipacionEnActividad(idActividad, idUsuario, rol)
        {
            var params = new Object();
            params.idActividad  = idActividad;
            params.idUsuario    = idUsuario;
            params.rol          = rol;
            var jsonParams = JSON.stringify(params);

            var data = new Object();
            data.rol = rol;
            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/enviarEmailParticipacionEnActividad", jsonParams, enviarEmailParticipacionEnActividad_Respuesta, data);
        }
        function enviarEmailParticipacionEnActividad_Respuesta(data, datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            /*
            if ( respuestaInfo.Resultado )
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje);    
            else
                Helpers.NotificationHelper.showError(respuestaInfo.Mensaje);
            */
            actualizarEstadoEnvioMailParticipacionEnActividad( respuestaInfo.Resultado, respuestaInfo.Mensaje, data.rol );
        }
        function actualizarEstadoEnvioMailParticipacionEnActividad(resultado, mensaje, rol)
        {
            var imgSrc  = "";
            if ( resultado )
                imgSrc  = "../Images/Comun/16x16/Aceptar.png";
            else
                imgSrc  = "../Images/Comun/16x16/Cancelar.png";
            
            $("#imgMail" + rol).attr("src", imgSrc);
            $("#lblMail" + rol).text( mensaje );

            $("#hdnColaboracionActividad_Mail" + rol + "_Enviado").val("1");

            if ( $("#hdnColaboracionActividad_MailOrganizador_Enviado").val() == "1" && $("#hdnColaboracionActividad_MailParticipante_Enviado").val() == "1" )
                $("#btnCerrarDialogoColaboracionActividad").fadeIn().click( function() { cerrarColaboracionActividadDialog(); } );
        }
    </script>
    <!-- Fin de [Actividaes] -->

    <!-- Actividades Rotativas -->
    <script type="text/javascript" >
        function obtenerActividadesRotativas() {
            var params = new Object();
            params.cantidad = 20;
            params.idPais = _filtroActual.Pais.Id;
            params.periodo = _filtroActual.Periodo;
            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerActividadesRotativas", jsonParams, obtenerActividadesRotativas_Respuesta);
        }
        function obtenerActividadesRotativas_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var actividades = respuestaInfo.Valor;
                var lista = new Array();
                for (var i = 0; i < actividades.length; i++) {
                    var actividadInfo = actividades[i];

                    var onUserClick = 'javascript:verInfoUsuario(' + actividadInfo.IdUsuario + ')';
                    var onTitleClick = 'javascript:verDetalleActividad(' + actividadInfo.Id + ')';

                    var content = '';
                    //                        content += '<div>';
                    //                        content += '<table border="0" cellpadding="0" cellspacing="0" width="100%" >';
                    //                        content += '<colgroup><col width="35%" /><col width="65%" /></colgroup>';
                    //                        content += '<tr><td colspan="2"><label><b><a href="' + onTitleClick + '" >' + actividadInfo.Descripcion.substring(0, 25)  + '</a></b></label></td></tr>';
                    //                        content += '<tr><td colspan="2">&nbsp;</td></tr>';
                    //                        content += '<tr><td><label><b>¿Quién?</b></label></td><td><label><a href="' + onUserClick + '">' + actividadInfo.Usuario.substring(0, 25) + '</a></label></td></tr>';
                    //                        content += '<tr><td><label><b>¿Dónde?</b></label></td><td><label>' + actividadInfo.Provincia + '</label></td></tr>';
                    //                        content += '<tr><td><label><b>¿Cuándo?</b></label></td><td><label>' + actividadInfo.FechaYHora + '</label></td></tr>';
                    //                        content += '</div>';
                    content += '<div>';
                    content += '<table border="0" cellpadding="0" cellspacing="0" width="100%" >';
                    content += '<colgroup><col width="100%" /></colgroup>';
                    content += '<tr><td><label class="' + UI_States.Highlight + '" style="float:left;width:98%;" ><a href="' + onTitleClick + '" >' + actividadInfo.Descripcion.substring(0, 25) + '</a></label></td></tr>';
                    content += '<tr><td>&nbsp;</td></tr>';
                    content += '<tr><td><label><a href="' + onUserClick + '">' + actividadInfo.Usuario.substring(0, 25) + '</a></label></td></tr>';
                    content += '<tr><td><label>' + actividadInfo.Pais + ', ' + actividadInfo.Provincia + '</label></td></tr>';
                    content += '<tr><td><label>' + actividadInfo.FechaYHora + '</label></td></tr>';
                    content += '</div>';

                    lista[i] = content;
                }
                if (lista.length > 0) {
                    $("#listaActividades").html(lista[0]);
                    rotarActividades(lista, 1);
                }
                else {
                    $("#actividadesVerListado").hide();
                }
            }
        }
        function rotarActividades(lista, indice) {
            timeout_Actividades = setTimeout(function () {
                $("#listaActividades").html(lista[indice]);

                if (lista.length == indice)
                    indice = 0;
                else
                    indice++;

                rotarActividades(lista, indice);
            }, 5000);
        }
    </script>

    <!-- Fin de [Actividades Rotativas] -->
<div id="agenda-actividades">
    <div id="panel-actividades">       
        <div id="pnlActividades" style="display:none;" >
            <label class="titulo-actividades" >Actividades agendadas</label>
            <div id="dtpActividades" ></div>
            <div id="btnAgregarNuevaActividad" >
                <p  style="float:left;">¡Sumá tu actividad!</p>
                <input type="button" class="button btnAgregar" value="+" style="width:165px" />
            </div>
        </div>

        <!-- Actividades Por Fecha -->
        <div id="pnlActividadesPorFechaInfo">
            <input id="hdnActividadPorFechaInfoId" type="hidden" />
            <div id="pnlActividadesPorFecha" >
            </div>
        </div>
        <!-- Fin de [Actividades Por Fecha] -->    
    </div>

    <!-- Dialogo Actividad Info -->
    <div id="dialogActividadInfo" style="display:none" >
        <input type="hidden" id="hdnActividadInfo_Id" />
        <input type="hidden" id="hdnActividadInfo_Latitud" />
        <input type="hidden" id="hdnActividadInfo_Longitud" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="45%" /> 
                <col width="55%" />                
            </colgroup>
            <tr>
                <td>
                    <label>Usuario:</label>
                </td>
                <td> 
                    <label id="lblActividadInfo_Usuario" ></label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>País:</label>
                    &nbsp;
                    <label id="lblActividadInfoAdvertencia" class="tooltipLeft ui-state-highlight" style="float:right; margin-right:5px; height:14px; cursor:pointer;" title="Asegúrese de seleccionar el país en donde se realiza la Actividad." >*</label>
                </td>
                <td>
                    <select id="selActividadInfo_Pais" ></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Provincia / Departamento / Estado:</label>
                </td>
                <td>
                    <select id="selActividadInfo_Provincia" ></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Fecha:</label>
                </td>
                <td> 
                    <input id="txtActividadInfo_FechaYHora" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Descripción:</label>
                </td>
                <td> 
                    <input id="txtActividadInfo_Descripcion" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Comentarios:</label>
                </td>
                <td style="vertical-align:top;" >
                    <div style="padding:5px 0px 5px 0px" >
                        <textarea id="txtActividadInfo_Comentarios" class="noResize" rows="4" cols="1" ></textarea>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right" >
                    <label for="chkActividadInfo_PermitirColaboracion" >Permitir colaboración de los Usuarios</label>&nbsp;<input id="chkActividadInfo_PermitirColaboracion" type="checkbox" />
                </td>
            </tr>
        </table>
    </div>
    <!-- Fin de [Dialogo Actividad Info] -->

    <!-- Dialogo Actividad Info -->
    <div id="dialogDetalleActividadInfo" style="display:none" >
        <textarea id="txtDetalleActividadInfo_Comentarios" class="noResize" rows="4" cols="1" readonly="readonly" ></textarea>
    </div>
    <!-- Fin de [Dialogo Detalle Actividad Info] -->    

    <!-- Diálogo Colaboración En Actividad -->
    <div id="dialogoColaboracionActividad" style="display:none" >
        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="100%" /> 
            </colgroup>
            <tr>
                <td>
                    <div>
                        <img id="imgMailOrganizador" src="../Images/Procesando.gif" alt="Enviando mail al Organizador" />
                        <label id="lblMailOrganizador" style="vertical-align:middle;" >Enviando email al Donante..</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <img id="imgMailParticipante" src="../Images/Procesando.gif" alt="Enviando mail al Participante" />
                        <label id="lblMailParticipante" style="vertical-align:middle;" >Enviando email al Participante..</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right" >
                    <input type="hidden" id="hdnColaboracionActividad_MailOrganizador_Enviado" />
                    <input type="hidden" id="hdnColaboracionActividad_MailParticipante_Enviado" />
                    <div id="btnCerrarDialogoColaboracionActividad" style="display:none" >
                        <input type="button" class="button btnCerrar" value="Cerrar" />
                    </div>                        
                </td>
            </tr>
        </table>
    </div>
    <!-- Fin de [Diálogo Colaboración En Actividad] -->

</div>