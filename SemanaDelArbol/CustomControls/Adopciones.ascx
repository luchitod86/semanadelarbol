﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Adopciones.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Dialogs.Adopciones" %>

<script type="text/javascript" >
    $(document).ready(function () {
        inicializarDialogAdopcionInfo();
        inicializarDialogAdopcionInfoDatosContacto();
        inicializarDialogDonacionesUsuarioAdopcionesInfo();
    });
</script>

<!-- Adopciones -->
<script type="text/javascript">
    function obtenerAdopcionesUsuario(idUsuario, idTable) {
        var data = new Object();
        data.IdTable = idTable;

        var params = new Object();
        params.idUsuario = idUsuario;
        params.idEspecie = -1;
        params.idPais = _filtroActual.Pais.Id;
        params.periodo = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerAdopcionesUsuario", jsonParams, obtenerAdopcionesUsuario_Response, data);
    }
    function obtenerAdopcionesUsuario_Response(data, datos) {
        var response = jQuery.parseJSON(datos.d);
        if (response.Resultado) {
            var adopcionesInfo = response.Valor;
            if (adopcionesInfo.length > 0) {
                for (var i = 0; i < adopcionesInfo.length; i++) {
                    var adopcionInfo = adopcionesInfo[i];
                    agregarAdopcioInfo(adopcionInfo, data.IdTable);
                }
            }
        }
    }
    function agregarAdopcioInfo(adopcionInfo, idTable) {
        actualizarListaObjetos(adopcionInfo, _tiposLista.Adopciones);

        if (!isEmpty(idTable)) {
            var id = adopcionInfo.Id;
            var row = '';
            row += '<td class="invisible" ><label>' + id + '</label></td>';
            row += '<td class="invisible" ><label>' + adopcionInfo.IdArbol + '</label></td>';
            row += '<td><label>' + adopcionInfo.Arbol + '</label></td>';
            row += '<td class="invisible" ><label>' + adopcionInfo.IdDonante + '</label></td>';
            row += '<td><label>' + adopcionInfo.Donante + '</label></td>';
            row += '<td><label>' + adopcionInfo.Cantidad + '</label></td>';

            $("<tr />")
                .attr("id", "adopcion_" + id)
                .addClass("fila")
                .addClass("rowAdopcionInfo")
                .html(row)
                .click(function ()
                {
                    ////var idMarkerItem = marker.get("id");
                    ////$(".markerItem").removeClass("markerSelected");
                    ////$("#markerItem_" + idMarker).addClass("markerSelected")
                    //if ( verificarUsuarioLogueado() )
                    //{
                    //    if ( adopcionInfo.IdDonante != _usuarioLogueado.Id )
                    //    {
                    //        abrirAdopcionInfoDialog(donacionInfo);
                    //        cerrarInfoDonaciones();
                    //    }
                    //}
                    //else
                    //{
                    //    Helpers.NotificationHelper.showWarning("Antes debe loguearse para realizar esta acción.", "Acceso restringido");
                    //}
                })
                .appendTo("#" + idTable);
        }
    }
</script>

<!-- Adopcion Info -->
<script type="text/javascript" >
    function inicializarDialogAdopcionInfo() {
        var adopcionInfoOptions =
        {
            title: "Realizar una adopción",
            width: _dialogos.AdopcionInfo.Width,
            height: _dialogos.AdopcionInfo.Height,
            buttons:
            [
                { id: "btnConfirmAdopcionInfoDialog", text: "Aceptar", click: confirmarAdopcionInfo }
            ]
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.AdopcionInfo.Id, adopcionInfoOptions);
    }
    function inicializarDialogAdopcionInfoDatosContacto() {
        var adopcionInfoDatosContactoOptions =
        {
            title: "Datos Contacto",
            width: _dialogos.AdopcionInfoDatosContacto.Width,
            height: _dialogos.AdopcionInfoDatosContacto.Height,
            buttons:
            [
                { id: "btnConfirmAdopcionInfoDatosContactoDialog", text: "Aceptar", click: cerrarAdopcionInfoDatosContactoDialog }
            ]
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.AdopcionInfoDatosContacto.Id, adopcionInfoDatosContactoOptions);
    }
    function abrirAdopcionInfoDialog(donacionInfo) {
        limpiarDatosAdopcionInfoDialog();
        if (!isEmpty(donacionInfo))
            cargarDatosAdopcionInfoDialog(donacionInfo);
        Helpers.DialogHelper.openDialog(_dialogos.AdopcionInfo.Id);
    }
    function cargarDatosAdopcionInfoDialog(donacionInfo) {
        $("#hdnDonacionInfoId").val(donacionInfo.Id);
        $("#hdnAdopcionInfoDonanteId").val(donacionInfo.IdDonante);
        $("#hdnAdopcionInfoEspecieId").val(donacionInfo.IdEspecie);

        $("#lblAdopcionInfoEspecie").text(donacionInfo.Especie);
        $("#lblAdopcionInfoDisponibles").text(donacionInfo.Disponibles);
        //$("#txtAdopcionInfoCantidad").val("");
    }
    function cerrarAdopcionInfoDialog() {
        Helpers.DialogHelper.closeDialog(_dialogos.AdopcionInfo.Id);
    }
    function limpiarDatosAdopcionInfoDialog() {
        $("#hdnDonacionInfoId").val("");
        $("#hdnAdopcionInfoDonanteId").val("");
        $("#hdnAdopcionInfoEspecieId").val("");

        $("#lblAdopcionInfoEspecie").text("");
        $("#lblAdopcionInfoDisponibles").text("");
        $("#txtAdopcionInfoCantidad").val("");

        desactivarMensajesDeError("#txtAdopcionInfoCantidad");
    }
    function confirmarAdopcionInfo() {
        var disponibles = parseInt($("#lblAdopcionInfoDisponibles").text());
        var infoValidacion =
            [
                { campo: "txtAdopcionInfoCantidad", validaciones: [{ operacion: _validaciones.Valor.MayorA, valor: 0, mensaje: "Debe ingresar una cantidad superior a cero." }, { operacion: _validaciones.Valor.MenorA, valor: disponibles + 1, mensaje: "La cantidad a adoptar no puede ser mayor a la cantidad disponible." }] }
            ];
        var arrErrores = validarFormulario(infoValidacion);
        if (arrErrores.length > 0)
            return false;

        Helpers.NotificationHelper.showConfirm("Adoptar un árbol es un <b><u>compromiso</u></b>.<br />Al hacerlo, se compromete a regarlo y cuidarlo, al menos durante los 2 primeros años de crecimiento inicial.", "Compromiso de adopción", "../Images/Indicadores/plantaciones.png", confirmarAdopcionInfo_Confirmacion, null);
    }
    function confirmarAdopcionInfo_Confirmacion() {
        guardarAdopcionInfo();
    }
    function guardarAdopcionInfo() {
        cerrarAdopcionInfoDialog();

        var adopcionInfo = new Object();
        //adopcionInfo.Id        = $("#hdnDonacionInfoId").val();
        adopcionInfo.idDonante = $("#hdnAdopcionInfoDonanteId").val();
        adopcionInfo.idUsuario = _usuarioLogueado.Id;
        //adopcionInfo.Usuario  = _usuarioLogueado.NombreUsuario;
        adopcionInfo.idEspecie = $("#hdnAdopcionInfoEspecieId").val(); //Helpers.SelectListHelper.getSelectedValueFromSelectList("selAdopcionInfoEspecie");
        //adopcionInfo.Especie   = $("hdnAdopcionInfoEspecie").val(); //Helpers.SelectListHelper.getSelectedTextFromSelectList("selAdopcionInfoEspecie");
        adopcionInfo.cantidad = $("#txtAdopcionInfoCantidad").val();
        var parametros = JSON.stringify(adopcionInfo);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/realizarAdopcion", parametros, guardarAdopcionInfo_Response);
    }
    function guardarAdopcionInfo_Response(datos) {
        var respuesta = jQuery.parseJSON(datos.d);
        if (respuesta.Resultado) {
            var adopcionInfo = respuesta.Valor;
            generarContenidoSocial_Adopcion(adopcionInfo);
            Helpers.NotificationHelper.showMessage(respuesta.Mensaje, "", "", null, true);
            obtenerDonante(adopcionInfo.IdDonante, abrirAdopcionInfoDatosContactoDialog);
        }
        else {
            Helpers.NotificationHelper.showWarning("La adopción no pudo ser realizada.");
        }
    }
    function abrirAdopcionInfoDatosContactoDialog(donanteInfo) {
        limpiarDatosAdopcionInfoDatosContactoDialog();
        if (!isEmpty(donanteInfo)) {
            var donante = (!isEmpty(donanteInfo.EsIndividuo) ? donanteInfo.Nombre + " " + donanteInfo.Apellido : donanteInfo.Institucion);
            $("#lblContacto_Donante").text(donante);
            $("#lblContacto_Telefono").text(donanteInfo.Telefono);
            $("#lblContacto_Email").text(donanteInfo.Email);
        }
        Helpers.DialogHelper.openDialog(_dialogos.AdopcionInfoDatosContacto.Id);
    }
    function cerrarAdopcionInfoDatosContactoDialog() {
        Helpers.DialogHelper.closeDialog(_dialogos.AdopcionInfoDatosContacto.Id);
    }
    function limpiarDatosAdopcionInfoDatosContactoDialog() {
        $("#lblContacto_Donante").text("");
        $("#lblContacto_Telefono").text("");
        $("#lblContacto_Email").text("");
    }
</script>

<!-- Donaciones Usuario - AdopcionesInfo -->
<script type="text/javascript">
    function inicializarDialogDonacionesUsuarioAdopcionesInfo()
    {
        var donacionesUsuarioAdopcionesOptions =
        {
            title: "Usuarios que realizaron adopciones",
            width: _dialogos.DonacionesUsuario_AdopcionesInfo.Width,
            height: _dialogos.DonacionesUsuario_AdopcionesInfo.Height,
            buttons:
            [
                { id: "btnConfirmDonacionesUsuarioAdopcionesInfoDialog", text: "Aceptar", click: cerrarDonacionesUsuarioAdopcionesInfoDialog }
            ]
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.DonacionesUsuario_AdopcionesInfo.Id, donacionesUsuarioAdopcionesOptions);
    }
    function limpiarDatosDonacionesUsuarioAdopcionesInfoDialog() {
        $("#lblDonacionesUsuario_AdopcionesInfo_Cantidad").text("0");
        $(".rowDonacionAdopcionInfo").remove();
    }
    function abrirDonacionesUsuarioAdopcionesInfoDialog(idDonante, idEspecie) {
        limpiarDatosDonacionesUsuarioAdopcionesInfoDialog();
        Helpers.DialogHelper.openDialog(_dialogos.DonacionesUsuario_AdopcionesInfo.Id);
        obtenerDonacionesUsuarioAdopcionesInfo(idDonante, idEspecie);
    }
    function cerrarDonacionesUsuarioAdopcionesInfoDialog() {
        Helpers.DialogHelper.closeDialog(_dialogos.DonacionesUsuario_AdopcionesInfo.Id);
    }
    function obtenerDonacionesUsuarioAdopcionesInfo(idDonante, idEspecie) {
        var params = new Object();
        params.idDonante = idDonante;
        params.idEspecie = idEspecie;
        params.pais = _filtroActual.Pais.Nombre;
        params.periodo = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerAdopcionesUsuario", jsonParams, obtenerDonacionesUsuarioAdopcionesInfo_Response);
    }
    function obtenerDonacionesUsuarioAdopcionesInfo_Response(datos) {
        var respuestaInfo = jQuery.parseJSON(datos.d);
        if (respuestaInfo.Resultado) {
            var adopcionesInfo = respuestaInfo.Valor;
            for (var i = 0; i < adopcionesInfo.length; i++) {
                var adopcionInfo = adopcionesInfo[i];

                var onUserClick = 'javascript:verInfoUsuario(' + adopcionInfo.IdUsuario + ')';
                var id = adopcionInfo.Id;
                var row = '';
                row += '<td><a href="' + onUserClick + '"><label>' + adopcionInfo.Usuario + '</label></a></td>';
                row += '<td align="center" ><label>' + adopcionInfo.Cantidad + '</label></td>';
                $("<tr />")
                    .attr("id", "donacionAdopcion_" + id)
                    .addClass("fila")
                    .addClass("rowDonacionAdopcionInfo")
                    .html(row)
                    .appendTo("#tblDonacionesUsuarioAdopciones");
            }
            $("#lblDonacionesUsuario_AdopcionesInfo_Cantidad").text(adopcionesInfo.length);
        }
    }
</script>
<!-- Fin de [Donaciones Usuario - AdopcionesInfo] -->

<!-- Dialogo Adopcion Info -->
<div id="dialogAdopcionInfo" style="display:none" >
    <input type="hidden" id="hdnDonacionInfoId" />
    <input type="hidden" id="hdnAdopcionInfoDonanteId" />
    <input type="hidden" id="hdnAdopcionInfoEspecieId" />
    <!-- Donaciones -->
    <div id="pnlAdopcionInfo_Donaciones" >
        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="50%" /> 
                <col width="50%" />                
            </colgroup>
            <tr>
                <td>
                    <label>Especie:</label>
                </td>
                <td>
                    <label id="lblAdopcionInfoEspecie"></label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Disponibles:</label>
                </td>
                <td>
                    <label id="lblAdopcionInfoDisponibles" >0</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Cantidad a adoptar:</label>
                </td>
                <td>
                    <input id="txtAdopcionInfoCantidad" type="text" style="width:50px" value="0" />
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- Fin de [Dialogo Adopcion Info] -->

<!-- Dialogo Adopcion Info [Datos Contacto] -->
<div id="dialogAdopcionInfo_DatosContacto" style="display:none" >
    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
        <colgroup>
            <col width="25%" /> 
            <col width="75%" />                
        </colgroup>
        <tr>
            <td colspan="2" class="ui-state-highlight" >
                <label>¡La adopción fue realizada exitosamente!</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <label>Ahora ponete en contacto con el donante, para coordinar la entrega.</label>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align="center" >
                <label id="lblContacto_Donante" style="font-size:20px; text-decoration:underline" ></label>
            </td>
        </tr>
        <tr>
            <td>
                <img src="../Images/Contact_Phone.png" alt="Teléfono" />
                <label style="vertical-align:middle" >Teléfono:</label>&nbsp;
            </td>
            <td>
                <label id="lblContacto_Telefono" ></label>
            </td>
        </tr>
        <tr>
            <td>
                <img src="../Images/Contact_Email.png" alt="Email" />
                <label style="vertical-align:middle" >E-mail:</label>&nbsp;
            </td>
            <td>
                <label id="lblContacto_Email" ></label>
            </td>
        </tr>
    </table>            
</div>

<!-- Dialogo Donaciones Usuario - Adopciones -->
<div id="dialogDonacionesUsuario_Adopciones" style="display:none" >
    <b><label>Cantidad total:</label></b>&nbsp;<label id="lblDonacionesUsuario_AdopcionesInfo_Cantidad" ></label>
    <br /><br />
    <table id="tblDonacionesUsuarioAdopciones" class="tblInteractiva" border="0" cellpadding="0" cellspacing="0" width="100%" >
        <colgroup>
            <col width="80%" /> 
            <col width="20%" /> 
        </colgroup>
        <tr>
            <td>
                <b><label>Usuario</label></b>
            </td>
            <td>
                <b><label>Cantidad</label></b>
            </td>
        </tr>
    </table>
</div>          
<!-- Fin de [Dialogo Donaciones Usuario - Adopciones] -->