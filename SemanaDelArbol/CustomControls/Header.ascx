﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Header" %>

    <!-- Estilos Header.ascx -->
    <style type="text/css" >
        #fecha_SemanaDelArbol
        {
              font-size: 13px;
              position: absolute;
              z-index: 5;
              margin: -5px 0px 0px 116px;
        }
        .dd-selected
        {
            font-weight: normal;
        }
        .dd-selected-text, .dd-option-text
        {
              line-height: 1.5em!important;
              font-size: 25px;              
              padding-left: 3px;
              cursor: pointer;
              text-transform: uppercase;
              color: #fff;
        }
        .dd-selected-text
        {
            /* font-size: 24px; */
            font-weight: bold;
        }
        .dd-select .dd-selected,.dd-select  .dd-option
        {
            padding: 20px 22px;
        }
        .dd-selected, .dd-option
        {
            padding: 0px 22px;
        }
        
        #listaPaises.dd-container{
              width: 92px!important;
        }
        #listaPaises .dd-select{
              width: 92px!important;
              background: none!important;
              border: none;
        }
        #listaPaises .dd-option-selected{
              background-color: #ccc;
        }
        #listaPaises .dd-selected{
              width: 92px!important;
        }
        #listaPaises .dd-pointer {
            position: absolute;
            left: 40px;
            top: 70px;
        }
        #listaPaises .dd-options 
        {
              width: 92px!important;
              height: Auto!important;
              background-color: #ccc;
        }
         #listaPaises .dd-option-text{
              font-size:10px;
              padding: 10px 2px;
              text-align: center;
        }
        .dd-option-image, .dd-selected-image
        {
            margin-right: 5px;
            margin-top: 9px;
        }       
       
        #listaPaises .dd-selected label
        {        
            display: none;            
        }

        #listaPaises .dd-options .no-value
        {
            display: block!important;   
        }

         #listaPaises .dd-options label
        {
            display: none;   
        }
              
        .socialMediaText
        {
            vertical-align: middle;
            cursor: pointer;
        }
        .socialMediaButton
        {
            vertical-align: middle;            
            position: relative;
            cursor: pointer;
        }  
        
        .imgConectarConRedSocial
        {
            padding: 0px;
        }
        #lnkRecuperarPassword,
        #lnkConectarConFacebook,
        #lnkConectarConTwitter,
        #lnkConectarConGooglePlus
        {
            font-size: 13px;
            font-weight: 100;
            margin-top: 5px;
        }
    </style>

    <!-- document.ready -->
    <script type="text/javascript" >
    
        var dialogId_Actividad = "dialogActividad";
    
        $(document).ready(function()
        {
            localizar_Header();
            enlazarMenuPrincipal();
            enlazarMenuSesion();
            animateHoverButtons();
            
        });
        function localizar_Header()
        {
            var localization = Helpers.LocalizationHelper;

            $("#mnuSemanaDelArbol").html(localization.Header.Menu_Item_SemanaDelArbol);
            $("#mnuParticipar").html(localization.Header.Menu_Item_Participar);
            $("#mnuComoPlantar").html(localization.Header.Menu_Item_ComoPlantar);
            $("#mnuViveros").html(localization.Header.Menu_Item_Viveros);
            $("#mnuMediakit").html(localization.Header.Menu_Item_Mediakit);
            $("#mnuWikiArbol").html(localization.Header.Menu_Item_Wiki_Arbol);
            $("#mnuQuienesSomos").html(localization.Header.Menu_Item_QuienesSomos);
            $("#mnuFotos").html(localization.Header.Menu_Item_Fotos);

            $("#lnkEntrar").html(localization.Header.Sesion_Item_Entrar);
            $("#lblAdministracion").html(localization.Header.Sesion_Item_Admin);
            $("#lblConectado").html(localization.Header.Sesion_Item_Conectado);
            $("#lnkRegistrarse").html(localization.Header.Sesion_Item_Registrarse);
            $("#lnkMisDatos").html(localization.Header.Sesion_Item_MisDatos);

            $("#social_Facebook").html(localization.Header.Social_Facebook);
            $("#social_Twitter").html(localization.Header.Social_Twitter);
            $("#contacto").html(localization.Header.Contacto);
        }
        function enlazarMenuPrincipal()
        {
            $("#pnlMenuPrincipal").click(function () { abrirPagina(_secciones.Default); } );

            $("#mnuSemanaDelArbol").click(function () { abrirPagina(_secciones.SemanaDelArbol); });
            $("#mnuParticipar").click(function () { abrirPagina(_secciones.Participar); });
            $("#mnuComoPlantar").click(function () { abrirPagina(_secciones.ComoPlantarUnArbol); });
            $("#mnuViveros").click(function () { abrirPagina(_secciones.Viveros); });
            $("#mnuMediakit").click(function () {
                var win = window.open('/Archivos/MediaKit_SDA_2016.zip', '_blank');
                  
            });
            $("#mnuWikiArbol").click(function () { abrirPagina(_secciones.WikiArbol); });
            $("#mnuQuienesSomos").click(function () { abrirPagina(_secciones.QuienesSomos); });
            $("#mnuFotos").click(function () { abrirPagina(_secciones.Galeria); });

   /*         var clsDisabled = "disabled";
            $("#pnlTopMenu .item").hover
            (
                function () { $(this).siblings().addClass(clsDisabled); },
                function () { $(this).siblings().removeClass(clsDisabled); }
            );*/

            var facebookUrl = $("#<%=hdnSemanaDelArbol_Facebook.ClientID %>").val();
            var twitterUrl  = $("#<%=hdnSemanaDelArbol_Twitter.ClientID %>").val();
            $("#facebookLink").click(function () { redirectTo(facebookUrl, true, true); });
            $("#twitterLink").click(function () { redirectTo(twitterUrl, true, true); });
        }
        function enlazarMenuSesion()
        {
            $("#lnkEntrar").click( function() { mostrarLoginDialog(); } );
            $("#lnkRegistrarse").click(function () { mostrarRegistrarse(); });
            $("#mnuSesion").click( function() { cerrarSesion(); } );
            $("#mnuMisDatos").click( function() { mostrarMisDatos(); } );
            $("#mnuAdmin").click( function() { mostrarAdministracion(); } );


            $("#lnkConectarConFacebook").click( function() { loginConFacebook(); } );
            //$("#lnkConectarConTwitter").click( function() { loginConTwitter(); } );
            //$("#lnkConectarConGooglePlus").click( function() { loginConGooglePlus(); } );
            $("#lnkConectarConTwitter, #lnkConectarConGooglePlus");

            $("#lnkRecuperarPassword").click( function() { verRecuperarPassword(); } );
        }
    </script>
    
    <!-- Social Media -->
    <script type="text/javascript">
        function animateHoverButtons()
        {
            $(".hoverButton").hover
            (
                function () { $(this).find("img").animate({ top: '-=7' }, "fast", function () { }); },
                function () { $(this).find("img").animate({ top: '+=7' }, "fast", function () { }); }
            );
        }
    </script>

    <!--HTML-->
    <div id="Header" >

        <asp:HiddenField ID="hdnSemanaDelArbol_Facebook" runat="server" />
        <asp:HiddenField ID="hdnSemanaDelArbol_Twitter" runat="server" />

       <nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header pull-left">
            <a id="pnlMenuPrincipal" class="navbar-brand logo" href="#"></a>
        </div>
        <div class="navbar-header pull-right clearfix">
          	  <div class="navbar-brand">
                <div id="pnlListaPaises" class="tooltipLeft navbar-right" title="Seleccione el país que corresponda" >
                    <select id="listaPaises" style="display:none;" ></select>
                </div>
                <div id="pnlSesion" class="navbar-right" >
                        <span id="mnuLogin" class="item hoverButton tooltipBottomLeft" title="Ingresar al sistema" style="display:none;">
                            <img id="imgEntrar" src="../Images/Iconos/Desconectado.png" alt="Entrar" class="link icono16x16" />
                            <label id="lnkEntrar" class="link" ></label>
                        </span>
                        <span id="mnuAdmin" class="item hoverButton tooltipBottomLeft" title="Ingresar al panel de Administración"  style="display:none;">
                           <!--  <img id="imgAdministracion" src="../Images/Iconos/Admin.png" alt="Administración" class="link icono16x16" />-->
                            <label id="lblAdministracion" class="link" ></label>
                        </span>
                        <span id="mnuMisDatos" class="item hoverButton tooltipBottomLeft" title="Editar datos del usuario logueado"  style="display:none;">
                           <!--  <img id="imgMisDatos" src="../Images/Iconos/MisDatos.png" alt="Conectado" class="link icono16x16" />-->
                            <label id="lnkMisDatos" class="link" ></label>
                        </span>
                        <span id="mnuSesion" class="item hoverButton tooltipBottomLeft" title="Desconectarse del sistema"  style="display:none;">
                           <!--  <img id="imgSesionEstado" src="../Images/Iconos/Conectado.png" alt="Salir" class="link icono16x16" />-->
                            <label id="lblConectado" class="link" ></label>
                        </span>
                        <span id="mnuRegistrarse" class="item hoverButton tooltipBottomLeft" title="Registrarse en el sistema"  style="display:none;">
                          <!-- <img id="imgRegistrar" src="../Images/Iconos/Registrar.png" alt="Registrar" class="link icono16x16" />-->
                            <label id="lnkRegistrarse" class="link" ></label>
                        </span>                        
                    </div>   
          	  </div>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
             <ul class="nav navbar-nav">
                 <li title="Infórmese más acerca de la Semana del Árbol"><a id="mnuSemanaDelArbol" href="#">La Campaña</a></li>
                 <li title="Conozca cómo puede participar"><a id="mnuParticipar" href="#">Participar</a></li>
                 <li title="Guía paso-a-paso de cómo realizar una plantación"><a id="mnuComoPlantar" href="#">¿Cómo Plantar?</a></li>
                 <li title="Guía de Viveros"><a id="mnuViveros" href="#">Viveros</a></li>
                 <li title="Mediakit"><a id="mnuMediakit" href="#">Mediakit</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<!--        <table cellpadding="0" border="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="195px" />
                <col width="925px" />
                <col width="230px" />
            </colgroup>
            <tr>
                <td style="vertical-align:top;" >
                    <div id="pnlMenuPrincipal" >
                        <img id="imgEdicion" src="../Images/Header-Logo.png" alt="Semana del Árbol" style="padding-bottom:0px" />
                        <!-- <div id="fecha_SemanaDelArbol" ></div>-->
         <!--           </div>
                </td>
                <td align="center" >
                    <div id="pnlTopMenu" style="width:100%" >
                        <ul >
                            <li id="mnuSemanaDelArbol" class="item" title="Infórmese más acerca de la Semana del Árbol"></li>
                            <li id="mnuParticipar" class="item" title="Conozca cómo puede participar"></li>
                            <li id="mnuComoPlantar" class="item" title="Guía paso-a-paso de cómo realizar una plantación"></li>
                           <!-- <li id="mnuViveros" class="item" title="Listado de viveros adheridos"></li> -->
                           <!--  <li id="mnuWikiArbol" class="item" title="Wiki de árboles nativos"></li> -->
                            <!--<li id="mnuQuienesSomos" class="item" title="Información acerca de nosotros"></li>-->
           <!--                 <li id="mnuFotos" class="item" title="Galería de fotos"></li>
                        </ul>
                    </div>
                </td>  
                <td>                    
                    <div id="pnlSesion" >
                        <span id="mnuLogin" class="item hoverButton tooltipBottomLeft" title="Ingresar al sistema" style="display:none;">
                            <img id="imgEntrar" src="../Images/Iconos/Desconectado.png" alt="Entrar" class="link icono32x32" />
                            <label id="lnkEntrar" class="link" ></label>
                        </span>
                        <span id="mnuAdmin" class="item hoverButton tooltipBottomLeft" title="Ingresar al panel de Administración"  style="display:none;">
                            <img id="imgAdministracion" src="../Images/Iconos/Admin.png" alt="Administración" class="link icono32x32" />
                            <label id="lblAdministracion" class="link" ></label>
                        </span>
                        <span id="mnuMisDatos" class="item hoverButton tooltipBottomLeft" title="Editar datos del usuario logueado"  style="display:none;">
                            <img id="imgMisDatos" src="../Images/Iconos/MisDatos.png" alt="Conectado" class="link icono32x32" />
                            <label id="lnkMisDatos" class="link" ></label>
                        </span>
                        <span id="mnuSesion" class="item hoverButton tooltipBottomLeft" title="Desconectarse del sistema"  style="display:none;">
                            <img id="imgSesionEstado" src="../Images/Iconos/Conectado.png" alt="Salir" class="link icono32x32" />
                            <label id="lblConectado" class="link" ></label>
                        </span>
                        <span id="mnuRegistrarse" class="item hoverButton tooltipBottomLeft" title="Registrarse en el sistema"  style="display:none;">
                            &nbsp;<img id="imgRegistrar" src="../Images/Iconos/Registrar.png" alt="Registrar" class="link icono32x32" />
                            <label id="lnkRegistrarse" class="link" ></label>
                        </span>                        
                    </div>
                    <div id="pnlListaPaises" class="tooltipLeft" title="Seleccione el país que corresponda" >
                        <select id="listaPaises" style="display:none;" ></select>
                    </div>
                </td>
            </tr>
        </table>
        -->
    </div>