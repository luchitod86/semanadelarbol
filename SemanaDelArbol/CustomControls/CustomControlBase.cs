﻿using SemanaDelArbol.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemanaDelArbol.CustomControls
{
    public class CustomControlBase : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var filtroInfo = SessionHelper.ObtenerFiltroActual();
            var key = String.Format("{0}_ID_PAIS_{1}", Comun.Constantes.CacheKeys.SECCION_CONTENIDO, filtroInfo.IdPais);
            Helpers.ContentHelper.EstablecerContenidoSeccion(this, filtroInfo.IdPais);
        }
    }
}