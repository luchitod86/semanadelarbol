﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Donaciones.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Dialogs.Donaciones" %>

<script type="text/javascript" >
    $(document).ready(function () {
        inicializarDialogDonacionesInfo();
    });
</script>

<!-- Donaciones -->
<script type="text/javascript">
    function obtenerDonacionesUsuario(idUsuario, idTable, idDialogAdopciones, onResponse) {
        var data = new Object();
        data.IdTable = idTable;
        data.IdDialog = idDialogAdopciones;
        data.OnResponse = onResponse;

        var params = new Object();
        params.idDonante = idUsuario;
        params.pais = _filtroActual.Pais.Nombre;
        params.periodo = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerDonaciones", jsonParams, obtenerDonacionesUsuario_Response, data);
    }
    function obtenerDonacionesUsuario_Response(data, datos) {
        var response = jQuery.parseJSON(datos.d);
        if (response.Resultado) {
            var donacionesInfo = response.Valor;
            if (donacionesInfo.length > 0) {
                for (var i = 0; i < donacionesInfo.length; i++) {
                    var donacionInfo = donacionesInfo[i];
                    agregarDonacionInfo(donacionInfo, data.IdTable, data.IdDialog);
                }
            }
            if (!isEmpty(data.OnResponse))
                data.OnResponse.call(this, donacionesInfo);
        }
    }
    function agregarDonacionInfo(donacionInfo, idTable, idDialog) {
        actualizarListaObjetos(donacionInfo, _tiposLista.Donaciones);

        var id = donacionInfo.Id;

        if (!isEmpty(idTable)) {
            var row = '';
            row += '<td class="invisible" ><label>' + id + '</label></td>';
            row += '<td class="invisible" ><label>' + donacionInfo.IdEspecie + '</label></td>';
            row += '<td><label>' + donacionInfo.Especie + '</label></td>';
            row += '<td><label>' + donacionInfo.Cantidad + '</label></td>';
            row += '<td><label>' + donacionInfo.Adoptados + '</label></td>';
            row += '<td><label>' + donacionInfo.Disponibles + '</label></td>';
            if (!isEmpty(idDialog)) {
                var onDonanteClick = 'javascript:abrirDonacionesUsuarioAdopcionesInfoDialog(' + donacionInfo.IdDonante + ',' + donacionInfo.IdEspecie + ');';
                row += '<td><a href="' + onDonanteClick + '" >Ver adopciones</a></td>';
            }

            $("<tr />")
                .attr("id", "donacion_" + id)
                .addClass("fila")
                .addClass("rowDonacionInfo")
                .html(row)
                .click(function () {
                    //var idMarkerItem = marker.get("id");
                    //$(".markerItem").removeClass("markerSelected");
                    //$("#markerItem_" + idMarker).addClass("markerSelected")
                    if (verificarUsuarioLogueado()) {
                        if (donacionInfo.IdDonante != _usuarioLogueado.Id) {
                            abrirAdopcionInfoDialog(donacionInfo);
                            cerrarInfoDonaciones();
                        }
                        else {
                            Helpers.NotificationHelper.showWarning("No puede realizar adopciones sobre sus propias donaciones.", "Advertencia");
                        }
                    }
                    else {
                        Helpers.NotificationHelper.showWarning("Antes debe loguearse para realizar esta acción.", "Acceso restringido");
                    }
                })
                .appendTo("#" + idTable);
        }
    }
    function agregarDonanteInfo(donanteInfo, itemNumber) {
        actualizarListaObjetos(donanteInfo, _tiposLista.Donantes);

        var idDonante = donanteInfo.Id;

        var onUserClick = 'javascript:verInfoUsuario(' + idDonante + ')';
        var onInfoClick = 'javascript:verInfoDonaciones(' + idDonante + ')';

        var burbuja = _googleMap.Marcadores[_googleMap.TiposMarcador.Donante].Burbuja;
        var ancho = burbuja.Width;
        var alto = burbuja.Height;

        var content = ''
        content += '<table border="0" cellpadding="0" cellspacing="0" width="100%" >';
        content += '<colgroup><col width="35%" /><col width="65%" /></colgroup>';
        content += '<tr><td><label><b>Donante:</b></label></td><td><label><b><a href="' + onUserClick + '">' + donanteInfo.NombreUsuario + '</a></b></label></td></tr>';
        content += '<tr><td colspan="2">&nbsp;</td></tr>';
        content += '<tr><td colspan="2"><a href="' + onInfoClick + '">Ver sus donaciones</a></td></tr>';
        content += '</table>';

        content = crearContenedorBurbujaMarcador(content, alto, ancho);

        var map = Helpers.GoogleMapHelper.getMap();
        var position = Helpers.GoogleMapHelper.createPosition(donanteInfo.Latitud, donanteInfo.Longitud);
        var title = "Click para más info.";

        var listItemText = "Donante #{0}";
        if (!isEmpty(itemNumber))
            listItemText = "Donante #" + itemNumber;

        var idMarker = donanteInfo.Id;
        Helpers.GoogleMapHelper.addMarker(map, position, idMarker, _googleMap.TiposMarcador.Donante, _googleMap.Marcadores[_googleMap.TiposMarcador.Donante].Icono, title, false, listItemText, content, null, onDonanteInfoWindow_Open);
    }
    function onDonanteInfoWindow_Open(infoWindow, marker) {
        aplicarEstiloBotonesMarcadorBurbuja(marker.id);
    }
</script>

<!-- Info Donaciones -->
<script type="text/javascript" >
    function inicializarDialogDonacionesInfo() {
        var infoDonacionesOptions =
        {
            title: "",
            width: _dialogos.InfoDonaciones.Width,
            height: _dialogos.InfoDonaciones.Height,
            buttons: [{ id: "btnAceptar", text: "Aceptar", click: cerrarInfoDonaciones }]
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.InfoDonaciones.Id, infoDonacionesOptions);
    }
    function verInfoDonaciones(idDonante) {
        var params = new Object();
        params.idDonante = idDonante;
        params.pais = _filtroActual.Pais.Nombre;
        params.periodo = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerDonaciones", jsonParams, verInfoDonaciones_Response);
    }
    function verInfoDonaciones_Response(datos) {
        var respuestaInfo = jQuery.parseJSON(datos.d);
        if (respuestaInfo.Resultado) {
            //var donante = "";
            var cantidadTotal = 0;
            limpiarDatosDonaciones();
            var donacionesInfo = respuestaInfo.Valor;
            if (donacionesInfo.length > 0) {
                for (var i = 0; i < donacionesInfo.length; i++) {
                    var donacionInfo = donacionesInfo[i];
                    cantidadTotal = cantidadTotal + donacionInfo.Cantidad;
                    agregarDonacionInfo(donacionInfo, "tblDonacionesInfo", null);
                }
                //if ( donacionesInfo.length > 0 )
                //    donante = donacionesInfo[0].Donante;
            }
            $("#lblDonante_CantidadDonaciones").text(cantidadTotal);
            Helpers.DialogHelper.openDialog(_dialogos.InfoDonaciones.Id, "");
        }
    }
    function limpiarDatosDonaciones() {
        $("#lblDonante_CantidadDonaciones").text("0");
        $(".rowDonacionInfo").remove();
    }
    function cerrarInfoDonaciones() {
        Helpers.DialogHelper.closeDialog(_dialogos.InfoDonaciones.Id);
    }
</script>


<!-- Dialogo Info Donaciones -->
<div id="dialogInfoDonaciones" style="display:none" >
    <label>Cantidad total:</label>&nbsp;<label id="lblDonante_CantidadDonaciones" ></label> 
    <br /><br />
    <fieldset class="ui-widget ui-widget-content" >
        <legend class="ui-widget-header" align="center" >Donaciones</legend>
        <table id="tblDonacionesInfo" class="tblInteractiva" border="0" cellspacing="0" cellpadding="0" width="100%" >
            <colgroup>
                <col width="25%" />
                <col width="25%" />               
                <col width="25%" />
                <col width="25%" />
            </colgroup>
            <thead>
                <tr class="rowHeader" >
                    <th class="invisible" >
                        <label>Id</label>
                    </th>
                </tr>
                <tr>
                    <th class="invisible" >
                        <label>IdEspecie</label>
                    </th>
                    <th>
                        <label>Especie</label>
                    </th>
                    <th>
                        <label>Cantidad</label>
                    </th>
                    <th>
                        <label>Adoptados</label>
                    </th>
                    <th>
                        <label>Disponibles</label>
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </fieldset>
</div>
<!-- Fin de [Dialogo Info Donaciones] -->