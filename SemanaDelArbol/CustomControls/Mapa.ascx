﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Mapa.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Mapa" %>

    <style type="text/css">
        #pnlMapa, #pnlMapa_Buscador
        {
            display: none;
            margin: 0px;
        }
        #pnlMapa
        {
            float: left;
            width: 100%;
            overflow: hidden;
            position: relative;
        }
        #pnlMapa fieldset{
              margin: 0px;
              border: none;
              padding: 0px;
              border-radius: 0px;
        }        
        #mapa
        {
            display: block;
            width: 100%;
            height: 336px;
        }

        /* Panel Flotante */
        .mapaPanelFlotante
        {
            position: absolute;
            padding: 5px;
            box-shadow: 1px 1px 3px #3d3b3d;
            z-index: 99;
        }
        .mapaPanelFlotante.progresoCargaMarcadores
        {
            left: 90px;
            top: 50px;
            background-color: white;
            border: 1px solid #ccc;
        }
        .mapaPanelFlotante.listaMarcadores
        {
            width: 160px;
            height: 150px;
            right: 25px;
            top: 225px;
            background-color: #8DF7C1;
            border: 2px solid #63C794;
            overflow-y: scroll;          
        }

        #mapaFiltroMarcadores.filtroMarcadores
        {
            width: 160px;
            right: 20px;
            top: 20px;
            background-color: rgba(52, 117, 87, .5);
            border: 2px solid #63C794;            
        }
        #mapaFiltroMarcadores.filtroMarcadores input[type="checkbox"],
        #mapaFiltroMarcadores.filtroMarcadores label
        {
            cursor: pointer;
            font-weight: lighter;
            color: #fff;
        }
        #mapaFiltroMarcadores.filtroMarcadores .opcion
        {
            border: 1px solid transparent;
            cursor: pointer;
            font-size: 12px;
        }
        /* Fin de [Panel Flotante] */
        
        /* tblUbicacionMapa */
        #tblUbicacionMapa tr td label
        {
            /* padding-left: 10px; */
        }
        #tblUbicacionMapa tr td input[type="text"]
        {
            width: 96%;
        }
        /* Fin de [tblUbicacionMapa] */

        #trBuscador_CamposPrimarios,
        #trBuscador_CamposSecundarios
        {
            line-height: 40px;
        }

        /* Marcadores */
        .markersList 
        {
            list-style: none;
            padding: 0px;
            margin: 0px;
        } 
        .markersList li
        {
            padding: 5px;
        } 
        .markersList li:hover
        {
            background-color: #757171;
            color: #fff;
            cursor: pointer;
        }
        .markerItem
        {
        }
        .markerItem.plantacion
        {
            background-color: #ACFA70;
        }
        .markerItem.donante
        {
            background-color: #D8FC6D;
        }
        .markerItem.actividad
        {
            background-color: #9FE874;    
        }
        .markerSelected
        {
            background-color: #757171;
            text-decoration: underline;
            cursor: pointer;
        }
        /* Fin de [Marcadores] */
    </style>

    <!-- Google Maps  -->
    <script type="text/javascript"  src="http://maps.googleapis.com/maps/api/js?v=3.10&sensor=false" ></script>

    <script type="text/javascript" >
        $(document).ready(function ()
        {
            cargarListasGeograficas("selPaises", "selProvincias", _filtroActual.Pais.Id );
        });
        function inicializarMapa(config)
        {
            if (config.MostrarMapa)
                $("#pnlMapa").fadeIn("slow");

            if (config.MostrarBuscador)
                $("#pnlMapa_Buscador").fadeIn("slow");

            if (config.MostrarFiltroMarcadores)
            {
                $("#mapaFiltroMarcadores").fadeIn("slow");
                $("#mapaFiltroMarcadores.filtroMarcadores .opcion").hover(function () { $(this).addClass(UI_States.Highlight); }, function () { $(this).removeClass(UI_States.Highlight); });
                var chkMostrar = $("#chkMostrarPlantaciones, #chkMostrarDonantes, #chkMostrarActividades");
                    chkMostrar.click(function () { mostrarMarcadores($(this).attr("id")); });
            }

            if (config.MostrarListaMarcadores)
                $("#mapaListaMarcadores").fadeIn("slow");

            if (config.MostrarBotonMiPosicionActual)
            {
                $("#pnlMiPosicionActual").fadeIn("slow");
                $("#btnMiPosicionActual").click(function () { geolocalizarPosicionActual(); } );
            }

            if (!isEmpty(config.TituloBuscador))
            {
                var leyendaBuscador = $("#leyendaBuscador");
                    leyendaBuscador.html(config.TituloBuscador);
                    leyendaBuscador.fadeIn("slow");
            }

            var datos                           = new Object();
                datos.IdMapa                    = config.IdMapa;
                datos.OnMapLoadComplete         = overloadFunction(onMapLoadComplete, config.OnMapLoadComplete);
                datos.Zoom                      = config.Zoom;
                datos.Direccion                 = config.Direccion;
                datos.ImagenMarcador            = config.ImagenMarcador;
                datos.ListaMarcadores           = "markersList";
                datos.DeshabilitarMarcadores    = config.DeshabilitarMarcadores;
                datos.OnClickMap                = config.OnClickMap;
                //datos.OnBuscarDireccion         = config.OnBuscarDireccion;

            Helpers.GoogleMapHelper.getPositionFromAddress(datos.Direccion, overloadFunction(onObtenerPosicionPais, datos), datos.IdMapa);

            $("#btnBuscarDireccion").click
            (
                function()
                {
                    //Helpers.GoogleMapHelper.removeAllMarkers(null, "DoNotRemove");
                    if (!isEmpty(config.OnBuscarDireccion))
                        config.OnBuscarDireccion.call(this);
                    buscarDireccionEnMapa();
                }
            );
        }
        function onMapLoadComplete(onMapLoadCompleteCallback, idMapa)
        {
            if (!isEmpty(onMapLoadCompleteCallback))
                onMapLoadCompleteCallback.call(this, idMapa);
        }
        function onObtenerPosicionPais(datos, position, status)
        {
            switch ( status )
            {
                case google.maps.GeocoderStatus.OK:
                    Helpers.GoogleMapHelper.initialize(datos.IdMapa, datos.OnMapLoadComplete, datos.Zoom, position.lat(), position.lng(), datos.ImagenMarcador, datos.ListaMarcadores, datos.DeshabilitarMarcadores, datos.OnClickMap);
                    break;
                case google.maps.GeocoderStatus.ZERO_RESULTS:
                    //Helpers.NotificationHelper.showError("Ocurrió un error al ubicar la posición en el mapa." + _BreakLine + "Código de retorno: " + status);
                    break;
                case google.maps.GeocoderStatus.OVER_QUERY_LIMIT:
                    //Helpers.NotificationHelper.showError("Ocurrió un error al ubicar la posición en el mapa." + _BreakLine + "Código de retorno: " + status);
                    break;
                case google.maps.GeocoderStatus.REQUEST_DENIED:
                    //Helpers.NotificationHelper.showError("Ocurrió un error al ubicar la posición en el mapa." + _BreakLine + "Código de retorno: " + status);
                    break;
                case google.maps.GeocoderStatus.INVALID_REQUEST:
                    //Helpers.NotificationHelper.showError("Ocurrió un error al ubicar la posición en el mapa." + _BreakLine + "Código de retorno: " + status);
                    break;
            }
        }
        function buscarDireccionEnMapa()
        {
            var pais        = Helpers.SelectListHelper.getSelectedTextFromSelectList("selPaises");
            var provincia   = Helpers.SelectListHelper.getSelectedTextFromSelectList("selProvincias");
            var ciudad      = $("#txtMapa_Buscador_Ciudad").getValue();
            var calle       = $("#txtMapa_Buscador_Calle").getValue();
            var altura      = $("#txtMapa_Buscador_Numero").getValue();
            geolocalizar(pais, provincia, ciudad, calle, altura);
        }
        function geolocalizar(pais, provincia, ciudad, calle, altura)
        {
            /*
                Niveles de zoom [1 al 19] tal cual Google: { Pais = 4, Provincia = 6, Ciudad = 11, Calle = 15, NroCalle = 16 } 
            */
            var direccion = "";
            if (!isEmpty(pais)) {
                direccion += pais;
                zoomLevel = 4;
            }
            if (!isEmpty(provincia)) {
                direccion += ", " + provincia;
                zoomLevel = 6;
            }
            if (!isEmpty(ciudad)) {
                direccion += ", " + ciudad;
                zoomLevel = 12;
            }
            if (!isEmpty(calle)) {
                direccion += ", " + calle;
                zoomLevel = 15;
            }
            if (!isEmpty(altura)) {
                direccion += " " + altura;
                zoomLevel = 17;
            }
            Helpers.GoogleMapHelper.getPositionFromAddress(direccion, geolocalizar_Response);
        }
        function geolocalizar_Response(position, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                var map = Helpers.GoogleMapHelper.getMap();
                Helpers.GoogleMapHelper.setCurrentLocation(map, position, zoomLevel);
            }
            else
            {
                var excepcion = crearObjetoException("Ocurrió un error al ubicar la posición en el mapa.<br />Detalle del error: " + status, "Helpers.GoogleMapHelper.getPositionFromAddress", null);
                //Helpers.NotificationHelper.showError(excepcion, "Error");
            }
        }
        function geolocalizarPosicionActual()
        {
            if (navigator.geolocation)
                navigator.geolocation.getCurrentPosition(geolocalizarPosicionActual_Response);
            else
                Helpers.NotificationHelper.showWarning("Lo sentimos, su navegador no tiene esta funcionalidad.");

            function geolocalizarPosicionActual_Response(position)
            {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                var position = Helpers.GoogleMapHelper.createPosition(lat, lng);
                var map = Helpers.GoogleMapHelper.getMap();
                var zoom = 17; // _googleMap.NivelZoom;
                Helpers.GoogleMapHelper.setCurrentLocation(map, position, zoom);
            }
        }
        function crearContenedorBurbujaMarcador(contenido, alto, ancho)
        {
            var divStyle = String.format("width:{0}px; min-width:{0}px; height:{1}px; min-height:{1}px", ancho, alto);
            return '<div style="' + divStyle + '" />' + contenido + '</div>';
        }
        function aplicarEstiloBotonesMarcadorBurbuja(idMarker)
        {
            var btnId = String.format("_{0}", idMarker);
            var btnEditar = "btnEditar" + btnId;
            var btnEliminar = "btnEliminar_" + btnId;

            //$("#{0}, #{1}", btnEditar, btnEliminar).button(); <-- ¡No se por qué no funciona esto!
            $("input[type=button]").button();
        }
        function parseAddressComponents(address_components)
        {
            var ac = address_components;
            var direccion =
                {
                    pais: !isEmpty(ac.country) ? ac.country.removeAccents() : null,
                    provincia: !isEmpty(ac.administrative_area_level_1) ? ac.administrative_area_level_1.removeAccents() : null,
                    ciudad: !isEmpty(ac.locality) ? ac.locality.removeAccents() : null,
                    barrio: !isEmpty(ac.neighborhood) ? ac.neighborhood.removeAccents() : null,
                    calle: !isEmpty(ac.street_address) ? ac.street_address.removeAccents() : null,
                    ruta: !isEmpty(ac.route) ? ac.route.removeAccents() : null,
                    altura: !isEmpty(ac.street_number) ? ac.street_number.removeAccents() : null
                };
            if (!isEmpty(direccion.provincia))
            {
                var divisionesPoliticas = ["Departamento", "Estado", "Región", "Condado"];
                for (i = 0; i < divisionesPoliticas.length; i++)
                {
                    direccion.provincia = direccion.provincia.replace(String.format("{0} de ", divisionesPoliticas[i]), "");
                }
            }
            return direccion;
        }
    </script>

    <!-- Buscador -->
    <div id="pnlMapa_Buscador" >
        <fieldset id="contenedorBuscador" class="ui-widget ui-widget-content ui-corner-all" >
            <legend id="leyendaBuscador" class="ui-widget-header" align="center" style="display:none" ></legend>
            <table id="tblUbicacionMapa" border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="12%" />
                    <col width="23%" />
                    <col width="1%" />
                    <col width="9%" />
                    <col width="6%" />
                    <col width="10%" />
                    <col width="1%" />
                    <col width="7%" />
                    <col width="10%"  />
                    <col width="11%" />
                    <col width="1%" />
                    <col width="11%" />
                </colgroup>
                <tr id="trBuscador_CamposPrimarios" >
                    <td>
                        <label>País:</label>
                    </td>
                    <td>
                        <select id="selPaises" ></select>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3" >
                        <label>Provincia / Departamento / Estado:</label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3" >
                        <select id="selProvincias" ></select>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr id="trBuscador_CamposSecundarios" >
                    <td>
                        <label>Ciudad / Pueblo:</label>
                    </td>
                    <td>
                        <input type="text" id="txtMapa_Buscador_Ciudad" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <label>Calle / Ruta:</label>
                    </td>
                    <td colspan="2" >
                        <input type="text" id="txtMapa_Buscador_Calle" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <label>Número:</label>
                    </td>
                    <td>
                        <input type="text" id="txtMapa_Buscador_Numero" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td align="right">
                        <input id="btnBuscarDireccion" type="button" class="button btnMapear" value="Buscar" style="width:100px" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <!-- Fin de [Buscador] -->

    <!-- Filtro Marcadores -->
    <script type="text/javascript">
        function mostrarMarcadores(idCheckbox)
        {
            var tipoMarcador = null;

            var plantacion  = _googleMap.TiposMarcador.Plantacion.toLowerCase();
            var donante     = _googleMap.TiposMarcador.Donante.toLowerCase();
            var actividad   = _googleMap.TiposMarcador.Actividad.toLowerCase();

            var idChk = idCheckbox.toLowerCase();
            if (idChk.indexOf(plantacion) > 0)
                tipoMarcador = plantacion;
            if (idChk.indexOf(donante) > 0)
                tipoMarcador = donante;
            if (idChk.indexOf(actividad) > 0)
                tipoMarcador = actividad;

            var visible = $("#{0}".format(idCheckbox)).getValue();
            Helpers.GoogleMapHelper.setMarkersVisibility(tipoMarcador, visible);
        }
    </script> 
    <!-- Fin de [Filtro Marcadores] -->

    <!-- Mapa -->
    <div id="pnlMapa" >
        <div>
            <span id="pnlMiPosicionActual" style="display:none;position:relative; top:45px; left:15px;" >
                <input id="btnMiPosicionActual" type="button" class="button btnPosicionActual" value="Mi posición actual" style="width:160px; z-index: 99;" >
            </span>
            <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <colgroup>
                        <col width="100%" />
                    </colgroup>
                    <tr>
                        <td>
                            <div id="mapa" ></div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="mapaEstadoCargas" class="mapaPanelFlotante progresoCargaMarcadores" style="display:none" >
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="100%" />
                </colgroup>
                <tr>
                    <td>
                        <div id="pnlCargandoPlantaciones" class="opcion" >
                            <label id="lblCargandoPlantaciones">Cargando Plantaciones..</label>&nbsp;
                            <img src="../Images/Comun/16x16/Procesando.gif" alt="Cargando Plantaciones.." style="vertical-align: middle;" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="pnlCargandoDonantes" class="opcion" >
                            <label id="lblCargandoDonantes">Cargando Donantes..</label>&nbsp;
                            <img src="../Images/Comun/16x16/Procesando.gif" alt="Cargando Donantes.." style="vertical-align: middle;" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="pnlCargandoActividades" class="opcion" >
                            <label id="lblCargandoActividades">Cargando Actividades..</label>&nbsp;
                            <img src="../Images/Comun/16x16/Procesando.gif" alt="Cargando Actividades.." style="vertical-align: middle;" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="mapaFiltroMarcadores" class="mapaPanelFlotante filtroMarcadores" style="display:none" >
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="100%" />
                </colgroup>
                <tr>
                    <td>
                        <div class="opcion" >
                            <input type="checkbox" id="chkMostrarPlantaciones" class="btn" checked="checked" />
                            <label for="chkMostrarPlantaciones" >Mostrar plantaciones</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="opcion" >
                            <input type="checkbox" id="chkMostrarDonantes" class="btn" checked="checked" />
                            <label for="chkMostrarDonantes" >Mostrar donantes</label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="opcion" >
                            <input type="checkbox" id="chkMostrarActividades" class="btn" checked="checked" />
                            <label for="chkMostrarActividades" >Mostrar actividades</label>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div id="mapaListaMarcadores" class="mapaPanelFlotante listaMarcadores" style="display:none" >
            <ul id="markersList" class="markersList" ></ul>
        </div>
    </div>
    <!-- Fin de [Mapa] -->   