﻿using Controladores;
using Entidades;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SemanaDelArbol.CustomControls
{
    public partial class Header : CustomControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            this.ObtenerConfiguracion();
        }

        private void ObtenerConfiguracion()
        {
            var configuracion = Helpers.ConfigurationHelper.ObtenerConfiguracion();
            if (configuracion != null)
            {
                if (configuracion.Social.Facebook != null)
                {
                    this.hdnSemanaDelArbol_Facebook.Value = configuracion.Social.Facebook;
                    this.hdnSemanaDelArbol_Twitter.Value = configuracion.Social.Twitter;
                }
            }
        }


    }
}