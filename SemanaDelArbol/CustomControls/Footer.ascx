﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Footer" %>
    <!-- document.ready -->
    <script type="text/javascript" >
        $(document).ready(function()
        {
            enlazarAcciones();            

            var lnkVerSponsors = $("#lnkVerSponsors");
                lnkVerSponsors.hover
                (
                    function () { $(this).addClass("footerTextoActivo"); },
                    function () { $(this).removeClass("footerTextoActivo"); }
                );
                lnkVerSponsors.click
                (
                    function () { abrirPagina(_secciones.Entidades); }
                );

                var lnkVerNoticias = $("#lnkVerNoticias");
                lnkVerNoticias.hover
                (
                    function () { $(this).addClass("footerTextoActivo"); },
                    function () { $(this).removeClass("footerTextoActivo"); }
                );
                lnkVerNoticias.click
                (
                    function () { abrirPagina(_secciones.Noticias); }
                );
        });
        function enlazarAcciones() {

            //$("#lnkInicio").click(function (e) { abrirPagina(_secciones.Default); });
            $("#lnkAcercaDe").click(function (e) { abrirPagina(_secciones.SemanaDelArbol); });
            $("#lnkQuienesSomos").click(function (e) { abrirPagina(_secciones.QuienesSomos); });
            $("#lnkContacto").click(function (e) { abrirPagina(_secciones.Contacto); });
            //$("#lnkWikiArbol").click(function (e) { abrirPagina(_secciones.WikiArbol); });
            $("#lnkNoticias").click(function (e) { abrirPagina(_secciones.Noticias); });
            
            $("#lnkComoParticipar").click(function (e) { abrirPagina(_secciones.Participar); });
            //$("#lnkGaleria").click(function (e) { abrirPagina(_secciones.Galeria); });
            $("#lnkViveros").click(function (e) { abrirPagina(_secciones.Viveros); });
            $("#lnkNoticias").click(function (e) { abrirPagina(_secciones.Noticias); });
            $("#lnkMediakit").click(function (e) { var win = window.open('/Archivos/MediaKit_SDA_2016.zip', '_blank'); });
            //$("#lnkPlantar").click(function (e) { e.preventDefault(); realizarAccion(_secciones.Plantar); });
            //$("#lnkAdoptar").click(function (e) { e.preventDefault(); realizarAccion(_secciones.Adoptar); });
            //$("#lnkDonar").click(function (e) { e.preventDefault(); realizarAccion(_secciones.Donar); });
        }
    </script> 
    
    <!-- Aritmica -->
    <div id="aritmica" >
        <table cellpadding="0" border="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="10%"/>
                <col width="15%"/>
                <col width="15%"/>
                <col width="30%"/>
                <col width="15%"/>
                <col width="15%" />
            </colgroup>
            <tr>
                <td>
                    <div >
                        <img style="width: 200px; margin-top: -25px; margin-left: -20px; padding: 0;" src="../Images/SdA-Logo.png">
                    </div>
                </td>
                <td style="vertical-align:top;">
                    <ul class="list-links">
                        <li><a id="lnkAcercaDe" >Acerca De</a></li>
                        <li><a id="lnkQuienesSomos" >¿Quiénes Somos?</a></li>
                        <li><a id="lnkContacto" >Contacto</a></li>                        
                    </ul>
                </td>
                <td style="vertical-align:top;">
                   <ul class="list-links">
                        <li><a id="lnkComoParticipar">¿Cómo Participar?</a></li>
                        <li><a id="lnkViveros">Viveros</a></li>
                        <li><a id="lnkNoticias" >Noticias</a></li>
                        <li><a id="lnkMediakit">Mediakit</a></li>
                    </ul>
                </td>                
                <td style="vertical-align:top;" >
                    <div>Seguinos en:</div> 
                    <div class="link-red-social">                        
                       <a href="https://www.facebook.com/Semanadelarbol"><img src="../Images/SocialButton-FB.png" ></a>
                       
                    </div>   
                    <div class="link-red-social">
                       <a  href="https://twitter.com/Semanadelarbol"><img src="../Images/SocialButton-Twitter.png"></a>
                    </div>                                       
                </td>
                <td >                    
                </td>
                <td>                                   
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <div class="copyright">Sigamos plantando el cambio | © Semana del Arbol. Derechos Reservados.</div>
                </td>
            </tr>
        </table>
    </div>
    <!-- Fin de [Aritmica] -->