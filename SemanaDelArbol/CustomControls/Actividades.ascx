﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Actividades.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Dialogs.Actividades" %>

<script type="text/javascript" >
    $(document).ready(function ()
    {
        inicializarDialogoActividadInfo();
        inicializarDialogoDetalleActividadInfo();
    });
</script>

<!-- ActividadInfo Dialog -->
<script type="text/javascript" >
    function inicializarDialogoActividadInfo() {
        inicializarDateTimePicker("#txtActividadInfo_FechaYHora");

        var actividadInfoOptions =
        {
            title: "Datos de la actividad",
            width: _dialogos.ActividadInfo.Width,
            height: _dialogos.ActividadInfo.Height,
            buttons:
            [
                { id: "btnCancelActividadInfoDialog", text: "Cancelar", click: cerrarActividadInfoDialog },
                { id: "btnConfirmActividadInfoDialog", text: "Aceptar", click: guardarActividadInfo }
            ]
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.ActividadInfo.Id, actividadInfoOptions);
    }
    function abrirActividadInfoDialog() {
        Helpers.DialogHelper.openDialog(_dialogos.ActividadInfo.Id);
    }
    function cerrarActividadInfoDialog() {
        Helpers.DialogHelper.closeDialog(_dialogos.ActividadInfo.Id);
    }
    function limpiarDatosActividadInfoDialog() {
        $("#hdnActividadInfo_Id").val("");
        $("#hdnActividadInfo_Latitud").val("");
        $("#hdnActividadInfo_Longitud").val("");
        $("#lblActividadInfo_Usuario").text("");
        $("#txtActividadInfo_FechaYHora").val("");
        $("#txtActividadInfo_Descripcion").val("");
        $("#txtActividadInfo_Comentarios").val("");
        $("#chkActividadInfo_PermitirColaboracion").setValue(false);
        //Helpers.SelectListHelper.unSelectAll("selActividadInfo_Pais");
        //Helpers.SelectListHelper.unSelectAll("selActividadInfo_Provincia");
        $("#selActividadInfo_Pais").setValue(null);
        $("#selActividadInfo_Provincia").setValue(null);
        desactivarMensajesDeError("#selActividadInfo_Pais, #selActividadInfo_Provincia, #txtActividadInfo_FechaYHora, #txtActividadInfo_Descripcion, #txtActividadInfo_Comentarios");
    }
    function inicializarDialogoDetalleActividadInfo()
    {
        var detalleActividadInfoOptions =
        {
            title: "Detalle de la actividad",
            width: _dialogos.DetalleActividadInfo.Width,
            height: _dialogos.DetalleActividadInfo.Height
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.DetalleActividadInfo.Id, detalleActividadInfoOptions);
    }
    function abrirDetalleActividadInfoDialog()
    {
        Helpers.DialogHelper.openDialog(_dialogos.DetalleActividadInfo.Id);
    }
    function cerrarDetalleActividadInfoDialog()
    {
        Helpers.DialogHelper.closeDialog(_dialogos.DetalleActividadInfo.Id);
    }
    function limpiarDatosDetalleActividadInfoDialog()
    {
        $("#txtDetalleActividadInfo_Comentarios").setValue(false);
    }
    function verEditarActividad(idActividad, readOnly)
    {
        limpiarDatosActividadInfoDialog();
        obtenerDatosActividad(idActividad);
        abrirActividadInfoDialog();
    }
    function verDetalleActividad(idActividad)
    {
        limpiarDatosDetalleActividadInfoDialog();
        obtenerDetalleActividad(idActividad);
        abrirDetalleActividadInfoDialog();
    }
    function verEliminarActividad(idActividad) {
        var onEliminarActividad_Confirmation = overloadFunction(onEliminarActividad_Confirm, idActividad);
        Helpers.NotificationHelper.showConfirm("¿Desea eliminar esta actividad?", "Eliminar", null, onEliminarActividad_Confirmation);
    }
    function onEliminarActividad_Confirm(idActividad) {
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/eliminarActividad", "{'idUsuario':'" + _usuarioLogueado.Id + "','idActividad':'" + idActividad + "'}", onEliminarActividad_Response);
    }
    function onEliminarActividad_Response(datos) {
        var respuestaInfo = jQuery.parseJSON(datos.d);
        if (respuestaInfo.Resultado) {
            var idActividad = respuestaInfo.Valor;
            Helpers.GoogleMapHelper.removeMarker(_googleMap.TiposMarcador.Actividad + "_" + idActividad);
        }
    }
</script>

<!-- Actividades -->
<script type="text/javascript" >
    function obtenerActividades()
    {
        var params          = new Object();
            params.idPais     = _filtroActual.Pais.Id;
            params.periodo  = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerActividades", jsonParams, obtenerActividades_Response);
    }
    function obtenerActividades_Response(datos)
    {
        var response = jQuery.parseJSON(datos.d);
        if (response.Resultado)
        {
            var actividadesInfo = response.Valor;
            if (actividadesInfo.length > 0)
            {
                for (var i = 0; i < actividadesInfo.length; i++)
                {
                    var actividadInfo = actividadesInfo[i];
                    var lat = actividadInfo.Latitud;
                    var lng = actividadInfo.Longitud;
                    var position = Helpers.GoogleMapHelper.createPosition(lat, lng);
                    agregarActividadInfo(actividadInfo, i + 1);

                    actualizarCantidadListasCargadas(_tiposLista.Actividades, i + 1, actividadesInfo.length);
                }
            }
        }

        actualizarListasCargadas(_tiposLista.Actividades);
        actualizarVisibilidadEstadoCargasEnMapa();
    }
    function obtenerActividadesUsuario(idUsuario) {
        var params = new Object();
        params.idUsuario = idUsuario;
        params.pais = _filtroActual.Pais.Nombre;
        params.periodo = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerActividadesUsuario", jsonParams, obtenerActividadesUsuario_Response);
    }
    function obtenerActividadesUsuario_Response(datos) {
        var response = jQuery.parseJSON(datos.d);
        if (response.Resultado) {
            var actividadesInfo = response.Valor;
            if (actividadesInfo.length > 0) {
                for (var i = 0; i < actividadesInfo.length; i++) {
                    var actividadInfo = actividadesInfo[i];
                    var lat = actividadInfo.Latitud;
                    var lng = actividadInfo.Longitud;
                    var position = Helpers.GoogleMapHelper.createPosition(lat, lng);
                    agregarActividadInfo(actividadInfo, i + 1);
                }
            }
        }
    }
    function agregarActividadInfo(actividadInfo, itemNumber) {
        actualizarListaObjetos(actividadInfo, _tiposLista.Actividades);

        var onUserClick = 'javascript:verInfoUsuario(' + actividadInfo.IdUsuario + ')';
        var onEditarClick = 'javascript:verEditarActividad(' + actividadInfo.Id + ')';
        var onEliminarClick = 'javascript:verEliminarActividad(' + actividadInfo.Id + ')';

        var burbuja = _googleMap.Marcadores[_googleMap.TiposMarcador.Actividad].Burbuja;
        var ancho = burbuja.Width;
        var alto = burbuja.Height;

        var botonParticiparEnActividad = '';
        if (actividadInfo.EsPublica) {
            botonParticiparEnActividad += '<tr><td colspan="2" >&nbsp;</td></tr>';
            botonParticiparEnActividad += '<tr><td>&nbsp;</td><td><div class="' + UI_States.Highlight + ' btnParticiparEnActividad" >Actividad abierta al público >><span><a href="javascript:participarEnActividad(' + actividadInfo.Id + ')">Participar</a></span></div></td></tr>';
            alto += 45;
        }

        var botonesEdicion = '';
        if (verificarUsuarioLogueado() && (actividadInfo.IdUsuario == _usuarioLogueado.Id)) {
            var btnEditar = '<input type="button" class="button btnEditar" value="Editar" style="width:90px; margin-right:10px;" onclick="' + onEditarClick + '" />';
            var btnEliminar = '<input type="button" class="button btnEliminar" value="Eliminar" style="width:100px" onclick="' + onEliminarClick + '" />';
            botonesEdicion += '<tr><td colspan="2" >&nbsp;</td></tr>';
            botonesEdicion += '<tr><td colspan="2" align="right" >' + btnEditar + btnEliminar + '</td></tr>';
            alto += 45;
        }

        var content = '';
        content += '<table border="0" cellpadding="0" cellspacing="0" width="100%" >';
        content += '<colgroup><col width="45%" /><col width="55%" /></colgroup>';
        content += '<tr><td><b>Usuario:</b></td><td><b><a href="' + onUserClick + '">' + actividadInfo.Usuario + '</a></b></td></tr>';
        content += '<tr><td><b>Provincia / Departamento / Estado:</b></td><td>' + actividadInfo.Provincia + '</td></tr>';
        content += '<tr><td><b>Fecha:</b></td><td>' + actividadInfo.FechaYHora + '</td></tr>';
        content += '<tr><td><b>Descripción:</b></td><td>' + actividadInfo.Descripcion + '</td></tr>';
        content += '<tr><td><b>Comentarios:</b></td><td>&nbsp;</td></tr>';
        content += '<tr><td colspan="2" ><textarea class="noResize" readonly="readonly" rows="5" >' + actividadInfo.Comentarios + '</textarea></td></tr>';
        content += botonParticiparEnActividad;
        content += botonesEdicion;
        content += '</table>';

        content = crearContenedorBurbujaMarcador(content, alto, ancho);

        var map = Helpers.GoogleMapHelper.getMap();
        var position = Helpers.GoogleMapHelper.createPosition(actividadInfo.Latitud, actividadInfo.Longitud);
        var title = "Click para más info.";

        var listItemText = "Actividad #{0}";
        if (!isEmpty(itemNumber))
            listItemText = "Actividad #" + itemNumber;

        var attribute = new Object();
        attribute.Name = "IdUsuario";
        attribute.Value = actividadInfo.IdUsuario

        var idMarker = actividadInfo.Id;
        Helpers.GoogleMapHelper.addMarker(map, position, idMarker, _googleMap.TiposMarcador.Actividad, _googleMap.Marcadores[_googleMap.TiposMarcador.Actividad].Icono, title, false, listItemText, content, null, onActividadInfoWindow_Open, attribute);
    }
    function onActividadInfoWindow_Open(infoWindow, marker) {
        aplicarEstiloBotonesMarcadorBurbuja(marker.id);
    }
</script>

<!-- ActividadInfo Datos -->
<script type="text/javascript" >
    function obtenerDatosActividad(idActividad)
    {
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerActividad", "{'idActividad':'" + idActividad + "'}", obtenerDatosActividad_Response);
    }
    function obtenerDatosActividad_Response(datos)
    {
        var respuesta = jQuery.parseJSON(datos.d);
        if (respuesta.Resultado)
        {
            var actividadInfo = respuesta.Valor;

            $("#hdnActividadInfo_Id").val(actividadInfo.Id);
            $("#hdnActividadInfo_Latitud").val(actividadInfo.Latitud);
            $("#hdnActividadInfo_Longitud").val(actividadInfo.Longitud);
            $("#lblActividadInfo_Usuario").text(actividadInfo.Usuario);
            $("#txtActividadInfo_FechaYHora").val(actividadInfo.FechaYHora.replace(' a.m.', '').replace(' p.m.', ''));
            $("#txtActividadInfo_Descripcion").val(actividadInfo.Descripcion);
            $("#txtActividadInfo_Comentarios").val(actividadInfo.Comentarios);
            if (actividadInfo.EsPublica)
                $("#chkActividadInfo_PermitirColaboracion").setValue(true);
            //Helpers.SelectListHelper.setSelectedValue("selActividadInfo_Pais", actividadInfo.IdPais);
            //Helpers.SelectListHelper.setSelectedValue("selActividadInfo_Provincia", actividadInfo.IdProvincia);
            $("#selActividadInfo_Pais").setValue(actividadInfo.IdPais);
            $("#selActividadInfo_Provincia").setValue(actividadInfo.IdProvincia);
        }
    }
    function obtenerDetalleActividad(idActividad)
    {
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerActividad", "{'idActividad':'" + idActividad + "'}", obtenerDetalleActividad_Response);
    }
    function obtenerDetalleActividad_Response(datos)
    {
        var respuesta = jQuery.parseJSON(datos.d);
        if (respuesta.Resultado)
        {
            var actividadInfo = respuesta.Valor;
            $("#txtDetalleActividadInfo_Comentarios").setValue(actividadInfo.Comentarios);
        }
    }
    function onClickToMap_Actividad(map, position, icon, title, idMarker)
    {
        if (!verificarUsuarioLogueado())
        {
            Helpers.NotificationHelper.showWarning("Antes debe loguearse para realizar esta acción.", "Acceso restringido");
            return;
        }

        limpiarDatosActividadInfoDialog();

        latLng = String.format("{0}, {1}", position.lat(), position.lng());
        Helpers.GoogleMapHelper.getPositionFromLatLng(latLng, "mapa", onGetPositionFromLatLng_Actividad_Response);

        $("#hdnActividadInfo_Id").val();
        $("#hdnActividadInfo_Latitud").val(position.lat());
        $("#hdnActividadInfo_Longitud").val(position.lng());

        $("#lblActividadInfo_Usuario").text(_usuarioLogueado.NombreUsuario);
    }
    function onGetPositionFromLatLng_Actividad_Response(address_components)
    {
        var direccion = parseAddressComponents(address_components);
        var pais = direccion.pais;
        var provincia = direccion.provincia;

        seleccionarPaisEnLista("selActividadInfo_Pais", pais, "selActividadInfo_Provincia", provincia);
        abrirActividadInfoDialog();
        mostrarPanelAdvertenciaUbicacionDistinta(_dialogos.ActividadInfo.Id, pais);
    }
    function guardarActividadInfo() {
        var infoValidacion =
            [
                { campo: "selActividadInfo_Pais", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar un país." }] },
                /*{ campo: "selActividadInfo_Provincia", validaciones: [{ operacion: _validaciones.Valor.MayorA, valor: 0, mensaje: "Debe ingresar una cantidad superior a cero." }] },*/
                { campo: "txtActividadInfo_FechaYHora", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una fecha y hora." }] },
                { campo: "txtActividadInfo_Descripcion", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una descripción." }] },
                { campo: "txtActividadInfo_Comentarios", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar algún comentario." }] }
            ];
        var arrErrores = validarFormulario(infoValidacion);
        if (arrErrores.length > 0)
            return false;

        cerrarActividadInfoDialog();

        var id = $("#hdnActividadInfo_Id").val();

        var actividadInfo = new Object();
        actividadInfo.Id = (id.length == 0 ? -1 : id);
        actividadInfo.IdUsuario = _usuarioLogueado.Id;
        actividadInfo.Usuario = _usuarioLogueado.NombreUsuario;
        actividadInfo.Fecha = $("#txtActividadInfo_FechaYHora").val();
        actividadInfo.Descripcion = $("#txtActividadInfo_Descripcion").val();
        actividadInfo.Comentarios = $("#txtActividadInfo_Comentarios").val();
        actividadInfo.Latitud = $("#hdnActividadInfo_Latitud").val();
        actividadInfo.Longitud = $("#hdnActividadInfo_Longitud").val();
        //actividadInfo.IdPais = Helpers.SelectListHelper.getSelectedValueFromSelectList("selActividadInfo_Pais");
        //actividadInfo.IdProvincia = Helpers.SelectListHelper.getSelectedValueFromSelectList("selActividadInfo_Provincia");
        actividadInfo.IdPais = $("#selActividadInfo_Pais").getValue();
        actividadInfo.IdProvincia = $("#selActividadInfo_Provincia").getValue();
        if (isEmpty(actividadInfo.IdPais))
            actividadInfo.IdPais = -1;
        if (isEmpty(actividadInfo.IdProvincia))
            actividadInfo.IdProvincia = -1;
        actividadInfo.EsPublica = $("#chkActividadInfo_PermitirColaboracion").getValue();
        var parametros = JSON.stringify(actividadInfo);

        Helpers.showLoadingMessage();
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/guardarActividad", parametros, guardarActividadInfo_Response);
    }
    function guardarActividadInfo_Response(datos) {
        Helpers.hideLoadingMessage();
        var respuestaInfo = jQuery.parseJSON(datos.d);
        if (respuestaInfo.Resultado) {
            var actividadInfo = respuestaInfo.Valor;
            agregarActividadInfo(actividadInfo);
            generarContenidoSocial_Actividad(actividadInfo);
            Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, '', null, null, true);
        }
        else {
            Helpers.NotificationHelper.showWarning(respuestaInfo.Mensaje, '');
        }
    }
</script>
