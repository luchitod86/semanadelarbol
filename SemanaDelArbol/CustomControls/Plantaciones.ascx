﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Plantaciones.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Dialogs.Plantaciones" %>

<script type="text/javascript" >
    $(document).ready(function ()
    {
        inicializarDialogPlantacionInfo();
        inicializarDialogPlantacionInfoFoto();
    });
</script>

<!-- Plantacion Info -->
<script type="text/javascript" >
    function inicializarDialogPlantacionInfo()
    {
        var plantacionInfoOptions =
        {
            title: "Información de la plantación",
            width: _dialogos.PlantacionInfo.Width,
            height: _dialogos.PlantacionInfo.Height,
            buttons:
            [
                { id: "btnCancelPlantacionInfoDialog", text: "Cancelar", click: cerrarPlantacionInfoDialog },
                { id: "btnConfirmPlantacionInfoDialog", text: "Aceptar", click: guardarPlantacionInfo }
            ]
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.PlantacionInfo.Id, plantacionInfoOptions);

        cargarListasGeograficas("selPlantacionInfoPais", null, -1, null);
        obtenerEspecies("selPlantacionInfoEspecie");

        $("#txtPlantacionInfoFecha").datepicker();
        inicializarFileUpload("btnPlantacionInfoSubirFoto", "imgPlantacionInfoFoto");
    }
    function inicializarDialogPlantacionInfoFoto()
    {
        var plantacionInfoFotoOptions =
        {
            title: "Foto de la plantación",
            width: _dialogos.PlantacionInfoFoto.Width,
            height: _dialogos.PlantacionInfoFoto.Height
        };
        Helpers.DialogHelper.initializeDialog(_dialogos.PlantacionInfoFoto.Id, plantacionInfoFotoOptions);
    }
    function abrirPlantacionInfoDialog() {
        //limpiarDatosPlantacionInfoDialog();
        Helpers.DialogHelper.openDialog(_dialogos.PlantacionInfo.Id);
    }
    function cerrarPlantacionInfoDialog() {
        Helpers.DialogHelper.closeDialog(_dialogos.PlantacionInfo.Id);
    }
    function limpiarDatosPlantacionInfoDialog() {
        $("#hdnPlantacionInfoId").val("");
        $("#hdnPlantacionInfoLatitud").val("");
        $("#hdnPlantacionInfoLongitud").val("");

        $("#lblPlantacionInfoUsuario").text("");
        $("#txtPlantacionInfoFecha").val("");
        Helpers.SelectListHelper.setSelectedIndex("selPlantacionInfoPais", -1);
        Helpers.MultiSelectHelper.setAll("selPlantacionInfoEspecie", false);
        $("#txtPlantacionInfoCantidad").val("");
        $("#imgPlantacionInfoFoto").attr("src", "");

        desactivarMensajesDeError("#selPlantacionInfoPais, #selPlantacionInfoEspecie + button, #txtPlantacionInfoFecha, #txtPlantacionInfoCantidad");
    }
    function guardarPlantacionInfo() {
        var infoValidacion =
            [
                { campo: "selPlantacionInfoPais", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar un país." }] },
                { campo: "selPlantacionInfoEspecie", seleccion: "#selPlantacionInfoEspecie + button", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar una especie." }] },
                { campo: "txtPlantacionInfoFecha", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una fecha." }] },
                { campo: "txtPlantacionInfoCantidad", validaciones: [{ operacion: _validaciones.Valor.MayorA, valor: 0, mensaje: "Debe ingresar una cantidad mayor a cero." }] }
            ];
        var arrErrores = validarFormulario(infoValidacion);
        if (arrErrores.length > 0)
            return false;

        cerrarPlantacionInfoDialog();

        var id = $("#hdnPlantacionInfoId").val();
        var plantacionInfo = new Object();
        plantacionInfo.Id = (id.length == 0 ? -1 : id);
        plantacionInfo.IdEspecie = Helpers.MultiSelectHelper.getFirstSelectedValue("selPlantacionInfoEspecie");
        plantacionInfo.Especie = Helpers.MultiSelectHelper.getFirstSelectedText("selPlantacionInfoEspecie");
        plantacionInfo.Latitud = $("#hdnPlantacionInfoLatitud").val();
        plantacionInfo.Longitud = $("#hdnPlantacionInfoLongitud").val();
        plantacionInfo.Cantidad = $("#txtPlantacionInfoCantidad").val();
        plantacionInfo.Fecha = $("#txtPlantacionInfoFecha").val();
        plantacionInfo.IdPais = Helpers.SelectListHelper.getSelectedValueFromSelectList("selPlantacionInfoPais");
        plantacionInfo.Foto = getBase64Image("imgPlantacionInfoFoto");
        plantacionInfo.IdUsuario = _usuarioLogueado.Id;
        plantacionInfo.Usuario = _usuarioLogueado.NombreUsuario;

        var parametros = JSON.stringify(plantacionInfo);

        Helpers.showLoadingMessage();
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/guardarPlantacion", parametros, guardarPlantacionInfo_Response);
    }
    function guardarPlantacionInfo_Response(datos) {
        Helpers.hideLoadingMessage();
        var respuestaInfo = jQuery.parseJSON(datos.d);
        if (respuestaInfo.Resultado) {
            var plantacionInfo = respuestaInfo.Valor;
            agregarPlantacionInfo(plantacionInfo);
            generarContenidoSocial_Plantacion(plantacionInfo);
            Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, "", "", null, true);
        }
        else {
            Helpers.NotificationHelper.showWarning(respuestaInfo.Mensaje, "");
        }
    }
</script>
<!-- Fin de [PlantacionInfo] -->

<!-- Plantaciones -->
<script type="text/javascript" >
    function obtenerPlantacionesUsuario(idUsuario) {
        var params = new Object();
        params.idUsuario = idUsuario;
        params.pais = _filtroActual.Pais.Nombre;
        params.periodo = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPlantacionesUsuario", jsonParams, obtenerPlantacionesUsuario_Response);
    }
    function obtenerPlantacionesUsuario_Response(datos) {
        var response = jQuery.parseJSON(datos.d);
        if (response.Resultado) {
            var plantacionesInfo = response.Valor;
            if (plantacionesInfo.length > 0) {
                for (var i = 0; i < plantacionesInfo.length; i++) {
                    var plantacionInfo = plantacionesInfo[i];
                    var lat = plantacionInfo.Latitud;
                    var lng = plantacionInfo.Longitud;
                    var position = Helpers.GoogleMapHelper.createPosition(lat, lng);
                    agregarPlantacionInfo(plantacionInfo, i + 1);
                }
            }
        }
    }
    function obtenerPlantaciones() {
        var params = new Object();
        params.pais = _filtroActual.Pais.Nombre;
        params.periodo = _filtroActual.Periodo;

        var jsonParams = JSON.stringify(params);

        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPlantaciones", jsonParams, obtenerPlantaciones_Response);
    }
    function obtenerPlantaciones_Response(datos) {
        var response = jQuery.parseJSON(datos.d);
        if (response.Resultado) {
            var plantacionesInfo = response.Valor;
            if (plantacionesInfo.length > 0) {
                for (var i = 0; i < plantacionesInfo.length; i++) {
                    var plantacionInfo = plantacionesInfo[i];
                    var lat = plantacionInfo.Latitud;
                    var lng = plantacionInfo.Longitud;
                    var position = Helpers.GoogleMapHelper.createPosition(lat, lng);
                    agregarPlantacionInfo(plantacionInfo, i + 1);

                    actualizarCantidadListasCargadas(_tiposLista.Plantaciones, i + 1, plantacionesInfo.length);
                }
            }
        }
        actualizarListasCargadas(_tiposLista.Plantaciones);
        actualizarVisibilidadEstadoCargasEnMapa();
    }
    function agregarPlantacionInfo(plantacionInfo, itemNumber) {
        actualizarListaObjetos(plantacionInfo, _tiposLista.Plantaciones);

        var onUserClick = 'javascript:verInfoUsuario(' + plantacionInfo.IdUsuario + ')';
        var onEditarClick = 'javascript:verEditarPlantacion(' + plantacionInfo.Id + ')';
        var onEliminarClick = 'javascript:verEliminarPlantacion(' + plantacionInfo.Id + ')';
        var onFotoClick = 'javascript:verFotoPlantacion(' + plantacionInfo.Id + ')';

        var burbuja = _googleMap.Marcadores[_googleMap.TiposMarcador.Plantacion].Burbuja;
        var ancho = burbuja.Width;
        var alto = burbuja.Height;

        var botonesEdicion = '';
        if (verificarUsuarioLogueado() && (plantacionInfo.IdUsuario == _usuarioLogueado.Id)) {
            var btnEditar = '<input type="button" class="button btnEditar" value="Editar" style="width:90px; margin-right:10px;" onclick="' + onEditarClick + '" />';
            var btnEliminar = '<input type="button" class="button btnEliminar" value="Eliminar" style="width:100px" onclick="' + onEliminarClick + '" />';
            botonesEdicion += '<tr><td colspan="2" >&nbsp;</td></tr>';
            botonesEdicion += '<tr><td colspan="2" align="right" >' + btnEditar + btnEliminar + '</td></tr>';
            alto += 45;
        }

        var botonFoto = '';
        if (plantacionInfo.TieneFoto) {
            botonFoto += '<tr><td><label><b>Foto:</b></label></td><td><a href="' + onFotoClick + '">Ver foto</a></td></tr><tr><td colspan="2" >&nbsp;</td></tr>';
             alto += 25;
            //botonFoto += '<tr><td><label><b>Foto:</b></label></td><td><a href="' + onFotoClick + '"><img src="data:image/png;base64,' + plantacionInfo.Foto + '" alt="Foto" class="foto" width="80" height="50" /></a></td></tr>';
            //alto += 65;
        }

        var content = '';
        content += '<table border="0" cellpadding="0" cellspacing="0" width="100%" >';
        content += '<colgroup><col width="35%" /><col width="65%" /></colgroup>';
        content += '<tr><td><label><b>Usuario:</b></label></td><td><label><b><a href="' + onUserClick + '">' + plantacionInfo.Usuario + '</a></b></label></td></tr>';
        content += '<tr><td><label><b>Fecha:</b></label></td><td><label>' + plantacionInfo.Fecha + '</label></td></tr>';
        content += '<tr><td><label><b>Especie:</b></label></td><td><label>' + plantacionInfo.Especie + '</label></td></tr>';
        content += '<tr><td><label><b>Cantidad:</b></label></td><td><label>' + plantacionInfo.Cantidad + '</label></td></tr>';
        content += botonFoto;
        content += botonesEdicion;
        content += '</table>';

        content = crearContenedorBurbujaMarcador(content, alto, ancho);

        var map = Helpers.GoogleMapHelper.getMap();
        var position = Helpers.GoogleMapHelper.createPosition(plantacionInfo.Latitud, plantacionInfo.Longitud);
        var title = "Click para más info.";

        var listItemText = "Plantación #{0}";
        if (!isEmpty(itemNumber))
            listItemText = "Plantación #" + itemNumber;

        var idMarker = plantacionInfo.Id;
        Helpers.GoogleMapHelper.addMarker(map, position, idMarker, _googleMap.TiposMarcador.Plantacion, _googleMap.Marcadores[_googleMap.TiposMarcador.Plantacion].Icono, title, false, listItemText, content, null, onPlantacionInfoWindow_Open);
    }
    function onPlantacionInfoWindow_Open(infoWindow, marker) {
        aplicarEstiloBotonesMarcadorBurbuja(marker.id);
    }
    function obtenerDatosPlantacion(idPlantacion) {
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPlantacion", "{'idPlantacion':'" + idPlantacion + "'}", obtenerDatosPlantacion_Response);
    }
    function obtenerDatosPlantacion_Response(datos) {
        var respuesta = jQuery.parseJSON(datos.d);
        if (respuesta.Resultado) {
            var plantacionInfo = respuesta.Valor;
            $("#hdnPlantacionInfoId").val(plantacionInfo.Id);
            $("#hdnPlantacionInfoLatitud").val(plantacionInfo.Latitud);
            $("#hdnPlantacionInfoLongitud").val(plantacionInfo.Longitud);
            $("#lblPlantacionInfoUsuario").text(plantacionInfo.Usuario);
            $("#txtPlantacionInfoFecha").val(plantacionInfo.Fecha);
            Helpers.SelectListHelper.setSelectedValue("selPlantacionInfoPais", plantacionInfo.IdPais);
            $("#txtPlantacionInfoCantidad").val(plantacionInfo.Cantidad);
            Helpers.MultiSelectHelper.setSelected("selPlantacionInfoEspecie", plantacionInfo.IdEspecie);
        }
    }
    function verEditarPlantacion(idPlantacion) {
        limpiarDatosPlantacionInfoDialog();
        obtenerDatosPlantacion(idPlantacion);
        abrirPlantacionInfoDialog();
    }
    function verFotoPlantacion(idPlantacion) {
        Helpers.showLoadingMessage();
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPlantacionFoto", "{'idPlantacion':'" + idPlantacion + "'}", verFotoPlantacion_Response);
    }
    function verFotoPlantacion_Response(datos) {
        Helpers.hideLoadingMessage();
        var respuestaInfo = jQuery.parseJSON(datos.d);
        if (respuestaInfo.Resultado) {
            var plantacionInfo = respuestaInfo.Valor;
            setBase64Image('imgFotoPlantacion', plantacionInfo.Foto);
            var onUserClick = 'javascript:verInfoUsuario(' + plantacionInfo.IdUsuario + ')';
            var contenido = String.format("Plantación de la especie <b>{0}</b>, realizada el <b>{1}</b> por <b><a href='" + onUserClick + "'>{2}</a></b>", plantacionInfo.Especie, plantacionInfo.Fecha, plantacionInfo.Usuario);
            $("#lblFotoPlantacionDescripcion").html(contenido);
            Helpers.DialogHelper.openDialog(_dialogos.PlantacionInfoFoto.Id);
        }
    }
    function verEliminarPlantacion(idPlantacion) {
        var onEliminarPlantacion_Confirmation = overloadFunction(onEliminarPlantacion_Confirm, idPlantacion);
        Helpers.NotificationHelper.showConfirm("¿Desea eliminar esta plantación?", "Eliminar", null, onEliminarPlantacion_Confirmation);
    }
    function onEliminarPlantacion_Confirm(idPlantacion) {
        Helpers.AJAXHelper.doAjaxCall("../Default.aspx/eliminarPlantacion", "{'idUsuario':'" + _usuarioLogueado.Id + "','idPlantacion':'" + idPlantacion + "'}", onEliminarPlantacion_Response);
    }
    function onEliminarPlantacion_Response(datos) {
        var respuestaInfo = jQuery.parseJSON(datos.d);
        if (respuestaInfo.Resultado) {
            var idPlantacion = respuestaInfo.Valor;
            Helpers.GoogleMapHelper.removeMarker(_googleMap.TiposMarcador.Plantacion + "_" + idPlantacion);
        }
    }
</script>

<!-- Dialogo Plantacion Info -->
<div id="dialogPlantacionInfo" style="display:none" >
    <input type="hidden" id="hdnPlantacionInfoId" />
    <input type="hidden" id="hdnPlantacionInfoLatitud" />
    <input type="hidden" id="hdnPlantacionInfoLongitud" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
        <colgroup>
            <col width="25%" />
            <col width="65%" />               
        </colgroup>
        <tr>
            <td>
                <label>Usuario:</label>
            </td>
            <td> 
                <label id="lblPlantacionInfoUsuario" ></label>
            </td>
        </tr>
        <tr>
            <td>
                <label>Fecha:</label>
            </td>
            <td> 
                <input id="txtPlantacionInfoFecha" type="text" />
            </td>
        </tr>
        <tr>
            <td>
                <label>País:</label>&nbsp;
                <label id="lblPlantacionInfoAdvertencia" class="tooltipLeft ui-state-highlight" title="Asegúrese de seleccionar el país en donde realizó la plantación." style="float:right; margin-right:3px; height:12px; cursor:pointer;" >*</label>
            </td>
            <td>
                <select id="selPlantacionInfoPais" ></select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Especie:</label>
            </td>
            <td>
                <select id="selPlantacionInfoEspecie" ></select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Cantidad:</label>
            </td>
            <td>
                <input id="txtPlantacionInfoCantidad" type="text" style="width:50px" value="" />
            </td>
        </tr>
        <tr>
            <td>
                <label>Foto:</label>
            </td>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <colgroup>
                        <col width="45%" />
                        <col width="55%" />
                    </colgroup>
                    <tr>
                        <td>
                            <input id="btnPlantacionInfoSubirFoto" class="button btnSubirImagen" type="button" value="Subir" /> 
                            <div id="divPlantacionInfoSubirFoto"></div>
                        </td>
                        <td>
                            <img id="imgPlantacionInfoFoto" src="" width="50px" height="40px" class="foto" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<!-- Fin de [Dialogo Plantacion Info] -->

<!-- Dialogo Plantacion Info [Foto] -->
<div id="dialogPlantacionInfoFoto" style="display:none" >
    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
        <colgroup>
            <col width="100%" /> 
        </colgroup>
        <tr>
            <td align="center" >
                <img id="imgFotoPlantacion" src="" alt="Foto" class="foto" width="550" height="400" />
            </td>
        </tr>
        <tr>
            <td align="center" >
                <label id="lblFotoPlantacionDescripcion" ></label>
            </td>
        </tr>
    </table>
</div>                                 
<!-- Fin de [Dialogo Plantacion Info [Foto]] -->