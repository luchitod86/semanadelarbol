﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuAcciones.ascx.cs" Inherits="SemanaDelArbol.CustomControls.MenuAcciones" %>

    <!-- MenuAcciones Styles -->
    <style type="text/css">
    </style>

    <script type="text/javascript" >
        $(document).ready(function ()
        {
            localizar_MenuAcciones();
            enlazarMenuAcciones();
        });
        function localizar_MenuAcciones()
        {
            var localization = Helpers.LocalizationHelper;

            $("#tituloAccionesMenu").html(localization.MenuAcciones.Titulo);
           
        }
        function enlazarMenuAcciones()
        {
            $("#btnPlantar").click(function () { realizarAccion(_secciones.Plantar); });
            $("#btnAdoptar").click(function () { realizarAccion(_secciones.Adoptar); });
            $("#btnDonar").click(function () { realizarAccion(_secciones.Donar); });

        }
    </script>

    <!-- Menú Acciones -->    
    
    <div id="accionesMenu">
		<span id="tituloAccionesMenu" class="titulo-acciones" ></span>
        <div class="options" >
            <div id="btnPlantar" class="item tooltipRight" title="Haga click aquí si quiere <b>plantar</b> un árbol" >
                <img src="../Images/Button-Planta.png" alt="Plantar un árbol" />
            </div>
			<div id="btnDonar" class="item tooltipRight" title="Haga click aquí si quiere <b>donar</b> un árbol." >
                <img src="../Images/Button-Donar.png" alt="Donar un árbol" />
            </div>
            <div id="btnAdoptar" class="item tooltipRight" title="Haga click aquí si quiere <b>adoptar</b> un árbol." >
                <img src="../Images/Button-Adopta.png" alt="Adoptar un árbol" />
            </div>
            
        </div>
    </div>
    <!-- Fin de [Menú Acciones] -->