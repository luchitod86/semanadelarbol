﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MisDatos.ascx.cs" Inherits="SemanaDelArbol.CustomControls.MisDatos" %>

    <!-- MisDatos Styles -->
    <style type="text/css">
        .imgMisTotales
        {
            padding: 0px;
            width: 24px;
        }
        .sectionContent
        {
            font-size: 15px !important;
            padding: 5px;
        }
    </style>

    <script type="text/javascript" >
        $(document).ready(function()
        {
        });
        function configurarMenuMisDatos(contenidoAyuda)
        {
            sideMenu("misDatosMenu", "right", 270, null, null, _preferencias.MostrarPanelFlotanteUsuarioLogueado.Valor);
            actualizarPreferencia("", "misDatosMenu .toggler", _preferencias.MostrarPanelFlotanteUsuarioLogueado);

            if (verificarUsuarioLogueado())
            {
                obtenerTotalesUsuario( _usuarioLogueado.Id );
            }
            $("#pnlAyudaContent").html(contenidoAyuda);
        }
        function obtenerTotalesUsuario(idUsuario)
        {
            var params              = new Object();
                params.idPais       = _filtroActual.Pais.Id;
                params.periodo      = _filtroActual.Periodo;
                params.idUsuario    = idUsuario;

            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerTotales", jsonParams, obtenerTotalesUsuario_Response);
        }
        function obtenerTotalesUsuario_Response(datos)
        {
            var respuesta = jQuery.parseJSON(datos.d);
            if (respuesta.Resultado)
            {
                var misTotales = respuesta.Valor;
                actualizarMisTotales(misTotales.Plantaciones, misTotales.Donaciones, misTotales.Adopciones, misTotales.CO2Capturado);
            }
        }
        function actualizarMisTotales(plantaciones, donaciones, adopciones, CO2capturado)
        {
            $("#misTotales_CO2Capturado").text(CO2capturado.toFixed(2).replace(".", ","));
            $("#misTotales_ArbolesPlantados").text(plantaciones);
            $("#misTotales_ArbolesDonados").text(donaciones);
            $("#misTotales_ArbolesAdoptados").text(adopciones);
        }
    </script>

    <div id="misDatosMenu" class="sideMenu right" >
        <div class="options" >
            <!-- Ayuda -->
            <div id="pnlAyuda" >
                <div class="recuadroVerde" >
                    <img src="../Images/Comun/16x16/Ayuda.png" alt="Ayuda" />
                    <label class="titulo" >Ayuda</label>
                </div>
                <div id="pnlAyudaContent" class="sectionContent" ></div>
            </div>
            <!-- Fin de [Ayuda] -->

            <!-- Mis totales -->
            <div id="pnlMisTotales" >
                <div class="recuadroVerde" >
                    <img src="../Images/Comun/16x16/DatosUsuario.png" alt="Mis datos" />
                    <label class="titulo" >Mis datos</label>
                </div>
                <div class="sectionContent" >
                    <div class="tooltipLeft"  title="Cantidad de árboles que plantaste." >
                        <img src="../Images/Indicadores/Plantaciones.png" alt="Árboles" class="imgMisTotales" />
                        <label id="misTotales_ArbolesPlantados" class="link negrita" >0</label>&nbsp;<label class="link" >árbol(es) plantados</label>
                    </div>
                    <div class="tooltipLeft" title="Cantidad de dióxido de carbono que ayudas a capturar." >
                        <img src="../Images/Indicadores/Co2capturado.png" alt="Dióxido de Carbono capturado" class="imgMisTotales" />
                        <label id="misTotales_CO2Capturado" class="link negrita" >0</label>&nbsp;<label class="link" >tn. de CO<sub>2</sub> capturada(s)</label>
                    </div>
                    <div class="tooltipLeft" title="Cantidad de árboles que donaste." >
                        <img src="../Images/Indicadores/Donaciones.png" alt="Donaciones" class="imgMisTotales" />
                        <label id="misTotales_ArbolesDonados" class="link negrita" >0</label>&nbsp;<label class="link" >árbol(es) donados</label>
                    </div>
                    <div class="tooltipLeft" title="Cantidad de árboles que adoptaste." >
                        <img src="../Images/Indicadores/Adopciones.png" alt="Adopciones" class="imgMisTotales" />
                        <label id="misTotales_ArbolesAdoptados" class="link negrita" >0</label>&nbsp;<label class="link" >árbol(es) adoptados</label>
                    </div>
                </div>
            </div>
            <!-- Fin de [Mis totales] -->
        </div>
    </div>