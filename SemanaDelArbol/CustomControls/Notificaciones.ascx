﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notificaciones.ascx.cs" Inherits="SemanaDelArbol.CustomControls.Notificaciones" %>

<!-- $(document).ready -->
<script type="text/javascript" >

    var notificationDialogId = "dialogNotification";
    
    $(document).ready(function()
    {
        //..
        $(".btnCompartir").click( function() { compartir($(this)); } );
    });

</script>

<!-- Public methods -->
<script type="text/javascript" >

    //var imgDir = "../CustomControls/Notification/"
    var imgDir = _root + "CustomControls/Notification/";

    var _notificationType = new Object();
        _notificationType.Message       = "Message";
        _notificationType.Info          = "Info";
        _notificationType.Warning       = "Warning";
        _notificationType.Confirm       = "Confirm";
        _notificationType.Error         = "Error";

    function showNotification(notificationType, message, title, customImage, onConfirm, onCancel, showSharePanel)
    {
        _showNotification(message, title, notificationType, customImage, onConfirm, onCancel, showSharePanel);
    }
</script>

<!-- Private methods -->
<script type="text/javascript" >
    function _clearNotificationDialogControls()
    {
        $("#imgNotification_Icon").attr("src", "");
        $("#lblNotification").text("");
        //$("#" + notificationDialogId).dialog("option", "buttons", {});
        //$("#" + notificationDialogId).dialog("option", "title", "");

        //Error..
        $("#pnlError").hide();
        $("#pnlErrorDetails").hide();
        $("#txtErrorDescription").html("");
        $("#lnkMoreDetails").hide();
        $("#txtErrorDetails").hide().html("");
        $("#lblErrorCompleteDescription").hide();
    }
    function _showNotification(message, title, type, img, onConfirm, onCancel, showSharePanel)
    {
        //Clear controls..
        _clearNotificationDialogControls();
        
        var dialogHeight = 220;
        var dialogWidth  = 470;

        var lines = 1;

        /*#region switch(type) */
        switch (type)
        {
            case _notificationType.Error:
                lines += 3;
                _setErrorMessage(message);
                //message = "¡Lo sentimos! Ha ocurrido un error..<br /><br />Por favor, intente recargar la página.<br />Si el error persiste, <span id='lnkSendError' style='cursor:pointer; text-decoration:underline' >reportelo al Administrador</span>.";
                message = "¡Lo sentimos! Ha ocurrido un error..";
                title = _getTitle(title, "Error");
                img = _getImage(img, "Error");
                break;
            case _notificationType.Message:
                //lines += 1;
                title = _getTitle(title, "Mensaje");
                img = _getImage(img, "Message");
                break;
            case _notificationType.Info:
                //lines += 1;
                title = _getTitle(title, "Información");
                img = _getImage(img, "Info");
                break;
            case _notificationType.Warning:
                //lines += 1;
                title = _getTitle(title, "Advertencia");
                img = _getImage(img, "Warning");
                break;
            case _notificationType.Confirm:
                //lines += 1;
                title = _getTitle(title, "Confirmación");
                img = _getImage(img, "Confirm");
                break;
        }
        //#endregion

        //$("#" + notificationDialogId).dialog({ width: dialogWidth, height: dialogHeight });
        $("#lblNotification").html(message);

        //bind onConfirm & onCancel functions to corresponding button's click event..
        $("#btnConfirmNotificationDialog, #btnCancelNotificationDialog").hide();
        if ( !isEmpty(onConfirm) )
        {
            $("#btnConfirmNotificationDialog").show();
            $("#btnConfirmNotificationDialog").unbind('click');
            $("#btnConfirmNotificationDialog").click( function() { onConfirm.call(); $("#" + notificationDialogId).dialog("close"); } );
        }
        if ( !isEmpty(onCancel) || (type == _notificationType.Confirm) )
        {
            $("#btnCancelNotificationDialog").show();
            $("#btnCancelNotificationDialog").unbind('click');
            $("#btnCancelNotificationDialog").click( function() { if (!isEmpty(onCancel)) { onCancel.call(); } $("#" + notificationDialogId).dialog("close"); } );
        }
        else
        {
            $("#btnCancelNotificationDialog").show();
            $("#btnCancelNotificationDialog").unbind('click');
            $("#btnCancelNotificationDialog").click( function() { $("#" + notificationDialogId).dialog("close"); } );
        }        

        if ( showSharePanel == true )
        {
            $("#divCompartirAccion").show();
            dialogHeight += 50;
        }
        else
        {
            $("#divCompartirAccion").hide();
            dialogHeight +- 50;
        }


        $("#imgNotification_Icon").attr("src", img);

        var dialogTitle = "Notificación";
        if (!isEmpty(title))
            dialogTitle = title;

        //Open notification dialog..
        var notificationDialog = $("#" + notificationDialogId);
            notificationDialog.dialog("option", "title", dialogTitle);
            notificationDialog.dialog("option", "height", dialogHeight);
            notificationDialog.dialog("option", "width", dialogWidth);
            if (!isEmpty(onConfirm))
            {
                notificationDialog.dialog("option", "closeOnEscape", false);
                notificationDialog.on("dialogopen", function (event, ui) { $(".ui-dialog-titlebar-close", this.parentNode).hide() });
            }
            else
            {
                notificationDialog.dialog("option", "closeOnEscape", true);
                notificationDialog.on("dialogopen", function (event, ui) { $(".ui-dialog-titlebar-close", this.parentNode).show() });
            }
            notificationDialog.dialog("open");

        ////get height of label and resize dialog to fit..
        //var lblHeight = $("#lblNotification").outerHeight();
        //var lineHeight = parseInt($("#lblNotification").css("line-height"));
        //var charLength = $("#lblNotification").text().length;

        //if ( charLength > 35 )
        //    dialogHeight -= 60;
        //if ( charLength > 70 )
        //    dialogHeight -= 30;
        //if ( charLength > 100 )
        //    dialogHeight += 0;
        //if ( charLength > 130 )
        //    dialogHeight += 30;

        if ( type == _notificationType.Error )
        {
            //dialogHeight += 80;
            $("#lnkSendError").click(function()
            {
                _hideNotification();
                if (!isEmpty(sendErrorEmail))
                {
                    var exception  = $("#txtErrorDescription").val();
                    var stackTrace = $("#txtErrorDetails").val();
                    sendErrorEmail(exception, stackTrace); //call to "sendErrorEmail" method declared somewhere else
                }
            });
        }

        //lines += (lblHeight / lineHeight);
        //dialogHeight = (lines * 55)
        _resizeDialog(notificationDialogId, dialogWidth, dialogHeight); //call to "_resizeDialog" method on DialogHelper.js
    }
    function _hideNotification()
    {
        var notificationDialog = $("#" + notificationDialogId);
            notificationDialog.dialog("close");
    }
    function _setErrorMessage(message)
    {
        var exception = message;
        var stackTrace = "";
        var details = "";

        if (!isEmpty(exception))
        {
            if (!isEmpty(exception.Message))
                $("#txtErrorDescription").html(exception.Message);
            else
                $("#txtErrorDescription").html(exception);

            if (!isEmpty(exception.StackTrace))
                stackTrace = exception.StackTrace.split(/\n/);
            if (!isEmpty(stackTrace))
            {
                details = "";
                $.each(stackTrace, function (index, item)
                {
                    var line = stackTrace[index].replace(/^\s+/, '');
                    details += line;
                });
            }
        }

        $("#txtErrorDescription").show();
        //$("#pnlError").show();

        if ( !isEmpty(stackTrace) )
        {
            $("#txtErrorDetails").html(details);
            $("#lnkMoreDetails").show();
        }
    }
    function _seeFullDescription()
    {
        $("#lnkMoreDetails").hide();
        $("#lblErrorCompleteDescription").show();
        $("#txtErrorDetails").show();
        $("#pnlErrorDetails").show();

        _resizeDialog(notificationDialogId, null, 420); //call to "_resizeDialog" method on DialogHelper.js
    }
    function _getTitle(title, defaultTitle)
    {
        if (!isEmpty(title))
            return title;
        else
            return defaultTitle;
    }
    function _getImage(image, defaultImage)
    {
        if (!isEmpty(image))
            return image;
        else
            return imgDir + defaultImage + ".png";
    }
</script>

<!-- HTML -->
<div id="dialogNotification" style="display:none; min-height:50px;" >
    <table border="0" cellspacing="0" cellpadding="0" width="100%" >
        <colgroup>
            <col width="12%" />
            <col width="88%" />
        </colgroup>
        <tr>
            <td>    
                <img id="imgNotification_Icon" src="" alt="Notification" width="48" height="48" />
            </td>
            <td>
                <div id="lblNotification" ></div>
            </td>
        </tr>
    </table>
    <div id="divCompartirAccion" style="display:none;" >
        <center class="ui-state-highlight" style="margin-top:10px;" >
            <div style="width:50%; padding-top:5px; padding-bottom:5px;" >
                <div class="title" style="padding: 2px;" >¿DESEA COMPARTIRLO?</div>
                <div style="padding-top:10px; padding-bottom:5px" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="33%" />
                            <col width="33%" />
                            <col width="33%" />
                        </colgroup>
                        <tr>
                            <td align="center">
                                <div id="btnCompartirEnFacebook" class="btnCompartir facebook" ></div>
                            </td>
                            <td align="center">
                                <div id="btnCompartirEnTwitter" class="btnCompartir twitter" ></div>
                            </td>
                            <td align="center">
                                <div id="btnCompartirEnGooglePlus" class="btnCompartir googlePlus" ></div>
                            </td>
                        </tr>
                  </table>
                </div>
            </div>
        </center>
    </div>
    <div id="pnlError" style="display:none" >
        <br />
        <a id="lnkMoreDetails" href="javascript:_seeFullDescription();">Ver detalles técnicos</a>
        <div id="pnlErrorDetails" style="display:none" >
            <label id="lblErrorDescription">Descripción del error:</label>
            <textarea id="txtErrorDescription" class="noResize" style="width:99%; height:99%" cols="5" rows="2" readonly="readonly" ></textarea>               
            <br /><br />
            <label id="lblErrorCompleteDescription">Detalle técnico del error:</label>
            <textarea id="txtErrorDetails" class="noResize" style="width:99%; height:99%;" cols="10" rows="5" readonly="readonly" ></textarea>               
        </div>
    </div>
</div>