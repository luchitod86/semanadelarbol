﻿using Controladores;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemanaDelArbol.Helpers
{
    public class ConfigurationHelper
    {
        public static Configuracion ObtenerConfiguracion()
        {
            Configuracion configuracion = null;
            try
            {
                configuracion = Helpers.CacheHelper.GetValue<Configuracion>(Comun.Constantes.CacheKeys.CONFIGURACION);
                if (configuracion == null)
                {
                    configuracion = new ConfiguracionControlador().ObtenerConfiguracion();
                    Helpers.CacheHelper.SetValue<Configuracion>(Comun.Constantes.CacheKeys.CONFIGURACION, configuracion);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return configuracion;
        }
    }
}