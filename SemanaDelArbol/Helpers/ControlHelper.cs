﻿using SemanaDelArbol.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace SemanaDelArbol.Helpers
{
    public class ControlHelper
    {
        public static Control ObtenerControl(Control controlContenedor, string controlId)
        {
            var masterPagePrefix            = "";
            var contentPlaceHolderPrefix    = "";
            var separator                   = "";

            var controlName = controlContenedor.GetType().FullName;
                controlName = controlName.Replace("ASP.", String.Empty);
            if (controlName.Contains("pages"))
            {
                masterPagePrefix            = "ctl00";
                contentPlaceHolderPrefix    = "MainContent";
                separator                   = "$";
            }
            if (controlName.Contains("customcontrols"))
            {
                masterPagePrefix            = "";
                contentPlaceHolderPrefix    = "";
                separator                   = "";
            }

            controlId = String.Format("{0}{1}{2}{1}{3}", masterPagePrefix, separator, contentPlaceHolderPrefix, controlId);

            return controlContenedor.FindControl(controlId);
        }

        public static string ObtenerNombreControl(Control control)
        {
            var nombre = control.GetType().FullName;
                nombre = nombre.Replace("ASP.", String.Empty);
                nombre = nombre.Replace("customcontrols_", String.Empty);
                nombre = nombre.Replace("_ascx", String.Empty);
                nombre = nombre.Replace("pages_", String.Empty);
                nombre = nombre.Replace("_aspx", String.Empty);
            return nombre;
        }

        public static void EstablecerValorControl(Control controlContenedor, string controlId, string valor)
        {
            var control = ObtenerControl(controlContenedor, controlId);
            if (control != null)
            {
                //var controlType = control.GetType();
                //if (controlType == typeof(HtmlTextArea))
                //    ((HtmlTextArea)control).InnerHtml = "";
                var htmlContainer = ((HtmlContainerControl)control);
                    htmlContainer.InnerHtml = valor;
            }
        }
    }
}