﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace SemanaDelArbol.Helpers
{
    public class CacheHelper
    {
        /// <summary>
        /// Gets a value from the Cache, according to a given key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetValue<T>(string key)
        {
            try
            {
                return (T)HttpContext.Current.Cache[key];
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Sets a value on the Cache, according to a given key.
        /// If does not exist, adds it
        /// If does exist, updates its value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetValue<T>(string key, T value)
        {
            try
            {
                if (value == null)
                    return;

                if ( GetValue<T>(key) == null )
                    HttpContext.Current.Cache.Add(key, value, null, DateTime.Now.AddMonths(1), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                else
                    HttpContext.Current.Cache[key] = value;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Removes a value from Cache, according to a given key.
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveValue(string key)
        {
            try
            {
                if (HttpContext.Current.Cache[key] != null)
                    HttpContext.Current.Cache.Remove(key);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}