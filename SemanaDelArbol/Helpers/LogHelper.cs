﻿using Controladores;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SemanaDelArbol.Helpers
{
    public class LogHelper
    {
        public static void LogError(string seccion, string exception, string stackTrace)
        {
            try
            {
                seccion = seccion.Replace(".aspx", String.Empty).Replace(".ascx", String.Empty);

                var lc = new LogControlador();
                var log = new Log();
                log.Tipo = lc.ObtenerTipo(Comun.Constantes.LogTipos.ERROR);
                log.FechaYHora = DateTime.Now;
                log.Seccion = new ControladorBase<Seccion>().ObtenerEntidad(String.Format("Nombre == \"{0}\"", seccion));
                log.Texto = exception;
                log.Detalle = stackTrace;

                lc.Guardar(log);
            }
            catch (Exception)
            {
                //throw;
            }
        }

    }
}