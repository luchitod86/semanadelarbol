﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Entidades;
using Entidades.jSON.Sistema;
using Comun;

namespace SemanaDelArbol.Helpers
{
    /// <summary>
    /// Helper class to assist setting/getting values from/to ASP .NET Session object
    /// </summary>
    public class SessionHelper
    {
        /// <summary>
        /// Obtiene el valor almacenado en el objeto de Sesión de ASP .NET, según una Key (clave)
        /// </summary>
        /// <param name="clave"></param>
        /// <returns></returns>
        public static T GetValue<T>(string clave)
        {
            try
            {
                return (T)HttpContext.Current.Session[clave];
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Almacena un valor en el objeto de Sesión de ASP .NET, según una Key (clave) y el valor mismo
        /// </summary>
        /// <param name="clave"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static bool SetValue<T>(string clave, T valor)
        {
            try
            {
                HttpContext.Current.Session[clave] = valor;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Construye un objeto RespuestaInfo, basado en parámetros recibidos, y lo devuelve como una cadena en formato jSON.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string BuildResponse(string message, bool result, object obj)
        {
            RespuestaInfo respuestaInfo = new RespuestaInfo();
            string jSONResponse = string.Empty;
            try
            {
                respuestaInfo.Resultado = result;
                respuestaInfo.Mensaje = message;
                respuestaInfo.Valor = obj;
                jSONResponse = Comun.Formateador.jSON.convertToJson(respuestaInfo);
            }
            catch (Exception)
            {
                throw;
            }
            return jSONResponse;
        }

        #region Usuario Logueado

        /// <summary>
        /// Devuelve el Usuario actualmente logueado
        /// </summary>
        /// <returns></returns>
        public static Usuario GetCurrentUser()
        {
            Usuario usuarioLogueado = null;
            try
            {
                if (HttpContext.Current.Session[Constantes.SessionKeys.USUARIO_LOGUEADO] != null)
                {
                    usuarioLogueado = (Usuario)HttpContext.Current.Session[Constantes.SessionKeys.USUARIO_LOGUEADO];
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuarioLogueado;
        }
            
        public static void SetCurrentUser(Usuario usuario)
        {
            try
            {
                SessionHelper.SetValue(Constantes.SessionKeys.USUARIO_LOGUEADO, usuario);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Log off
        /// </summary>
        /// <returns></returns>
        public static bool LogOff()
        {
            var result = true;
            try
            {
                HttpContext.Current.Session.Remove(Constantes.SessionKeys.USUARIO_LOGUEADO);
                HttpContext.Current.Session.Abandon();
            }
            catch (Exception)
            {
                result = false;
                throw;
            }
            return result;
        }

        #endregion

        #region Preferencias de Usuario

        public static void SetUserPreferences(List<UsuarioPreferencia> preferencias)
        {
            SetValue<List<UsuarioPreferencia>>(Comun.Constantes.SessionKeys.USUARIO_PREFERENCIAS, preferencias);
        }

        public static List<UsuarioPreferencia> GetUserPreferences()
        {
            return GetValue<List<UsuarioPreferencia>>(Comun.Constantes.SessionKeys.USUARIO_PREFERENCIAS);
        }

        #endregion

        #region Filtro

        /// <summary>
        /// Obtiene el filtro actual
        /// En caso de no existir, establece valores por default { Pais = "Argentina", IdPais = 1, Periodo = DateTime.Now.Year }
        /// </summary>
        /// <returns></returns>
        public static FiltroInfo ObtenerFiltroActual()
        {
            FiltroInfo filtro = null;

            try
            {
                filtro = SessionHelper.GetValue<FiltroInfo>(Constantes.FILTRO);
            }
            catch (Exception)
            {
            }

            if (filtro == null)
                filtro = SessionHelper.ActualizarValoresFiltro(DateTime.Now.Year , "Argentina", 1);

            return filtro;
        }

        /// <summary>
        /// Actualiza los valores del filtro actual
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="idPais"></param>
        /// <returns></returns>
        public static FiltroInfo ActualizarValoresFiltro(int periodo = -1, string pais = "", int idPais = -1)
        {
            FiltroInfo filtro = null;
            try
            {
                filtro = SessionHelper.GetValue<FiltroInfo>(Constantes.FILTRO);

                if (filtro == null)
                    filtro = new FiltroInfo();

                if (!string.IsNullOrEmpty(pais))
                    filtro.Pais = pais;

                if (idPais != -1)
                    filtro.IdPais = idPais;

                if (periodo != -1)
                    filtro.Periodo = periodo;

                SessionHelper.SetValue(Constantes.FILTRO, filtro);
            }
            catch (Exception)
            {
                throw;
            }
            return filtro;
        }

        #endregion
    }
}
