﻿using Controladores;
using Entidades;
using SemanaDelArbol.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace SemanaDelArbol.Helpers
{
    public class ContentHelper
    {
        #region Methods

        public static List<SeccionContenido> ObtenerSeccionContenidos(int idPais)
        {
            var cc = new ContenidoControlador();
            var key = String.Format("{0}_ID_PAIS_{1}", Comun.Constantes.CacheKeys.SECCION_CONTENIDO, idPais);
            var content = Helpers.CacheHelper.GetValue<List<SeccionContenido>>(key);
            if ( content.IsNull() )
            {
                content = cc.ObtenerTodosLosSeccionContenido(idPais);
                Helpers.CacheHelper.SetValue<List<SeccionContenido>>(key, content);
            }
            return content;
        }

        public static List<SeccionContenido> ObtenerSeccionContenidosDefault()
        {
            var cc = new ContenidoControlador();
            var defaultKey = String.Format("{0}_DEFAULT", Comun.Constantes.CacheKeys.SECCION_CONTENIDO);
            var defaultContent = Helpers.CacheHelper.GetValue<List<SeccionContenido>>(defaultKey);
            if ( defaultContent.IsNull() )
            {
                defaultContent = cc.ObtenerTodosLosSeccionContenido(Comun.Constantes.Defaults.Pais.ID);
                Helpers.CacheHelper.SetValue<List<SeccionContenido>>(defaultKey, defaultContent);
            }
            return defaultContent;
        }

        public static bool GuardarContenido(int idPais, string seccion, string campo, string contenido, string valor)
        {
            var success = false;
            try
            {
                var cc = new ContenidoControlador();
                var seccionContenido = cc.Obtener(idPais, seccion, campo);
                
                if (seccionContenido == null)
                {
                    seccionContenido = new SeccionContenido();
                    seccionContenido.Pais = new PaisControlador().ObtenerPorId(idPais);
                    seccionContenido.Seccion = cc.ObtenerSeccion(seccion);
                    seccionContenido.Campo = campo;
                }
                    seccionContenido.Contenido = contenido;
                    seccionContenido.Valor = valor;

                cc.Guardar(seccionContenido);

                success = true;
            }
            catch (Exception)
            {
                throw;
            }
            return success;
        }

        public static void EstablecerContenidoSeccion(Control controlContenedor, int idPais)
        {
            var nombreSeccion = Helpers.ControlHelper.ObtenerNombreControl(controlContenedor);
            var seccionContenidos = ObtenerSeccionContenidos(idPais);
            var contenidos = seccionContenidos.Where(x => x.Seccion.Nombre.ToLower() == nombreSeccion.ToLower());
            if ( contenidos.IsNull() || contenidos.Count() == 0 )
            {
                seccionContenidos = ObtenerSeccionContenidosDefault();
                contenidos = seccionContenidos.Where(x => x.Seccion.Nombre.ToLower() == nombreSeccion.ToLower());
            }
            if ( contenidos.IsNotNull() )
            {
                foreach(var contenido in contenidos)
                {
                    Helpers.ControlHelper.EstablecerValorControl(controlContenedor, String.Format("{0}_{1}", "contenido", contenido.Campo), contenido.Contenido);
                }
            }
        }

        //public static Seccion ObtenerSeccion(List<Seccion> contenidos, string seccion)
        //{
        //    return contenidos.Where(x => x.Nombre.ToLower() == seccion.ToLower()).FirstOrDefault();
        //}

        //public static SeccionContenido ObtenerCampo(Seccion seccion, string campo)
        //{
        //    return seccion.Contenidos.Where(x => x.Campo.ToLower() == campo.ToLower()).FirstOrDefault();
        //}

        //public static SeccionContenido ObtenerCampo(List<Seccion> contenidos, string seccion, string campo)
        //{
        //    var seccionObjeto = ObtenerSeccion(contenidos, seccion);
        //    return ObtenerCampo(seccionObjeto, campo);
        //}

        //public static SeccionContenido ObtenerCampo(string seccion, string campo)
        //{
        //    var contenidos = ObtenerSeccionContenidos();
        //    var seccionObjeto = ObtenerSeccion(contenidos, seccion);
        //    return ObtenerCampo(seccionObjeto, campo);
        //}

        #endregion Methods
    }
}