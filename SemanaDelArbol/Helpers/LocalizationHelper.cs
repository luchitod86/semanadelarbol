﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;

namespace SemanaDelArbol.Helpers
{
    public class LocalizationHelper
    {
        public static void SetCurrentLanguage(string language)
        {
            try
            {
                var culture = new System.Globalization.CultureInfo(language);
                System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                System.Threading.Thread.CurrentThread.CurrentUICulture = culture;

                #region Resource Files
                Resources.Header.Culture = culture;
                Resources.Footer.Culture = culture;
                Resources.MenuAcciones.Culture = culture;
                #endregion
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static Dictionary<string, Dictionary<string, string>> GetLocalizedStrings()
        {
            var dictionary = new Dictionary<string, Dictionary<string, string>>();
            try
            {
                var dictionary_Header = CreateDictionary(Resources.Header.ResourceManager.GetResourceSet(CultureInfo.CurrentCulture, true, true));
                var dictionary_MenuAcciones = CreateDictionary(Resources.MenuAcciones.ResourceManager.GetResourceSet(CultureInfo.CurrentCulture, true, true));
                var dictionary_Footer = CreateDictionary(Resources.Footer.ResourceManager.GetResourceSet(CultureInfo.CurrentCulture, true, true));

                dictionary.Add("Header", dictionary_Header);
                dictionary.Add("MenuAcciones", dictionary_MenuAcciones);
                dictionary.Add("Footer", dictionary_Footer);
            }
            catch (Exception)
            {
                throw;
            }
            return dictionary;
        }

        private static Dictionary<string, string> CreateDictionary(ResourceSet resourceSet)
        {
            var dictionary = new Dictionary<string, string>();
            try
            {
                foreach (DictionaryEntry entry in resourceSet)
                {
                    dictionary.Add(entry.Key.ToString(), entry.Value.ToString());
                }
            }
            catch (Exception)
            {                
                throw;
            }
            return dictionary;
        }

        public static Dictionary<string, string> GetResourceFileContent(string filename)
        {
            var dictionary = new Dictionary<string, string>();
            try
            {
                var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                var resourceReader = new ResXResourceReader(stream);
                foreach (DictionaryEntry item in resourceReader)
                {
                    dictionary.Add(item.Key.ToString(), item.Value.ToString());
                }
                resourceReader.Close();
                stream.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
            return dictionary;
        }
    }
}