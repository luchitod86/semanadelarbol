﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Default.aspx.cs" Inherits="SemanaDelArbol._Default" %>

<%@ Register Src="~/CustomControls/Mapa.ascx" TagName="Mapa" TagPrefix="Mapa" %>
<%@ Register Src="~/CustomControls/Agenda.ascx" TagName="Agenda" TagPrefix="Agenda" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Default);

            generarContenidoSocial();

            _personalizacion = obtenerPersonalizacion();
            if ( _personalizacion[_Personalizacion_VideoInstitucional_Ocultar] != "true" )
            {
                //configurarVisualizador("videoInstitucional", "http://www.youtube.com/embed/yON-6SxCzsg", true, true, true, false);

            //    var url = "http://www.youtube.com/embed/NRiN0a0kcBQ?autoplay=1&controls=1&rel=0";
            //   $("#videoInstitucional > iframe").attr("src", url);
            //    $("#videoInstitucional").slideDown();
                $.fancybox.open({
                    href: 'http://www.youtube.com/embed/4IUdNKEajpI?autoplay=1&controls=1&rel=0',
                    type: 'iframe',
                    padding: 5
                });
                actualizarPersonalizacion(_Personalizacion_VideoInstitucional_Ocultar, "true");
                
            }
            $("#titulo_Noticias").html(Helpers.LocalizationHelper.Footer.Titulo_Noticias);
            obtenerUltimasNoticias();
        });
        function cargarPagina_Default()
        {
            var configMapa =
                {
                    IdMapa: "mapa",
                    TituloMapa: "Semana del Árbol en {0} - {1}".format(_filtroActual.Pais.Nombre, _filtroActual.Periodo),
                    MostrarMapa: true,
                    MostrarBuscador: false,
                    MostrarFiltroMarcadores: true,
                    MostrarListaMarcadores: false,
                    MostrarBotonMiPosicionActual: false,
                    OnMapLoadComplete: onMapLoadComplete_Default,
                    Zoom: _googleMap.NivelZoom,
                    Direccion: _filtroActual.Pais.Nombre,
                    ImagenMarcador: null,
                    DeshabilitarMarcadores: false,
                    OnClickMap: null,
                    OnBuscarDireccion: null
                };
            inicializarMapa(configMapa);

            obtenerTotales();
            obtenerMensajes();
            obtenerUltimasNovedades();
            //obtenerActividadesRotativas();

            //obtener últimos feeds de Redes Sociales..
//            _facebookUrl = 'semanadelarbol';
//            _twitterUrl  = 'semanadelarbol';
//            inicializarFacebookFeed(_facebookUrl, 270, 220);
//            inicializarTwitterFeed(_twitterUrl);
        }
        function onMapLoadComplete_Default(idMapa)
        {
            obtenerDonantes(_filtroActual.Pais.Id, _filtroActual.Pais.Nombre, -1, null, null, null);
            obtenerActividades()
            obtenerPlantaciones();
        }
        function obtenerMensajes()
        {
            Helpers.AJAXHelper.doAjaxCall("Default.aspx/obtenerMensajes", "", obtenerMensajes_Respuesta);
        }
        function obtenerMensajes_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var mensajes = respuestaInfo.Valor;
                var lista = new Array();
                for (var i=0; i < mensajes.length; i++)
                {
                    lista[i] = mensajes[i].Texto;
                }
                $("#lblMensajeRotativo").text( lista[0] );
                rotarMensajes(lista, 1);
            }
        }
        function rotarMensajes(lista, indice)
        {
            timeout_Mensajes = setTimeout(function()
            {
                $("#lblMensajeRotativo").text(lista[indice]);
                
                if ( lista.length == indice )
                    indice = 0;
                else
                    indice++;
                
                rotarMensajes(lista, indice);
                
            }, 7000);
            //clearTimeout(timeout_Mensajes);
        }

        function obtenerUltimasNoticias() {
            var params = new Object();
            params.cantidad = 3;
            params.pais = _filtroActual.Pais.Nombre;

            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("Default.aspx/obtenerUltimasNoticias", jsonParams, obtenerUltimasNoticias_Respuesta);
        }
        function obtenerUltimasNoticias_Respuesta(datos) {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var listaNoticias = respuestaInfo.Valor;
                for (var i = 0; i < listaNoticias.length; i++) {
                    var noticiaInfo = listaNoticias[i];
                    agregarNoticia(i, noticiaInfo);
                }
                var indicadores = $('.noticias-indicators');
                var indicador = '<li class="link-ver-mas"><a href="Pages/Noticias.aspx">Ver más</a></li>';
                $(indicador).appendTo(indicadores);

                if (listaNoticias.length > 0) {
                    $("#pnlNoticias").fadeIn();
                }
            }
        }

        function agregarNoticia(position, noticiaInfo) {
            var imgDirectory = "Images/Sponsors/Secundarios/1/";
            var pagesFolder = "Pages/";
            var currentPage = getCurrentPageName();
            if (currentPage != _secciones.Default && currentPage != "") {
                imgDirectory = "../" + imgDirectory;
                pagesFolder = ""
            }

            /*

            var contenido = $("#pnlNoticias_Contenido_Lista");
            var img = '';
            if (!isEmpty(noticiaInfo.Foto1)) {
                img = $("<img />", { 'src': '..' + noticiaInfo.Foto1, 'alt': noticiaInfo.Titulo, 'data-description': noticiaInfo.Resumen });
            }
            else {
                img = $("<img />", { 'src': '../Images/Logo.gif', 'alt': noticiaInfo.Titulo, 'data-description': noticiaInfo.Resumen });
            }
            var val = "<span>" + noticiaInfo.Titulo + "</span>";
            var span = $(val);
            var link = $("<a />", { href: pagesFolder + "VerNoticia.aspx?idNoticia=" + noticiaInfo.Id, target: "_blank" });
            var item = $("<li />");
            img.appendTo(link);
            span.appendTo(link);
            link.appendTo(item);
            item.appendTo(contenido);
            */

            var imgSRC = '';
            if (!isEmpty(noticiaInfo.Foto1)) {
                imgSRC = noticiaInfo.Foto1;
            }
            else {
                imgSRC = "Images/Logo.gif";
            }

            var indicadores = $('.noticias-indicators');
            var items = $('.noticias-items');
            var indicador = '';
            var item = '';
            if (position == 0) {
                indicador = '<li data-target="#carouselNoticias" data-slide-to="' + position + '" class="active"><div class="img-cont"><img src="' + imgSRC + '"></div><p>' + noticiaInfo.Titulo + '</p></li>'
                item = '<div class="item active"><img src="' + imgSRC + '" alt="' + noticiaInfo.Titulo + '"><div class="carousel-caption"><h4>' + noticiaInfo.Titulo + '</h4><p>' + noticiaInfo.Resumen + '</p></div></div>';
            }
            else {
                indicador = '<li data-target="#carouselNoticias" data-slide-to="' + position + '"><div class="img-cont"><img src="' + imgSRC + '"></div><p>' + noticiaInfo.Titulo + '</p></li>'
                item = '<div class="item"><img src="' + imgSRC + '" alt="' + noticiaInfo.Titulo + '"><div class="carousel-caption"><h4>' + noticiaInfo.Titulo + '</h4><p>' + noticiaInfo.Resumen + '</p></div></div>';
            }
            $(indicador).appendTo(indicadores);
            $(item).appendTo(items);            
        }
    </script>

    <!-- Últimas novedades -->
    <script type="text/javascript" >
        function obtenerUltimasNovedades()
        {
            var params              = new Object();
                params.cantidad     = 3;
                params.pais         = _filtroActual.Pais.Nombre;
                params.periodo      = _filtroActual.Periodo;
                
            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("Default.aspx/obtenerUltimasNovedades", jsonParams, obtenerUltimasNovedades_Response);
        }
        function obtenerUltimasNovedades_Response(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var listas = respuestaInfo.Valor;

                //limpieza de listas de novedades vacías..
                for (var i = 0; i < listas.length; i++)
                {
                    if (listas[i].Valor.length == 0)
                    {
                        listas.splice(i, 1);
                        i--;
                    }
                }

                var contenido = generarContenidoNovedades(listas, 0, 0);
                $("#listaNovedades").empty().hide().html(contenido).fadeIn("slow");

                rotarNovedades(listas, 1, 0);
            }
        }
        function rotarNovedades(listas, indiceLista, indiceItem)
        {
            
            timeout_Mensajes = setTimeout(function()
            {
                var contenido = generarContenidoNovedades(listas, indiceLista, indiceItem);
                if (isEmpty(contenido))
                {
                    clearTimeout(timeout_Mensajes);
                }
                else
                {
                    $("#listaNovedades").empty().hide().html(contenido).fadeIn("slow");
                    
                    //cambio de n° de lista..
                    if (listas.length == (indiceLista + 1))
                    {
                        indiceLista = 0;
                        if (listas[indiceLista].Valor.length == (indiceItem + 1))
                            indiceItem = 0;
                        else
                            indiceItem++;
                    }
                    else
                    {
                        indiceLista++;
                    }
                    rotarNovedades(listas, indiceLista, indiceItem);
                }
            }, 8000);
        }
        function generarContenidoNovedades(listas, indiceLista, indiceItem)
        {
            var actividadInfo, plantacionInfo, adopcionInfo, donacionInfo, mensajeInfo;
            var lista = listas[indiceLista];

            if ( isEmpty(lista) )
                return null;

            if ( lista.Texto.toLowerCase() == "actividades" )
                actividadInfo = lista.Valor[indiceItem];
            if ( lista.Texto.toLowerCase() == "plantaciones" )
                plantacionInfo = lista.Valor[indiceItem];
            if ( lista.Texto.toLowerCase() == "adopciones" )
                adopcionInfo = lista.Valor[indiceItem];
            if ( lista.Texto.toLowerCase() == "donaciones" )
                donacionInfo = lista.Valor[indiceItem];
            if ( lista.Texto.toLowerCase() == "mensajes" )
                mensajeInfo = lista.Valor[indiceItem];
            
            var row = '';

            if ( !isEmpty(actividadInfo) )
            {
                row += '<img src="../Images/Indicadores/actividades.png" alt="Actividad" />';
                row += '<div class="details"><a href="javascript:verUsuario(' + actividadInfo.IdUsuario + ');" >' + actividadInfo.Usuario + '</a> agendó una <b>actividad</b> en ' + actividadInfo.Provincia + ', el ' + actividadInfo.FechaYHora + '.</div>';
            }
            if ( !isEmpty(plantacionInfo) )
            {
                row += '<img src="../Images/Indicadores/plantaciones.png" alt="Plantación" />';
                row += '<div class="details"><a href="javascript:verUsuario(' + plantacionInfo.IdUsuario + ');" >' + plantacionInfo.Usuario + '</a> realizó una <b>plantación</b> de ' + plantacionInfo.Cantidad + ' ' + plantacionInfo.Especie + '(s).</div>';
            }
            if ( !isEmpty(adopcionInfo) )
            {
                row += '<img src="../Images/Indicadores/adopciones.png" alt="Adopción" />';
                row += '<div class="details"><a href="javascript:verUsuario(' + adopcionInfo.IdUsuario + ');" >' + adopcionInfo.Usuario + '</a> realizó una <b>adopción</b> de ' + adopcionInfo.Cantidad + ' ' + adopcionInfo.Arbol + '(s) a <a href="javascript:verUsuario(' + adopcionInfo.IdDonante + ');" >' + adopcionInfo.Donante + '</a>.</div>';
            }
            if ( !isEmpty(donacionInfo) )
            {
                row += '<img src="../Images/Indicadores/donaciones.png" alt="Donación" />';
                row += '<div class="details"><a href="javascript:verUsuario(' + donacionInfo.IdDonante + ');" >' + donacionInfo.Donante + '</a> realizó una <b>donación</b> de ' + donacionInfo.Cantidad + ' ' + donacionInfo.Especie + '(s).</div>';
            }
            if (!isEmpty(mensajeInfo)) {
                row += '<div class="details marquee" title="' + mensajeInfo.Texto + '">Sabias que ' + mensajeInfo.Texto + '.</div>';
            }

            return "<div class='entrada'>" + row + "</div>";
        }
    </script>
    <!-- Fin de [Últimas novedades] -->

    <!-- Social Media feed -->
    <!--<script type="text/javascript" src="http://widgets.twimg.com/j/2/widget.js" ></script>-->
    <script type="text/javascript" >
        function inicializarTwitterFeed(url)
        {
            //url = url.replace('http://twitter.com/#!/', '');
            url = 'http://twitter.com/' + url;
            new TWTR.Widget
            ({
                id: 'twitterFeed',
                version: 2,
                type: 'profile',
                interval: 6000,
                width: 240,
                height: 200,
                theme:
                {
                    shell:
                    {
                        background: '#333634',
                        color: '#ffffff'
                    },
                    tweets:
                    {
                        background: '#ffffff',
                        color: '#444444',
                        links: '#506d41'
                    }
                },
                features:
                {
                    scrollbar: false,
                    loop: true,
                    live: true,
                    hashtags: true,
                    timestamp: true,
                    avatars: true,
                    behavior: 'default'
                },
                ready: function()
                { 
                }              
            }).render().setUser(url).start();
        }
        function inicializarFacebookFeed(url, width, height)
        {
            var content = '<div class="fb-like-box" data-href="http://www.facebook.com/' + url + '" data-width="' + width + '" data-height="' + height + '" data-show-faces="true" data-border-color="#315C99" data-stream="false" data-header="false"></div>';
            $("#facebookFriends").html(content);
        }
    </script>
    
    <!--HTML-->
    <div id="content"  >

        <div id="videoInstitucional" style="display:none" >
            <li><a id="fancybox-manual-b" href="javascript:;">Open single item, custom options</a></li>
            <iframe width="100%" height="450px" src="http://" frameborder="0" allowfullscreen="" style="border:1px solid black"></iframe>
        </div>

        <div class="row bloque-ultimo">
            <div class="lo-ultimo">
                 <p>Lo Último</p>
            </div>
            <div id="listaNovedades" >
            </div>
        </div>  
        
        <div class="row bloque-mapa">
            <div id="periodos" class="periodos">
             
            </div>       
            <div class="mapa">
                <Mapa:Mapa ID="Mapa1" runat="server" />
            </div>       
            <div class="indicadores">
                <div id="plantaciones" class="indicador">
                   <span class="tooltipBottom" title="Cantidad total de <b>Plantaciones</b> registradas" >
                      <label id="lblArboles"  class="titulo-indicador">Plantaciones</label>
                      <label id="lblCantidadPlantacionesTotales" class="valor-indicador">0</label>                            
                   </span>
                </div>
                <div id="donaciones" class="indicador">
                   <span class="tooltipBottom" title="Cantidad total de <b>Donaciones</b> registradas" >
                     <label id="lblDonaciones" class="titulo-indicador" >Donaciones</label>
                     <label id="lblCantidadDonacionesTotales" class="valor-indicador">0</label>                            
                   </span>
                </div>
                <div id="adopciones" class="indicador">
                   <span class="tooltipBottom" title="Cantidad total de <b>Adopciones</b> registradas" >
                      <label id="lblAdopciones" class="titulo-indicador">Adopciones</label>
                      <label id="lblCantidadAdopcionesTotales" class="valor-indicador">0</label>                            
                   </span>
                </div>
                <div id="individuos" class="indicador">
                   <span class="tooltipBottom" title="Cantidad total de <b>Individuos</b> registrados" >
                      <label id="lblIndividuos"  class="titulo-indicador">Individuos</label>
                      <label id="lblCantidadIndividuosTotales" class="valor-indicador">0</label>                           
                   </span>
                </div>
                <div id="instituciones" class="indicador">
                   <span   class="tooltipBottom" title="Cantidad total de <b>Instituciones</b> registradas" >
                      <label id="lblInstituciones"  class="titulo-indicador">Instituciones</label>
                      <label id="lblCantidadInstitucionesTotales" class="valor-indicador">0</label>                            
                   </span>
                </div>
                <div id="municipios" class="indicador">
                   <span class="tooltipBottom" title="Cantidad total de <b>Municipios</b> registrados" >
                      <label id="lblMunicipios"  class="titulo-indicador">Municipios</label>
                      <label id="lblCantidadMunicipiosTotales"  class="valor-indicador">0</label><br />                            
                   </span>
                </div>
                <div id="co2capturados" class="indicador">
                   <span class="tooltipBottom" title="Toneladas de <b>Dióxido de Carbono</b> capturado.<br /><br />Para conocer más acerca del potencial de captura de CO2 de los árboles y hacer un cálculo más detallado, podés visitar el sitio web www.balancecero.gov.ar" >
                       <label id="lblCO2"  class="titulo-indicador">CO<sub>2</sub> capturado</label>
                       <label id="lblCantidadCapturasCO2Totales" class="valor-indicador" >0</label><label class="valor-indicador" >&nbsp;tons.</label>                            
                   </span>
                </div>
            </div>     
        </div>       
        <div class="row bloque-actividades">
            <Agenda:Agenda ID="Agenda" runat="server" />
        </div>

        <div class="row bloque-noticias">
            <div id="titulo_Noticias" class="titulo centrado"></div>
            <div id="carouselNoticias" class="carousel slide" data-ride="carousel">  
                  <ol class="carousel-indicators noticias-indicators">
                  </ol>

                  <div class="carousel-inner noticias-items" role="listbox">    
                  </div> 
               <!--   <a class="left carousel-control" href="#carouselNoticias" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carouselNoticias" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>-->
            </div>
        </div>
        <!-- Mensajes rotativos -->
  <!--      <div id="mensajeRotativo" class="banner" >
            <fieldset  >
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="72px" />
                    <col width="500px" />
                </colgroup>
                <tr>
                    <td>
                        <p>¿Sabías que?</p>
                    </td>
                    <td>
                        <label id="lblMensajeRotativo" ></label>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>-->
        <!-- Fin de [Mensajes rotativos] --> 

               <!-- Ultimas novedades -->
      <!--  <div id="ultimasNovedades" class="banner" >
            <fieldset >
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <colgroup>
                        <col width="72px" />
                        <col width="500px" />
                    </colgroup>
                    <tr>
                        <td class="lo-ultimo">
                            <p>Lo Último</p>
                        </td>
                        <td>
                            <div id="listaNovedades" >
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>-->
        <!-- Fin de [Ultimas novedades] -->

        <!-- Indicadores -->
      <!--  <div id="indicadores" >            
            <table border="0" cellpadding="0" cellspacing="0" >
                <colgroup>
                    <col width="150px" />
                    <col width="120px" />
                    <col width="120px" />
                    <col width="120px" />
                    <col width="120px" />
                    <col width="120px" />
                    <col width="120px" />
                    <col width="130px" />
                </colgroup>
                <tr>
                    <td>
                          <div style="padding-left:5px;" class="tooltipTop" title="Seleccione el año para filtrar datos" >
                               <select id="listaPeriodos" style="display:none" ></select>
                          </div>                                
                    </td>
                    <td id="plantaciones" class="indicador">
                        <span class="tooltipBottom" title="Cantidad total de <b>Plantaciones</b> registradas" >
                            <label id="lblArboles"  class="titulo-indicador">Plantaciones</label>
                            <label id="lblCantidadPlantacionesTotales" class="valor-indicador">0</label>                            
                        </span>
                    </td>
                    <td id="donaciones" class="indicador">
                        <span class="tooltipBottom" title="Cantidad total de <b>Donaciones</b> registradas" >
                            <label id="lblDonaciones" class="titulo-indicador" >Donaciones</label>
                            <label id="lblCantidadDonacionesTotales" class="valor-indicador">0</label>                            
                        </span>
                    </td>
                    <td id="adopciones" class="indicador">
                        <span class="tooltipBottom" title="Cantidad total de <b>Adopciones</b> registradas" >
                             <label id="lblAdopciones" class="titulo-indicador">Adopciones</label>
                             <label id="lblCantidadAdopcionesTotales" class="valor-indicador">0</label>                            
                        </span>
                    </td>
                    <td id="individuos" class="indicador">
                        <span class="tooltipBottom" title="Cantidad total de <b>Individuos</b> registrados" >
                            <label id="lblIndividuos"  class="titulo-indicador">Individuos</label>
                            <label id="lblCantidadIndividuosTotales" class="valor-indicador">0</label>                           
                        </span>
                    </td>
                    <td id="instituciones" class="indicador">
                        <span   class="tooltipBottom" title="Cantidad total de <b>Instituciones</b> registradas" >
                            <label id="lblInstituciones"  class="titulo-indicador">Instituciones</label>
                            <label id="lblCantidadInstitucionesTotales" class="valor-indicador">0</label>                            
                        </span>
                    </td>
                    <td id="municipios" class="indicador">
                        <span class="tooltipBottom" title="Cantidad total de <b>Municipios</b> registrados" >
                            <label id="lblMunicipios"  class="titulo-indicador">Municipios</label>
                            <label id="lblCantidadMunicipiosTotales"  class="valor-indicador">0</label><br />                            
                        </span>
                    </td>
                    <td  id="co2capturados" class="indicador">
                        <span class="tooltipBottom" title="Toneladas de <b>Dióxido de Carbono</b> capturado.<br /><br />Para conocer más acerca del potencial de captura de CO2 de los árboles y hacer un cálculo más detallado, podés visitar el sitio web www.balancecero.gov.ar" >
                            <label id="lblCO2"  class="titulo-indicador">CO<sub>2</sub> capturado</label>
                            <label id="lblCantidadCapturasCO2Totales" class="valor-indicador" >0</label><label class="valor-indicador" >&nbsp;tons.</label>                            
                        </span>
                    </td>
                </tr>
            </table>
        </div>-->
        <!-- Fin de [Indicadores] --> 

        <!-- Barra Redes Sociales -->
        <%--
        <div id="feedRedesSociales" class="feedRedesSociales" style="float:left; " >
            <!-- Twitter Feed -->
            <div id="twitterFeed" class="tweet" style="width:240px; overflow:hidden;" ></div>
            <br />
            <!-- Facebook Feed -->
            <div id="facebookFriends" >
                <fb:like-box href="http://semanadelarbol.org" width="270" height="220" show_faces="true" stream="true" header="true"></fb:like-box>
            </div>
        </div>
        --%>
        <!-- Fin de [Barra Redes Sociales]-->

        <!--
        <a id="videoInstitucional" class="fancybox" href="http://" ></a>
        -->

    </div>

</asp:Content>