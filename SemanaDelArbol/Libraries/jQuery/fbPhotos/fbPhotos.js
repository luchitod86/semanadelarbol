//Facebook Graph API reference: https://developers.facebook.com/docs/reference/api/album/
$.fn.fbPhotos = function(album, limit, onLoadPhotoFunction, onLoadAlbumFunction)
{
	var _base;
    var _onLoadPhotoFunction = onLoadPhotoFunction;
    var _onLoadAlbumFunction = onLoadAlbumFunction;

	if ( $(this).size() > 0 )
	{
		_base = $(this);
		var topLimit = ( limit ? limit : 30 );
		var albumId = ( album ? album : ($(this).attr("fbAlbum") ? $(this).attr("fbAlbum") : '0') );
		requestPhotos();
	}
	function requestPhotos()
	{
        $(_base).empty();

		var data = {};
		$.ajax
        ({
            url: 'https://graph.facebook.com/' + albumId + '/photos?limit=' + topLimit,
			type : 'GET',
			dataType : 'jsonp',
			data : data,
			success: function(obj)
			{
				var results = [];
				if ( obj.error )
					return false;

				for (var i = 0; i < obj.data.length; i++)
				{
					if ( obj.data[i].images )
						results[i] = { 'id' : obj.data[i].id, 'img': obj.data[i].images[2].source, 'link': obj.data[i].link };
				}
				i = 0;
				for (var i in results)
				{
					$(_base).append( getPhoto(results[i]) )
					i++;
					if ( i >= topLimit )
						break;
				}

                if ( _onLoadAlbumFunction != undefined && _onLoadAlbumFunction != null && _onLoadAlbumFunction != "" )
                    _onLoadAlbumFunction.call(this, results);
			},
			error: function(obj)
			{
                //
			}
		});
	}
	function getPhoto(imgObject)
	{
		var id      = imgObject.iad;
		var img     = imgObject.img;
		var link    = imgObject.link;

		var wrap = $('<div />').attr
		({
			'class' : 'fb-photo',
			'id' : id
		});

		var avatar = new Image();
		    avatar.src = img;

		var _avatar = $('<a />')
			.attr('href', link)
			.attr('target', '_blank')
			.attr('class', 'avatar')
			.html(avatar);
		
		$(wrap).append($(_avatar));

        if ( _onLoadPhotoFunction != undefined && _onLoadPhotoFunction != null && _onLoadPhotoFunction != "" )
            _onLoadPhotoFunction.call(this, imgObject);

		return wrap;
	}
};