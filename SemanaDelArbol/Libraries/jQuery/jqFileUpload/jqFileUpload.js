﻿(function($)
{
	//If jQuery isn't loaded, return..
	if ( ! $) return;

    var settings = null;

    //Public constructor
	$.fileUpload = function(options)
    {
		return new uploadFile(options);		
	};

    //Helper class
    var Helper = 
    {
        form: null,
        container: null,
        extensions: null,
        filesList: null,
        deleteImage: null,
        status: null,
        wrapper : null,
        input : null,
        iframe: null,
        selectTrigger: null,
        uploadTrigger: null
    }

    //Constructor..
	var uploadFile = function(options)
    {
        this.settings =
        {
			action: "",
            container: "",		
            deleteImage: "",
            extensions: "",
			data: {},
            onSelect: function(file, extension) {},
			onSubmit: function(file, extension) {},
			onComplete: function(file, response) {},
			onSuccess: function(file){},
			onError: function(file, response){}
		};
        settings = this.settings;

		//Merge the users options with defaults..
		$.extend(this.settings, options);

        Helper.extensions = this.settings.extensions;

        var container = $("#" + this.settings.container);
            Helper.container = container;
            Helper.container.addClass('jqFileUploadPanel');
            Helper.container.css
            ({
                'position': 'relative',
                'width': '97%'
            });

        var table = $('<table border="0" cellpadding="2" cellspacing="0" width="280px" ></table>');
            table.appendTo(Helper.container);
        var colgroup = $('<colgroup><col width="50%" /><col width="50%" /></colgroup>');
            colgroup.appendTo(table);
        var tr = $('<tr></tr>');
            tr.appendTo(table);
        var tdSelect = $('<td><input id="btnSelectFile" type="button" value="Seleccionar.." style="width:135px" /></td>');                
            tdSelect.appendTo(tr);
        var tdUpload = $('<td><input id="btnUploadFile" type="button" value="Subir archivo" style="width:135px" /></td>');                
            tdUpload.appendTo(tr);

        var statusDiv = $('<div style="padding: 5px 0px 5px 0px" ></div>');
            statusDiv.css
            ({
                'padding': '5px 0px 5px 0px'
            });
            statusDiv.append('<b>Estado:</b>&nbsp;');
        var status = $('<label id="status" ></label>');
            status.appendTo(statusDiv);
            statusDiv.appendTo(Helper.container);
        Helper.status = status;

        var filesList = $('<ul id="filesList" ></ul>');
            filesList.css
            ({
                'list-style-type': 'none',
                'padding-left': '0px'
            });
            filesList.appendTo(Helper.container);
        Helper.filesList   = filesList;
        Helper.deleteImage = this.settings.deleteImage;

        $("#btnSelectFile, #btnUploadFile").button();

        Helper.selectTrigger = $("#btnSelectFile");
        Helper.uploadTrigger = $("#btnUploadFile");

        Helper.iframe   = this.create_iframe("jqFileUpload_iframe");
        Helper.form     = this.create_form();
        Helper.wrapper  = this.create_wrapper(Helper.selectTrigger);
        Helper.input    = this.create_input(Helper.container);
	}

    //Set class' methods
	uploadFile.prototype =
    {
        //Creates the wrapper
        create_wrapper : function(btnSelect)
        {
            var wrapper = $("<div id='jqFileUpload_Wrapper' ></div>");
                wrapper.insertAfter(btnSelect)
                wrapper.append(btnSelect);
            return wrapper;
        },
        //Creates input[file] with unique name
        create_input : function(container)
        {
            var input = $('<input id="jqFileUpload_inputFile" type="file" title="Seleccionar.." />');
                input.attr('name', 'jqFileUpload_inputFile'); //important! If name attribute is not set, won't submit file.
			    input.css
                ({
                        'background-color': 'Yellow',
					    'position' : 'absolute',
                        'top': 0,
                        'margin-left': 2,
                        'margin-top': 9,
					    'padding': 2,
					    'width': '130px',
					    //'heigth': '35px',
                        'overflow': 'hidden',
					    'opacity': 0
                });
                input.change
                (
                    function()
                    {
                        var fileName = $(this).val();
					    if (fileName == '')
						    return;

                        select(fileName, get_extension(fileName));
                    }
                );
			    input.appendTo(container);
            return input;
        },
        // Creates iframe with unique name
        create_iframe : function(iframeName)
        {
		    //Get unique name (can't use getTime, because it sometimes return same value in safari)
		    var name = 'iframe_au' + get_uid();

		    //Create iframe, so we dont need to refresh page..
		    var iframe = $("<iframe name='" + iframeName + "'></iframe>");
                iframe.css("display", "none");
			    iframe.appendTo("body");

            return iframe;
        },
		//Creates form, that will be submitted to iframe
		create_form : function()
        {
			//enctype must be specified here because changing this attr on the fly is not allowed..
			var form = $('<form id="jqFileUpload_form" method="post" enctype="multipart/form-data" ></form>');
                form.appendTo("body")
				form.attr
                ({
                    "id"     : "jqFileUpload_form",
					"action" : settings.action,
					"target" : Helper.iframe.attr('name')					
				});
            return form;
		}
    }

    //Actions taken post file selection.
    function select(fileName, extension)
    {
        //get filename from input
		var fileName = file_from_path(fileName);

        //check if file extension is allowed..
        var allowed = validateExtension(fileName);
        if ( !allowed )
        {
            updateStatus('Tipo de archivo no válido.');
            return;
        }

        //call custom onSelect..
        settings.onSelect.call(this, fileName, extension);

        Helper.uploadTrigger.unbind('click');
        Helper.uploadTrigger.click( function() { upload(fileName, extension); } );

        //add pending file to list..
        addPendingFileToList(fileName);
    }
    function validateExtension(fileName)
    {
        var arrExtensions = Helper.extensions.trim().split(',');
        var extension = get_extension(fileName);
        var allowed = false;
        for(var i=0; i < arrExtensions.length; i++)
        {
            if ( arrExtensions[i].toLowerCase() == extension.toLowerCase() )
            {
                allowed = true;
                break;
            }
        }
        return allowed
    }
    //Adds a file to filelist
    function addPendingFileToList(fileName)
    {
        if ( Helper.filesList[0].children.length > 0 )
        {
            updateStatus("No se puede subir más de 1 archivo a la vez.")
            return;
        }

        var id = "listItem_" + get_uid();
        var btnDelete = $('<img src="' + Helper.deleteImage + '" style="vertical-align:middle; cursor:pointer" />');
            btnDelete.click( function() { deletePendingFile(id); } );
        var listItem = $('<li id="' + id + '" ></li>');
            listItem.append(btnDelete);
            listItem.append("&nbsp;" + fileName);
        Helper.filesList.append(listItem);
    }
    //Removes a file from filelist
    function deletePendingFile(id)
    {
        var listItem = $("#" + id);
            listItem.remove();
    }
    //Updates current status
    function updateStatus(message)
    {
        var status = Helper.status;
            status.text(message);
            status.addClass("ui-state-highlight");
            status.fadeIn();
        setTimeout(function () { status.fadeOut('slow', function () { /*..*/ } ); }, 3000);
    }
    //Submits form to server, uploading all fileinput controls inside
    function upload(fileName, extension)
    {
        var result = settings.onSubmit.call(this, fileName, extension);
        if ( result == false )
            return;

        //remove previously appended fileInputs..
        Helper.form.empty();

        //append fileInput to Form..
        Helper.input.appendTo(Helper.form);

        //submit form..
        Helper.form.submit();

        //load iframe to get response from server..
        Helper.iframe.load
        (
            function()
            {
				var response = Helper.iframe.contents().find('body').html();
                var responseInfo = jQuery.parseJSON(response);
				if (responseInfo.Result)
                {
					settings.onSuccess.call(this, fileName, responseInfo);
                    updateStatus("Archivo subido.");
                    Helper.filesList.empty();
                }
                else
				{
                    updateStatus("Error en el servidor.");
                	settings.onError.call(this, fileName, responseInfo);	
                }
			}
        );

        //create new inputFile (to continue uploading files)..
        var input = uploadFile.prototype.create_input(Helper.container);
            Helper.input = input;
    }

   
	//Generates unique id
	var get_uid = function()
    {
		var uid = 0;
		return function()
        {
			return uid++;
		}
	}();
    //Gets filename from filepath
	function file_from_path(file)
    {
		var i = file.lastIndexOf('\\');
		if (i !== -1 )
        {
			return file.slice(i+1);
		}			
		return file;				
	}
    //Gets file extension..
    function get_extension(file)
    {
		var i = file.lastIndexOf('.');
		if (i !== -1 )
        {
			return file.slice(i+1);				
		}			
		return '';	
	}

})(jQuery);