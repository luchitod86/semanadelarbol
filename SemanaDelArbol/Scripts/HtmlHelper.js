﻿function HtmlHelper()
{
    return {
        Button:
        {
            create: function (id, value, className, style, attribute)
            {
                var _id         = !isEmpty(id) ? id : "";
                var _value      = !isEmpty(value) ? value : "";
                var _class      = !isEmpty(className) ? className : "";
                var _style      = !isEmpty(style) ? style : "";
                var _attribute  = !isEmpty(attribute) ? attribute : "";
                return '<input type="button" id="{0}" value="{1}" class="button {2}" style="{3}" {4} />'.format(_id, _value, _class, _style, _attribute);
            }
        },
        Table:
        {
            getAllRows: function(idTable)
            {
                return $("#{0} > tbody > tr".format(idTable));
            },
            insertNewRow: function (idTable, idRow)
            {
                var row = $("<tr />", { id: idRow } );
                var table = $("#{0}".format(idTable));
                    table.append(row);
                return row;
            },
            insertFooterRow: function (idTable, idRow)
            {
                var row = $("<tr />", { id: idRow });
                var footer = $("<tfoot />");
                    footer.append(row);
                var table   = $("#{0}".format(idTable));
                    table.append(footer);
                return row;
            },
            addNewRow: function(idTable, rowContent, rowProperty, rowValue, insertFirst)
            {
                var row = $("<tr />");
                if ( !isEmpty(rowProperty) )
                    row.attr(rowProperty, rowValue)
                    row.append(rowContent);
                    row.find('td').wrapInner('<div style="display:none;" />');

                var table = $("#{0}".format(idTable));
                if (insertFirst)
                    table.prepend(row);
                else
                    table.append(row);

                row.children().children().slideDown("fast");
            },
            deleteRow: function(idTable, rowProperty, rowValue)
            {
                var row = $('#{0} > tbody > tr[{1}="{2}"]'.format(idTable, rowProperty, rowValue));
                    row.find('td').wrapInner('<div style="display:block;" />').parent().find('td > div').slideUp("fast", function () { $(this).parent().parent().remove(); });
            },
            deleteFooterRow: function(idTable)
            {
                var row = $('#{0} > tfoot'.format(idTable));
                    row.remove();
            },
            deleteAllRows: function (idTable)
            {
                var rows = $("#{0} > tbody".format(idTable));
                    rows.empty();
            },
            addCellData: function (tr, cellContent, className, attr, attrValue)
            {
                var td = $("<td />");
                    td.addClass(className);

                if (!isEmpty(attr))
                    td.attr(attr, attrValue)

                    td.html(cellContent);

                tr.append(td);
            },
            crearPaginador: function(paginaActual, totalPaginas, htmlSelector, urlDestino) {
                var paginador = $('#'+htmlSelector);

                if(paginaActual != 1)
                {
                    var paginaAnterior = parseInt(paginaActual) - 1;
                    paginador.append('<a class="page larger" href="' + urlDestino + '?PaginaActual=' + paginaAnterior + '">Anterior</a>');
                }
                for (var a = 1; a <= totalPaginas; a++)
                {
                    if (a == paginaActual)
                    {                            
                        paginador.append('<span class="current">' + a + '</span>');
                    }                        
                    else
                    {
                        if ((a == 1) || ((a == paginaActual - 1) && (paginaActual - 1 >= 1)) || ((a == paginaActual - 2) && (paginaActual - 2 >= 1)) || ((a == parseInt(paginaActual) + 2) && (parseInt(paginaActual) + 2 <= totalPaginas)) || ((a == parseInt(paginaActual) + 1) && (parseInt(paginaActual) + 1 <= totalPaginas)) || (a == totalPaginas))
                            paginador.append('<a class="page larger" href=' + urlDestino + '?PaginaActual=' + a + '>' + a + '</a>');
                    }                 
                }      
                if (totalPaginas > 1 && totalPaginas != paginaActual)
                {                    
                    var paginaSiguiente = parseInt(paginaActual) + 1;
                    paginador.append('<a class="page larger" href="' + urlDestino + '?PaginaActual=' + paginaSiguiente + '">Siguiente</a>');
                }   
            },
            addExportPanel: function (idTable)
            {
                var btnExport = $("<input />", { type: "button", id: "btnExportar", value: "" });
                    btnExport.addClass("button btnGuardar");
                    btnExport.width("37px");
                    btnExport.button();
                var container = $("<span />", { style: "z-index:0" });
                    container.css({ position: "absolute", left: "10px", top: "-10px" });
                    container.append(btnExport);

                var btnExportExcel = $("<input />", { type: "button", id: "btnExportarExcel", value: "" });
                    btnExportExcel.addClass("button btnExportarExcel");
                    btnExportExcel.width("37px");
                    btnExportExcel.button();
                    btnExportExcel.click(function () { exportTo(idTable, "excel"); });

                var btnExportWord = $("<input />", { type: "button", id: "btnExportarWord", value: "" });
                    btnExportWord.addClass("button btnExportarWord");
                    btnExportWord.width("37px");
                    btnExportWord.button();
                    btnExportWord.click(function () { exportTo(idTable, "word"); });

                var btnExportPdf = $("<input />", { type: "button", id: "btnExportarPdf", value: "" });
                    btnExportPdf.addClass("button btnExportarPdf");
                    btnExportPdf.width("37px");
                    btnExportPdf.button();
                    btnExportPdf.click(function () { exportTo(idTable, "pdf"); });

                var btnExportHtml = $("<input />", { type: "button", id: "btnExportarHtml", value: "" });
                    btnExportHtml.addClass("button btnExportarHtml");
                    btnExportHtml.width("37px");
                    btnExportHtml.button();
                    btnExportHtml.click(function () { exportTo(idTable, "html"); });

                var btnPrint = $("<input />", { type: "button", id: "btnImprimir", value: "" });
                    btnPrint.addClass("button btnImprimir");
                    btnPrint.width("37px");
                    btnPrint.button();
                    btnPrint.click(function () { print(idTable); });

                    var containerOpts = $("<span />", { style: "z-index:1; display:none; background-color:#eee;border: 1px solid #ccc;border-radius: 4px; padding-left: 10px;" });
                    containerOpts.css({ position: "absolute", left: "0px", top: "-30px" });

                    var label = $("<span />", { style: "vertical-align:middle" });
                    label.html("Exportar: ");

                    containerOpts.append(label);
                    containerOpts.append(btnExportExcel);
                    containerOpts.append(btnExportWord);
                    containerOpts.append(btnExportPdf);
                    containerOpts.append(btnExportHtml);
                    containerOpts.append(btnPrint);

                var cls = "expandido";
                btnExport.click(function () {
                    if (!containerOpts.hasClass(cls)) {
                        containerOpts.slideDown("fast");
                        containerOpts.animate
                        (
                            { top: -50 }, 'fast', 'swing',
                            function () {
                                $(this).addClass(cls);
                            }
                        );
                    }
                    else {
                        containerOpts.animate
                        (
                            { top: -20 }, 'fast', 'swing',
                            function () {
                                $(this).removeClass(cls);
                                $(this).slideUp("fast");
                            }
                        );
                    }
                });

                var table = $('#{0}'.format(idTable));
                var parent = table.parent();
                parent.css({ "position": "relative" });
                parent.prepend(container);
                parent.prepend(containerOpts);

        /*        activarMensajesDeAyuda(".btnExportarExcel", "Mensaje de Ayuda", "Exportar a Excel", "top");
                activarMensajesDeAyuda(".btnExportarWord", "Mensaje de Ayuda", "Exportar a Word", "top");
                activarMensajesDeAyuda(".btnExportarPdf", "Mensaje de Ayuda", "Exportar a Pdf", "top");
                activarMensajesDeAyuda(".btnExportarHtml", "Mensaje de Ayuda", "Exportar a página web", "top");
                activarMensajesDeAyuda(".btnImprimir", "Mensaje de Ayuda", "Imprimir", "top");*/

                function exportTo(idTable, format) {
                    var content = $('#{0}'.format(idTable))[0].outerHTML;
                    content = $.base64.encode(content);
                    var mimeType = "";
                    switch (format) {
                        case "word":
                            mimeType = "application/msword";
                            break;
                        case "excel":
                            mimeType = "application/vnd.ms-excel";
                            break;
                        case "pdf":
                            mimeType = "application/pdf";
                            break;
                        case "html":
                            mimeType = "text/html";
                            break;
                    }
                    window.open('data:{0};base64,'.format(mimeType) + content);
                }
                function print(idTable) {
                    var content = $('#{0}'.format(idTable))[0].outerHTML;
                    var printwin = window.open('', '', 'left=100,top=100,width=400,height=400,tollbar=0,scrollbars=1,status=0,resizable=1');
                    printwin.document.write(content);
                    printwin.document.close();
                    printwin.focus();
                    printwin.print();
                    printwin.close();
                }
            }
        }
    };
}