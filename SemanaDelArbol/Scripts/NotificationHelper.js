﻿/*
    NOTE:
    Purpose of this helper class is only to be a provide easy access to NotificationDialog.ascx public methods.
*/

var NotificationHelper = new Object();
    NotificationHelper.showMessage      = _showMessage;
    NotificationHelper.showInfo         = _showInfo;
    NotificationHelper.showWarning      = _showWarning;
    NotificationHelper.showConfirm      = _showConfirm;
    NotificationHelper.showError        = _showError;

/* initializeDialog */  
function _showMessage(message, title, customImage, onConfirm, showSharePanel)
{
    showNotification(_notificationType.Message, message, title, customImage, onConfirm, null, showSharePanel);
}
function _showInfo(message, title, customImage, onConfirm, showSharePanel)
{
    showNotification(_notificationType.Info, message, title, customImage, onConfirm, null, showSharePanel);
}
function _showWarning(message, title, customImage, onConfirm)
{
    showNotification(_notificationType.Warning, message, title, customImage, onConfirm, null, null)
}
function _showConfirm(message, title, customImage, onConfirm, onCancel)
{
    showNotification(_notificationType.Confirm, message, title, customImage, onConfirm, onCancel, null)
}
function _showError(message, title, customImage)
{
    showNotification(_notificationType.Error, message, title, customImage, null, null, null)
}