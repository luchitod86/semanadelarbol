﻿/* Date Units */
var _DateUnits = new Object();
    _DateUnits.Day   = "day";
    _DateUnits.Month = "month";
    _DateUnits.Year  = "year";

var DateTimeHelper = new Object();
/* Methods */
    DateTimeHelper.AddUnits       = _addUnits;
    DateTimeHelper.ConvertToDate  = _convertToDate;
    DateTimeHelper.FormatDate     = _formatDate;
    DateTimeHelper.GetCurrentDate = _getCurrentDate;
    DateTimeHelper.GetCurrentTime = _getCurrentTime;
    DateTimeHelper.GetDateParts   = _getDateParts;
    DateTimeHelper.GetDay         = _getDay;
    DateTimeHelper.GetMonth       = _getMonth;
    DateTimeHelper.GetYear        = _getYear;
/* Enums */    
    DateTimeHelper.DateUnits      = _DateUnits;

/* Dates */
function _addUnits(date, unit, amount)
{
    switch ( unit )
    {
        case _DateUnits.Day:
            date.setDate( date.getDate() + amount );
            break;
        case _DateUnits.Month:
            if ( amount >= 0 )
                date.setMonth( date.getMonth() + amount ); /* date.setMonth( (date.getMonth() +1) + amount ); */
            else
                date.setMonth( date.getMonth() + amount );
            break;
        case _DateUnits.Year:
            date.setFullYear( date.getFullYear() + amount );
            break;
    }
    return date;
}
function _convertToDate(date, splitter)
{
    /* Example: "2013-04-17T03:00:00" */
    var _splitter = '-';
    if ( !isEmpty(splitter) )
        _splitter = splitter;
    var arrDateParts = date.split( _splitter );
    var year    = parseInt( arrDateParts[0] );
    var month   = parseInt( arrDateParts[1] );
    var day     = parseInt( arrDateParts[2] );
    return new Date(year, month -1, day);
}
function _formatDate(date, format)
{
    var defaultFormat = 'yy-mm-dd';
    if ( !isEmpty(format) )
        defaultFormat = format;

    var splitter = '-';
    if ( defaultFormat.indexOf('/') > -1 )
        splitter = '/';
    if ( defaultFormat.indexOf('-') > -1 )
        splitter = '-';
    if ( defaultFormat.indexOf('.') > -1 )
        splitter = '.';

    date = _convertToDate(date, splitter);
    return $.datepicker.formatDate(defaultFormat, date);
}
function _getCurrentDate()
{
    return new Date();
}
function _getCurrentTime()
{
    var currentDate = new Date();
    return currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
}
function _getDay(date)
{
    return date.getDay();
}
function _getMonth(date)
{
    return date.getMonth() +1;
}
function _getYear(date)
{
    return date.getFullYear();
}
function _getDateParts(date)
{
    var year    = date.getFullYear();
    var month   = date.getMonth() +1;
    var day     = date.getDate();

    var dateInfo = new Object();
        dateInfo.Year   = year;
        dateInfo.Month  = month;
        dateInfo.Day    = day;

    return dateInfo;
}