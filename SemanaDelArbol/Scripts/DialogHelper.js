﻿var DialogHelper = new Object();
    DialogHelper.initializeDialog   = _initializeDialog;
    DialogHelper.resizeDialog       = _resizeDialog;
    DialogHelper.getCurrentHeight   = _getCurrentHeight;
    DialogHelper.getCurrentWidth    = _getCurrentWidth;
    DialogHelper.openDialog         = _openDialog;
    DialogHelper.closeDialog        = _closeDialog;
    DialogHelper.setTitle           = _setTitle;

/* initializeDialog */  
function _initializeDialog(dialogId, options)
{
    var autoOpen        = (options.autoOpen ? options.autoOpen : false);
    var closeOnEscape   = (options.closeOnEscape ? options.closeOnEscape : true);
    var modal           = (options.modal ? options.modal : true);
    var resizable       = (options.resizable ? options.resizable : false);
    var draggable       = (options.draggable ? options.draggable : true);
    var buttons         = (options.buttons ? options.buttons: {} );
    var width           = (options.width ? options.width : 400);
    var height          = (options.height ? options.height : 200);
    var close           = (options.close ? options.close: function(event, ui) { } );
        
    $("#" + dialogId).dialog
    (
        {
            autoOpen:       autoOpen,
            closeOnEscape:  closeOnEscape,
            modal:          modal,
            resizable:      resizable,
            draggable:      draggable,
            title:          options.title,
            zIndex:         900,
            width:          width,
            height:         height,
            close:          close,
            buttons:        buttons
        }
    );
}

/* GetCurrentHeight */
function _getCurrentHeight(dialogId)
{
    return $("#" + dialogId).parent().outerHeight();
}

/* GetCurrentWidth */
function _getCurrentWidth(dialogId)
{
    return $("#" + dialogId).parent().outerWidth();
}

/* ResizeDialog */
function _resizeDialog(dialogId, width, height)
{
    //adjust size..
    if (width > 0)
    {
        $("#" + dialogId).dialog('option', 'width', width);
    }
    if (height > 0)
    {
        $("#" +dialogId).dialog('option', 'height', height);
    }
    //adjust position...
    $("#" + dialogId).dialog("option", "position", $("#" + dialogId).dialog("option", "position"));
}

/* Open dialog */
function _openDialog(dialogId, title)
{
    if ( !isEmpty(title) )
        $("#" + dialogId).dialog( { title: title } );

    $("#" + dialogId).dialog("open");
}

/* Close dialog */
function _closeDialog(dialogId)
{
    $("#" + dialogId).dialog("close");
}

/* Set title */
function _setTitle(dialogId, title)
{
    $("#" + dialogId).dialog("option", "title", title);
}

/* Let jQuery UI Dialog allow HTML instead of just text */
$.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype,
{
    _title: function (title)
    {
        if (!this.options.title)
        {
            title.html("&#160;");
        }
        else
        {
            title.html(this.options.title);
        }
}
}));