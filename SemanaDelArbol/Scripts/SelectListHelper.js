﻿var _lists = new Array();
var _bindings = new Array();

var responseResult  = "Resultado";
var responseValue   = "Valor";
var responseMessage = "Mensaje";

var SelectListHelper = new Object();
    SelectListHelper.loadSelectList                         = _loadSelectList
    SelectListHelper.fillSelectList                         = _fillSelectList;
    SelectListHelper.formatForSelectList                    = _formatForSelectList;
    SelectListHelper.getDataFromSelectList                  = _getDataFromSelectList;
    SelectListHelper.getDataFromList                        = _getDataFromList;
    SelectListHelper.disableOptionsFromSelectList           = _disableOptionsFromSelectList;
    SelectListHelper.getSelectedIndexFromSelectList         = _getSelectedIndexFromSelectList;
    SelectListHelper.getSelectedValueFromSelectList         = _getSelectedValueFromSelectList;
    SelectListHelper.getSelectedTextFromSelectList          = _getSelectedTextFromSelectList;
    SelectListHelper.getSelectLists                         = _getSelectLists;
    SelectListHelper.getSelectList                          = _getSelectList;
    SelectListHelper.findOptionByText                       = _findOptionByText;
    SelectListHelper.bindListOnChangeEvent                  = _bindListOnChangeEvent;
    SelectListHelper.setBindings                            = _setBindings;
    SelectListHelper.getBindings                            = _getBindings;
    SelectListHelper.unSelectAll                            = _unSelectAll;
    SelectListHelper.setSelectListBasedOnSelection          = _setSelectListBasedOnSelection;
    SelectListHelper.setSelectListBasedOnSelectionBindings  = _setSelectListBasedOnSelectionBindings;
    SelectListHelper.setSelectedValue                       = _setSelectedValue;
    SelectListHelper.setSelectedIndex                       = _setSelectedIndex;
    SelectListHelper.removeOption                           = _removeOption;
    SelectListHelper.removeAllOptions                       = _removeAllOptions;
    SelectListHelper.appendOption                           = _appendOption;

function _fillSelectList(selectListId, data)
{
    var selectList = $("#" + selectListId);
    _removeAllOptions(selectListId);

    if (data.length == 0)
        return;

    var options = data.split(';')
    for (var i = 0; i < options.length; i++)
    {
        var option = options[i].split(':');
        selectList.append('<option value=' + option[0] + '>' + option[1] + '</option>');
    }
}
function _formatForSelectList(list, propertyValue, propertyName)
{
    var selectList = "";
    var value, text;

    if ((propertyValue == null || propertyValue == "" || propertyValue == undefined) || (propertyName == null || propertyName == "" || propertyName == undefined))
    {
        value = "Value";
        text = "Text";
    }

    var keys = _getObjectKeys(list);
    for (var i = 0; i < keys.length; i++)
    {
        var currentElement = list[keys[i]];

        value = currentElement[propertyValue];
        text  = currentElement[propertyName];

        if ( selectList.length == 0 )
            selectList = value + ":" + text;
        else
            selectList += ";" + value + ":" + text;
    }

    return selectList;
}
function _getDataFromSelectList(selectListId, copyTextAsValue)
{
    var list = "";
    $("#" + selectListId + " > option").each
    (
        function ()
        {
            var text = this.text;
            var value = this.value;

            if (copyTextAsValue != undefined && copyTextAsValue == true)
                value = text;

            if (list.length == 0)
                list = value + ":" + text;
            else
                list += ";" + value + ":" + text;
        }
    );
    return list;
}
function _getDataFromList(selectList, copyTextAsValue)
{
    var list = "";
    if ( selectList.length > 0 )
    {
        for (var i=0; i < selectList.length; i++)
        {
            var currentList = selectList[i].List;
            for (var j=0; j < currentList.length; j++)
            {
                var listData = currentList[j]; 
                var text  = listData.Text;
                var value = listData.Value;

                if (copyTextAsValue != undefined && copyTextAsValue == true)
                    value = text;

                if (list.length == 0)
                    list = value + ":" + text;
                else
                    list += ";" + value + ":" + text;
            }
        }
    }
    return list;
}
function _loadSelectList(url, params, listId, onChangeFn)
{
    var onResponse = overloadFunction(_getSelectList_Response, listId, onChangeFn);
    _callPageMethodExtended(url, params, onResponse, false); //call to _callPageMethod method on AJAXHelper.js
}
function _getSelectList_Response(listId, onChangeFn, data)
{
    var response = jQuery.parseJSON(data.d);
    if (response[responseResult])
    {
        //create list object based upon list data retrieved..
        var listData = response[responseValue];
        var listObject = new Object();
            listObject.ListId = listId;
            listObject.List = listData;

        //add list object to array of lists..
        _lists[_lists.length] = listObject;

        var selectedParentId = -1;
        var bindings = _getBindings();
        for (var i=0; i < bindings.length; i++)
        {
            var bindingList = bindings[i];
            for (var j=0; j < bindingList.length; j++)
            {
                var bindingListId = bindingList[j];
                if ( bindingListId.toLowerCase() == listId.toLowerCase() )
                {
                    if ( bindings[i][j-1] != undefined )
                    {
                        var parentBindingListId = bindingList[j-1];
                        selectedParentId = _getSelectedValueFromSelectList(parentBindingListId);
                        break;
                    }
                }
            }
        }

        //Find the selected Parent Id of the HTML Select List that needs to be filled..
        if (listData.length > 0)
        {
            //format object as "id:value;id:value;id:value"
            var currentList = "";

            if ( selectedParentId == -1 )
            {
                currentList = listData[0].List;
            }
            else
            {
                for(var i=0; i < listData.length; i++)
                {
                    if ( listData[i].Id == selectedParentId )
                    {
                        currentList = listData[i].List;
                        break;
                    }
                }
            }

            //var selectListData = _formatForSelectList(listData[0].List);
            var selectListData = _formatForSelectList(currentList);

            //fill HTML Select List with options..
            _fillSelectList(listId, selectListData);

            //bind HTML Select List's onChange event..
            _bindListOnChangeEvent(listId, onChangeFn);
        }
    }
}
function _bindListOnChangeEvent(listId, onChangeFn)
{
    var htmlSelect = $("#" + listId);
        htmlSelect.unbind('change');
        htmlSelect.change(function(event)
        {
            if (onChangeFn != undefined && onChangeFn != null & onChangeFn != "")
            {
                var listId          = event.currentTarget.id;
                //var selectedValue   = _getSelectedValueFromSelectList(listId);
                var selectedValue   = event.currentTarget.value;
                //var selectedText    = _getSelectedTextFromSelectList(listId);
                var selectedText    = event.currentTarget.options[event.currentTarget.selectedIndex].text;                            
                onChangeFn.call(this, selectedValue, selectedText);
            }
        });
}
function _setSelectListBasedOnSelection(selectedId, listId, cascadeActions)
{
    var currentList;    //list of listObject. Contains all lists, each identified by its own (parent) Id
    var targetList;     //listObject. Contains all elements of a listObject.

    //loop through all list objects inside lists array..
    for (var i = 0; i < _lists.length; i++)
    {
        var listObject = _lists[i];
        //find object with corresponding listId as its Id..
        if (listObject.ListId == listId)
        {
            currentList = listObject;
            break;
        }
    }

    //loop through all options inside current list..
    for (var i = 0; i < currentList.List.length; i++)
    {
        //find selected option on parent list to match with currentList.ListId..
        if (currentList.List[i].Id == selectedId)
        {
            targetList = currentList.List[i];
            break;
        }
    }

    if (targetList != undefined)
    {
        //format object as "id:value;id:value;id:value"
        var selectListData = _formatForSelectList(targetList.List);

        //fill HTML Select List with currentList's options..
        _fillSelectList(listId, selectListData);
    }
    else
    {
        //if no targetList was found, clear all existing items on listId..
        var selectList = $("#" + listId);
        _removeAllOptions(listId);
    }

    //if existing, run cascade actions..
    if (cascadeActions != undefined && cascadeActions != null && cascadeActions.length > 0)
    {
        for (var i = 0; i < cascadeActions.length; i++)
        {
            var action = cascadeActions[i];
                action.call(this);
        }
    }
}
function _disableOptionsFromSelectList(selectId, ids, enable)
{
    if (enable)
    {
        $("#" + selectId).children('option').attr("disabled", "disabled");
        for (var i = 0; i < ids.length; i++)
        {
            var optionValue = ids[i];
            $("#" + selectId + " option[value='" + optionValue + "']").removeAttr("disabled", "disabled");
        }
    }
    else
    {
        for (var i = 0; i < ids.length; i++)
        {
            var optionValue = ids[i];
            $("#" + selectId + " option[value='" + optionValue + "']").attr("disabled", "disabled");
        }
    }
}
function _getSelectedIndexFromSelectList(listId)
{
    var selectedIndex = -1;
    var list = $("#" + listId)
    if (list.length > 0)
        selectedIndex = list[0].selectedIndex;
    return selectedIndex;
}
function _getSelectedValueFromSelectList(listId, defaultValue)
{
    var selectedValue = defaultValue != null ? defaultValue : null;
    //return $("#" + listId + " :selected").val();
    var selectedIndex = _getSelectedIndexFromSelectList(listId);
    var list = $("#" + listId);
    if ( list.length > 0 && selectedIndex > -1 )
        selectedValue = list[0].options[selectedIndex].value;
    return selectedValue;
}
function _getSelectedTextFromSelectList(listId, defaultText)
{
    var selectedText = defaultText != null ? defaultText : null;
    //var selectedValue = _getSelectedValueFromSelectList(listId);
    //return $("#" + listId + " option[value='" + selectedValue + "']").text();
    var selectedIndex = _getSelectedIndexFromSelectList(listId);
    var list = $("#" + listId);
    if (list.length > 0 && selectedIndex > -1)
        selectedText = list[0].options[selectedIndex].text;
    return selectedText;
}
function _getSelectLists()
{
    return _lists;
}
function _getSelectList(selectListId)
{
    var list = "";
    var lists = _getSelectLists();
    if ( lists.length > 0 )
    {
        for(var i=0; i < lists.length; i++)
        {
            if ( lists[i].ListId == selectListId )
            {
                list = lists[i].List;
                break;
            }
        }
    }    
    return list;
}
function _findOptionByText(listId, text)
{
    var index = null;
    var value = null;
    var list = $("#" + listId);
    if ( list.length > 0 )
    {
        list = list[0];
        for(var i=0; i < list.options.length; i++)
        {
            if (text.removeAccents().toLowerCase() == list.options[i].text.removeAccents().toLowerCase())
            {
                index = i;
                value = list.options[i].value;
                break;
            }
        }
    }
    return { index: index, value: value, text: text };
}

function _setBindings(list)
{
    var bindings = _getBindings();
        bindings[bindings.length] = list;
}
function _getBindings()
{
    return _bindings;
}

function _setSelectListBasedOnSelectionBindings(parentListId, selectedParentId, childListId, selectedChildId)
{
    var currentList;    //list of listObject. Contains all lists, each identified by its own (parent) Id
    var targetList;     //listObject. Contains all elements of a listObject.

    /* 1. Finds the lists which its Id corresponds to parentListId (this lists holds N list arrays, all belonging to same parentListId */
    for (var i = 0; i < _lists.length; i++)
    {
        var listObject = _lists[i];
        //find object with corresponding listId as its Id..
        if (listObject.ListId == parentListId)
        {
            currentList = listObject.List;
            break;
        }
    }

    /* 2. Look inside N arrays of currentList which is the list that corresponds to (parent) Id */
    for (var i = 0; i < currentList.length; i++)
    {
        //find selected option on parent list to match with currentList.ListId..
        if (currentList[i].Id == selectedParentId)
        {
            targetList = currentList[i].List;
            break;
        }
    }

    //3. Assign data options (pairs of value:text) of targetList to (target) HTML Select List..
    if (targetList != undefined)
    {
        //format object as "id:value;id:value;id:value"
        var selectListData = _formatForSelectList(targetList);

        //fill HTML Select List with currentList's options..
        _fillSelectList(childListId, selectListData);

        //set selectedChildId as selected on target list..
        if ( selectedChildId != undefined && selectedChildId != null && selectedChildId.length > 0 )
            $("#" + childListId).val(selectedChildId);
    }
    else
    {
        //if no targetList was found, clear all existing items on listId..
        var selectList = $("#" + childListId);
        _removeAllOptions(childListId);
    }

    return $("#" + childListId).val();
}
function _unSelectAll(selectListId)
{
    $("#" + selectListId).prop('selectedIndex', -1)
}
function _setSelectedIndex(selectListId, index)
{
    $("#" + selectListId)[0].selectedIndex = index;
}
function _setSelectedValue(selectListId, value)
{
    $("#" + selectListId).find("option[value=" + value + "]").attr("selected", true);
}
function _removeOption(selectListId, value)
{
    $("#" + selectListId).find("option[value=" + value + "]").remove();
}
function _removeAllOptions(selectListId)
{
    $("#" + selectListId).empty();
}
function _appendOption(selectListId, value, text, insertFirst, selected)
{
    var isSelected = (selected == true ? "selected='selected'" : "" );
    if ( insertFirst )
        $("#" + selectListId).prepend("<option value='" + value + "' " + isSelected + " >" + text + "</option>");
    else
        $("#" + selectListId).append("<option value='"  + value + "' " + isSelected + " >" + text + "</option>");
}

function _getObjectKeys(obj)
{
    var keys = Object.keys(obj);
    return keys;
}
function _getElementInArray(obj, propertyName, propertyValue)
{
    var element = null;
    var keys = _getObjectKeys(obj);
    for (var i = 0; i < keys.length; i++)
    {
        var currentElement = obj[keys[i]];
        if (currentElement[propertyName] == propertyValue)
        {
            element = currentElement;
            break;
        }
    }
    return element;
}