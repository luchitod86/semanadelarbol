﻿var AJAXHelper = new Object();
    AJAXHelper.doAjaxCall   = _doAjaxCall;
    AJAXHelper.handleError  = _handleError;

function _doAjaxCall(methodURL, parameters, successFunction, data)
{
    /*
    If methodURL already specifies a page, i.e.: Default.aspx/logIn,
    discard current location page. Otherwise, consider location page.
    */

    //if (httpVerb == undefined)
    //    httpVerb = "POST";

    var contentType = "application/json; charset-utf-8";

    jQuery.support.cors = true;

    if ( parameters.length == 0 )
         parameters = "{}";

    var arr = methodURL.split('/');
    if (arr.length > 1)
        methodURL = methodURL;
    else
        methodURL = window.location.pathname + "/" + methodURL; //window.location.pathname + "/" + arr[arr.length - 1];

    if ( methodURL.substring(0,1) == '/' )  //if ( methodURL[0] == '/' )
        methodURL = methodURL.substring(1, methodURL.length);
   
    methodURL = methodURL.replace('../', '');

    var fnSuccess = successFunction;
    if ( data != null && data != undefined )
        fnSuccess = overloadFunction(successFunction, data);

    //make AJAX call to the server..
    $.ajax
    (
        {
            url: _root + methodURL,
            data: parameters,
            datatype: "json",
            async: true,
            type: "POST",
            crossDomain: true,
            contentType: contentType,
            beforeSend: function (xhr, settings)
            {
                xhr.setRequestHeader("Content-type", contentType);
            },
            complete: function (jqXHR, textStatus) //(data|jqXHR, textStatus, jqXHR|errorThrown)
            {
                var statusCode = parseInt(jqXHR.status);
//                //var headers = resp.getAllResponseHeaders();
//                if ( !isEmpty(onResponseFunction) )
//                    onResponseFunction.call(this, jQuery.parseJSON(resp.responseText.d));
            },
            success: fnSuccess,
            error: _handleError
        }
    );
}
function _handleError(xmlHttpRequest, textStatus, errorThrown)
{
    Helpers.hideLoadingMessage();
    var error = jQuery.parseJSON(xmlHttpRequest.responseText);
    if (isEmpty(error))
    {
        var exception = new Object();
            exception.Message = "Lo sentimos! Ocurrió un error imprevisto en el sistema.";
            exception.StackTrace = "No hay información.";
        error = exception;
    }

    var statusText = xmlHttpRequest.statusText;
    var statusCode = xmlHttpRequest.status;

    var isCancellation  = false;
    var isRedirection   = false;
    var isClientError   = false;
    var isServerError   = false;

    //0 : Cancelled
    if (statusCode == 0)
        isCancellation = true;

    //3xx : Redirections
    if (statusCode >= 300 && statusCode <= 399)
        isRedirection = true;

    //4xx : Client Errors
    if (statusCode >= 400 && statusCode <= 499)
        isClientError = true;

    //5xx : Server Errors
    if (statusCode >= 500 && statusCode <= 599)
        isServerError = true;

    if (!isRedirection && !isCancellation)
    {
        _displayErrorMessage(error);

        //if ( !isEmpty(registrarError) )
        //    registrarError(error);
    }
}
function _displayErrorMessage(error)
{
    Helpers.NotificationHelper.showError(error, "Error"); //call to "showError" method on NotificationDialog.ascx..
}
