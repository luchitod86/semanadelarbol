﻿var MultiSelectHelper = new Object();
    MultiSelectHelper.configure             = _configure;
    MultiSelectHelper.getFirstSelectedValue = _getFirstSelectedValue;
    MultiSelectHelper.getFirstSelectedText  = _getFirstSelectedText;
    MultiSelectHelper.getSelected           = _getSelected;
    MultiSelectHelper.setSelected           = _setSelected;
    MultiSelectHelper.setAll                = _setAll;
    MultiSelectHelper.bindOnChange          = _bindOnChange;

/* Set settings for Multiselect */
function _configure(selector, multiple, height, width, selectedList)
{
    var _multiple = false;
    if ( multiple != null )
        _multiple = multiple;

    var _height = 175;
    if ( height != null && height != undefined && height != "" )
        _height = height;

    var _width  = 225;
    if ( width != null && width != undefined && width != "" )
        _width = width;

    var _selectedList = 1;
    if ( selectedList != null && selectedList != undefined && selectedList != "" )
        _selectedList = selectedList;

    $(selector).multiselect
    ({
        header: 0, /* Either a boolean value denoting whether or not to display the header, or a string value. If you pass a string, the default "check all", "uncheck all", and "close" links will be replaced with the specified text. */
        height: _height,
        minWidth: _width,
        multiple: _multiple,
        checkAllText: "Marcar todos",
        uncheckAllText: "Desmarcar todos",
        noneSelectedText: "Seleccione",
        selectedText: "# seleccionados",
        selectedList: _selectedList, /* A numeric (or boolean to disable) value denoting whether or not to display the checked opens in a list, and how many. A number greater than 0 denotes the maximum number of list items to display before switching over to the selectedText parameter. A value of 0 or false is disabled. */
        show: null, /* The name of the effect to use when the menu opens. To control the speed as well, pass in an array: ['slide', 500] */
        hide: null, /* The name of the effect to use when the menu closes. To control the speed as well, pass in an array: ['explode', 500] */
        autoOpen: 0,
        position:{}, /* This option allows you to position the menu anywhere you'd like relative to the button; centered, above, below (default), etc. Also provides collision detection to flip the menu above the button when near the bottom of the window. If you do not set this option or if the position utility has not been included, the menu will open below the button. Requires the jQuery UI position utility and jQuery 1.4.3+. */
        open: function()
        {
            //var multiselectFilter = $(this).multiselectfilter("widget");
            //if ( multiselectFilter.length == 0 )
            //    return;

            //give filter input focus..
            var multiselect = $(this).multiselect("widget");
            var header      = multiselect.children()[0];
            var checkboxes  = multiselect.children()[1];
            
            var filter      = $(header).children()[0];
            var helperReset = $(header).children()[1];

            var inputFilter = $(filter).children()[0];          
                inputFilter.focus();
        },
        close: function()
        {
            //..
        }
    });
    //.multiselectfilter
    //({
    //    label: "Filtrar por:", /* The text to appear left of the input. */
    //    width: _width / 2, /* The width of the input in pixels. Defaults to 100px in the style sheet, but you can override this for each instance. */
    //    placeholder: "Escribir..", /* The HTML5 placeholder attribute value of the input. Only supported in webkit as of this writing. */
    //    autoReset: false, /* A boolean value denoting whether or not to reset the search box & any filtered options when the widget closes. */
    //    filter: function(event, matches)
    //    {
    //        if ( !matches.length )
    //        {
    //            //..
    //        }
    //    }
    //});

    //MultiSelect automatically removes multiple="multiple" atribute from <select> if multiple param is set to false.
    //This behavior makes the select to mark the first option as selected if it's the only option available..
    //So I explicitly re-add the multiple="multiple" attribute..
    $(selector).attr("multiple", "multiple");
}

/* Returns an array of all selected items */
function _getSelected(id)
{
    //var arrChecked = $("#selDirector").multiselect("getChecked").map( function() { return this.value; } ).get();
    var arrChecked = $("#" + id).val();
    return arrChecked;
}

/* Returns first selected item's Value */
function _getFirstSelectedValue(id, defaultValue)
{
    var _defaultValue = -1;
    if ( defaultValue != null )
        _defaultValue = defaultValue;

    var arrChecked = _getSelected( id );
    if ( arrChecked != null && arrChecked.length > 0 )
        return arrChecked[0];
    else
        return _defaultValue;
}

/* Returns first selected item's Text */
function _getFirstSelectedText(id, defaultText)
{
    var _defaultText = -1;
    if ( defaultText != null )
        _defaultText = defaultText;

    var arrChecked = _getSelected( id );
    if ( arrChecked != null && arrChecked.length > 0 )
    {
        return $("#" + id + " option[value='" + arrChecked[0] + "']").text();
    }
    else
        return _defaultText;
}

/* Check/Uncheck option that corresponds to given value */
function _setSelected(id, values, selected, clearSelection)
{
    /*$("#selDirector").multiselect("widget").find('input[value="{value}"]').each( function() { this.click(); } );*/

    var isMultiple = $("#" + id).multiselect("option", "multiple");
    if ( (isMultiple) && (clearSelection == true) )
    {
        /* clear all previous selections */
        _setAll(id, false);
    }    
    if ( !isMultiple )
    {
        /* clear all previous selections */
        /* _setAll(id, false); <-- does not seem to work when isMultiple is FALSE (as no checkboxes are present) */
        $("#" + id + " > option").each( function() { $(this).removeAttr('selected'); } );
    }

    var _selected = true;
    if ( selected == true || selected == false )
        _selected = selected;

    var _values = [].concat(values);
    if ( $.isArray(_values) )
    {
        for(var i=0; i < _values.length; i++)
        {
            $('#' + id + ' option[value="' + _values[i] + '"]').attr("selected", _selected);
        }
    }

    $("#" + id).multiselect("refresh");
}
/* Set all items checked/unchecked */
function _setAll(id, selected)
{
    //$("#" + id).multiselect("widget").find(":checkbox").each( function() { this.click(); });
    var checkAll = "checkAll";
    if ( !selected )
        checkAll = "un" + checkAll;
    $("#" + id).multiselect(checkAll); /* methods: "checkAll" / "uncheckAll" */
}

/* Bind "click" event */
function _bindOnChange(selector, onChangeFn)
{
    $(selector).multiselect
    ({
        click: function(event, ui)
        {
            if ( !isEmpty(onChangeFn) )
                onChangeFn.call(this, ui.checked, ui.value, ui.text);
        }
    });
}