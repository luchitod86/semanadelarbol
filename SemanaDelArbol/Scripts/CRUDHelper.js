﻿function CRUDHelper() {
    return {
        Delete: function (urlDelete, id, onResponse, onCancel, title, message) {
            if (isEmpty(title) && isEmpty(message))
            {
                onDelete_Continue(id, urlDelete, onResponse);
            }
            else
            {
                var onDelete_CancelFn = null;
                if (!isEmpty(onCancel))
                    onDelete_CancelFn = overloadFunction(onCancel, id);

                var onDelete_ContinueFn = null;
                if (!isEmpty(onResponse))
                    onDelete_ContinueFn = overloadFunction(onDelete_Continue, id, urlDelete, onResponse);

                Helpers.NotificationHelper.showConfirm(message, title, null, onDelete_ContinueFn, onDelete_CancelFn);
            }
            function onDelete_Continue(id, urlDelete, onDeleteFn) {
                Helpers.showLoadingMessage();
                var data = new Object();
                    data.OnResponseFn = onDeleteFn;
                var onDeleteEntity_Response = overloadFunction(onDelete_Response, data);

                Helpers.AJAXHelper.doAjaxCall(urlDelete, "{'id':'{0}' }".format(id), onDeleteEntity_Response);
            };
            function onDelete_Cancel(id) {
                if (!isEmpty(params.OnCancelFn))
                    params.OnCancelFn.call(this);
            };
            function onDelete_Response(params, data) {
                Helpers.hideLoadingMessage();
                var responseInfo = jQuery.parseJSON(data.d);
                if (responseInfo.Resultado) {
                    if (!isEmpty(params.OnResponseFn))
                        params.OnResponseFn.call(this, responseInfo);
                }
                else {
                    Helpers.NotificationHelper.showError(responseInfo.Mensaje);
                }
            }
        },
        Get: function (urlGet, params, onResponseFn) {
            Helpers.showLoadingMessage();
            var data = new Object();
                data.OnResponseFn = onResponseFn;
            var onGetEntity_Response = overloadFunction(onGet_Response, data);
            Helpers.AJAXHelper.doAjaxCall(urlGet, params, onGetEntity_Response);

            function onGet_Response(params, data)
            {
                Helpers.hideLoadingMessage();
                var responseInfo = jQuery.parseJSON(data.d);
                if (responseInfo.Resultado)
                {
                    if (!isEmpty(params.OnResponseFn))
                        params.OnResponseFn.call(this, responseInfo);
                }
                else {
                    Helpers.NotificationHelper.showError(responseInfo.Mensaje);
                }
            }
        },
        Save: function (urlSave, params, onResponseFn) {
            Helpers.showLoadingMessage();
            var data = new Object();
                data.OnResponseFn = onResponseFn;
            var onGetEntity_Response = overloadFunction(onGet_Response, data);
            Helpers.AJAXHelper.doAjaxCall(urlSave, params, onGetEntity_Response);

            function onGet_Response(params, data) {
                Helpers.hideLoadingMessage();
                var responseInfo = jQuery.parseJSON(data.d);
                if (responseInfo.Resultado) {
                    if (!isEmpty(params.OnResponseFn))
                        params.OnResponseFn.call(this, responseInfo);
                }
                else {
                    Helpers.NotificationHelper.showError(responseInfo.Mensaje);
                }
            }
        }
    }
}