﻿jQuery.fn.extend
({
    isVisible : function()
    {
        var control = this;
        return control.css("display") == "none" ? false : true;
    },
	getType: function()
	{
	    var controlType = null;
	    var control = this;
	    var propName = "type"; //"tagName"; //nodeName //localName

	    if (control.prop(propName) == undefined)
	        propName = "tagName";
	    if (control.prop(propName) == undefined)
	        propName = "nodeName";

	    if (control.prop(propName) != undefined)
	        controlType = control.prop(propName).toLowerCase();

	    return controlType;
	},
	getValue: function()
	{
	    var value = null;
		var control = $(this);
		var controlId = control.attr("id");
		var type = control.getType();
		switch ( type )
		{
		    case "label":
		        value = control.text();
		        break;
			case "text":
		    case "textarea":
		    case "hidden":
            case "password":
				value = control.val();
				break;
			case "radio":
			case "checkbox":
				value = control.prop("checked");
				break;
			case "select-one":
			case "select-multiple":
			    //value = Helpers.SelectListHelper.getSelectedValueFromSelectList(controlId);
			    value = $("#" + controlId + " :selected").val();
			    break;
		    case "div":
		        value = control.prop("value");
		        break;
		}
		return value;
	},
	setValue: function(value)
	{
	    $.each(this, function(index, element)
	    {
	        var control = $($(element)[0]);
	        var controlId = control.attr("id");
	        if ( control != undefined && control != null && control.length > 0)
	        {
	            var type = control.getType();
	            switch (type)
	            {
	                case "label":
	                    control.text(value != null ? value : "");
	                    break;
	                case "text":
	                case "textarea":
	                case "hidden":
                    case "password":
	                    control.val(value != null ? value : "");
	                    break;
	                case "radio":
	                case "checkbox":
	                    control.prop("checked", value != null ? value : false);
	                    break;
	                case "select-one":
	                case "select-multiple":
	                    //Helpers.SelectListHelper.setSelectedValue(controlId, value);
	                    var select = $("#" + controlId);
	                    if (value != null)
	                        select.find("option[value=" + value + "]").attr("selected", true);
                        else
	                        select[0].selectedIndex = -1;
	                    break;
	                case "div":
	                    control.prop("value", value);
	                    break;
	            }
	        }
	    });
	},
    setEnabled: function(enabled)
    {
        var disabledClass = disabledClass != null ? disabledClass : "disabled";
        $.each(this, function(index, element)
        {
            var control  = $(this);
            var isInput  = control.is("input");
            var isDiv    = control.is("div");
            var isSelect = control.is("select");

            if (isDiv) 
                enabled ? control.show() : control.hide();
            //if (isSelect)
            //    control.multiselect(enabled == true ? "enable" : "disable");

            enabled ? control.removeClass(disabledClass) : control.addClass(disabledClass);
            control.prop("disabled", !enabled);

            /* Unbind 'click' event handler */
            /*
            if ( hasHandler(control, "click") )
            {
                control.off('click')
                control.on('click', function() { mostrarNotificacion(_notifications.warning, "No tiene permisos para realizar esta acción.", false); } );
            }
            */
        });
    }
});