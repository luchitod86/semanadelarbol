﻿var _SearchMode = new Object();
    _SearchMode.FromAddress = "address";
    _SearchMode.FromLatLng  = "latlng";

var GoogleMapHelper = new Object();
    GoogleMapHelper.initialize                              = _initialize;
    
    GoogleMapHelper.getMap                                  = _getMap;
    GoogleMapHelper.getIcon                                 = _getIcon;

    GoogleMapHelper.createPosition                          = _createPosition;
    GoogleMapHelper.getPositionFromAddress                  = _getPositionFromAddress;
    GoogleMapHelper.getPositionFromLatLng                   = _getPositionFromLatLng;
    GoogleMapHelper.addMarker                               = _addMarker;
    GoogleMapHelper.addRandomMarkers                        = _addRandomMarkers;
    GoogleMapHelper.setCurrentLocation                      = _setCurrentLocation;
    GoogleMapHelper.setMarkersVisibility                    = _setMarkersVisibility;
    GoogleMapHelper.setMarkersVisibilityBasedOnAttribute    = _setMarkersVisibilityBasedOnAttribute;
    GoogleMapHelper.removeAllMarkers                        = _removeAllMarkers;
    GoogleMapHelper.removeMarker                            = _removeMarker;
    GoogleMapHelper.getAllMarkers                           = _getAllMarkers;
    GoogleMapHelper.getMarker                               = _getMarker;
    GoogleMapHelper.setZoom                                 = _setZoom;

    GoogleMapHelper.SearchMode                              = _SearchMode;

var _geocoder;
var _map;
var _idMap;
var _zoom;
var _bounds;
var _icon = '../Images/Marcadores/Arbol.png';
var _title = 'Click for more info.';
var _markers = []; 
var _idMarkersList;

/* Private variables */
var _isDraggingMarker   = false;
var _enableMarkers      = true;

function _initializeGeoCoder()
{
    if ( _geocoder == null )
        _geocoder = new google.maps.Geocoder();
}
function _initialize(idMap, onResponseFunction, zoom, latitude, longitude, customMarker, idMarkersList, disableMarkers, onClickToMapResponseFunction)
{
    _initializeGeoCoder();

    _idMap = idMap;

    if ( !isEmpty(customMarker) )
        _icon = customMarker;

    if ( !isEmpty(idMarkersList) )
        _idMarkersList = idMarkersList;        

    if ( !isEmpty(disableMarkers) )
        _enableMarkers = !disableMarkers;

    if ( !isEmpty(zoom) )
        _zoom = zoom;
    else
        _zoom = 4;

    //create Map object given a latitude & altitude..
    var latlng = new google.maps.LatLng(latitude, longitude);
    var options =
    {
        zoom: _zoom,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    _map = new google.maps.Map(document.getElementById(idMap), options);

    _addClickEventToMap(_map, _title, onClickToMapResponseFunction);
      
    google.maps.event.addListenerOnce
    (
        _map, 'bounds_changed', function()
        {
            _bounds = _map.getBounds();
        }
    );

    if ( !isEmpty(onResponseFunction) )
        onResponseFunction.call(this, idMap);
}
function _getMap()
{
    return _map;
}
function _getIcon()
{
    return _icon;
}
function _getMarker(idMarker)
{
    var marker;
    for(var i=0; i < _markers.length; i++)
    {
        if ( _markers[i].__gm_id == idMarker )
        {
            marker = _markers[i];
            break;
        }
    }
    return marker;
}
function _createPosition(lat, lng)
{
    return new google.maps.LatLng(lat, lng);
}
function _getPositionFromAddress(address, onResponseFunction, idMapa)
{
    _initializeGeoCoder();

    _idMap = idMapa;
    
    if ( _map == null )
        _map = new google.maps.Map(document.getElementById(_idMap));

    _geocoder.geocode
    (
        { 'address': address },
        function(results, status)
        {
            var position;
            if ( status == google.maps.GeocoderStatus.OK )
                position = results[0].geometry.location;
            if ( !isEmpty(onResponseFunction) )
                onResponseFunction.call(this, position, status);
        }
    );
}
function _getPositionFromLatLng(pos, idMap, onGetPositionFromLatLngResponse)
{
    _initializeGeoCoder();

    _idMap = idMap;

    if ( _map == null )
        _map = new google.maps.Map(document.getElementById(_idMap));

    var latlng;
    if ( isEmpty(pos.ab) )
    {
        var latlngStr   = pos.split(",", 2);
        var lat         = parseFloat(latlngStr[0]);
        var lng         = parseFloat(latlngStr[1]);
        latlng          = new google.maps.LatLng(lat, lng);
    }
    else
    {
        latlng = pos; /* lat = pos.ab, lng = pos.$a */
    }

    var arrComponents = new Array();

    _geocoder.geocode
    (
        {
            latLng: latlng
        },
        function(results, status)
        {
            if (status == google.maps.GeocoderStatus.OK)
            {
                if ( !isEmpty(results) )
                {
                    var components =
                        {
                            "country": null,                        //pais
                            "administrative_area_level_1": null,    //provincia
                            "locality": null,                       //municipio/localidad
                            "neighborhood": null,                   //barrio
                            "route": null,                          //calle
                            "street_address": null,                 //calle 
                            "street_number": null                   //calle 
                        };
                    $.each(components, function (index, item)
                    {
                        arrComponents[index] = item;
                    });
                    $.map(results, function(item)
                    {
                        $.each(item.address_components, function(i, address_component)
                        {
                            var keys = Object.keys(arrComponents);
                            for (var i = 0; i < keys.length; i++)
                            {
                                if ( keys[i] == address_component.types[0] )
                                {
                                    arrComponents[keys[i]] = address_component.long_name;
                                    break;
                                }
                            }
                        });
                    });
                }
            }
            //else
            //{
            //    alert("Geocoder failed due to: " + status);
            //}

            if ( !isEmpty(onGetPositionFromLatLngResponse) )
                onGetPositionFromLatLngResponse.call(this, arrComponents);
        }
    );
}
function _addClickEventToMarker(map, marker, content, onInfoWindowOpenFunction)
{
    //create InfoWindow..
    var infoWindow = _addInfoWindow(map, marker, content);

    //add click event listener to marker..
    google.maps.event.addListener
    (
        marker, 'click', function()
        {
            //map.setZoom(8);                     //set zoom level = 8
            map.setCenter(marker.getPosition());  //set center at Marker's position
            
            //open Info Window..
            infoWindow.open(map, marker);

            if ( !isEmpty(onInfoWindowOpenFunction) )
                onInfoWindowOpenFunction.call(this, infoWindow, marker);

            var idMarker = marker.get("id");
            $(".markerItem").removeClass("markerSelected");
            $("#markerItem_" + idMarker).addClass("markerSelected");
        }
    );
}
function _addClickEventToMap(map, title, onClickToMapResponseFunction)
{
    //add Click Event Listener to Map..
    google.maps.event.addListener
    (
        map, 'click', function(event)
        {
            if ( _isDraggingMarker == false )
            {
                if ( _enableMarkers )
                {
                    if ( !isEmpty(onClickToMapResponseFunction) )
                        onClickToMapResponseFunction.call(this, map, event.latLng, _icon, title, (_markers.length +1));
                    //_addMarker(map, event.latLng, _icon, title, "", onAddMarkerResponseFunction);
                }
            }
        }
    );        
}
function _addMarker(map, position, idMarker, markerType, icon, title, draggable, listItemText, content, onResponseFunction, onInfoWindowOpenFunction, attribute)
{
    //var markerIcon = new google.maps.MarkerImage
    //    (
    //        icon,
    //        new google.maps.Size(48, 48),
    //        new google.maps.Point(0, 0),
    //        new google.maps.Point(0, 0),
    //        new google.maps.Size(48, 48)
    //    );
    var marker = new google.maps.Marker
                 ({
                    map: map,
                    position: position, //map.getCenter(), <-- si quisiese centrarlo en el mapa
                    title: title,
                    icon: icon,
                    draggable: draggable
                 });
    _markers[_markers.length] = marker;

    //var idMarker = _markers.length;

    markerType = markerType.toLowerCase();
    idMarker = markerType + "_" + idMarker;

    marker.set("id", idMarker);
    marker.set("type", markerType);

    if ( !isEmpty(attribute) )
        marker.set(attribute.Name, attribute.Value);

    //marker.setValues( { type: "point", id: _markers.length } );
    //market already has a unique id: marker.__gm_id
    // or..
    //marker.set("type", "point");
    //marker.set("id", 1);
    //var val = marker.get("id");     

    //add an Event Listener to Marker so if clicked, it centers map on it..
    google.maps.event.addListener 
    (
        marker, "click", function()
        {
            map.panTo(marker.getPosition()); 
        }
    );
                    
    _addClickEventToMarker(map, marker, content, onInfoWindowOpenFunction);
    _addDragEventToMarker(map, marker);
   
    if ( !isEmpty(_idMarkersList) )
    {
        var listItem = $("<li />");
            listItem
                .attr("id", "markerItem_" + idMarker)
                .addClass("markerItem")
                .addClass(markerType)
                .addClass(markerType + "_listItem")
                .html(listItemText.replace("{0}", _markers.length))
                .click(function()
                {
                    //get list item selected..
                    var idMarker = marker.get("id");
                    $(".markerItem").removeClass("markerSelected");
                    $("#markerItem_" + idMarker).addClass("markerSelected")

                    //center marker on map..
                    var position = marker.getPosition();
                    //if ( !(map.getBounds().contains(position)) )
                    //{
                        //_map.setCenter(position);
                        map.panTo(position);
                    //}
                });

        if ( !isEmpty(attribute) )
            listItem.addClass(markerType + "_" + attribute.Name + "_" + attribute.Value);
            listItem.appendTo("#" + _idMarkersList); 
    }

    if ( !isEmpty(onResponseFunction) )
        onResponseFunction.call(this, marker);
}
function _addDragEventToMarker(map, marker)
{
    //add dragging event listeners..
    google.maps.event.addListener(marker, 'dragstart', function()
    {
        _isDraggingMarker = true;
    });
    google.maps.event.addListener(marker, 'drag', function()
    {
        //marker.getPosition();
    });
    google.maps.event.addListener(marker, 'dragend', function()
    {
        var position = marker.getPosition();
        map.panTo(position);
        _isDraggingMarker = false;
        _getPositionFromLatLng(position);
    });
}
function _addInfoWindow(map, marker, text)
{
    var infoWindow;
    if ( !isEmpty(marker) )
    {
        //attach InfoWindow to Marker..
        infoWindow = new google.maps.InfoWindow
        ({
            content: text
        });
    }
    else
    {
        //attach InfoWindow to the center of Map..
        infoWindow = new google.maps.InfoWindow
        ({
            content: "...",
            position: map.getCenter()
        });  
    }
    return infoWindow;
}
function _setCurrentLocation(map, position, zoom)
{
    map.setZoom(zoom);
    map.panTo(position); 
}
function _removeAllMarkers(markerType, excludingAttribute)
{
    var i;
    var remove;
    for (i=0; i < _markers.length; i++)
    {
        remove = true;
        if ( !isEmpty(markerType) )
        {
            if ( _markers[i].get("type") != markerType )
                remove = false;
        }

        if ( !isEmpty(excludingAttribute) && !isEmpty(_markers[i].get(excludingAttribute)) )
            remove = false;

        if ( remove )
            _markers[i].setMap(null);
    }
    _markers = [];
}
function _removeMarker(idMarker, removeDuplicates)
{
    for(var i=0; i < _markers.length; i++)
    {
        //if ( _markers[i].__gm_id == idMarker )
        if ( _markers[i].get("id") == idMarker )
        {
            _markers[i].setMap(null);
            if ( removeDuplicates == false || removeDuplicates == null )
                break;
        }
    }
}
function _setMarkersVisibility(markerType, visible)
{
    for(var i=0; i < _markers.length; i++)
    {
        if ( _markers[i].get("type") == markerType )
        {
            _markers[i].setVisible(visible);
        }
    }
}
function _setMarkersVisibilityBasedOnAttribute(attrName, attrValue, visible, reverse)
{
    for(var i=0; i < _markers.length; i++)
    {
        if ( reverse == true )
        {
            if ( _markers[i].get(attrName) != attrValue )
                _markers[i].setVisible(visible);
        }
        else
        {
            if ( _markers[i].get(attrName) == attrValue )
                _markers[i].setVisible(visible);
        }
    }
}
function _getAllMarkers()
{
    return _markers;
}
function _addRandomMarkers(count)
{
    var southWest   = _bounds.getSouthWest(); 
    var northEast   = _bounds.getNorthEast(); 
    var lngSpan     = northEast.lng() - southWest.lng(); 
    var latSpan     = northEast.lat() - southWest.lat(); 

    //Setup N(count) random points..
    for (var i = 0; i < count; i++)
    { 
        var position = new google.maps.LatLng(southWest.lat() + latSpan * Math.random(), southWest.lng() + lngSpan * Math.random());            
        _addMarker(_map, position, _icon, "Árbol plantado", "");
        _markers[_markers.length] = marker; 
    }        
}
function _setZoom(map, level)
{
    map.setZoom(level);
    //map.setCenter(marker.getPosition());
}