﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Galeria.aspx.cs" Inherits="SemanaDelArbol.Pages.Galeria" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
        .fb-photo
        {
            float: left;
            border: 1px solid #63C794;
            margin: 8px;
            padding: 5px;
        }
        .fb-photo .avatar img
        {
            width: 200px;
            height: 150px;
        }
        .sda-photo
        {
            float: left;
            border: 1px solid #63C794;
            margin: 8px;
            padding: 5px;
        }
        .sda-photo img
        {
            width: 200px;
            height: 150px;
        }
        #pnlFotosPlantaciones, #pnlFotosFacebook
        {
            float: left;
            width: 100%;
            margin: 5px 0px -1px 0px;
            border: 1px solid #ccc;
            padding: 10px 0px 10px 10px;
        }
        #pnlFotosPlantaciones .progreso, #pnlFotosFacebook .progreso
        {
            display: none;
        }
        #fotosFacebook, #fotosPlantaciones
        {
            border: 1px solid #ccc;
            float: left;
        }
        #btnFotosFacebook, #btnFotosPlantaciones
        {
            border: 1px solid transparent;
        }
        #btnFotosFacebook :hover, #btnFotosPlantaciones :hover
        {
            cursor: pointer;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        //   var _albumsFacebook     = [];
        var _albumsPlantaciones = [];
        
        var _paginado = 10;

        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Galeria);
        });
        function cargarPagina_Galeria()
        {
            obtenerListasFotosPlantaciones("selFotosPlantaciones");
      //      obtenerListasFotosFacebook("selFotosFacebook");

            $("#btnFotosPlantaciones").click
            (
                function()
                {
                    var id = $(this).attr("id");
                    var content;
                    var colapsado = "colapsado";

               //     if ( id == "btnFotosFacebook" )
               //         content = $("#fotosFacebook");
                    if ( id == "btnFotosPlantaciones" )
                        content = $("#fotosPlantaciones");

                    content.toggleClass(colapsado);
                    content.slideToggle();
                    var img = $(this).find("img");
                    var src = img.attr("src");
                    img.attr("src", (src == "../Images/up.gif" ? "../Images/down.gif" : "../Images/up.gif") );
                }
            );
        }
    </script>

    <!-- Fotos Facebook -->
    <script type="text/javascript" >
        function obtenerListasFotosFacebook(idLista)
        {
            var params              = new Object();
                params.pais         = ""; //_filtroActual.Pais.Nombre;
                params.periodo      = _filtroActual.Periodo;
                params.idEntidad    = "Facebook";
            var jsonParams = JSON.stringify(params);

            var data            = new Object();
                data.IdLista    = idLista;

            Helpers.AJAXHelper.doAjaxCall("obtenerAlbums", jsonParams, obtenerListasFotosFacebook_Respuesta, data);
        }
        function obtenerListasFotosFacebook_Respuesta(data, datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var albumsInfo = respuestaInfo.Valor;
                _albumsFacebook = albumsInfo;

                var selectList = $("#" + data.IdLista);

                var defaultOption = "";
                if ( albumsInfo.length == 0 )
                {
                    var option = $("<option />", { value: 0, text: "< No hay Albumes de Facebook en " + _filtroActual.Pais.Nombre + " durante el año " + _filtroActual.Periodo + " >"} );
                    selectList.append(option);
                    return;
                }
                else
                {
                    defaultOption = "-1:< SELECCIONAR >;";
                }

                var albums = Helpers.SelectListHelper.formatForSelectList(albumsInfo, "Id", "Nombre");
                albums = defaultOption + albums;
                Helpers.SelectListHelper.fillSelectList(data.IdLista, albums);

                var htmlSelect = $("#" + data.IdLista);
                    htmlSelect.unbind('change');
                    htmlSelect.change(function(event)
                    {
                        limpiarFotosFacebook();
                        var idSeleccion = Helpers.SelectListHelper.getSelectedValueFromSelectList(data.IdLista);
                        if ( idSeleccion == -1 )
                            return;
                        obtenerAlbumFacebookSeleccionado(idSeleccion);
                    });
            }
        }
        function obtenerAlbumFacebookSeleccionado(idSeleccion)
        {
            var album;
            for(var i=0; i < _albumsFacebook.length; i++)
            {
                if ( idSeleccion == _albumsFacebook[i].Id )
                {
                    album = _albumsFacebook[i];
                    cargarFotosAlbumFacebook(album.IdAlbum);
                    break;
                }
            }
        }
        function limpiarFotosFacebook()
        {
            $("#fotosFacebook").empty();
        }
        function cargarFotosAlbumFacebook(albumId)
        {
            $("#pnlFotosFacebook .progreso").fadeIn();
            $("#fotosFacebook").fbPhotos(albumId, null, onLoadFotoFacebook, onLoadAlbumFacebook);
        }
        function onLoadFotoFacebook(imagen)
        {
            //
        }
        function onLoadAlbumFacebook(resultados)
        {
            $("#pnlFotosFacebook .progreso").fadeOut();
            $("#lblCantidadFotosFacebook").text( resultados.length );
        }
    </script>
    <!-- Fin de [Fotos Facebook] -->

    <!-- Fotos Plantaciones -->
    <script type="text/javascript" >
        function obtenerListasFotosPlantaciones(idLista)
        {
            var params              = new Object();
                params.pais         = _filtroActual.Pais.Nombre;
                params.periodo      = _filtroActual.Periodo;
            var jsonParams = JSON.stringify(params);

            var data = new Object();
                data.idLista = idLista;

            Helpers.AJAXHelper.doAjaxCall("obtenerListasFotosPlantaciones", jsonParams, obtenerListasFotosPlantaciones_Respuesta, data);
        }
        function obtenerListasFotosPlantaciones_Respuesta(data, datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var selectList = $("#" + data.idLista);

                var listaPlantaciones   = respuestaInfo.Valor;
                if ( listaPlantaciones.length == 0 )
                {
                    var option = $("<option />", { value: -1, text: "< No hay fotos de Plantaciones en " + _filtroActual.Pais.Nombre + " durante el año " + _filtroActual.Periodo + " >"} );
                    selectList.append(option);
                    return;
                }
                else
                {
                    var option = $("<option />", { value: -1, text: "< SELECCIONAR >"} );
                    selectList.append(option);
                }

                var cantidadPaginas = listaPlantaciones.length / _paginado;
                if ( cantidadPaginas % 1 != 0 )
                    cantidadPaginas = parseInt(cantidadPaginas) +1;
                else
                    cantidadPaginas = parseInt(cantidadPaginas);

                var total = 0;

                for(var i=0; i < cantidadPaginas; i++)
                {
                    _albumsPlantaciones[i] = new Object();
                    var indice  = i+1;
                    var inicio  = (indice * _paginado) - _paginado;
                    var fin     = indice * _paginado;
                    var k = 0;
                    for(var j=inicio; j < fin; j++)
                    {
                        if ( isEmpty(listaPlantaciones[total]) )
                            break;
                        if ( k < _paginado )
                        {
                            _albumsPlantaciones[i][k] = ( listaPlantaciones[total] );
                        }
                        total++;
                        k++;
                    }
                    var option = $("<option />", { value: i, text: "Fotos Plantaciones " + (inicio +1) + " - " + (((inicio +1) +_paginado)-1) } );
                    selectList.append(option);
                }

                $("#lblCantidadFotosPlantaciones").text( listaPlantaciones.length );

                selectList.unbind('change');
                selectList.change(function(event)
                {
                    limpiarFotosPlantaciones();
                    var indice = Helpers.SelectListHelper.getSelectedValueFromSelectList( data.idLista );
                    if ( indice == -1 )
                        return;
                    var listaPlantaciones = _albumsPlantaciones[indice];
                    obtenerFotosPlantaciones( listaPlantaciones );
                })
            }
        }
        function limpiarFotosPlantaciones()
        {
            $("#fotosPlantaciones").empty();
        }
        function obtenerFotosPlantaciones(lista)
        {
            var cantidad = getObjectKeys(lista).length;

            for(var i=0; i < cantidad; i++)
            {
                var idPlantacion = lista[i].Id;

                var imgContainer = $("<div />", { id: "imgPlantacionFoto_" + idPlantacion });
                    imgContainer.addClass("sda-photo");
                var loaderText      = $("<label />", { text: "Cargando.." } );
                var loaderImage = $("<img />", { src: "../Images/Procesando.gif" });
                    loaderImage.addClass("imgLoading");
                imgContainer.append( loaderText );
                imgContainer.append( loaderImage );

                $("#fotosPlantaciones").append( imgContainer );

                var params              = new Object();
                    params.idPlantacion = idPlantacion;
                    params.width        = 200;
                    params.height       = 150;
                var jsonParams  = JSON.stringify(params); 
                
                Helpers.AJAXHelper.doAjaxCall("obtenerPlantacionFotoPreview", jsonParams, obtenerFoto_Response);
            }
        }
        function obtenerFoto_Response(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var plantacionInfo = respuestaInfo.Valor;
                var imgContainer    = $("#imgPlantacionFoto_" + plantacionInfo.Id);
                    imgContainer.empty();
                var imgPlantacion = $("<img />", { src: "data:image/png;base64," + plantacionInfo.Foto });
                    imgPlantacion.addClass("imgSponsor");
                imgPlantacion.appendTo( imgContainer );
                $("#imgPlantacionFoto_" + plantacionInfo.Id).click( function() { verFotoPlantacion(plantacionInfo.Id); } );
            }
        }
    </script>
    <!-- Fin de [Fotos Plantaciones] -->

    <!--HTML-->
    <div id="content"  >

        <div>
            <div id="pnlPrincipal" >
                <!-- Fotos Album Facebook -->
          <%--      <div id="pnlFotosFacebook" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="15%" />
                            <col width="42%" />
                            <col width="16%" />
                            <col width="18%" />
                            <col width="10%" />
                        </colgroup>
                        <tr>
                            <td>
                                &nbsp;<label>Album Facebook:</label>
                            </td>
                            <td>
                                <select id="selFotosFacebook" style="font-size:16px;" ></select>
                            </td>
                            <td align="center" >
                                <label style="text-decoration:underline" >Cantidad de fotos</label>:&nbsp;
                                <label id="lblCantidadFotosFacebook" >0</label>
                            </td>
                            <td>
                                <span class="progreso" >
                                    <label>Cargando fotos..</label>
                                    <img src="../Images/Procesando.gif" alt="Cargando.." />
                                </span>
                            </td>
                            <td>
                                <div id="btnFotosFacebook" >
                                    <img  src="../Images/up.gif" alt="Ocultar" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="fotosFacebook" >
                </div>--%>
                <!-- Fin de [Fotos Album Facebook] -->
                <!--<br />-->
                <!-- Fotos Plantaciones -->
                <div id="pnlFotosPlantaciones" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="15%" />
                            <col width="42%" />
                            <col width="16%" />
                            <col width="18%" />
                            <col width="10%" />
                        </colgroup>
                        <tr>
                            <td>
                                &nbsp;<label>Fotos Plantaciones:</label>
                            </td>
                            <td>
                                <select id="selFotosPlantaciones" style="font-size:16px;"></select>
                            </td>
                            <td align="center" >
                                <label style="text-decoration:underline" >Cantidad de fotos</label>:&nbsp;
                                <label id="lblCantidadFotosPlantaciones" >0</label>
                            </td>
                            <td>
                                <span class="progreso" >
                                    <label>Cargando fotos..</label>
                                    <img src="../Images/Procesando.gif" alt="Cargando.." />
                                </span>
                            </td>
                            <td>
                                <div id="btnFotosPlantaciones" >
                                    <img src="../Images/up.gif" alt="Ocultar" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="fotosPlantaciones" >
                </div>
                <!-- Fin de [Fotos Plantaciones] -->
            </div>
        </div>
        
    </div>

</asp:Content>