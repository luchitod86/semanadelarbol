﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="DetalleNoticia.aspx.cs" Inherits="SemanaDelArbol.Pages.DetalleUsuario" %>
 
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

<style type="text/css" >
        .panelAgregarnoticia
        {
            width: 65%;
            margin: 5px;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idNoticia;
        $(document).ready(function () {
            obtenerUsuarioLogueado(cargarPagina_DetalleNoticia);
        });
        function cargarPagina_DetalleNoticia() {
            idNoticia = getQuerystring("idNoticia");
            obtenerDatosNoticia(idNoticia);

            $("#btnGuardarNoticia").click(function () { guardarNoticia(); });

            $("#txtNoticia_Fecha").datepicker();

            inicializarFileUpload("imgNoticiaFoto1", "imgNoticiaFoto1");
            inicializarFileUpload("imgNoticiaFoto2", "imgNoticiaFoto2");
            inicializarFileUpload("imgNoticiaFoto3", "imgNoticiaFoto3");
            inicializarFileUpload("imgNoticiaFoto4", "imgNoticiaFoto4");
            inicializarFileUpload("imgNoticiaFoto5", "imgNoticiaFoto5");
            inicializarFileUpload("imgNoticiaFoto6", "imgNoticiaFoto6");

            tinymce.init({
                selector: '#txtNoticia_Texto',
                plugins: [
                    "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
                ],

                toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | cut copy paste | bullist numlist",
                toolbar2: "outdent indent blockquote | undo redo | link unlink | insertdatetime | forecolor backcolor | removeformat | subscript superscript | charmap emoticons | spellchecker preview",
                toolbar3: "styleselect formatselect fontselect fontsizeselect",

                menubar: false,
                toolbar_items_size: 'small',
                autosave_ask_before_unload: false,

                style_formats: [
                        { title: 'Bold text', inline: 'b' },
                        { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
                        { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
                        { title: 'Example 1', inline: 'span', classes: 'example1' },
                        { title: 'Example 2', inline: 'span', classes: 'example2' },
                        { title: 'Table styles' },
                        { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
                ],

                templates: [
                        { title: 'Test template 1', content: 'Test 1' },
                        { title: 'Test template 2', content: 'Test 2' }
                ]
            });            
        }

        function obtenerDatosNoticia(idNoticia) {
            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("obtenerNoticia", "{'idNoticia':'" + idNoticia + "'}", obtenerDatosNoticia_Response);
        }
        function obtenerDatosNoticia_Response(datos) {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var noticiaInfo = respuestaInfo.Valor;
                
                $("#txtNoticia_Titulo").val(noticiaInfo.Titulo);
                $("#txtNoticia_Resumen").val(noticiaInfo.Resumen);
                $("#txtNoticia_Texto").val(noticiaInfo.Texto);
                $("#txtNoticia_Fecha").val(noticiaInfo.Fecha);               
                
                if (noticiaInfo.Foto1 != null && noticiaInfo.Foto1 != '')
                    $("#imgNoticiaFoto1").attr("src", '..' + noticiaInfo.Foto1);
                if (noticiaInfo.Foto2 != null && noticiaInfo.Foto2 != '')
                    $("#imgNoticiaFoto2").attr("src", '..' + noticiaInfo.Foto2);
                if (noticiaInfo.Foto3 != null && noticiaInfo.Foto3 != '')
                    $("#imgNoticiaFoto3").attr("src", '..' + noticiaInfo.Foto3);
                if (noticiaInfo.Foto4 != null && noticiaInfo.Foto4 != '')
                    $("#imgNoticiaFoto4").attr("src", '..' + noticiaInfo.Foto4);
                if (noticiaInfo.Foto5 != null && noticiaInfo.Foto5 != '')
                    $("#imgNoticiaFoto5").attr("src", '..' + noticiaInfo.Foto5);
                if (noticiaInfo.Foto6 != null && noticiaInfo.Foto6 != '')
                    $("#imgNoticiaFoto6").attr("src", '..' + noticiaInfo.Foto6);

                tinymce.get("txtNoticia_Texto").setContent(noticiaInfo.Texto);
            }
        }

        function guardarNoticia() {
            $("#txtNoticia_Texto").val(tinymce.get("txtNoticia_Texto").getContent());
            var infoValidacion =
                [
                    { campo: "txtNoticia_Resumen", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un Resumen." }] },
                    { campo: "txtNoticia_Titulo", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una Titulo." }] },
                    { campo: "txtNoticia_Texto", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un Texto." }] },
                    { campo: "txtNoticia_Fecha", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una Fecha." }] }
                ];

            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            var noticia = new Object();

            noticia.Id = idNoticia;
            noticia.Titulo = $("#txtNoticia_Titulo").getValue();
            noticia.Resumen = $("#txtNoticia_Resumen").getValue();
            noticia.Fecha = $("#txtNoticia_Fecha").getValue();
            noticia.Foto1 = getBase64Image("imgNoticiaFoto1");
            noticia.Foto2 = getBase64Image("imgNoticiaFoto2");
            noticia.Foto3 = getBase64Image("imgNoticiaFoto3");
            noticia.Foto4 = getBase64Image("imgNoticiaFoto4");
            noticia.Foto5 = getBase64Image("imgNoticiaFoto5");
            noticia.Foto6 = getBase64Image("imgNoticiaFoto6");
            noticia.IdPais = _filtroActual.Pais.Id;

            var jsonParams = JSON.stringify(noticia).replace(/'/g, "\\'");

            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("Pages/AgregarNoticia.aspx/guardarNoticia", "{'infoNoticia':'" + jsonParams + "', 'textoNoticia': '" + $("#txtNoticia_Texto").getValue() + "'}", guardarNoticia_Respuesta);
        }

        function guardarNoticia_Respuesta(datos) {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var noticiaInfo = respuestaInfo.Valor;
                var onConfirmarNoticiaGuardada = overloadFunction(confirmarNoticiaGuardada, noticiaInfo);
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, 'Mensaje', null, onConfirmarNoticiaGuardada);
            }
            else {
                Helpers.NotificationHelper.showWarning(respuestaInfo.Mensaje);
            }
        }
        function confirmarNoticiaGuardada(noticiaInfo) {
            abrirPagina(_secciones.AdminNoticias);
        }

    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >
            <input type="button" id="btnGuardarNoticia" value="Guardar Noticia" class="button button btnAgregar" style="width:145px" />
            <div class="seccion" >
                <div class="titulo" >
                    Datos de la Noticia
                </div>
                <div id="pnlAgregarNoticia" class="panelAgregarnoticia" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="40%" />
                            <col width="60%" />
                        </colgroup>
                        <tr>
                            <td>
                                <label>Titulo:</label>
                            </td>
                            <td>
                                <input id="txtNoticia_Titulo" type="text" value="" />
                            </td>
                        </tr>                        
                        <tr>
                            <td>
                                <label>Resumen:</label>
                            </td>
                            <td>
                                <textarea id="txtNoticia_Resumen" rows="4" maxlength="255"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Texto:</label>
                            </td>
                            <td>
                                <textarea id="txtNoticia_Texto" rows="8"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Fecha:</label>
                            </td>
                            <td>
                                <input id="txtNoticia_Fecha" type="text" readonly="true" style="width:275px;"/>
                            </td>
                        </tr>
                        <td>   
                            <label>Imágenes:</label>                         
                        </td>
                        <td class="fotos">
                            <div style="display:inline-block;">
                                <img id="imgNoticiaFoto1" src="" width="100px" height="80px" class="foto" />
                                <input type="button" value="Eliminar" onclick="$('#imgNoticiaFoto1').attr('src', '');" style="display:block;" />
                            </div>
                            <div style="display:inline-block;">
                                <img id="imgNoticiaFoto2" src="" width="100px" height="80px" class="foto" />
                                <input type="button" value="Eliminar" onclick="$('#imgNoticiaFoto2').attr('src', '');" style="display:block;" />
                            </div>
                            <div style="display:inline-block;">
                                <img id="imgNoticiaFoto3" src="" width="100px" height="80px" class="foto" />
                                <input type="button" value="Eliminar" onclick="$('#imgNoticiaFoto3').attr('src', '');" style="display:block;" />
                            </div>
                            <div style="display:inline-block;">
                                <img id="imgNoticiaFoto4" src="" width="100px" height="80px" class="foto" />
                                <input type="button" value="Eliminar" onclick="$('#imgNoticiaFoto4').attr('src', '');" style="display:block;" />
                            </div>
                            <div style="display:inline-block;">
                                <img id="imgNoticiaFoto5" src="" width="100px" height="80px" class="foto" />
                                <input type="button" value="Eliminar" onclick="$('#imgNoticiaFoto5').attr('src', '');" style="display:block;" />
                            </div>
                            <div style="display:inline-block;">
                                <img id="imgNoticiaFoto6" src="" width="100px" height="80px" class="foto" />
                                <input type="button" value="Eliminar" onclick="$('#imgNoticiaFoto6').attr('src', '');" style="display:block;" />
                            </div>
                        </td>
                    </table>
                </div>
            </div>      
        </div>     
     
    </div>

</asp:Content>