﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using SemanaDelArbol.Helpers;
using Controladores;
using Entidades;
using Entidades.jSON;
using Entidades.jSON.Sistema;
using System.Web.Script.Services;
using System.Text;
using Comun;
using System.Configuration;
using System.Net;
using System.IO;
using System.Drawing;
using System.Web.Caching;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Reflection;

namespace SemanaDelArbol.Pages
{
    public class PaginaBase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //var request = this.Context.Request;
                var pagina = Helpers.ControlHelper.ObtenerNombreControl(this).ToLower();

                this.ValidarPermisosUsuario(pagina);

                FiltroInfo filtroInfo = SessionHelper.ObtenerFiltroActual();

                #region Localization
                var paisesInfo = ObtenerListaPaises(false);
                if ( paisesInfo != null )
                {
                    var paisInfo = (PaisInfo)(paisesInfo.Where(x => x.Id == filtroInfo.IdPais).FirstOrDefault());
                    if (paisInfo != null)
                        Helpers.LocalizationHelper.SetCurrentLanguage(paisInfo.Lenguaje);
                }
                #endregion

                MasterPage  masterPage = (MasterPage)this.Master;

                #region Filtro Actual
                masterPage.PaisActual(filtroInfo.Pais);
                masterPage.PaisActual(filtroInfo.IdPais);
                masterPage.PeriodoActual(filtroInfo.Periodo);
                #endregion

                #region Tipos Usuario
                var usuarioTipos = ObtenerDesdeCache<UsuarioTipo>(Comun.Constantes.CacheKeys.LISTA_USUARIO_TIPOS);
                masterPage.UsuarioTipos(Comun.Formateador.jSON.convertToJson(usuarioTipos));
                #endregion

                #region Perfiles Usuario
                var usuarioPerfiles = ObtenerDesdeCache<UsuarioPerfil>(Comun.Constantes.CacheKeys.LISTA_USUARIO_PERFILES);
                masterPage.UsuarioPerfiles(Comun.Formateador.jSON.convertToJson(usuarioPerfiles));
                #endregion

                #region Tipos Sponsor
                var tiposSponsor = ObtenerDesdeCache<SponsorTipo>(Comun.Constantes.CacheKeys.LISTA_SPONSOR_TIPOS);
                masterPage.SponsorsTipos(Comun.Formateador.jSON.convertToJson(tiposSponsor));
                #endregion

                #region Preferencias Usuario
                var preferenciasUsuario = Helpers.SessionHelper.GetUserPreferences();
                if (preferenciasUsuario.IsNull())
                {
                    preferenciasUsuario = new List<UsuarioPreferencia>()
                    {
                        new UsuarioPreferencia() { Nombre="MostrarAccionesComoParticipar", Valor = true },
                        new UsuarioPreferencia() { Nombre="MostrarActividadesAgendadas", Valor = true },
                        new UsuarioPreferencia() { Nombre="MostrarPanelFlotanteUsuarioLogueado", Valor = true }
                    };
                    Helpers.SessionHelper.SetUserPreferences(preferenciasUsuario);
                }
                masterPage.PreferenciasUsuario(Comun.Formateador.jSON.convertToJson(preferenciasUsuario));
                #endregion

                Helpers.ContentHelper.EstablecerContenidoSeccion(this, filtroInfo.IdPais);

                //this.SetMetaTags(pagina);
                //this.ProcessPage(request);
                //this.ProcessUserIdentity(request);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
        }

        #region Web Methods

        #region Filtros

        [WebMethod]
        public static string obtenerFiltrosActuales()
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                valor = SessionHelper.ObtenerFiltroActual();
                resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string actualizarFiltro(int periodo, string pais, int idPais)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                FiltroInfo filtroInfo = SessionHelper.ActualizarValoresFiltro(periodo, pais, idPais);
                valor = filtroInfo;
                resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Sesion

        [WebMethod]
        public static string informarFechaCreacionUsuario(int dia, int mes, int año)
        {
            string mensaje  = Resources.Global.Actualizar_Mensaje_Error;
            bool resultado  = false;
            object valor    = null;
            try
            {
                var usuario = SessionHelper.GetCurrentUser();
                if (usuario != null)
                {
                    var uc = new UsuarioControlador();
                    
                    usuario = uc.ObtenerPorId(usuario.Id);
                    usuario.FechaCreacion = new DateTime(año, mes, dia);

                    uc.Guardar(usuario);

                    mensaje = "La fecha se ha registrado exitosamente.<br /><br />¡Muchas gracias por su colaboración!";
                    resultado = true;

                    Helpers.SessionHelper.SetCurrentUser(usuario);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Obtiene el usuario logueado
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerUsuarioLogueado()
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                Usuario usuario = SessionHelper.GetCurrentUser();
                if (usuario != null)
                {
                    usuario     = new UsuarioControlador().ObtenerPorId(usuario.Id);
                    valor       = new UsuarioInfo(usuario);
                    resultado   = true;
                }
                else
                {
                    mensaje = "No se ha podido recuperar la información del Usuario logueado.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Abre una nueva sesión
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [WebMethod]
        public static string abrirSesion(string nombreUsuario, string password)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                Usuario usuario = uc.ObtenerPorUsuarioYPassword(nombreUsuario, password);
                if ( usuario != null)
                {
                    SessionHelper.SetCurrentUser(usuario);
                    mensaje     = usuario.Nombre + " ha iniciado sesión.";
                    valor       = new UsuarioInfo(usuario);
                    resultado   = true;
                }
                else
                {
                    mensaje = "Los datos son incorrectos.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Valida la asociación la cuenta de Facebook/Twitter con el Usuario
        /// </summary>
        /// <param name="redSocial"></param>
        /// <param name="nombreUsuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellido"></param>
        /// <returns></returns>
        [WebMethod]
        public static string validarAsociacionCuentas(string redSocial, string nombreUsuario, string nombre, string apellido)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                UsuarioInfo usuarioInfo = new UsuarioInfo();

                switch ( redSocial )
                {
                    case Constantes.RedSocial.FACEBOOK:
                        usuarioInfo.NombreUsuarioFacebook   = nombreUsuario;
                        break;
                    case Constantes.RedSocial.TWITTER:
                        usuarioInfo.NombreUsuarioTwitter    = nombreUsuario;
                        break;
                    case Constantes.RedSocial.GOOGLE_PLUS:
                        usuarioInfo.NombreUsuarioGooglePlus = nombreUsuario;
                        break;
                }

                Usuario usuario = uc.ObtenerPorUsuarioRedSocial( redSocial, nombreUsuario );
                if ( usuario != null)
                {
                    //Existe un Usuario con esa cuenta asignada, entonces la asociación ya está hecha..
                    SessionHelper.SetValue(Constantes.SessionKeys.USUARIO_LOGUEADO, usuario);
                    mensaje = "";
                    usuarioInfo = new UsuarioInfo(usuario);
                    valor = usuarioInfo;
                    resultado = true;
                }
                else
                {
                    mensaje = "";
                    usuarioInfo.Apellido = apellido;
                    usuarioInfo.Nombre   = nombre;
                    valor = usuarioInfo;
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Cierra la sesión actual
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string cerrarSesion()
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                Usuario usuario = SessionHelper.GetCurrentUser();
                if ( usuario != null)
                {
                    Helpers.SessionHelper.LogOff();
                    mensaje = "Sesión cerrada";
                    resultado = true;
                }
                else
                {
                    mensaje = "La sesión no pudo ser cerrada.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Registra un nuevo Usuario en el sistema
        /// </summary>
        /// <param name="datos"></param>
        /// <returns></returns>
        [WebMethod]
        public static string registrar(string infoUsuario)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                Usuario usuario = null;
                UsuarioControlador uc = new UsuarioControlador();

                //Parseo los datos enviados..
                var usuarioInfo = Comun.Formateador.jSON.convertFromJson<UsuarioInfo>(infoUsuario);
            
                resultado = true;

                usuario = uc.ObtenerPorUsuario(usuarioInfo.NombreUsuario);
                if ( usuario != null)
                {
                    mensaje = "Ya existe una cuenta registrada con ese nombre de usuario.";
                    resultado = false;
                }

                usuario = uc.ObtenerPorEmail(usuarioInfo.Email);
                if ( usuario != null)
                {
                    mensaje = "Ya existe una cuenta registrada con esa casilla de e-mail.";
                    resultado = false;
                }

                if ( resultado )
                {
                    usuario = new Usuario();

                    #region Web
                    usuario.NombreUsuario           = usuarioInfo.NombreUsuario;
                    usuario.NombreUsuarioFacebook   = usuarioInfo.NombreUsuarioFacebook;
                    usuario.NombreUsuarioTwitter    = usuarioInfo.NombreUsuarioTwitter;
                    usuario.NombreUsuarioGooglePlus = usuarioInfo.NombreUsuarioGooglePlus;
                    usuario.Password                = usuarioInfo.Password;
                    usuario.FechaCreacion           = DateTime.Now;
                    #endregion

                    usuario.Tipo        = uc.ObtenerTipoUsuario(usuarioInfo.IdTipo);
                    usuario.Perfil      = uc.ObtenerPerfilUsuario(usuarioInfo.IdPerfil);

                    usuario.Nombre      = usuarioInfo.Nombre;
                    usuario.Apellido    = usuarioInfo.Apellido;
                    usuario.Email       = usuarioInfo.Email;
                    usuario.Telefono    = usuarioInfo.Telefono;
                    usuario.Web         = usuarioInfo.Web;

                    #region Ubicacion
                    usuario.Pais        = new PaisControlador().ObtenerPorId(Convert.ToInt32(usuarioInfo.IdPais));
                    usuario.Provincia   = new ProvinciaControlador().ObtenerPorId(Convert.ToInt32(usuarioInfo.IdProvincia));
                    usuario.Ciudad      = usuarioInfo.Ciudad;
                    #endregion
                    
                    resultado = uc.Guardar(usuario);
                    if ( resultado )
                    {
                        mensaje = "El usuario ha sido registrado exitosamente.";
                        resultado = true;
                        valor = new UsuarioInfo(usuario);
                        SessionHelper.SetValue(Constantes.SessionKeys.USUARIO_LOGUEADO, usuario);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Guarda los datos de un Usuario
        /// </summary>
        /// <param name="infoUsuario"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarUsuario(string infoUsuario)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();

                var usuarioInfo = Comun.Formateador.jSON.convertFromJson<UsuarioInfo>(infoUsuario);

                var usuario = uc.ObtenerPorId(usuarioInfo.Id);
                if (usuario.IsNotNull())
                {
                    usuario.NombreUsuario = usuarioInfo.NombreUsuario;
                    usuario.Password = usuarioInfo.Password;

                    usuario.Tipo = uc.ObtenerTipoUsuario(usuarioInfo.IdTipo);
                    usuario.Perfil = uc.ObtenerPerfilUsuario(usuarioInfo.IdPerfil);

                    usuario.Nombre = usuarioInfo.Nombre;
                    usuario.Apellido = usuarioInfo.Apellido;
                    usuario.Email = usuarioInfo.Email;
                    usuario.Telefono = usuarioInfo.Telefono;
                    usuario.Web = usuarioInfo.Web;

                    usuario.Pais = new PaisControlador().ObtenerPorId(Convert.ToInt32(usuarioInfo.IdPais));
                    usuario.Provincia = new ProvinciaControlador().ObtenerPorId(Convert.ToInt32(usuarioInfo.IdProvincia));
                    usuario.Ciudad = usuarioInfo.Ciudad;

                    resultado = uc.Guardar(usuario);

                    if (resultado)
                    {
                        mensaje = Resources.Global.Actualizar_Mensaje_Exito;
                        resultado = true;
                        valor = new UsuarioInfo(usuario);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Actualizar las preferencias del Usurio
        /// </summary>
        /// <param name="preferencias"></param>
        /// <returns></returns>
        [WebMethod]
        public static string actualizarPreferenciasUsuario(string preferencias)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                var listaPreferencias = Comun.Formateador.jSON.convertFromJson<List<UsuarioPreferencia>>(preferencias);
                Helpers.SessionHelper.SetUserPreferences(listaPreferencias);

                //var preferenciasUsuario = new PreferenciasUsuario();
                //preferenciasUsuario.Preferencias = listaPreferencias;
                //resultado = new ControladorBase<UsuarioPreferencia>().GuardarEntidad(preferenciasUsuario);
                //if (resultado)
                //{
                //    mensaje = "Las preferencias de usuario han sido guardadas exitosamente.";
                //    resultado = true;
                //}
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Email

        /// <summary>
        /// Enviar mail al sitio
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="mail"></param>
        /// <param name="comentarios"></param>
        /// <returns></returns>
        [WebMethod]
        public static string enviarEmailContacto(string nombre, string mail, string comentarios)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                Comun.Email objEmail = new Comun.Email();
                Dictionary<string, string> destinatarios = new Dictionary<string,string>();

                ConfiguracionControlador cc = new ConfiguracionControlador();
                Configuracion configuracion = cc.ObtenerConfiguracion();

                destinatarios.Add(configuracion.Email.EmisorAlias, configuracion.Email.EmisorDireccion);

                string asunto = "Mail de contacto enviado desde web Semana del Árbol";
              
                string[] cuerpo = new string[] { nombre, mail, comentarios };

                string Emisor_Nombre    = configuracion.Email.EmisorAlias;
                string Emisor_Direccion = configuracion.Email.EmisorDireccion;
                string Emisor_Password  = configuracion.Email.EmisorPassword;
                string SMTP_Servidor    = configuracion.Email.SMTPServidor;
                int SMTP_Puerto         = configuracion.Email.SMTPPuerto;
                bool SMTP_HabilitarSSL  = configuracion.Email.SMTPHabilitarSSL;

                objEmail.EnviarMailContacto(Emisor_Nombre, Emisor_Direccion, Emisor_Password, SMTP_Servidor, SMTP_Puerto, SMTP_HabilitarSSL, destinatarios, asunto, cuerpo, System.Text.Encoding.UTF8, true);

                //persistir la información del email en la base de datos..
                new EmailControlador().Guardar(new Entidades.Email(nombre, mail, comentarios));

                mensaje = "Mail enviado exitosamente.";
                resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Enviar mail al Donante para informar de una Adopción
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="mail"></param>
        /// <param name="comentarios"></param>
        /// <returns></returns>
        [WebMethod]
        public static string enviarEmailAdopcion(string usuario, string especie, string cantidad, string mailUsuario, string donante, string mailDonante)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                Comun.Email objEmail = new Comun.Email();
                Dictionary<string, string> destinatarios = new Dictionary<string,string>();

                ConfiguracionControlador cc = new ConfiguracionControlador();
                Configuracion configuracion = cc.ObtenerConfiguracion();

                destinatarios.Add(donante, mailDonante);

                string asunto = "¡Te han realizado una adopción! - Web Semana del Árbol";
              
                string[] cuerpo = new string[] { usuario, especie, cantidad, mailUsuario };

                string Emisor_Nombre    = configuracion.Email.EmisorAlias;
                string Emisor_Direccion = configuracion.Email.EmisorDireccion;
                string Emisor_Password  = configuracion.Email.EmisorPassword;
                string SMTP_Servidor    = configuracion.Email.SMTPServidor;
                int SMTP_Puerto         = configuracion.Email.SMTPPuerto;
                bool SMTP_HabilitarSSL  = configuracion.Email.SMTPHabilitarSSL;

                objEmail.EnviarMailAdopcion(Emisor_Nombre, Emisor_Direccion, Emisor_Password, SMTP_Servidor, SMTP_Puerto, SMTP_HabilitarSSL, destinatarios, asunto, cuerpo, System.Text.Encoding.UTF8, true);

                mensaje = "Se ha enviado un mail al donante para informarle de la adopción.";
                resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Enviar mail con password del Usuario al que corresponde el nombre de usuario provisto
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [WebMethod]
        public static string enviarEmailRecuperacion(string nombreUsuario, string email)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                Comun.Email objEmail = new Comun.Email();
                Dictionary<string, string> destinatarios = new Dictionary<string,string>();

                UsuarioControlador uc = new UsuarioControlador();
                ConfiguracionControlador cc = new ConfiguracionControlador();
                Configuracion configuracion = cc.ObtenerConfiguracion();

                Usuario usuario = null;

                if ( !string.IsNullOrEmpty(nombreUsuario) )
                    usuario = uc.ObtenerPorUsuario(nombreUsuario);

                if ( !string.IsNullOrEmpty(email) )
                    usuario = uc.ObtenerPorEmail(email);
                
                if ( usuario != null )
                {
                    if ( usuario.Email != null && usuario.Email.Trim().Length > 0 )
                    {
                        destinatarios.Add(usuario.NombreUsuario, usuario.Email);

                        string asunto = "Recuperación de Contraseña - Web Semana del Árbol";
              
                        string[] cuerpo = new string[] { usuario.NombreUsuario, usuario.Password, usuario.Email };

                        string Emisor_Nombre    = configuracion.Email.EmisorAlias;
                        string Emisor_Direccion = configuracion.Email.EmisorDireccion;
                        string Emisor_Password  = configuracion.Email.EmisorPassword;
                        string SMTP_Servidor    = configuracion.Email.SMTPServidor;
                        int SMTP_Puerto         = configuracion.Email.SMTPPuerto;
                        bool SMTP_HabilitarSSL  = configuracion.Email.SMTPHabilitarSSL;

                        objEmail.EnviarMailRecuperacionPassword(Emisor_Nombre, Emisor_Direccion, Emisor_Password, SMTP_Servidor, SMTP_Puerto, SMTP_HabilitarSSL, destinatarios, asunto, cuerpo, System.Text.Encoding.UTF8, true);

                        mensaje = "Se ha enviado un mail con la contraseña a la casilla de correo asociada a este usuario.";
                        resultado = true;
                    }
                    else
                    {
                        mensaje = "El Usuario no registró su casilla de email.";
                    }
                }
                else
                {
                    mensaje = "No se encontró ningún Usuario con ese Nombre de Usuario.";
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Enviar mail al Administrador reportando un error
        /// </summary>
        /// <param name="pagina"></param>
        /// <param name="exception"></param>
        /// <param name="stackTrace"></param>
        /// <returns></returns>
        [WebMethod]
        public static string enviarEmailError(string pagina, string exception, string stackTrace)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                Comun.Email objEmail = new Comun.Email();
                Dictionary<string, string> destinatarios = new Dictionary<string, string>();

                ConfiguracionControlador cc = new ConfiguracionControlador();
                Configuracion configuracion = cc.ObtenerConfiguracion();

                destinatarios.Add(configuracion.Email.EmisorAlias, configuracion.Email.EmisorDireccion);

                string asunto = "Email de reporte de error ocurrido en la web Semana del Árbol";

                var now     = DateTime.Now;
                var fecha   = now.ToShortDateString();
                var hora    = now.ToShortTimeString();

                string[] cuerpo = new string[] { fecha, hora, pagina, exception, stackTrace };

                string Emisor_Nombre = configuracion.Email.EmisorAlias;
                string Emisor_Direccion = configuracion.Email.EmisorDireccion;
                string Emisor_Password = configuracion.Email.EmisorPassword;
                string SMTP_Servidor = configuracion.Email.SMTPServidor;
                int SMTP_Puerto = configuracion.Email.SMTPPuerto;
                bool SMTP_HabilitarSSL = configuracion.Email.SMTPHabilitarSSL;

                //objEmail.EnviarMailError(Emisor_Nombre, Emisor_Direccion, Emisor_Password, SMTP_Servidor, SMTP_Puerto, SMTP_HabilitarSSL, destinatarios, asunto, cuerpo, System.Text.Encoding.UTF8, true);

                //LogHelper.LogError(pagina, exception, stackTrace);

                mensaje = "Muchas gracias por su colaboración.<br />El error ha sido reportado al Administrador del sitio.";
                resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Enviar mail de solicitud de país
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="mail"></param>
        /// <param name="comentarios"></param>
        /// <returns></returns>
        [WebMethod]
        public static string enviarEmailSolicitudPais(string pais, string email, string organizacion, string paginaWeb, string comentarios)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                Comun.Email objEmail = new Comun.Email();
                Dictionary<string, string> destinatarios = new Dictionary<string, string>();

                ConfiguracionControlador cc = new ConfiguracionControlador();
                Configuracion configuracion = cc.ObtenerConfiguracion();

                destinatarios.Add(configuracion.Email.EmisorAlias, configuracion.Email.EmisorDireccion);

                string asunto = "Mail de solicitud de país enviado desde web Semana del Árbol";

                string[] cuerpo = new string[] { pais, email, organizacion, paginaWeb, comentarios };

                string Emisor_Nombre = configuracion.Email.EmisorAlias;
                string Emisor_Direccion = configuracion.Email.EmisorDireccion;
                string Emisor_Password = configuracion.Email.EmisorPassword;
                string SMTP_Servidor = configuracion.Email.SMTPServidor;
                int SMTP_Puerto = configuracion.Email.SMTPPuerto;
                bool SMTP_HabilitarSSL = configuracion.Email.SMTPHabilitarSSL;

                objEmail.EnviarMailSolicitudPais(Emisor_Nombre, Emisor_Direccion, Emisor_Password, SMTP_Servidor, SMTP_Puerto, SMTP_HabilitarSSL, destinatarios, asunto, cuerpo, System.Text.Encoding.UTF8, true);

                mensaje = "Su solicitud ha sido enviada exitosamente.";
                resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Especies

        [WebMethod]
        public static string obtenerEspecies(int idPais)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            object valor   = null;
            try
            {
                valor       = ObtenerListaEspecies(idPais);
                resultado   = true;
                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerListadoEspecies()
        {
            bool resultado = false;
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            object valor   = null;
            try
            {
                var especies = new EspecieControlador().ObtenerTodas();
                if (especies != null)
                {
                    var especiesInfo = new List<EspecieInfo>();
                    foreach (var especie in especies)
                    {
                        especiesInfo.Add(new EspecieInfo(especie));
                    }
                    valor       = especiesInfo;
                    resultado   = true;
                    mensaje     = Resources.Global.Obtener_Mensaje_Exito;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerListadoEspecieLocalizaciones(int idEspecie)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            object valor = null;
            try
            {
                var localizaciones = new EspecieControlador().ObtenerLocalizaciones(idEspecie);
                if (localizaciones != null)
                {
                    var localizacionesEspecieInfo = new List<EspeciePaisInfo>();
                    foreach (var localizacion in localizaciones)
                    {
                        localizacionesEspecieInfo.Add(new EspeciePaisInfo(localizacion));
                    }
                    valor = localizacionesEspecieInfo;
                    resultado = true;
                    mensaje = Resources.Global.Obtener_Mensaje_Exito;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerEspecie(int id)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            object valor = null;
            try
            {
                var especie = new EspecieControlador().ObtenerPorId(id);
                if (especie.IsNotNull())
                {
                    valor = new EspecieInfo(especie, true);
                }
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerEspecieLocalizacion(int id)
        {
            bool resultado  = false;
            string mensaje  = Resources.Global.Obtener_Mensaje_Error;
            object valor    = null;
            try
            {
                var especiePais = new ControladorBase<EspeciePais>().ObtenerEntidad(id);
                if (especiePais.IsNotNull())
                {
                    valor = new EspeciePaisInfo(especiePais);
                }
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string guardarEspecie(int id, string nombre, string nombreCientifico, string factorCO2, bool aprobado)
        {
            bool resultado  = false;
            string mensaje  = Resources.Global.Guardar_Mensaje_Error;
            object valor    = null;
            try
            {
                var cb = new ControladorBase<Especie>();

                var especie = new Especie();
                if (id != -1)
                    especie = cb.ObtenerEntidad(id);

                especie.Nombre = nombre;
                especie.NombreCientifico = nombreCientifico;
                especie.FactorCO2 = Convert.ToDouble(factorCO2.Replace(".", ","));
                especie.Aprobado = aprobado;

                cb.GuardarEntidad(especie);

                var paises = ObtenerListaPaises(true);
                foreach (var pais in paises)
                {
                    Helpers.CacheHelper.RemoveValue(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_ESPECIES, pais.Id));
                }
                mensaje     = Resources.Global.Guardar_Mensaje_Exito;
                valor       = new EspecieInfo(especie, false);
                resultado   = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string guardarEspecieWiki(string especieInfo, int idUsuario, string paises,
                                string follaje ,string hojas ,string flores ,string frutos ,string masInfo)
        {

            bool resultado = false;
            string mensaje = Resources.Global.Guardar_Mensaje_Error;
            object valor = null;
            try
            {                
                var uc = new EspecieControlador();
                var pc = new PaisControlador();
                
                //Parseo los datos enviados..
                var espInfo = Comun.Formateador.jSON.convertFromJson<EspecieInfo>(especieInfo);

                resultado = true;

                if (resultado)
                {
                    var especie = new Especie();

                    #region Web
                    if (espInfo.Id != 0)
                        especie = uc.ObtenerPorId(espInfo.Id);
                    especie.Nombre = espInfo.Nombre;
                    especie.NombreCientifico = espInfo.NombreCientifico;
                    especie.FactorCO2 = 0.56;
                    especie.Origen = espInfo.Origen;
                    especie.Altura = espInfo.Altura;
                    especie.Follaje = follaje;
                    especie.Hojas = hojas;
                    especie.Flores = flores;
                    especie.Frutos = frutos;
                    especie.MasInfo = masInfo;
                    especie.DesdeWiki = true;
                    especie.Aprobado = false;
                    especie.Ecorregion = espInfo.Ecorregion;
                    especie.AptoCantero = espInfo.AptoCantero;
                    especie.AptoPlaza = espInfo.AptoPlaza;
                    especie.AptoVereda = espInfo.AptoVereda;
                    especie.EsNativo = espInfo.EsNativo;
                    especie.EsExotico = espInfo.EsExotico;

                    if (especie.Paises == null)
                        especie.Paises = new List<EspeciePaisWiki>();
                    string[] stringSeparators = new string[] {"***"};
                    string[] listPaises = paises.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    foreach(string nombrePais in listPaises)
                    {
                        bool existPais = false;
                        foreach (EspeciePaisWiki pais in especie.Paises)
                        {
                            if (!existPais && pais.Pais.Nombre == nombrePais)
                                existPais = true;
                        }
                        if (!existPais){
                            Pais p = pc.ObtenerPorNombre(nombrePais);
                            especie.Paises.Add(new EspeciePaisWiki() { Pais = p, Especie = especie });
                        }
                    }


                    Usuario us = new UsuarioControlador().ObtenerPorId(idUsuario);
                    if(especie.Usuarios == null)
                        especie.Usuarios = new List<EspecieUsuario>();

                    bool existUser = false;
                    foreach (EspecieUsuario usuario in especie.Usuarios)
                    {
                        if (!existUser && usuario.Usuario.Id == idUsuario)
                            existUser = true;
                    }
                    if (!existUser)
                        especie.Usuarios.Add(new EspecieUsuario() { Usuario = us, Especie = especie });
                    

                    string folder = "/Images/Especies/";
                    string path = HttpContext.Current.Server.MapPath(folder);
                    string fileName = string.Empty;
                    string format = "png";
                    if(especie.Imagenes == null)
                        especie.Imagenes = new List<EspecieImagen>();

                    List<int> removeList = new List<int>();

                    if (espInfo.Foto1 != null && espInfo.Foto1 != "" && !espInfo.Foto1.Contains(folder))                        
                    {
                            fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                            if (especie.Imagenes.Count >= 1 && especie.Imagenes.ElementAt(0) != null)
                                especie.Imagenes.ElementAt(0).URLImagen = folder + fileName;
                            else
                                especie.Imagenes.Add(new EspecieImagen() { Especie = especie, URLImagen = folder + fileName });
                            uploadImagen(espInfo.Foto1, path, fileName, 600, 450);
                    }
                    else
                    {
                        if (espInfo.Foto1 == "" && especie.Imagenes.Count >= 1)
                             removeList.Insert(0, 0);
                    }

                    if (espInfo.Foto2 != null && espInfo.Foto2 != "" && !espInfo.Foto2.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (especie.Imagenes.Count >= 2 && especie.Imagenes.ElementAt(1) != null)
                            especie.Imagenes.ElementAt(1).URLImagen = folder + fileName;
                        else
                            especie.Imagenes.Add(new EspecieImagen() { Especie = especie, URLImagen = folder + fileName });
                        uploadImagen(espInfo.Foto2, path, fileName, 600, 450);
                    }
                    else
                    {
                        if (espInfo.Foto2 == "" && especie.Imagenes.Count >= 2)
                            removeList.Insert(0, 1);
                    }

                    if (espInfo.Foto3 != null && espInfo.Foto3 != "" && !espInfo.Foto3.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (especie.Imagenes.Count >= 3 && especie.Imagenes.ElementAt(2) != null)
                            especie.Imagenes.ElementAt(2).URLImagen = folder + fileName;
                        else
                            especie.Imagenes.Add(new EspecieImagen() { Especie = especie, URLImagen = folder + fileName });
                        uploadImagen(espInfo.Foto3, path, fileName, 600, 450);
                    }
                    else
                    {
                        if (espInfo.Foto3 == "" && especie.Imagenes.Count >= 3)
                            removeList.Insert(0, 2);
                    }
                    if (espInfo.Foto4 != null && espInfo.Foto4 != "" && !espInfo.Foto4.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (especie.Imagenes.Count >= 4 && especie.Imagenes.ElementAt(3) != null)
                            especie.Imagenes.ElementAt(3).URLImagen = folder + fileName;
                        else
                            especie.Imagenes.Add(new EspecieImagen() { Especie = especie, URLImagen = folder + fileName });
                        uploadImagen(espInfo.Foto4, path, fileName, 600, 450);
                    }
                    else
                    {
                        if (espInfo.Foto4 == "" && especie.Imagenes.Count >= 4)
                            removeList.Insert(0, 3);
                    }
                    if (espInfo.Foto5 != null && espInfo.Foto5 != "" && !espInfo.Foto5.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (especie.Imagenes.Count >= 5 && especie.Imagenes.ElementAt(4) != null)
                            especie.Imagenes.ElementAt(4).URLImagen = folder + fileName;
                        else
                            especie.Imagenes.Add(new EspecieImagen() { Especie = especie, URLImagen = folder + fileName });
                        uploadImagen(espInfo.Foto5, path, fileName, 600, 450);
                    }
                    else
                    {
                        if (espInfo.Foto5 == "" && especie.Imagenes.Count >= 5)
                            removeList.Insert(0, 4);
                    }
                    if (espInfo.Foto6 != null && espInfo.Foto6 != "" && !espInfo.Foto6.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (especie.Imagenes.Count >= 6 && especie.Imagenes.ElementAt(5) != null)
                            especie.Imagenes.ElementAt(5).URLImagen = folder + fileName;
                        else
                            especie.Imagenes.Add(new EspecieImagen() { Especie = especie, URLImagen = folder + fileName });
                        uploadImagen(espInfo.Foto6, path, fileName, 600, 450);
                    }
                    else
                    {
                        if (espInfo.Foto6 == "" && especie.Imagenes.Count >= 6)
                            removeList.Insert(0, 5);
                    }

                    foreach (int index in removeList)
                    {
                        especie.Imagenes.RemoveAt(index);
                    }

                    #endregion

                    resultado = uc.GuardarEspecie(especie);
                    if (resultado)
                    {
                        mensaje = "La especie se ha guardado exitosamente.";
                        resultado = true;
                        valor = new EspecieInfo(especie, false);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string eliminarEspecie(int id)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Eliminar_Mensaje_Error;
            object valor = null;
            try
            {
                var cb = new ControladorBase<Especie>();
                var especie = cb.ObtenerEntidad(id);
                if (especie.IsNotNull())
                {
                    var especieInfo = new EspecieInfo(especie, false);
                    cb.EliminarEntidad(especie);

                    mensaje     = Resources.Global.Eliminar_Mensaje_Exito;
                    valor       = especieInfo;
                    resultado   = true;

                    var paises = ObtenerListaPaises(true);
                    foreach (var pais in paises)
                    {
                        Helpers.CacheHelper.RemoveValue(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_ESPECIES, pais.Id));
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string eliminarEspecieLocalizacion(int id)
        {
            bool resultado  = false;
            string mensaje  = Resources.Global.Eliminar_Mensaje_Error;
            object valor    = null;
            try
            {
                var cb = new ControladorBase<EspeciePais>();
                var localizacion = cb.ObtenerEntidad(id);
                if (localizacion.IsNotNull())
                {
                    var localizacionInfo = new EspeciePaisInfo(localizacion);
                    cb.EliminarEntidad(localizacion);

                    mensaje     = Resources.Global.Eliminar_Mensaje_Exito;
                    valor       = localizacionInfo;
                    resultado   = true;

                    var paises = ObtenerListaPaises(true);
                    foreach (var pais in paises)
                    {
                        Helpers.CacheHelper.RemoveValue(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_ESPECIES, pais.Id));
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }
        
        [WebMethod]
        public static string guardarEspecieLocalizacion(int id, int idEspecie, int idPais, string nombre)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Guardar_Mensaje_Error;
            object valor = null;
            try
            {
                var cb = new ControladorBase<EspeciePais>();

                var localizacion = new EspeciePais();
                if (id != -1)
                    localizacion = cb.ObtenerEntidad(id);

                localizacion.Especie    = new EspecieControlador().ObtenerPorId(idEspecie);
                localizacion.Pais       = new PaisControlador().ObtenerPorId(idPais);
                localizacion.Nombre     = nombre;

                cb.GuardarEntidad(localizacion);

                var paises = ObtenerListaPaises(true);
                foreach (var pais in paises)
                {
                    Helpers.CacheHelper.RemoveValue(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_ESPECIES, pais.Id));
                }
                mensaje = Resources.Global.Guardar_Mensaje_Exito;
                valor = new EspeciePaisInfo(localizacion);
                resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Obtiene la lista Paginada de Noticias
        /// </summary>
        /// <param name="paisId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerEspeciesWiki()
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            List<EspecieInfo> especiesInfo = new List<EspecieInfo>();
            try
            {
                List<Especie> especies = new EspecieControlador().ObtenerTodasWiki();

                #region Conversiones a listas jSON
                if (especies != null)
                    foreach (Especie noticia in especies)
                        especiesInfo.Add(new EspecieInfo(noticia, false));
                #endregion

                _mensaje = "Datos obtenidos";
                _resultado = true;
                _valor = especiesInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// <summary>
        /// Obtiene los usuarios que hicieron aportes en una especie lista Paginada de Especies
        /// </summary>
        /// <param name="idEspecie"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerUsuariosAporteWiki(int idEspecie)
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            List<UsuarioInfo> usuariosInfo = new List<UsuarioInfo>();
            try
            {
                List<Usuario> usuarios = new EspecieControlador().ObtenerUsuariosAporteWiki(idEspecie);

                #region Conversiones a listas jSON
                if (usuarios != null)
                    foreach (Usuario usuario in usuarios)
                        usuariosInfo.Add(new UsuarioInfo(usuario));
                #endregion

                _mensaje = "Datos obtenidos";
                _resultado = true;
                _valor = usuariosInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// <summary>
        /// Obtiene los paises asociados a una especie de la Wiki
        /// </summary>
        /// <param name="idEspecie"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerPaisesWiki(int idEspecie)
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            List<PaisInfo> paisesInfo = new List<PaisInfo>();
            try
            {
                List<Pais> paises = new EspecieControlador().ObtenerPaisesEspecieWiki(idEspecie);

                #region Conversiones a listas jSON
                if (paises != null)
                    foreach (Pais pais in paises)
                        paisesInfo.Add(new PaisInfo(pais));
                #endregion

                _mensaje = "Datos obtenidos";
                _resultado = true;
                _valor = paisesInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// Obtiene el Nro Total de Especies de la Wiki
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerTotalEspeciesWiki()
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            try
            {
                EspecieControlador uc = new EspecieControlador();
                _valor = uc.ObtenerTotalEspeciesWiki();
                _resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        #endregion

        #region Mensajes

        /// <summary>
        /// Devuelve una lista de Mensajes
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerMensajes()
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            string respuesta = string.Empty;
            try
            {
                MensajeControlador mc = new MensajeControlador();
                List<ListaInfo> listaMensajes = new List<ListaInfo>();
                List<Mensaje> mensajes = mc.ObtenerTodos();
                if ( mensajes != null)
                {
                    foreach(Mensaje mensaje in mensajes)
                    {
                        ListaInfo mensajeInfo = new ListaInfo(mensaje, null);
                        listaMensajes.Add(mensajeInfo);
                    }
                    _valor = listaMensajes;
                    _resultado = true;
                    respuesta = SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        #endregion

        #region Paises & Provincias

        [WebMethod]
        public static string obtenerPaises(bool incluirInactivos = false)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            object valor   = null;
            try
            {
                valor       = ObtenerListaPaises(incluirInactivos);
                resultado   = true;
                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string guardarPaises(string paisInfoItems)
        {
            bool resultado = false;
            string mensaje = string.Empty;
            object valor = null;
            try
            {
                var cb = new ControladorBase<Pais>();
                var pc = new PaisControlador();
                var paisesInfo = Comun.Formateador.jSON.convertFromJson<List<PaisInfo>>(paisInfoItems);
                foreach (var paisInfo in paisesInfo)
                {
                    var pais = cb.ObtenerEntidad(paisInfo.Id);
                    if (paisInfo.Activo != pais.Activo)
                    {
                        pais.Activo = paisInfo.Activo;
                        cb.GuardarEntidad(pais);
                    }
                }

                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;

                Helpers.CacheHelper.RemoveValue(Comun.Constantes.CacheKeys.LISTA_PAISES + "_Inactivos_" + true);
                Helpers.CacheHelper.RemoveValue(Comun.Constantes.CacheKeys.LISTA_PAISES + "_Inactivos_" + false);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerProvincias(int idPais)
        {
            bool resultado = false;
            string mensaje = string.Empty;
            object valor = null;
            try
            {
                valor       = ObtenerListaProvincias(idPais);
                resultado   = true;
                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerProvincia(int idProvincia)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            object valor = null;
            try
            {
                var provincia = new ControladorBase<Provincia>().ObtenerEntidad(idProvincia);
                if ( provincia != null )
                    valor = new ProvinciaInfo(provincia);
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string guardarProvincia(int id, int idPais, string nombre)
        {
            bool resultado = false;
            string mensaje = string.Empty;
            object valor = null;
            try
            {
                var cb = new ControladorBase<Provincia>();

                var provincia = new Provincia();
                if (id != -1)
                    provincia = cb.ObtenerEntidad(id);
                else
                    provincia.Pais = new PaisControlador().ObtenerPorId(idPais);

                    provincia.Nombre = nombre;

                cb.GuardarEntidad(provincia);

                Helpers.CacheHelper.RemoveValue(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_PROVINCIAS, idPais));

                mensaje     = Resources.Global.Guardar_Mensaje_Exito;
                valor       = new ProvinciaInfo(provincia);
                resultado   = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string eliminarProvincia(int id)
        {
            bool resultado  = false;
            string mensaje  = Resources.Global.Eliminar_Mensaje_Error;
            object valor    = null;
            try
            {
                if (id != -1)
                {
                    var cb = new ControladorBase<Provincia>();
                    var provincia = new Provincia();
                        provincia = cb.ObtenerEntidad(id);
                    if (provincia.IsNotNull())
                    {
                        var idPais = provincia.Pais.Id;
                        var idProvincia = provincia.Id;
                        var provinciaInfo = new ProvinciaInfo(provincia);
                        
                        cb.EliminarEntidad(provincia);

                        mensaje     = Resources.Global.Eliminar_Mensaje_Exito;
                        valor       = provinciaInfo;
                        resultado   = true;

                        Helpers.CacheHelper.RemoveValue(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_PROVINCIAS, idPais));
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Totales

        /// <summary>
        /// Devuelve los totales
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        [WebMethod]
        public static string obtenerTotales(int idPais = -1, int periodo = -1, int idUsuario = -1)
        {
            bool _resultado = false;
            string _mensaje = Resources.Global.Obtener_Mensaje_Error;
            object _valor   = null;
            try
            {
                UsuarioControlador uc   = new UsuarioControlador();

                double factorCO2        = Convert.ToDouble(ConfigurationManager.AppSettings["FactorCO2"].ToString());

                int periodoUsuarios     = -1;
                int paisUsuarios        = -1;

                int totalIndividuos     = uc.ObtenerTotalUsuarios(paisUsuarios, periodoUsuarios, idUsuario, Comun.Constantes.UsuarioTipos.Individuo);
                int totalInstituciones  = uc.ObtenerTotalUsuarios(paisUsuarios, periodoUsuarios, idUsuario, Comun.Constantes.UsuarioTipos.Institucion);
                int totalMunicipios     = uc.ObtenerTotalUsuarios(paisUsuarios, periodoUsuarios, idUsuario, Comun.Constantes.UsuarioTipos.Municipio);
                int totalPlantaciones   = new PlantacionControlador().ObtenerTotalPlantaciones(idPais, periodo, idUsuario);
                int totalDonaciones     = new DonacionControlador().ObtenerTotalDonaciones(idPais, periodo, idUsuario);
                int totalAdopciones     = new AdopcionControlador().ObtenerTotalAdopciones(idPais, periodo, idUsuario);
                double totalCO2Capturas = totalPlantaciones * factorCO2;

                totalPlantaciones = totalPlantaciones + totalAdopciones;

                var totalesInfo         = new TotalesInfo(totalPlantaciones, totalDonaciones, totalAdopciones, totalIndividuos, totalInstituciones, totalMunicipios, totalCO2Capturas);

                _resultado  = true;
                _mensaje    = Resources.Global.Obtener_Mensaje_Exito;
                _valor      = totalesInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        #endregion

        #region Configuracion

        ///// <summary>
        ///// Obtiene la Configuración
        ///// </summary>
        ///// <returns></returns>
        //[WebMethod]
        //public static string obtenerConfiguracion()
        //{
        //    bool _resultado = false;
        //    string _mensaje = string.Empty;
        //    object _valor = null;
        //    try
        //    {
        //        var cc = new ConfiguracionControlador();
        //        Configuracion configuracion = cc.ObtenerConfiguracion();
        //        if ( configuracion != null)
        //        {
        //            //ConfiguracionVideoInfo  configVideoInfo  = new ConfiguracionVideoInfo(configuracion.Video);
        //            ConfiguracionSocialInfo configSocialInfo = new ConfiguracionSocialInfo(configuracion.Social);

        //            List<object> configuraciones = new List<object>();
        //                         configuraciones.Add(configSocialInfo);
        //                         //configuraciones.Add(configVideoInfo);

        //            _valor = configuraciones;
        //            _resultado = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        HandleException(ex);
        //        throw;
        //    }
        //    return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        //}

        /// <summary>
        /// Obtiene la Configuración de Email
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerConfiguracionEmail()
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            try
            {
                var cc = new ConfiguracionControlador();
                
                var configuracion = Helpers.ConfigurationHelper.ObtenerConfiguracion();
                var configuracionEmail = cc.ObtenerConfiguracionEmail();
                if (configuracionEmail != null)
                {
                    _valor = configuracionEmail;
                    _resultado = true;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// <summary>
        /// Guarda la Configuración de Email
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string guardarConfiguracionEmail(string infoConfiguracionEmail)
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            try
            {
                var configuracionEmailInfo = Comun.Formateador.jSON.convertFromJson<ConfiguracionEmailInfo>(infoConfiguracionEmail);
                var cc = new ConfiguracionControlador();
                var configuracion = cc.ObtenerConfiguracion();
                if (configuracion != null)
                {
                    if (configuracion.Email != null)
                    {
                        configuracion.Email.EmisorAlias         = configuracionEmailInfo.EmisorAlias;
                        configuracion.Email.EmisorDireccion     = configuracionEmailInfo.EmisorDireccion;
                        configuracion.Email.EmisorUsuario       = configuracionEmailInfo.EmisorUsuario;
                        configuracion.Email.EmisorPassword      = configuracionEmailInfo.EmisorPassword;
                        configuracion.Email.SMTPServidor        = configuracionEmailInfo.SMTPServidor;
                        configuracion.Email.SMTPPuerto          = configuracionEmailInfo.SMTPPuerto;
                        configuracion.Email.SMTPHabilitarSSL    = configuracionEmailInfo.SMTPHabilitarSSL;        
                    }
                    cc.Guardar(configuracion);

                    Helpers.CacheHelper.RemoveValue(Comun.Constantes.CacheKeys.CONFIGURACION);

                    _mensaje = "Datos guardados exitosamente.";
                    _valor = null;
                    _resultado = true;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        #endregion

        #region Usuario

        /// <summary>
        /// Obtiene un Usuario, según su Id
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerUsuario(int idUsuario)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                UsuarioInfo usuarioInfo = null;
                Usuario usuario = new UsuarioControlador().ObtenerPorId(idUsuario);
                if (usuario != null)
                {
                    usuarioInfo = new UsuarioInfo(usuario);
                    valor = usuarioInfo;
                    resultado = true;
                }
                else
                {
                    mensaje = "No se ha podido recuperar la información del Usuario.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Obtiene todos los Usuarios
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerUsuarios(int periodo = -1, string pais = "")
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor   = null;
            List<ListaInfo> usuariosInfo = new List<ListaInfo>();
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                List<Usuario> usuarios = uc.ObtenerTodos(periodo, pais);
                if ( usuarios != null)
                {
                    foreach(Usuario usuario in usuarios)
                    {
                        ListaInfo   usuarioInfo         = new ListaInfo();
                                    usuarioInfo.Id      = usuario.Id;
                                    usuarioInfo.Texto   = usuario.Nombre + " " + usuario.Apellido;
                                    usuarioInfo.Valor   = new UsuarioInfo(usuario);
                        usuariosInfo.Add(usuarioInfo);
                    }
                    _resultado = true;
                }
                _valor = usuariosInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// <summary>
        /// Obtiene la lista Paginada de Usuarios
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="pagina"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerUsuarios_Paginado(int periodo = -1, string pais = "", int pagina = 1, int cantidad = 10)
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            List<ListaInfo> usuariosInfo = new List<ListaInfo>();
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                List<Usuario> usuarios = uc.ObtenerTodos_Paginado(periodo, pais, pagina, cantidad);
                if (usuarios != null)
                {
                    foreach (Usuario usuario in usuarios)
                    {
                        ListaInfo usuarioInfo = new ListaInfo();
                        usuarioInfo.Id = usuario.Id;
                        usuarioInfo.Texto = usuario.Nombre + " " + usuario.Apellido;
                        usuarioInfo.Valor = new UsuarioInfo(usuario);
                        usuariosInfo.Add(usuarioInfo);
                    }
                    _resultado = true;
                }
                _valor = usuariosInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// Obtiene el Nro Total de  Usuarios
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerTotalUsuarios(int periodo = -1, string pais = "")
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                _valor = uc.ObtenerTotalUsuarios(pais);
                _resultado = true; 
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        #endregion

        #region Actividades

        /// <summary>
        /// Obtiene todas las plantaciones
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <param name="mes"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerActividadesDelMes(int idPais = -1, int periodo = -1, int mes = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                ActividadControlador ac = new ActividadControlador();
                List<ActividadInfo> actividadesInfo = new List<ActividadInfo>();
                List<UsuarioActividad> actividades = ac.ObtenerTodas(idPais, periodo, mes, -1);
                if ( actividades != null )
                {
                    foreach(UsuarioActividad actividad in actividades)
                    {
                        actividadesInfo.Add
                            (
                                new ActividadInfo()
                                {
                                    Id = actividad.Id,
                                    FechaYHora = actividad.FechaYHora.ToShortDateString()
                                }
                            );
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = actividadesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene todas las Actividades de una fecha
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerActividadesPorFecha(DateTime fecha, string pais = "")
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                ActividadControlador ac = new ActividadControlador();
                List<ActividadInfo> actividadesInfo = new List<ActividadInfo>();

                List<UsuarioActividad> actividades = ac.ObtenerTodasPorFecha(fecha, pais);
                if ( actividades != null )
                {
                    foreach(UsuarioActividad actividad in actividades)
                    {
                        actividadesInfo.Add( new ActividadInfo(actividad) );
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = actividadesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene todas las plantaciones
        /// </summary>
        /// <returns></returns>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        [WebMethod]
        public static string obtenerActividadesRotativas(int cantidad, int idPais = -1, int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                ActividadControlador ac = new ActividadControlador();
                List<ActividadInfo> actividadesInfo = new List<ActividadInfo>();
                List<UsuarioActividad> actividades = ac.ObtenerTodas(idPais, periodo, -1, cantidad);
                if ( actividades != null )
                {
                    foreach(UsuarioActividad actividad in actividades)
                    {
                        ActividadInfo   actividadInfo = new ActividadInfo(actividad);
                                        //actividadInfo.Descripcion = Formateador.ReducirTexto(actividadInfo.Comentarios, 15);
                                        //actividadInfo.Comentarios = Formateador.ReducirTexto(actividadInfo.Comentarios, 150);
                        actividadesInfo.Add(actividadInfo);
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = actividadesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene todas las plantaciones
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerActividades(int idPais = -1, int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                ActividadControlador ac = new ActividadControlador();
                List<ActividadInfo> actividadesInfo = new List<ActividadInfo>();
                List<UsuarioActividad> actividades = ac.ObtenerTodas(idPais, periodo, -1, -1);
                if ( actividades != null )
                {
                    foreach(UsuarioActividad actividad in actividades)
                    {
                        actividadesInfo.Add(new ActividadInfo(actividad));
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = actividadesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene una Actividad, según su Id
        /// </summary>
        /// <param name="idActividad"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerActividad(int idActividad)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                ActividadControlador ac = new ActividadControlador();
                ActividadInfo actividadInfo = null;
                UsuarioActividad actividad = ac.ObtenerPorId(idActividad);
                if ( actividad != null )
                    actividadInfo = new ActividadInfo(actividad);

                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = actividadInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene todas las Actividades agendadas por el Usuario
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerActividadesUsuario(int idUsuario, string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                List<ActividadInfo> actividadesInfo = new List<ActividadInfo>();
                Usuario usuario = uc.ObtenerPorId(idUsuario);
                if ( usuario != null )
                {
                    IList<UsuarioActividad> actividades = usuario.Actividades;
                    if ( actividades != null )
                    {
                        foreach(UsuarioActividad actividad in actividades)
                        {
                            actividadesInfo.Add(new ActividadInfo(actividad));
                        }
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = actividadesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Guarda una actividad
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="IdUsuario"></param>
        /// <param name="Usuario"></param>
        /// <param name="Fecha"></param>
        /// <param name="Descripcion"></param>
        /// <param name="Comentarios"></param>
        /// <param name="Latitud"></param>
        /// <param name="Longitud"></param>
        /// <param name="idPais"></param>
        /// <param name="idProvincia"></param>
        /// <param name="EsPublica"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarActividad(int Id, int IdUsuario, string Usuario, string Fecha, string Descripcion, string Comentarios, float Latitud, float Longitud, int IdPais, int IdProvincia, bool EsPublica)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                ActividadControlador ac = new ActividadControlador();

                Usuario usuario = uc.ObtenerPorId(IdUsuario);

                //Si no existía ninguna Actividad previamente, instancia una nueva lista..
                if ( usuario.Actividades == null)
                    usuario.Actividades = new List<UsuarioActividad>();

                UsuarioActividad actividad;
                if ( Id == -1 )
                    actividad = new UsuarioActividad();
                else
                    actividad = usuario.Actividades.Where(x => x.Id == Id).FirstOrDefault();

                    actividad.Usuario       = usuario;
                    actividad.FechaYHora    = Convert.ToDateTime(Fecha);
                    actividad.Descripcion   = Descripcion;
                    actividad.Comentarios   = Comentarios;
                    actividad.Latitud       = Latitud;
                    actividad.Longitud      = Longitud;
                    actividad.Pais          = new PaisControlador().ObtenerPorId(IdPais);
                    actividad.Provincia     = new ProvinciaControlador().ObtenerPorId(IdProvincia);
                    actividad.EsPublica     = EsPublica;

                //agrego la Actividad a la lista..
                usuario.Actividades.Add(actividad);

                //Guardo el Usuario y sus datos asociados..
                uc.Guardar(usuario);

                mensaje     = "Datos guardados exitosamente";
                resultado   = true;
                valor       = new ActividadInfo(actividad);
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Elimina una actividad
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="idActividad"></param>
        /// <returns></returns>
        [WebMethod]
        public static string eliminarActividad(int idUsuario, int idActividad)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                Usuario usuario = uc.ObtenerPorId(idUsuario);

                if ( usuario.Actividades != null )
                {
                    //elimino la actividad de la lista de actividades del Usuario..
                    usuario.Actividades.Remove(usuario.Actividades.Where(x => x.Id == idActividad).FirstOrDefault());
                }

                //guardaro el usuario...
                uc.Guardar(usuario);

                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = idActividad;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Envía un email al Usuario (Organizador) de la Actividad, notificandole del deseo de participar del Usuario (Colaborador), y viceversa.
        /// </summary>
        /// <param name="idActividad"></param>
        /// <param name="idUsuario"></param>
        /// <param name="rol"></param>
        /// <returns></returns>
        [WebMethod]
        public static string enviarEmailParticipacionEnActividad(int idActividad, int idUsuario, string rol)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioActividad actividad  = new ActividadControlador().ObtenerPorId(idActividad);

                Usuario organizador         = actividad.Usuario;
                Usuario participante        = new UsuarioControlador().ObtenerPorId(idUsuario);

                string titulo               = string.Empty;
                string emailDestinatario    = string.Empty;
                string nombreDestinatario   = string.Empty;
                string nombreUsuario        = string.Empty;
                string telefonoUsuario      = string.Empty;
                string emailUsuario         = string.Empty;

                switch ( rol.ToLower() )
                {
                    case "organizador":
                        titulo              = "¡Un usuario quiere colaborar en su Actividad!";
                        emailDestinatario   = organizador.Email;
                        nombreDestinatario  = organizador.Nombre + " " + organizador.Apellido;
                        nombreUsuario       = participante.Nombre + " " + participante.Apellido;
                        telefonoUsuario     = participante.Telefono;
                        emailUsuario        = participante.Email;
                        break;
                    case "participante":
                        titulo              = "¡Te has comprometido a colaborar con una Actividad!";
                        emailDestinatario   = participante.Email;
                        nombreDestinatario  = participante.Nombre + " " + participante.Apellido;
                        nombreUsuario       = organizador.Nombre + " " + organizador.Apellido;
                        telefonoUsuario     = organizador.Telefono;
                        emailUsuario        = organizador.Email;
                        break;
                }

                if ( string.IsNullOrEmpty(emailDestinatario) )
                {
                    mensaje     = "No se pudo enviar el email porque el " + rol.ToLower() + " no informó su casilla de email.";
                    resultado   = false;
                }
                else
                {
                    EnviarEmailParticipacionEnActividad(titulo, nombreDestinatario, emailDestinatario, nombreUsuario, telefonoUsuario, emailUsuario);
                    mensaje     = "Mail enviado exitosamente.";
                    resultado   = true;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                //throw;
                mensaje = "Ocurrió un error al enviar el mail.";
                resultado = false;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        private static void EnviarEmailParticipacionEnActividad(string titulo, string nombreDestinatario, string emailDestinatario, string nombreUsuario, string telefonoUsuario, string emailUsuario)
        {
            try
            {
                Comun.Email objEmail = new Comun.Email();
                Dictionary<string, string> destinatarios = new Dictionary<string, string>();

                ConfiguracionControlador cc = new ConfiguracionControlador();
                Configuracion configuracion = cc.ObtenerConfiguracion();

                destinatarios.Add(nombreDestinatario, emailDestinatario);

                string asunto = "Web Semana del Árbol - " + titulo;

                string[] cuerpo = new string[] { nombreUsuario, telefonoUsuario, emailUsuario };

                string Emisor_Nombre = configuracion.Email.EmisorAlias;
                string Emisor_Direccion = configuracion.Email.EmisorDireccion;
                string Emisor_Password = configuracion.Email.EmisorPassword;
                string SMTP_Servidor = configuracion.Email.SMTPServidor;
                int SMTP_Puerto = configuracion.Email.SMTPPuerto;
                bool SMTP_HabilitarSSL = configuracion.Email.SMTPHabilitarSSL;

                objEmail.EnviarMailParticipacionEnActividad(Emisor_Nombre, Emisor_Direccion, Emisor_Password, SMTP_Servidor, SMTP_Puerto, SMTP_HabilitarSSL, destinatarios, asunto, cuerpo, System.Text.Encoding.UTF8, true);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Plantaciones

        /// <summary>
        /// Obtiene todas las plantaciones
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerPlantaciones(string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                int idUsuario = -1;

                List<PlantacionInfo> plantacionesInfo = new List<PlantacionInfo>();
                List<UsuarioPlantacion> plantaciones = new PlantacionControlador().ObtenerTodas(idUsuario, pais, periodo);
               
                if ( plantaciones != null )
                {
                    foreach(UsuarioPlantacion plantacion in plantaciones)
                    {
                        plantacionesInfo.Add(new PlantacionInfo(plantacion, false));
                    }
                }

                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = plantacionesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene todos los árboles plantados por Usuario
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerPlantacionesUsuario(int idUsuario = -1, string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                List<PlantacionInfo> plantacionesInfo = new List<PlantacionInfo>();
                List<UsuarioPlantacion> plantaciones = new PlantacionControlador().ObtenerTodas(idUsuario, pais, periodo);
                if ( plantaciones != null )
                {
                    foreach(UsuarioPlantacion plantacion in plantaciones)
                    {
                        plantacionesInfo.Add(new PlantacionInfo(plantacion));
                    }
                }

                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = plantacionesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene una Plantación
        /// </summary>
        /// <param name="idPlantacion"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerPlantacion(int idPlantacion)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioPlantacion plantacion = new PlantacionControlador().ObtenerPorId(idPlantacion);
                if ( plantacion != null )
                {
                    PlantacionInfo plantacionInfo = new PlantacionInfo(plantacion);
                    mensaje     = "Datos obtenidos";
                    resultado   = true;
                    valor       = plantacionInfo;
                }
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtener lista de Fotos de Plantaciones
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerListasFotosPlantaciones(string pais = "", int periodo = -1)
        {
            string mensaje  = string.Empty;
            bool resultado  = false;
            object valor    = null;
            try
            {
                PlantacionControlador pc = new PlantacionControlador();
                List<ListaInfo> lista = new List<ListaInfo>();
                List<UsuarioPlantacion> plantaciones = pc.ObtenerTodas(-1, pais, periodo, true);
                if ( plantaciones != null )
                {
                    foreach(UsuarioPlantacion plantacion in plantaciones)
                    {
                        ListaInfo listaInfo = new ListaInfo();
                        listaInfo.Id    = plantacion.Id;
                        //listaInfo.Texto = plantacion.Fecha.ToShortDateString();
                        //listaInfo.Valor = Comun.Formateador.Base64.ConvertirEnBase64(plantacion.Foto);
                        lista.Add(listaInfo);
                    }
                }
                mensaje     = "Lista de Fotos obtenida exitosamente.";
                resultado   = true;
                valor       = lista;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Obtiene la foto en miniatura de una Plantación
        /// </summary>
        /// <param name="idPlantacion"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerPlantacionFotoPreview(int idPlantacion, int width = 200, int height = 150)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                PlantacionControlador pc = new PlantacionControlador();
                UsuarioPlantacion plantacion = pc.ObtenerPorId(idPlantacion);
                PlantacionInfo plantacionInfo = new PlantacionInfo();
                if ( plantacion != null )
                {
                    MemoryStream stream = Comun.Formateador.Imagen.Formatear(plantacion.Foto, width, height, System.Drawing.Imaging.ImageFormat.Png);
                    plantacion.Foto = stream.GetBuffer();
                    plantacionInfo = new PlantacionInfo( plantacion, true );
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = plantacionInfo;
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene la foto de una Plantación
        /// </summary>
        /// <param name="idPlantacion"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerPlantacionFoto(int idPlantacion)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                PlantacionControlador pc = new PlantacionControlador();
                UsuarioPlantacion plantacion = pc.ObtenerPorId(idPlantacion);
                PlantacionInfo plantacionInfo = new PlantacionInfo();
                if ( plantacion != null )
                    plantacionInfo = new PlantacionInfo( plantacion, true );
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = plantacionInfo;
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Guarda una plantación
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="IdEspecie"></param>
        /// <param name="Especie"></param>
        /// <param name="Latitud"></param>
        /// <param name="Longitud"></param>
        /// <param name="Cantidad"></param>
        /// <param name="Fecha"></param>
        /// <param name="IdPais"></param>
        /// <param name="Foto"></param>
        /// <param name="IdUsuario"></param>
        /// <param name="Usuario"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarPlantacion(int Id, int IdEspecie, string Especie, float Latitud, float Longitud, int Cantidad, string Fecha, int IdPais, string Foto, int IdUsuario, string Usuario)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                EspecieControlador ac = new EspecieControlador();
                PaisControlador pc = new PaisControlador();

                Usuario usuario = uc.ObtenerPorId(IdUsuario);

                //Si no existía ninguna Plantación previamente, instancia una nueva lista..
                if ( usuario.Plantaciones == null)
                    usuario.Plantaciones = new List<UsuarioPlantacion>();

                UsuarioPlantacion   plantacion;
                if ( Id == -1 )
                    plantacion = new UsuarioPlantacion();
                else
                    plantacion = usuario.Plantaciones.Where(x => x.Id == Id).FirstOrDefault();

                    plantacion.Usuario      = usuario;
                    plantacion.Arbol        = ac.ObtenerPorId(IdEspecie);
                    plantacion.Cantidad     = Cantidad;
                    plantacion.Fecha        = Convert.ToDateTime(Fecha);
                    plantacion.Pais         = pc.ObtenerPorId(IdPais);
                    plantacion.Foto         = Formateador.Base64.ConvertirEnArrayDeBytes(Foto);
                    plantacion.TieneFoto    = Foto.Length > 0;
                    plantacion.Latitud      = Latitud;
                    plantacion.Longitud     = Longitud;

                //agrego la Plantacion a la lista..
                usuario.Plantaciones.Add(plantacion);

                //Guardo el Usuario y sus datos asociados..
                uc.Guardar(usuario);

                mensaje     = "Datos guardados exitosamente.";
                resultado   = true;
                valor       = new PlantacionInfo(plantacion);
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Elimina una plantación
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="idPlantacion"></param>
        /// <returns></returns>
        [WebMethod]
        public static string eliminarPlantacion(int idUsuario, int idPlantacion)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                Usuario usuario = uc.ObtenerPorId(idUsuario);

                if ( usuario.Plantaciones != null )
                {
                    //elimino la plantacion de la lista de plantaciones del Usuario..
                    usuario.Plantaciones.Remove(usuario.Plantaciones.Where(x => x.Id == idPlantacion).FirstOrDefault());
                }

                //guardaro el usuario...
                uc.Guardar(usuario);

                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = idPlantacion;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }
        
        #endregion

        #region Donaciones

        /// <summary>
        /// Obtiene todas los donantes, según un País, Provincia y Ciudad (opcional)
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="idProvincia"></param>
        /// <param name="ciudad"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerDonantes(int idPais, int idProvincia, string ciudad, string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                List<UsuarioInfo> donantesInfo = new List<UsuarioInfo>();
                List<Usuario> donantes = null;
                if ( idPais != -1 && periodo != -1 )
                    donantes = uc.ObtenerDonantes(periodo, idPais, idProvincia, ciudad);
                else
                    donantes = uc.ObtenerDonantes(pais, periodo);
                if ( donantes != null )
                {
                    foreach(Usuario donante in donantes)
                    {
                        donantesInfo.Add(new UsuarioInfo(donante));
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = donantesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene todas las donaciones de un Donante
        /// </summary>
        /// <param name="idDonante"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerDonaciones(int idDonante, string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                DonacionControlador dc = new DonacionControlador();
                List<DonacionInfo> donacionesInfo = new List<DonacionInfo>();
                List<UsuarioDonacion> donaciones = dc.ObtenerTodas(idDonante, pais, periodo);
                if ( donaciones != null )
                {
                    foreach(UsuarioDonacion donacion in donaciones)
                    {
                        donacionesInfo.Add(new DonacionInfo(donacion));
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = donacionesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Guarda la ubicación geográfica del Donante
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarUbicacionDonante(int idUsuario, float latitud, float longitud)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();

                Usuario usuario = uc.ObtenerPorId(idUsuario);
                if ( usuario != null )
                {
                    usuario.Latitud     = latitud;
                    usuario.Longitud    = longitud;

                    //Guardo el Usuario y sus datos asociados..
                    uc.Guardar(usuario);

                    mensaje     = "Datos guardados exitosamente.";
                    resultado   = true;
                }

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Guarda una donación
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="IdDonante"></param>
        /// <param name="IdEspecie"></param>
        /// <param name="Especie"></param>
        /// <param name="Cantidad"></param>
        /// <param name="Disponibles"></param>
        /// <param name="Adoptados"></param>
        /// <param name="IdPais"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarDonacion(int Id, int IdDonante, int IdEspecie, string Especie, int Cantidad, int Disponibles, int Adoptados, int IdPais)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                EspecieControlador ac = new EspecieControlador();

                Usuario usuario = uc.ObtenerPorId(IdDonante);

                //Si no existía ninguna Plantación previamente, instancia una nueva lista..
                if ( usuario.Donaciones == null)
                    usuario.Donaciones = new List<UsuarioDonacion>();

                UsuarioDonacion donacion;
                if ( Id == -1 )
                    donacion = new UsuarioDonacion();
                else
                    donacion = usuario.Donaciones.Where(x => x.Id == Id).FirstOrDefault();

                    donacion.Donante        = usuario;
                    donacion.Arbol          = ac.ObtenerPorId(IdEspecie);
                    donacion.Cantidad       = Cantidad;
                    donacion.Adoptados      = Adoptados;
                    donacion.Disponibles    = Disponibles;
                    donacion.Fecha          = DateTime.Now;
                    donacion.Pais           = new PaisControlador().ObtenerPorId(IdPais);

                //agrego la Donación a la lista..
                usuario.Donaciones.Add(donacion);

                //guardo el usuario..
                uc.Guardar(usuario);

                mensaje     = "Datos guardados exitosamente.";
                resultado   = true;
                valor       = new DonacionInfo(donacion);
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        #endregion

        #region Adopciones

        /// <summary>
        /// Obtiene todas los adopciones
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerAdopciones(string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                List<UsuarioInfo> donantesInfo = new List<UsuarioInfo>();
                List<Usuario> donantes = uc.ObtenerDonantes(pais, periodo);
                if ( donantes != null )
                {
                    foreach(Usuario donante in donantes)
                    {
                        donantesInfo.Add(new UsuarioInfo(donante));
                    }
                }
                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
                resultado   = true;
                valor       = donantesInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Obtiene todas las adopciones de un Usuario
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerAdopciones(int idUsuario, string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                DonacionControlador dc = new DonacionControlador();
                List<DonacionInfo> donacionesInfo = new List<DonacionInfo>();
                List<UsuarioDonacion> donaciones = dc.ObtenerTodas(idUsuario, pais, periodo);
                if ( donaciones != null )
                {
                    foreach(UsuarioDonacion donacion in donaciones)
                    {
                        donacionesInfo.Add(new DonacionInfo(donacion));
                    }
                }
                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
                resultado   = true;
                valor       = donacionesInfo;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene todos los árboles adoptados por Usuario
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="idEspecie"></param>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerAdopcionesUsuario(int idUsuario, int idEspecie = -1, int idPais = -1, int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                AdopcionControlador ac = new AdopcionControlador();
                List<AdopcionInfo> adopcionessInfo = new List<AdopcionInfo>();

                IList<UsuarioAdopcion> adopciones = ac.ObtenerTodas(idUsuario, idEspecie, idPais, periodo);
                if (adopciones != null)
                {
                    foreach (UsuarioAdopcion adopcion in adopciones)
                    {
                        adopcionessInfo.Add(new AdopcionInfo(adopcion));
                    }
                }

                mensaje = Resources.Global.Obtener_Mensaje_Exito;
                resultado = true;
                valor = adopcionessInfo;

                respuesta = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Realiza la adopción de una cantidad de especies
        /// </summary>
        /// <param name="idDonante"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idEspecie"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        [WebMethod]
        public static string realizarAdopcion(int idDonante, int idUsuario, int idEspecie, int cantidad)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                UsuarioControlador uc = new UsuarioControlador();
                EspecieControlador ac = new EspecieControlador();

                Usuario usuario = uc.ObtenerPorId(idUsuario);
                Usuario donante = uc.ObtenerPorId(idDonante);

                ////Si existen, elimino arboles asociados previamente..
                //if ( usuario.Adopciones != null && usuario.Adopciones.Count > 0)
                //    usuario.Adopciones.Clear();

                //Si no existían, creo una nueva lista..
                if ( usuario.Adopciones == null)
                    usuario.Adopciones = new List<UsuarioAdopcion>();

                UsuarioAdopcion adopcion = new UsuarioAdopcion();
                                //adopcion.Id = ..
                                adopcion.Arbol      = ac.ObtenerPorId(idEspecie);
                                adopcion.Cantidad   = cantidad;
                                adopcion.Donante    = donante;
                                adopcion.Usuario    = usuario;
                                adopcion.Fecha      = DateTime.Now;
                                adopcion.Pais       = donante.Pais;

                usuario.Adopciones.Add(adopcion);

                //Guardo el Usuario y sus datos asociados..
                uc.Guardar(usuario);

                //Guardo el Donante (para que se persista la modificacion en la cantidad disponible de su donacion)..
                foreach(UsuarioDonacion donacion in donante.Donaciones)
                {
                    if ( donacion.Arbol.Id == idEspecie )
                    {
                        int adoptados           = donacion.Adoptados + cantidad;
                        donacion.Adoptados      = adoptados;
                        donacion.Disponibles    = donacion.Cantidad - (adoptados);
                    }
                }
                uc.Guardar(donante);

                string adopcionUsuario      = EntidadesHelper.ObtenerNombre(adopcion.Usuario);
                string adopcionEspecie      = adopcion.Arbol.Nombre;
                string adopcionCantidad     = adopcion.Cantidad.ToString();
                string adopcionEmail        = adopcion.Usuario.Email;
                string adopcionDonante      = EntidadesHelper.ObtenerNombre(adopcion.Donante);
                string adopcionDonanteEmail = adopcion.Donante.Email;
                if ( adopcionDonanteEmail != null && adopcionDonanteEmail.Trim().Length > 0 )
                    enviarEmailAdopcion(adopcionUsuario, adopcionEspecie, adopcionCantidad, adopcionEmail, adopcionDonante, adopcionDonanteEmail);

                mensaje     = "Datos guardados exitosamente.";
                resultado   = true;
                valor       = new AdopcionInfo(adopcion); //new UsuarioInfo(donante);
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        #endregion

        #region Novedades

        /// <summary>
        /// Obtiene las últimas N novedades
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerUltimasNovedades(int cantidad, string pais = "", int periodo = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                List<UsuarioActividad> actividades      = new ActividadControlador().ObtenerUltimasActividades(cantidad, pais, periodo);
                List<UsuarioPlantacion> plantaciones    = new PlantacionControlador().ObtenerUltimasPlantaciones(cantidad, pais, periodo);
                List<UsuarioDonacion> donaciones        = new DonacionControlador().ObtenerUltimasDonaciones(cantidad, pais, periodo);
                List<UsuarioAdopcion> adopciones        = new AdopcionControlador().ObtenerUltimasAdopciones(cantidad, pais, periodo);
                List<Mensaje> mensajes                  = new MensajeControlador().ObtenerTodos();
                
                List<ActividadInfo> actividadesInfo     = new List<ActividadInfo>();
                List<PlantacionInfo> plantacionesInfo   = new List<PlantacionInfo>();
                List<DonacionInfo> donacionesInfo       = new List<DonacionInfo>();
                List<AdopcionInfo> adopcionesInfo       = new List<AdopcionInfo>();
                List<ListaInfo> listaMensajes = new List<ListaInfo>();

                #region Conversiones a listas jSON
                if ( actividades != null )
                    foreach(UsuarioActividad actividad in actividades)
                        actividadesInfo.Add(new ActividadInfo(actividad));

                if ( plantaciones != null )
                    foreach(UsuarioPlantacion plantacion in plantaciones)
                        plantacionesInfo.Add(new PlantacionInfo(plantacion));

                if ( donaciones != null )
                    foreach(UsuarioDonacion donacion in donaciones)
                        donacionesInfo.Add(new DonacionInfo(donacion));

                if ( adopciones != null )
                    foreach(UsuarioAdopcion adopcion in adopciones)
                        adopcionesInfo.Add(new AdopcionInfo(adopcion));

                if (mensajes != null)
                {
                    foreach (Mensaje msj in mensajes)
                        listaMensajes.Add(new ListaInfo(msj, null));
                }

                #endregion

                ColeccionInfo[] coleccionInfo = new ColeccionInfo[4];

                ListaInfo[] listas = new ListaInfo[5];
                            listas[0] = new ListaInfo();
                            listas[0].Id = 0;
                            listas[0].Texto = "Actividades";
                            listas[0].Valor = actividadesInfo;
                            listas[1] = new ListaInfo();
                            listas[1].Id = 1;
                            listas[1].Texto = "Plantaciones";
                            listas[1].Valor = plantacionesInfo;
                            listas[2] = new ListaInfo();
                            listas[2].Id = 2;
                            listas[2].Texto = "Adopciones";
                            listas[2].Valor = adopcionesInfo;
                            listas[3] = new ListaInfo();
                            listas[3].Id = 3;
                            listas[3].Texto = "Donaciones";
                            listas[3].Valor = donacionesInfo;
                            listas[4] = new ListaInfo();
                            listas[4].Id = 4;
                            listas[4].Texto = "Mensajes";
                            listas[4].Valor = listaMensajes;

                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = listas;

                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        #endregion

        #region Viveros

        /// <summary>
        /// Guarda un Vivero
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="IdUsuario"></param>
        /// <param name="Direccion"></param>
        /// <param name="Telefono"></param>
        /// <param name="Email"></param>
        /// <param name="Web"></param>
        /// <param name="Imagen"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarVivero(int Id, int IdUsuario, string Direccion, string Telefono, string Email, string Web, string Imagen )
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                ViveroControlador vc = new ViveroControlador();

                Vivero vivero = null;
                if ( Id == -1 )
                    vivero = new Vivero();
                else
                    vivero = vc.ObtenerPorId(Id);

                    vivero.Direccion    = Direccion;
                    vivero.Email        = Email;
                    vivero.Imagen       = Formateador.Base64.ConvertirEnArrayDeBytes(Imagen);
                    vivero.Usuario      = new UsuarioControlador().ObtenerPorId(IdUsuario);
                    vivero.Telefono     = Telefono;
                    vivero.Web          = Web;

                //Guardar el Vivero..
                vc.Guardar(vivero);

                var idPais = vivero.Usuario.Pais.Id;
                Helpers.CacheHelper.RemoveValue(Comun.Constantes.CacheKeys.LISTA_VIVEROS + "_" + idPais);

                mensaje     = "Datos guardados exitosamente.";
                resultado   = true;
                valor       = new ViveroInfo(vivero);
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene un Vivero, según su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerVivero(int id)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                Vivero vivero = new ViveroControlador().ObtenerPorId(id);
                if (vivero != null)
                {
                    mensaje = "Datos del Vivero obtenidos exitosamente.";
                    valor = new ViveroInfo(vivero);
                    resultado = true;
                }
                else
                {
                    mensaje = "No se ha podido recuperar la información del Usuario.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Obtiene todos los Viveros
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="idPais"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerViveros(int periodo = -1, string pais = "", int idPais = -1)
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor   = null;
            List<ListaInfo> viverosInfo = new List<ListaInfo>();
            try
            {
                ViveroControlador uc = new ViveroControlador();

                var viveros = Helpers.CacheHelper.GetValue<List<Vivero>>(Comun.Constantes.CacheKeys.LISTA_VIVEROS + "_" + idPais);
                if (viveros == null)
                {
                    viveros = uc.ObtenerTodos(periodo, pais);
                    Helpers.CacheHelper.SetValue(Comun.Constantes.CacheKeys.LISTA_VIVEROS, viveros);
                }
                if ( viveros != null)
                {
                    foreach(Vivero vivero in viveros)
                    {
                        ListaInfo   usuarioInfo = new ListaInfo();
                                    usuarioInfo.Id      = vivero.Id;
                                    usuarioInfo.Texto   = vivero.Usuario.Nombre + " " + vivero.Usuario.Apellido;
                                    usuarioInfo.Valor   = new ViveroInfo(vivero);
                        viverosInfo.Add(usuarioInfo);
                    }
                    _resultado = true;
                }
                _valor = viverosInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// <summary>
        /// Elimina un Vivero
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        public static string eliminarVivero(int id)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                ViveroControlador vc = new ViveroControlador();
                Vivero vivero = vc.ObtenerPorId(id);
                if ( vivero != null )
                {
                    vc.Eliminar(vivero);

                    mensaje     = "Datos eliminados exitosamente";
                    resultado   = true;
                    valor       = new ViveroInfo(vivero);
                }
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        #endregion

        #region Sponsors

        /// <summary>
        /// Obtiene todos los Sponsors
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerSponsors(int periodo = -1, string pais = "", int idPais = -1)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                var sponsorsInfo = Helpers.CacheHelper.GetValue<List<SponsorInfo>>(Comun.Constantes.CacheKeys.LISTA_SPONSORS + "_" + idPais);
                if (sponsorsInfo == null)
                {
                    sponsorsInfo = obtenerSponsorsInfo(periodo, pais, idPais);
                    Helpers.CacheHelper.SetValue<List<SponsorInfo>>(Comun.Constantes.CacheKeys.LISTA_SPONSORS + "_" + idPais, sponsorsInfo);
                }

                mensaje = "Datos obtenidos";
                resultado = true;
                valor = sponsorsInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        private static List<SponsorInfo> obtenerSponsorsInfo(int periodo = -1, string pais = "", int idPais = -1)
        {
            string root = HttpContext.Current.Server.MapPath("");
            int lastIndex = root.LastIndexOf(@"\");
            root = root.Substring(lastIndex, root.Length - lastIndex).Replace("\\", "");
            if (root == "Pages")
                root = @"..\";
            else
                root = "";

            var sponsorsInfo = new List<SponsorInfo>();
            var fileNames = new List<string>();

            var sponsorTipos = new List<string>()
                {
                    Comun.Constantes.SponsorTipos.PRIMARIO,
                    Comun.Constantes.SponsorTipos.SECUNDARIO,
                    Comun.Constantes.SponsorTipos.TERCIARIO
                };
            foreach (var tipo in sponsorTipos)
            {
                string path = HttpContext.Current.Server.MapPath(String.Format("{0}Images/Sponsors/{1}s/{2}/", root, tipo, idPais));
                if (Directory.Exists(path))
                {
                    var files = new DirectoryInfo(path).GetFiles();
                    foreach (FileInfo file in files)
                    {
                        fileNames.Add(file.Name);
                    }
                }
            }

            var sponsors = new SponsorControlador().ObtenerTodos(periodo, pais, idPais);
            foreach (Sponsor sponsor in sponsors)
            {
                sponsorsInfo.Add
                    (
                        new SponsorInfo()
                        {
                            Id = sponsor.Id,
                            Nombre = sponsor.Nombre,
                            Web = sponsor.Web,
                            IdTipo = sponsor.Tipo.Id,
                            Tipo = sponsor.Tipo.Nombre,
                            IdPais = sponsor.Pais.Id,
                            Imagen = fileNames.Where(x => x.Substring(0, x.Length - 4) == sponsor.Id.ToString()).FirstOrDefault()
                        }
                    );
            }
            
            return sponsorsInfo;
        }

        /// <summary>
        /// Obtiene un Sponsor, según su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerSponsor(int id)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                string root = HttpContext.Current.Server.MapPath("");
                int lastIndex = root.LastIndexOf(@"\");
                root = root.Substring(lastIndex, root.Length - lastIndex).Replace("\\", "");
                if (root == "Pages")
                    root = @"..\";
                else
                    root = "";

                var controladorSponsor = new ControladorBase<Sponsor>();

                var sponsor = controladorSponsor.ObtenerEntidad(id);
                if (sponsor != null)
                {
                    mensaje = "Datos del Sponsor obtenidos exitosamente.";
                    string path = HttpContext.Current.Server.MapPath( String.Format("{0}Images/Sponsors/{1}s/", root, sponsor.Tipo.Nombre) );
                    sponsor.Imagen = new FileInfo( path + sponsor.Id + ".gif").Name;
                    valor = new SponsorInfo(sponsor);
                    resultado = true;
                }
                else
                {
                    mensaje = "No se ha podido recuperar la información del Sponsor.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Guarda un Sponsor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idTipo"></param>
        /// <param name="nombre"></param>
        /// <param name="web"></param>
        /// <param name="imagen"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarSponsor(int id, int idTipo, string nombre, string web, int idPais, string imagen)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                var controladorSponsor = new ControladorBase<Sponsor>();
                var controladorSponsorTipo = new ControladorBase<SponsorTipo>();
                var controladorPais = new ControladorBase<Pais>();

                var tipoSponsor = controladorSponsorTipo.ObtenerEntidad(idTipo);

                string path = HttpContext.Current.Server.MapPath(String.Format("Images/Sponsors/{0}s/{1}/", tipoSponsor.Nombre, idPais));
                string fileName = string.Empty;
                string format = "gif";

                Sponsor sponsor = null;
                if (id == -1)
                    sponsor = new Sponsor();
                else
                    sponsor = controladorSponsor.ObtenerEntidad(id);

                    sponsor.Nombre  = nombre;
                    sponsor.Tipo    = tipoSponsor;
                    sponsor.Web     = web;
                    sponsor.Pais    = controladorPais.ObtenerEntidad(idPais);

                //Guardar el Sponsor..
                controladorSponsor.GuardarEntidad(sponsor);

                Helpers.CacheHelper.RemoveValue(Comun.Constantes.CacheKeys.LISTA_SPONSORS + "_" + idPais);

                fileName = sponsor.Id.ToString() + "." + format;
                sponsor.Imagen  = fileName;

                mensaje     = "Datos guardados exitosamente.";
                resultado   = true;
                valor       = new SponsorInfo(sponsor);
                respuesta   = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);

                #region Guardar imagen
                if (!String.IsNullOrEmpty(imagen))
                {
                    byte[] bytesImagen = Comun.Formateador.Base64.ConvertirEnArrayDeBytes(imagen);
                    MemoryStream stream = Comun.Formateador.Imagen.Formatear(bytesImagen, 170, 90, System.Drawing.Imaging.ImageFormat.Png);
                    Image archivoImagen = Image.FromStream(stream);

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    archivoImagen.Save(path + fileName);
                    //Image result = new Bitmap(new MemoryStream(bytesImagen));
                    //using (Image archivoImagen = result)
                    //{
                    //    archivoImagen.Save( path + fileName );
                    //}
                }
                #endregion
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Obtiene un Sponsor, según su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        public static string eliminarSponsor(int id)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                SponsorControlador sc = new SponsorControlador();
                Sponsor sponsor = sc.ObtenerPorId(id);
                if (sponsor != null)
                {
                    resultado = sc.Eliminar(sponsor);
                    var idPais = sponsor.Pais.Id;
                    Helpers.CacheHelper.RemoveValue(Comun.Constantes.CacheKeys.LISTA_SPONSORS + "_" + idPais);
                    if ( resultado )
                    {
                        mensaje = "Datos del Sponsor eliminados exitosamente.";
                        resultado = true;
                        valor = new SponsorInfo( sponsor );
                        string path = HttpContext.Current.Server.MapPath(String.Format("Images/Sponsors/{0}s/", sponsor.Tipo.Nombre));

                        //eliminar archivo de imagen..
                        new FileInfo( path + sponsor.Id + ".gif").Delete();
                    }
                }
                else
                {
                    mensaje = "No se ha podido eliminar el Sponsor.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Albums

        /// <summary>
        /// Obtiene todos los Albums
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="idEntidad"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerAlbums(int periodo = -1, string pais = "", string idEntidad = "")
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;
            try
            {
                AlbumControlador ac = new AlbumControlador();
                List<AlbumInfo> albumsInfo = new List<AlbumInfo>();
                List<Album> albums = ac.ObtenerTodos(periodo, pais, idEntidad);
                if ( albums != null)
                {
                    foreach(Album album in albums)
                    {
                        albumsInfo.Add( new AlbumInfo(album) );
                    }
                }
                mensaje     = "Datos obtenidos";
                resultado   = true;
                valor       = albumsInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Contenidos

        /// <summary>
        /// Guarda el Contenido de un Campo de una Seccion
        /// </summary>
        /// <param name="seccion"></param>
        /// <param name="campo"></param>
        /// <param name="contenido"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarSeccionContenido(string seccion, string campo, string contenido, string valor)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            string respuesta = string.Empty;
            try
            {
                var filtroInfo = SessionHelper.ObtenerFiltroActual();

                Helpers.ContentHelper.GuardarContenido(filtroInfo.IdPais, seccion, campo, contenido, valor);

                //fuerzo la actualización en la Cache..
                var key = String.Format("{0}_ID_PAIS_{1}", Comun.Constantes.CacheKeys.SECCION_CONTENIDO, filtroInfo.IdPais);
                Helpers.CacheHelper.RemoveValue(key);
                var contenidos = Helpers.ContentHelper.ObtenerSeccionContenidos(filtroInfo.IdPais);

                mensaje = "Datos guardados exitosamente.";
                resultado = true;
                respuesta = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        #endregion

        #region Noticias

        /// <summary>
        /// Obtiene las últimas N noticias
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerUltimasNoticias(int cantidad, string pais = "")
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            string respuesta = string.Empty;

            List<NoticiaInfo> noticiasInfo = new List<NoticiaInfo>();
            try
            {
                List<Noticia> noticias = new NoticiaControlador().ObtenerUltimasNoticias(cantidad, pais);

                #region Conversiones a listas jSON
                if (noticias != null)
                    foreach (Noticia noticia in noticias)
                        noticiasInfo.Add(new NoticiaInfo(noticia));                
                #endregion                

                mensaje = "Datos obtenidos";
                resultado = true;
                valor = noticiasInfo;

                respuesta = Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene la lista Paginada de Noticias
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="pagina"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerNoticias_Paginado(string pais = "", int pagina = 1, int cantidad = 10)
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            List<NoticiaInfo> noticiasInfo = new List<NoticiaInfo>();
            try
            {
                List<Noticia> noticias = new NoticiaControlador().ObtenerTodas_Paginado(pais, pagina, cantidad);

                #region Conversiones a listas jSON
                if (noticias != null)
                    foreach (Noticia noticia in noticias)
                        noticiasInfo.Add(new NoticiaInfo(noticia));
                #endregion

                _mensaje = "Datos obtenidos";
                _resultado = true;
                _valor = noticiasInfo;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// Obtiene el Nro Total de Noticias
        /// </summary>
        /// <param name="pais"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerTotalNoticias( string pais = "")
        {
            bool _resultado = false;
            string _mensaje = string.Empty;
            object _valor = null;
            try
            {
                NoticiaControlador uc = new NoticiaControlador();
                _valor = uc.ObtenerTotalNoticias(pais);
                _resultado = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(_mensaje, _resultado, _valor);
        }

        /// <summary>
        /// Agrega una nueva noticia
        /// </summary>
        /// <param name="datos"></param>
        /// <returns></returns>
        [WebMethod]
        public static string agregarNoticia(string infoNoticia, string textoNoticia)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                Noticia noticia = null;
                NoticiaControlador uc = new NoticiaControlador();

                //Parseo los datos enviados..
                var noticiaInfo = Comun.Formateador.jSON.convertFromJson<NoticiaInfo>(infoNoticia);

                resultado = true;

                if (resultado)
                {
                    noticia = new Noticia();

                    #region Web
                    noticia.Titulo = noticiaInfo.Titulo;
                    noticia.Resumen = noticiaInfo.Resumen;
                    noticia.Texto = textoNoticia;
                    noticia.Fecha = DateTime.Parse(noticiaInfo.Fecha);
                    noticia.Pais = new PaisControlador().ObtenerPorId(Convert.ToInt32(noticiaInfo.IdPais));

                    string folder = "/Images/Noticias/";
                    string path = HttpContext.Current.Server.MapPath(folder);
                    string fileName = string.Empty;
                    string format = "png";
                    noticia.Imagenes = new List<NoticiaImagen>();
                    if (noticiaInfo.Foto1 != null && noticiaInfo.Foto1 != ""){
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto1, path, fileName, 600, 450);
                    }

                    if (noticiaInfo.Foto2 != null && noticiaInfo.Foto2 != "")
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto2, path, fileName, 600, 450);
                    }
                    if (noticiaInfo.Foto3 != null && noticiaInfo.Foto3 != "")
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto3, path, fileName, 600, 450);
                    }
                    if (noticiaInfo.Foto4 != null && noticiaInfo.Foto4 != "")
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto4, path, fileName, 600, 450);
                    }
                    if (noticiaInfo.Foto5 != null && noticiaInfo.Foto5 != "")
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto5, path, fileName, 600, 450);
                    }
                    if (noticiaInfo.Foto6 != null && noticiaInfo.Foto6 != "")
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto6, path, fileName, 600, 450);
                    }

                    #endregion

                    resultado = uc.Guardar(noticia);
                    if (resultado)
                    {
                        mensaje = "La noticia se ha guardado exitosamente.";
                        resultado = true;
                        valor = new NoticiaInfo(noticia);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        private static void uploadImagen(string imagen, string path, string fileName, int maxWidth, int maxHeight){ 
           

                    #region Guardar imagen
                    if (!String.IsNullOrEmpty(imagen))
                    {
                        byte[] bytesImagen = Comun.Formateador.Base64.ConvertirEnArrayDeBytes(imagen);
                        MemoryStream stream = Comun.Formateador.Imagen.Formatear(bytesImagen, 1, System.Drawing.Imaging.ImageFormat.Png);
                        Image archivoImagen = Image.FromStream(stream);

                        if (archivoImagen.Width > archivoImagen.Height && archivoImagen.Width > maxWidth)
                        {

                            int newHeight = (int)(archivoImagen.Height * (maxWidth / (decimal)archivoImagen.Width));
                            stream = Comun.Formateador.Imagen.Formatear(bytesImagen, maxWidth, newHeight, System.Drawing.Imaging.ImageFormat.Png);
                            archivoImagen = Image.FromStream(stream);
                        }
                        else if (archivoImagen.Height > archivoImagen.Width && archivoImagen.Height > maxHeight)
                        {
                            int newWidth = (int)(archivoImagen.Width * (maxHeight / (decimal)archivoImagen.Height));
                            stream = Comun.Formateador.Imagen.Formatear(bytesImagen, newWidth, maxHeight, System.Drawing.Imaging.ImageFormat.Png);
                            archivoImagen = Image.FromStream(stream);
                        }


                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);
                        archivoImagen.Save(path + fileName);
                    }
                    #endregion
        }

        /// <summary>
        /// Obtiene una Noticia, según su Id
        /// </summary>
        /// <param name="idNoticia"></param>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerNoticia(int idNoticia)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                NoticiaInfo noticiaInfo = null;
                Noticia noticia = new NoticiaControlador().ObtenerPorId(idNoticia);
                if (noticia != null)
                {
                    noticiaInfo = new NoticiaInfo(noticia);
                    valor = noticiaInfo;
                    resultado = true;
                }
                else
                {
                    mensaje = "No se ha podido recuperar la información del Usuario.";
                    resultado = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        /// <summary>
        /// Guarda los datos de una Noticia
        /// </summary>
        /// <param name="infoNoticia"></param>
        /// <param name="textoNoticia"></param>
        /// <returns></returns>
        [WebMethod]
        public static string guardarNoticia(string infoNoticia, string textoNoticia)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            object valor = null;
            try
            {
                NoticiaControlador uc = new NoticiaControlador();

                var noticiaInfo = Comun.Formateador.jSON.convertFromJson<NoticiaInfo>(infoNoticia);

                var noticia = uc.ObtenerPorId(noticiaInfo.Id);
                if (noticia.IsNotNull())
                {
                    noticia.Titulo = noticiaInfo.Titulo;
                    noticia.Resumen = noticiaInfo.Resumen;

                    noticia.Texto = textoNoticia;
                    noticia.Fecha = DateTime.Parse(noticiaInfo.Fecha);

                    string folder = "/Images/Noticias/";
                    string path = HttpContext.Current.Server.MapPath(folder);
                    string fileName = string.Empty;
                    string format = "png";
                    if (noticia.Imagenes == null)
                        noticia.Imagenes = new List<NoticiaImagen>();

                    List<int> removeList = new List<int>();

                    if (noticiaInfo.Foto1 != null && noticiaInfo.Foto1 != "" && !noticiaInfo.Foto1.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (noticia.Imagenes.Count >= 1 && noticia.Imagenes.ElementAt(0) != null)
                            noticia.Imagenes.ElementAt(0).URLImagen = folder + fileName;
                        else
                            noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto1, path, fileName, 450, 500);
                    }
                    else
                    {
                        if (noticiaInfo.Foto1 == "" && noticia.Imagenes.Count >= 1)
                            removeList.Insert(0, 0);
                    }

                    if (noticiaInfo.Foto2 != null && noticiaInfo.Foto2 != "" && !noticiaInfo.Foto2.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (noticia.Imagenes.Count >= 2 && noticia.Imagenes.ElementAt(1) != null)
                            noticia.Imagenes.ElementAt(1).URLImagen = folder + fileName;
                        else
                            noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto2, path, fileName, 450, 500);
                    }
                    else
                    {
                        if (noticiaInfo.Foto2 == "" && noticia.Imagenes.Count >= 2)
                            removeList.Insert(0, 1);
                    }

                    if (noticiaInfo.Foto3 != null && noticiaInfo.Foto3 != "" && !noticiaInfo.Foto3.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (noticia.Imagenes.Count >= 3 && noticia.Imagenes.ElementAt(2) != null)
                            noticia.Imagenes.ElementAt(2).URLImagen = folder + fileName;
                        else
                            noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto3, path, fileName, 450, 500);
                    }
                    else
                    {
                        if (noticiaInfo.Foto3 == "" && noticia.Imagenes.Count >= 3)
                            removeList.Insert(0, 2);
                    }
                    if (noticiaInfo.Foto4 != null && noticiaInfo.Foto4 != "" && !noticiaInfo.Foto4.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (noticia.Imagenes.Count >= 4 && noticia.Imagenes.ElementAt(3) != null)
                            noticia.Imagenes.ElementAt(3).URLImagen = folder + fileName;
                        else
                            noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto4, path, fileName, 450, 500);
                    }
                    else
                    {
                        if (noticiaInfo.Foto4 == "" && noticia.Imagenes.Count >= 4)
                            removeList.Insert(0, 3);
                    }
                    if (noticiaInfo.Foto5 != null && noticiaInfo.Foto5 != "" && !noticiaInfo.Foto5.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (noticia.Imagenes.Count >= 5 && noticia.Imagenes.ElementAt(4) != null)
                            noticia.Imagenes.ElementAt(4).URLImagen = folder + fileName;
                        else
                            noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto5, path, fileName, 450, 500);
                    }
                    else
                    {
                        if (noticiaInfo.Foto5 == "" && noticia.Imagenes.Count >= 5)
                            removeList.Insert(0, 4);
                    }
                    if (noticiaInfo.Foto6 != null && noticiaInfo.Foto6 != "" && !noticiaInfo.Foto6.Contains(folder))
                    {
                        fileName = DateTime.Now.ToString("yyyyMMdd_hhmmss_fff") + "." + format;
                        if (noticia.Imagenes.Count >= 6 && noticia.Imagenes.ElementAt(5) != null)
                            noticia.Imagenes.ElementAt(5).URLImagen = folder + fileName;
                        else
                            noticia.Imagenes.Add(new NoticiaImagen() { Noticia = noticia, URLImagen = folder + fileName });
                        uploadImagen(noticiaInfo.Foto6, path, fileName, 450, 500);
                    }
                    else
                    {
                        if (noticiaInfo.Foto6 == "" && noticia.Imagenes.Count >= 6)
                            removeList.Insert(0, 5);
                    }

                    foreach (int index in removeList)
                    {
                        noticia.Imagenes.RemoveAt(index);
                    }

                    resultado = uc.Guardar(noticia);

                    if (resultado)
                    {
                        mensaje = Resources.Global.Actualizar_Noticia_Exito;
                        resultado = true;
                        valor = new NoticiaInfo(noticia);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string eliminarNoticia(int id)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Eliminar_Mensaje_Error;
            object valor = null;
            try
            {
                var cb = new ControladorBase<Noticia>();
                var noticia = cb.ObtenerEntidad(id);
                if (noticia.IsNotNull())
                {
                    var noticiaInfo = new NoticiaInfo(noticia);
                    cb.EliminarEntidad(noticia);

                    mensaje = Resources.Global.Eliminar_Mensaje_Exito;
                    valor = noticiaInfo;
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }


        #endregion

        #region Meta Tags

        /// <summary>
        /// Devuelve las MetaTags
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string obtenerMetaTags()
        {
            bool resultado  = false;
            string mensaje  = Resources.Global.Obtener_Mensaje_Error;
            object valor    = null;
            try
            {
                valor       = ObtenerMetaTags();
                resultado   = true;
                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string guardarMetaTags(string metaTagInfoItems)
        {
            bool resultado = false;
            string mensaje = string.Empty;
            object valor = null;
            try
            {
                var cb = new ControladorBase<SeccionMeta>();
                var metaTagsInfo = Comun.Formateador.jSON.convertFromJson<List<SeccionMetaInfo>>(metaTagInfoItems);
                foreach (var metaTagInfo in metaTagsInfo)
                {
                    var metaTag             = cb.ObtenerEntidad(metaTagInfo.Id);
                        metaTag.Description = metaTagInfo.Description;
                        metaTag.Keywords    = metaTagInfo.Keywords;
                    cb.GuardarEntidad(metaTag);
                }

                Helpers.CacheHelper.RemoveValue(Comun.Constantes.CacheKeys.META_TAGS);

                resultado = true;
                valor = null;
                mensaje = Resources.Global.Guardar_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                mensaje = Resources.Global.Guardar_Mensaje_Error;
                HandleException(ex);
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Recursos

        [WebMethod]
        public static string obtenerListadoRecursos(bool soloDefaults, string recurso)
        {
            bool resultado  = false;
            string mensaje  = Resources.Global.Obtener_Mensaje_Error;
            object valor    = null;
            try
            {
                var recursosInfo = new List<RecursoInfo>();
                string resourcesPath = HttpContext.Current.Request.PhysicalApplicationPath + "Resources";
                var files = new DirectoryInfo(resourcesPath).GetFiles().Where(x => x.Extension.ToLower() == ".resx").ToList();
                int i = 1;
                foreach (var file in files)
                {
                    if (soloDefaults)
                    {
                        if (file.Name.IndexOf(".") == file.Name.LastIndexOf("."))
                        {
                            var recursoInfo         = new RecursoInfo();
                                recursoInfo.Id      = i;
                                recursoInfo.Nombre = file.Name;
                                recursoInfo.Ruta    = file.FullName;
                                recursoInfo.Pais    = new PaisInfo()
                                {
                                    Id = 1,
                                    Nombre = "",
                                    Continente = "",
                                    IdContinente = -1,
                                    Lenguaje = "",
                                    Bandera = "",
                                    Activo = true
                                };
                            recursosInfo.Add(recursoInfo);
                            i++;
                        }
                    }
                    else
                    {
                        //recursosInfo.Add(new ListaInfo() { Id = i, Texto = file.Name, Valor = file.FullName });
                        i++;
                    }
                }
                resultado   = true;
                valor       = recursosInfo;
                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerRecurso(string recurso)
        {
            bool resultado  = false;
            string mensaje  = Resources.Global.Obtener_Mensaje_Error;
            object valor    = null;
            try
            {
                string resourcesPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\Resources";
                var dirInfo = new DirectoryInfo(resourcesPath);
                var resource = dirInfo.GetFiles().Where(x => x.Name == recurso).FirstOrDefault();
                Helpers.LocalizationHelper.GetResourceFileContent(resource.FullName);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string guardarRecurso(string fileName, string claveNombre, string claveValor)
        {
            bool resultado = false;
            string mensaje = Resources.Global.Guardar_Mensaje_Exito;
            object valor = null;
            try
            {
                //filename = Request.QueryString["file"];
                //int id = Convert.ToInt32(Request.QueryString["id"]);
                //filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + filename;
                //XmlDocument xmlDoc = new XmlDocument();
                //xmlDoc.Load(filename);
                //XmlNodeList nlist = xmlDoc.GetElementsByTagName("data");
                //XmlNode childnode = nlist.Item(id);
                //childnode.Attributes["xml:space"].Value = "default";
                //xmlDoc.Save(filename);
                //XmlNode lastnode = childnode.SelectSingleNode("value");
                //lastnode.InnerText = txtResourceValue.Text;
                //xmlDoc.Save(filename);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        /// <summary>
        /// Registra un error
        /// </summary>
        /// <param name="seccion"></param>
        /// <param name="metodo"></param>
        /// <param name="exception"></param>
        /// <param name="stackTrace"></param>
        /// <returns></returns>
        [WebMethod]
        public static string registrarError(string seccion, string metodo, string exception, string stackTrace)
        {
            string mensaje = string.Empty;
            bool resultado = false;
            string respuesta = string.Empty;
            try
            {
                Helpers.LogHelper.LogError(seccion, exception, stackTrace);

                mensaje = "Datos guardados exitosamente.";
                resultado = true;
                respuesta = Helpers.SessionHelper.BuildResponse(mensaje, resultado, null);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                //throw;
            }
            return respuesta;
        }

        #endregion

        #region Private Methods

        #region Page Process

        private void ValidarPermisosUsuario(string pagina)
        {
            var redirect = true;

            var paginasPublicas = new List<string>()
            {
                Comun.Constantes.Secciones.DEFAULT,
                Comun.Constantes.Secciones.COMO_PLANTAR_UN_ARBOL,
                Comun.Constantes.Secciones.DETALLE_USUARIO,
                Comun.Constantes.Secciones.ENTIDADES,
                Comun.Constantes.Secciones.GALERIA,
                Comun.Constantes.Secciones.PARTICIPAR,
                Comun.Constantes.Secciones.QUIENES_SOMOS,
                Comun.Constantes.Secciones.REGISTRARSE,
                Comun.Constantes.Secciones.SEMANA_DEL_ARBOL,
                Comun.Constantes.Secciones.VIVEROS,
                Comun.Constantes.Secciones.WIKI_ARBOL,
                Comun.Constantes.Secciones.NOTICIAS,
                Comun.Constantes.Secciones.VER_NOTICIA,
                Comun.Constantes.Secciones.WIKI_ARBOL,
                Comun.Constantes.Secciones.AGREGAR_ESPECIE,
                Comun.Constantes.Secciones.EDITAR_ESPECIE,
                Comun.Constantes.Secciones.CONTACTO
            }.Select(x => x.ToLower()).ToList();

            var paginasConUsuarioLogueado = new List<string>()
            {
                Comun.Constantes.Secciones.ADOPTAR,
                Comun.Constantes.Secciones.DONAR,
                Comun.Constantes.Secciones.NUEVA_ACTIVIDAD,
                Comun.Constantes.Secciones.PARTICIPAR,
                Comun.Constantes.Secciones.PLANTAR
            }.Select(x => x.ToLower()).ToList();

            var paginasSoloAdmin = new List<string>()
            {
                Comun.Constantes.Secciones.ADMINISTRACION,
                Comun.Constantes.Secciones.PAISES,
                Comun.Constantes.Secciones.PROVINCIAS,
                Comun.Constantes.Secciones.REPORTES,
                Comun.Constantes.Secciones.SPONSORS,
                Comun.Constantes.Secciones.CONFIGURACION_EMAIL,
                Comun.Constantes.Secciones.META_TAGS,
                Comun.Constantes.Secciones.LOCALIZACION,
                Comun.Constantes.Secciones.ESPECIES,
                Comun.Constantes.Secciones.USUARIOS,
                Comun.Constantes.Secciones.REPORTES,
                Comun.Constantes.Secciones.ADMIN_NOTICIAS,
                Comun.Constantes.Secciones.AGREGAR_NOTICIA,
                Comun.Constantes.Secciones.DETALLE_NOTICIA
            }.Select(x => x.ToLower()).ToList();

            if ( !paginasPublicas.Contains(pagina) )
            {
                var usuarioLogueado = Helpers.SessionHelper.GetCurrentUser();
                if (usuarioLogueado != null)
                {
                    if (paginasConUsuarioLogueado.Contains(pagina))
                        redirect = false;
                    if (paginasSoloAdmin.Contains(pagina) )
                    {
                        if (usuarioLogueado.Perfil.Nombre.ToLower() == Comun.Constantes.UsuarioPerfiles.Administrador.ToLower())
                            redirect = false;
                    }
                }
                if (redirect)
                    HttpContext.Current.Response.Redirect(String.Format("/{0}.aspx", Comun.Constantes.Secciones.DEFAULT), true); 
            }
        }

        private void SetMetaTags(string pageName)
        {
            var metaTags = ObtenerMetaTags();
            if (metaTags.IsNotNull())
            {
                var metaTag = metaTags.Where(x => x.Seccion.ToLower() == pageName.ToLower()).FirstOrDefault();
                if (metaTag.IsNotNull())
                {
                    this.Page.MetaKeywords      = metaTag.Keywords;
                    this.Page.MetaDescription   = metaTag.Description;
                }
            }
            
        }

        private void ProcessUserIdentity(HttpRequest request)
        {
            var userIdentity = request.LogonUserIdentity;
            //.ImpersonationLevel	Impersonation
            //.IsAnonymous		    false
            //.IsAuthenticated	    true
            //.Name			        "ToshibaL845\\Matías Bello"
        }

        #endregion

        #region Errors

        protected static void HandleException(Exception ex)
        {
            try
            {
                var segments = HttpContext.Current.Request.UrlReferrer.Segments;
                var seccion = segments[segments.Length - 1];
                LogHelper.LogError(seccion, ex.Message, ex.StackTrace);
            }
            catch (Exception)
            {
                //throw;
            }
        }

        //private static RespuestaInfo HandleException(Exception ex)
        //{
        //    RespuestaInfo respuesta = new RespuestaInfo();
        //    try
        //    {
        //        if ( ex != null )
        //        {
        //            respuesta.Mensaje   = ex.Message;
        //            respuesta.Resultado = false;
        //            respuesta.Valor     = GetAllErrors(ex);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return respuesta;
        //}
        
        //private static string GetAllErrors(Exception ex)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    if ( ex.InnerException != null )
        //    {
        //        sb.AppendLine( ex.InnerException.Message );
        //        GetAllErrors( ex.InnerException );
        //    }
        //    return sb.ToString();
        //}
        
        #endregion

        private static List<PaisInfo> ObtenerListaPaises(bool incluirInactivos)
        {
            var paisesInfo = new List<PaisInfo>();
            try
            {
                paisesInfo = Helpers.CacheHelper.GetValue<List<PaisInfo>>(Comun.Constantes.CacheKeys.LISTA_PAISES + "_Inactivos_" + incluirInactivos);
                if (paisesInfo == null)
                {
                    var paises = new PaisControlador().ObtenerTodos(incluirInactivos);
                    if (paises != null)
                    {
                        paisesInfo = new List<PaisInfo>();
                        foreach (var pais in paises)
                        {
                            paisesInfo.Add(new PaisInfo(pais));
                        }
                    }
                    Helpers.CacheHelper.SetValue<List<PaisInfo>>(Comun.Constantes.CacheKeys.LISTA_PAISES + "_Inactivos_" + incluirInactivos, paisesInfo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return paisesInfo;
        }

        private static List<ProvinciaInfo> ObtenerListaProvincias(int idPais)
        {
            var provinciasInfo = new List<ProvinciaInfo>();
            try
            {
                provinciasInfo = Helpers.CacheHelper.GetValue<List<ProvinciaInfo>>(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_PROVINCIAS, idPais));
                if (provinciasInfo == null)
                {
                    var provincias = new ProvinciaControlador().ObtenerTodas(idPais);
                    if (provincias != null)
                    {
                        provinciasInfo = new List<ProvinciaInfo>();
                        foreach (var provincia in provincias)
                        {
                            provinciasInfo.Add(new ProvinciaInfo(provincia));
                        }
                    }
                    Helpers.CacheHelper.SetValue<List<ProvinciaInfo>>(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_PROVINCIAS, idPais), provinciasInfo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return provinciasInfo;
        }

        private static List<EspecieInfo> ObtenerListaEspecies(int idPais)
        {
            var especiesInfo = new List<EspecieInfo>();
            try
            {
                especiesInfo = Helpers.CacheHelper.GetValue<List<EspecieInfo>>(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_ESPECIES, idPais));
                if (especiesInfo == null)
                {
                    var especies = new EspecieControlador().ObtenerTodasPorPais(idPais);
                    if (especies != null)
                    {
                        especiesInfo = new List<EspecieInfo>();
                        foreach (var especie in especies)
                        {
                            var especieInfo = new EspecieInfo(especie, false);
                            if (especie.Localizaciones != null && especie.Localizaciones.Count() > 0)
                            {
                                var especieLocalizada = especie.Localizaciones.Where(x => x.Pais.Id == idPais).FirstOrDefault();
                                if ( especieLocalizada.IsNotNull() )
                                    especieInfo.Nombre = especieLocalizada.Nombre;
                            }
                            especiesInfo.Add(especieInfo);
                        }
                        especiesInfo = especiesInfo.OrderBy(x => x.Nombre).ToList();
                    }
                    Helpers.CacheHelper.SetValue<List<EspecieInfo>>(String.Format("{0}_{1}", Comun.Constantes.CacheKeys.LISTA_ESPECIES, idPais), especiesInfo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return especiesInfo;
        }

        private static List<SeccionMetaInfo> ObtenerMetaTags()
        {
            var metaTagsInfo = new List<SeccionMetaInfo>();
            try
            {
                metaTagsInfo = Helpers.CacheHelper.GetValue<List<SeccionMetaInfo>>(Comun.Constantes.CacheKeys.META_TAGS);
                if (metaTagsInfo.IsNull())
                {
                    var metaTags = new ControladorBase<SeccionMeta>().ObtenerEntidades();
                    if (metaTags.IsNotNull())
                    {
                        metaTags = metaTags.Where(x => x.Seccion.Privada == false).ToList();
                        metaTags = metaTags.OrderBy(x => x.Seccion.Descripcion).ToList();
                        metaTagsInfo = new List<SeccionMetaInfo>();
                        foreach (var metaTag in metaTags)
                        {
                            metaTagsInfo.Add(new SeccionMetaInfo(metaTag));
                        }
                    }
                    Helpers.CacheHelper.SetValue<List<SeccionMetaInfo>>(Comun.Constantes.CacheKeys.META_TAGS, metaTagsInfo);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return metaTagsInfo;
        }

        private static List<T> ObtenerDesdeCache<T>(string key) where T : Entidad
        {
            List<T> lista = null;
            try
            {
                lista = Helpers.CacheHelper.GetValue<List<T>>(key);
                if (lista.IsNull())
                {
                    lista = new ControladorBase<T>().ObtenerEntidades();
                    Helpers.CacheHelper.SetValue<List<T>>(key, lista);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lista;
        }

        #endregion
    }
}
