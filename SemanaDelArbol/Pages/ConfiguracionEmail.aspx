﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="ConfiguracionEmail.aspx.cs" Inherits="SemanaDelArbol.Pages.ConfiguracionEmail" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
        .panelConfiguracionEmail
        {
            width: 65%;
            margin: 5px;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        $(document).ready(function ()
        {
            obtenerUsuarioLogueado(cargarPagina_ConfiguracionEmail);
        });
        function cargarPagina_ConfiguracionEmail()
        {
            $("#btnActualizarConfiguracionEmail").click(function () { guardarConfiguracionEmail(); });
            obtenerConfiguracionEmail();
        }
        function obtenerConfiguracionEmail()
        {
            var params = new Object();
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoConfiguracionEmail").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerConfiguracionEmail", jsonParams, obtenerConfiguracionEmail_Respuesta);
        }
        function obtenerConfiguracionEmail_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var configuracionEmailInfo = respuestaInfo.Valor;
                cargarConfiguracionEmail(configuracionEmailInfo);
            }
            $("#pnlCargandoConfiguracionEmail").fadeOut('400', function () { $("#pnlPrincipal").slideDown(); });
        }
        function cargarConfiguracionEmail(configuracionEmailInfo)
        {
            $("#txtConfiguracionEmail_Emisor_Alias").setValue(configuracionEmailInfo.EmisorAlias);
            $("#txtConfiguracionEmail_Emisor_Direccion").setValue(configuracionEmailInfo.EmisorDireccion);
            $("#txtConfiguracionEmail_Emisor_Usuario").setValue(configuracionEmailInfo.EmisorUsuario);
            $("#txtConfiguracionEmail_Emisor_Password").setValue(configuracionEmailInfo.EmisorPassword);
            $("#txtConfiguracionEmail_SMTP_Servidor").setValue(configuracionEmailInfo.SMTPServidor);
            $("#txtConfiguracionEmail_SMTP_Puerto").setValue(configuracionEmailInfo.SMTPPuerto);
            $("#chkConfiguracionEmail_SMTP_SSL").setValue(configuracionEmailInfo.SMTPHabilitarSSL);
        }
        function guardarConfiguracionEmail()
        {
            var infoValidacion =
                [
                    { campo: "txtConfiguracionEmail_Emisor_Alias", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un alias para la casilla de mail." }] },
                    { campo: "txtConfiguracionEmail_Emisor_Direccion", validaciones: [ { operacion: _validaciones.Especiales.Email, mensaje: "Debe ingresar una dirección de e-mail válida." }] },
                    { campo: "txtConfiguracionEmail_Emisor_Usuario", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un usuario." }] },
                    { campo: "txtConfiguracionEmail_Emisor_Password", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una contraseña." }] },
                    { campo: "txtConfiguracionEmail_SMTP_Servidor", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un servidor." }] },
                    { campo: "txtConfiguracionEmail_SMTP_Puerto", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un número de puerto." }, { operacion: _validaciones.Valor.MayorA, valor: 0, mensaje: "El número del puerto debe ser mayor a cero." }] }
                ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            Helpers.showLoadingMessage();

            var params = new Object();
                params.EmisorAlias      = $("#txtConfiguracionEmail_Emisor_Alias").getValue();
                params.EmisorDireccion  = $("#txtConfiguracionEmail_Emisor_Direccion").getValue();
                params.EmisorUsuario    = $("#txtConfiguracionEmail_Emisor_Usuario").getValue();
                params.EmisorPassword   = $("#txtConfiguracionEmail_Emisor_Password").getValue();
                params.SMTPServidor     = $("#txtConfiguracionEmail_SMTP_Servidor").getValue();
                params.SMTPPuerto       = $("#txtConfiguracionEmail_SMTP_Puerto").getValue();
                params.SMTPHabilitarSSL = $("#chkConfiguracionEmail_SMTP_SSL").getValue();

            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("guardarConfiguracionEmail", "{'infoConfiguracionEmail':'" + jsonParams + "'}", guardarConfiguracionEmail_Respuesta);
        }
        function guardarConfiguracionEmail_Respuesta(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                //var respuestaInfo = respuestaInfo.Valor;
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje);
            }
        }
    </script>

    <!--HTML-->
    <div id="content" >

        <span id="pnlCargandoConfiguracionEmail" style="display:none" >
            <img src="../Images/Procesando.gif" alt="Cargando configuraión.." />
            <label>Cargando..</label>
        </span>

        <div id="pnlPrincipal" style="display:none" >

            <div class="seccion" >
                <div class="titulo" >
                    Datos del Emisor
                </div>
                <div id="pnlConfiguracionEmail_DatosEmisor" class="panelConfiguracionEmail" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="40%" /> 
                            <col width="60%" />                
                        </colgroup>
                        <tr>
                            <td>
                                <label>Alias:</label>
                            </td>
                            <td>
                                <input id="txtConfiguracionEmail_Emisor_Alias" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Dirección:</label>
                            </td>
                            <td>
                                <input id="txtConfiguracionEmail_Emisor_Direccion" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Usuario:</label>
                            </td>
                            <td>
                                <input id="txtConfiguracionEmail_Emisor_Usuario" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Contraseña:</label>
                            </td>
                            <td>
                                <input id="txtConfiguracionEmail_Emisor_Password" type="text"  value="" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="seccion" >
                <div class="titulo" >
                    Datos del Servidor SMTP
                </div>
                <div id="pnlConfiguracionEmail_DatosServidorSMTP" class="panelConfiguracionEmail" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="40%" /> 
                            <col width="60%" />                
                        </colgroup>
                        <tr>
                            <td>
                                <label>Servidor:</label>
                            </td>
                            <td>
                                <input id="txtConfiguracionEmail_SMTP_Servidor" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Puerto:</label>
                            </td>
                            <td>
                                <input id="txtConfiguracionEmail_SMTP_Puerto" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Habilitar SSL:</label>
                            </td>
                            <td>
                                <input id="chkConfiguracionEmail_SMTP_SSL" type="checkbox" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <input type="button" class="button btnEditar" id="btnActualizarConfiguracionEmail" value="Actualizar" style="width:105px" />

        </div>

    </div>

</asp:Content>