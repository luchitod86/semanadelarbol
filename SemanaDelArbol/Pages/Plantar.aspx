﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Plantar.aspx.cs" Inherits="SemanaDelArbol.Pages.Plantar" %>

<%@ Register Src="~/CustomControls/MisDatos.ascx" TagName="MisDatos" TagPrefix="MisDatos" %>
<%@ Register Src="~/CustomControls/Mapa.ascx" TagName="Mapa" TagPrefix="Mapa" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Plantar);
        });
        function cargarPagina_Plantar()
        {
            var configMapa =
                {
                    IdMapa: "mapa",
                    TituloMapa: "Mis plantaciones",
                    TituloBuscador: null, //"Localice en el mapa la ubicación aproximada",
                    MostrarMapa: true,
                    MostrarBuscador: true,
                    MostrarFiltroMarcadores: false,
                    MostrarListaMarcadores: false,
                    MostrarBotonMiPosicionActual: true,
                    OnMapLoadComplete: onMapLoadComplete_Plantar,
                    Zoom: _googleMap.NivelZoom,
                    Direccion: _filtroActual.Pais.Nombre,
                    ImagenMarcador: _googleMap.Marcadores[_googleMap.TiposMarcador.Plantacion].Icono,
                    DeshabilitarMarcadores: false,
                    OnClickMap: onClickToMap_Plantar,
                    OnBuscarDireccion: null
                };
            inicializarMapa(configMapa);
            configurarMenuMisDatos("Puede ubicar la zona de <b>plantación</b> ingresando una dirección, y luego navegar el mapa hasta encontrar el punto exacto que está buscando.");
        }
        function onMapLoadComplete_Plantar(idMapa)
        {
            if ( verificarUsuarioLogueado() )
            {
                obtenerPlantacionesUsuario(_usuarioLogueado.Id);
            }
        }
        function verAyudaPlantacion()
        {
            var colapsado = $("#ayudaPlantacion").hasClass("colapsado");
            if ( colapsado )
            {
                $("#ayudaPlantacion").removeClass("colapsado");
                $("#imgAyudaPlantacion").attr("src", "../Images/Comun/16x16/Colapsar.png");
                $("#ayudaPlantacion").slideDown();
            }
            else
            {
                $("#ayudaPlantacion").addClass("colapsado");
                $("#imgAyudaPlantacion").attr("src", "../Images/Comun/16x16/Expandir.png");
                $("#ayudaPlantacion").slideUp();
            }
        }
        function onClickToMap_Plantar(map, position, icon, title, idMarker)
        {
            if (!verificarUsuarioLogueado())
            {
                Helpers.NotificationHelper.showWarning("Antes debe loguearse para realizar esta acción.", "Acceso restringido");
                return;
            }

            limpiarDatosPlantacionInfoDialog();

            latLng = String.format("{0}, {1}", position.lat(), position.lng());
            Helpers.GoogleMapHelper.getPositionFromLatLng(latLng, "mapa", onGetPositionFromLatLng_Plantar_Response);

            $("#hdnPlantacionInfoId").val();
            $("#hdnPlantacionInfoLatitud").val(position.lat());
            $("#hdnPlantacionInfoLongitud").val(position.lng());

            $("#lblPlantacionInfoUsuario").text(_usuarioLogueado.NombreUsuario);
        }
        function onGetPositionFromLatLng_Plantar_Response(address_components)
        {
            var direccion = parseAddressComponents(address_components);
            var pais = direccion.pais;
            seleccionarPaisEnLista("selPlantacionInfoPais", pais);
            abrirPlantacionInfoDialog();
            mostrarPanelAdvertenciaUbicacionDistinta(_dialogos.PlantacionInfo.Id, pais);
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <!-- Notificacion -->
            <div id="pnlNotificacion" style="padding:5px 0px 5px 0px;" >
                <div id="notificacionPlantacion" class="ui-state-highlight" >
                    <div onclick="javascript:verAyudaPlantacion();" class="link" >
                        <img id="imgAyudaPlantacion" src="../Images/Comun/16x16/Colapsar.png" alt="Mostrar más" class="link" />¿Cómo realizar la plantación de mis árboles?
                    </div>
                </div>
                <div id="ayudaPlantacion" class="panelAyuda" >
                    <label><b><u>Pasos</u></b></label>
                    <ol>
                        <li><b>Ubique el área</b> de plantado en el mapa llenando los campos.</li>
                        <li>Haga <b>click sobre el mapa</b> cuando encuentre el punto donde plantar su árbol.</li>
                        <li>Indique la <b>especie</b> del árbol, la <b>cantidad</b> plantada, suba una foto ¡y listo!</li>
                    </ol>
                    <div style="padding: 5px 0px 5px 0px" >
                        <label>Recuerde que se pueden realizar todas las plantaciones que desee.</label>
                    </div>
                </div>
            </div>
           
            <!-- Mapa -->
            <div>
                <Mapa:Mapa ID="Mapa" runat="server" />
            </div>
            <!-- Fin de [Mapa]-->

        </div>

    </div>

    <!-- Menú Mis Datos -->
    <div>
        <MisDatos:MisDatos ID="MisDatos" runat="server" />
    </div>
    <!-- Fin de [Menú Mis Datos] -->

</asp:Content>