﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master"  CodeBehind="Noticias.aspx.cs" Inherits="SemanaDelArbol.Pages.AdminNoticias" %>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idTabla = "tblNoticias";
        var totalNoticias = 0;
        var paginaActual = 1;
        var cantidadNoticias = 5;
        $(document).ready(function ()
        {
            paginaActual = getQuerystring("PaginaActual") != '' ? getQuerystring("PaginaActual") : 1;            
            obtenerUsuarioLogueado(cargarPagina_Noticias);
        });
        function cargarPagina_Noticias()
        {
            obtenerNoticias_Admin();
        }
        function obtenerNoticias_Admin()
        {
            var params = new Object();
                params.pais = _filtroActual.Pais.Nombre;
                params.pagina = paginaActual,
                params.cantidad = cantidadNoticias
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoNoticias").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerNoticias_Paginado", jsonParams, obtenerNoticias_Admin_Respuesta);
            Helpers.AJAXHelper.doAjaxCall("obtenerTotalNoticias", jsonParams, actualizarTotalNoticias);
        }
        function obtenerNoticias_Admin_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var noticiasInfo = respuestaInfo.Valor;
                cargarNoticias(noticiasInfo);
            }
            $("#pnlCargandoNoticias").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function cargarNoticias(noticiasInfo)
        {
            //  Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
            if (noticiasInfo.length == 0)
                $("#tblNoticias").append('<p>No se han encontrado Noticias.</p>');
            for (var i = 0; i < noticiasInfo.length; i++)
            {
                crearFilaNoticias(noticiasInfo[i]);
            }            
        }
        function crearFilaNoticias(noticiaInfo) {
            var idNoticia = noticiaInfo.Id;

            var noticia = "<div class='entrada'>" +
                           "  <a href='VerNoticia.aspx?idNoticia=" + idNoticia + "'>";
                            
                            if (!isEmpty(noticiaInfo.Foto1))
                                noticia += "<img src='.." + noticiaInfo.Foto1 + "'>"
                            else
                                noticia += "<img src='../Images/Logo.gif'>"

                            noticia += "  </a>     " +
                           "  <h2> " +
                           "      <a href='VerNoticia.aspx?idNoticia=" + idNoticia + "'>" + noticiaInfo.Titulo + "</a>" +
                           "  </h2>" +
                           "  <p class='fecha'>" + noticiaInfo.Fecha + "</p>" +
                           "  <p>" + noticiaInfo.Resumen + "</p>" +
                           "</div>";

            $("#tblNoticias").append(noticia);

        }

        function actualizarTotalNoticias(datos) {
            totalNoticias = jQuery.parseJSON(datos.d).Valor;
            var totalPaginas = ~~(totalNoticias / cantidadNoticias);
            if ((totalNoticias % cantidadNoticias) != 0)
                totalPaginas++;
            if (totalPaginas > 1)
                Helpers.HtmlHelper.Table.crearPaginador(paginaActual, totalPaginas, 'paginador', 'Noticias.aspx');
        }
    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlNoticias" >

            <div class="seccion" >
                <div class="titulo" >
                    Últimas Noticias
                </div>
            </div>

           
            <span id="pnlCargandoNoticias" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando noticias.." />
                <label>Cargando..</label>
            </span>

            <div id="tblNoticias" class="listado-noticias" style="display:none" >
               
            </div>

            <div id="paginador"></div>
        </div>

    </div>

</asp:Content>
