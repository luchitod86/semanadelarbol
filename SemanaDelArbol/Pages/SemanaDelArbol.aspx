﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="SemanaDelArbol.aspx.cs" Inherits="SemanaDelArbol.Pages.SemanaDelArbol" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
        .btnFolleto a,
        .btnInforme a
        {
            text-decoration: none;
            color: #5B5E5A;
        }
        .btnFolleto a:hover,
        .btnInforme a:hover
        {
            text-decoration: underline;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        var id_editor_contenido_descripcion_semana_del_arbol = "<%=contenido_Descripcion_Semana_Del_Arbol.ClientID %>";

        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_SemanaDelArbol);
        });
        function cargarPagina_SemanaDelArbol()
        {
            configurarVisualizador("lnkVisualizarFolleto", "../Archivos/Sda_Proyecto_2014.pdf", true, false, false);
            configurarVisualizador("lnkVisualizarInforme", "../Archivos/Sda_Informe_2015.pdf", true, false, false);
            configurarEditorContenido(id_editor_contenido_descripcion_semana_del_arbol);
            if ( validarPerfilAdministrador(_usuarioLogueado) || validarPerfilEditor(_usuarioLogueado) )
                $("#btnGuardar").click(function () { guardarContenido_SemanaDelArbol(); }).fadeIn();
        }
        function guardarContenido_SemanaDelArbol()
        {
            var contenido = obtenerValorEditorContenido(id_editor_contenido_descripcion_semana_del_arbol);
            guardarSeccionContenido(_secciones.SemanaDelArbol, "Descripcion_Semana_Del_Arbol", contenido, null);
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >
            <img src="../Images/Iconos/Pdf.png" alt="Descargar" class="icono48x48" />Folletos:
            <span id="btnVisualizarFolleto" class="btnFolleto" >
                <a id="lnkVisualizarFolleto" href="" target="_blank" >Proyecto</a>
            </span>
            /
            <span id="btnVisualizarInforme" class="btnInforme" >
                <a id="lnkVisualizarInforme" href="" target="_blank" >Informe resultados</a>
            </span>

            <span style="float:right" >
                <input id="btnGuardar" type="button" class="button btnGuardar" value="Guardar" style="width:100px; display:none" />
            </span>

            <div id="pnlSemanaDelArbol" >
                <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                    <legend class="ui-widget-header" align="center" >Semana del Árbol</legend>
                    <textarea id="contenido_Descripcion_Semana_Del_Arbol" runat="server" style="display:none;" ></textarea>
                </fieldset>
            </div>
        </div>

    </div>

</asp:Content>