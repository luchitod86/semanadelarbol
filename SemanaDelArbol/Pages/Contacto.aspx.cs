﻿using System;
using System.Net;
using System.Net.Mail;

namespace SemanaDelArbol.Pages
{
    public partial class Contacto : PaginaBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);
        }

        protected void SendMail()
        {
            var fromAddress = new MailAddress("info@semanadelarbol.org", "Info Semana del Arbol");
            var toAddress = new MailAddress("info@semanadelarbol.org", "Info Semana del Arbol");
            var fromPassword = "Gonzalo1983";
            var subject = string.Format("Nuevo contacto desde la pagina web - {0}", YourSubject.Text);
            string body = "De: " + YourName.Text + "\n";
            body += "Email: " + YourEmail.Text + "\n";
            body += "Asunto: " + YourSubject.Text + "\n";
            body += "Comentario: \n" + Comments.Text + "\n";


            var smtp = new SmtpClient
            {
                Host = "mail.semanadelarbol.org",
                Port = 587, //set proper port 587
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var message = new MailMessage(fromAddress, toAddress) { Subject = subject, Body = body })
            {
                smtp.Send(message);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SendMail();
                DisplayMessage.Text = "Hemos recibido su mensaje! Gracias por contactarnos!";
                DisplayMessage.Visible = true;
                YourSubject.Text = "";
                YourEmail.Text = "";
                YourName.Text = "";
                Comments.Text = "";
            }
            catch (Exception ex)
            {
                DisplayMessage.Text = "Error al enviar el mensaje!";
                DisplayMessage.Visible = true;
            }
        }
    }
}