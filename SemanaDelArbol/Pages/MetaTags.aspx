﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="MetaTags.aspx.cs" Inherits="SemanaDelArbol.Pages.MetaTags" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
        .bootstrap-tagsinput
        {
            /*max-width: 355px;*/
            background-color: transparent;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var tblId = "tblMetaTags";
        $(document).ready(function ()
        {
            obtenerUsuarioLogueado(cargarPagina_MetaTags);
        });
        function cargarPagina_MetaTags()
        {
            $("#btnActualizarMetaTags").click(function () { guardarMetaTags(); });

            obtenerMetaTags();
        }
        function obtenerMetaTags()
        {
            var params = new Object();
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoMetaTags").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerMetaTags", jsonParams, obtenerMetaTags_Respuesta);
        }
        function obtenerMetaTags_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var metaTagsInfo = respuestaInfo.Valor;
                cargarMetaTags(metaTagsInfo);
            }
            $("#pnlCargandoMetaTags").fadeOut('400', function () { $("#" + tblId).slideDown(); });
        }
        function cargarMetaTags(metaTagsInfo)
        {
            eliminarFilaMetaTag();
            for (var i = 0; i < metaTagsInfo.length; i++)
            {
                crearFilaMetaTag(metaTagsInfo[i]);
            }

            $(".metaTags").tagsinput();
        }
        function eliminarFilaMetaTag()
        {
            $("#" + tblId + " > tbody").html("");
        }
        function crearFilaMetaTag(metaTagInfo)
        {
            var input    = '<input type="text" value="{0}" class="metaTags" data-role="tagsinput" />';
            var textArea = '<textarea cols="1" rows="1" style="float:left; width:100%; background-color:transparent; border:none; height:70px; padding-left:5px;" class="noResize" >{0}</textarea>';

            var fila = '';
                fila += '<td class="invisible" >' + metaTagInfo.IdSeccion + '</td>';
                fila += '<td>' + metaTagInfo.SeccionDescripcion + '</td>';
                fila += '<td>' + String.format(input, metaTagInfo.Keywords) + '</td>';
                fila += '<td>' + String.format(textArea, metaTagInfo.Description) + '</td>';
                fila = '<tr idMetaTag="' + metaTagInfo.Id + '" >' + fila + '</tr>';

            $("#" + tblId).append(fila);
        }
        function guardarMetaTags()
        {
            Helpers.showLoadingMessage();

            var metaTags = new Array();
            var filas = $("#" + tblId + " tbody tr");
            for (var i = 0; i < filas.length; i++)
            {
                var fila = $(filas[i]);

                var metaTag = new Object();
                    metaTag.Id          = fila.attr("idMetaTag");
                    metaTag.Keywords    = $(fila.find("input")[0]).getValue();
                    metaTag.Description = $(fila.find("textarea")[0]).getValue();

                metaTags.push(metaTag);
            }

            Helpers.AJAXHelper.doAjaxCall("guardarMetaTags", "{'metaTagInfoItems':'" + JSON.stringify(metaTags) + "'}", guardarMetaTags_Respuesta);
        }
        function guardarMetaTags_Respuesta(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                //var respuestaInfo = respuestaInfo.Valor;
                Helpers.NotificationHelper.showMessage(respuestaInfo.Message);
            }
            else
            {
                Helpers.NotificationHelper.showError(respuestaInfo.Message);
            }
        }
    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" >
                    Administración de Meta-Tags
                </div>
            </div>

            <input type="button" class="button btnEditar" id="btnActualizarMetaTags" value="Actualizar" style="width:105px" />
            <span id="pnlCargandoMetaTags" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando meta tags.." />
                <label>Cargando..</label>
            </span>

            <table id="tblMetaTags" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="20%" /> <!-- Sección -->
                    <col width="40%" /> <!-- Palabras Clave -->
                    <col width="40%" /> <!-- Descripción -->
                </colgroup>
                <thead>
                    <tr>
                        <th>Sección</th>
                        <th>Palabras clave</th>
                        <th>Descripción</th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>

        </div>

    </div>

</asp:Content>