﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="QuienesSomos.aspx.cs" Inherits="SemanaDelArbol.Pages.QuienesSomos" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        var id_editor_contenido_descripcion_quienes_somos   = "<%=contenido_Descripcion_Quienes_Somos.ClientID %>";
        var id_editor_contenido_equipo_semana_del_arbol     = "<%=contenido_Equipo_Semana_Del_Arbol.ClientID %>";

        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_QuienesSomos);
        });
        function cargarPagina_QuienesSomos()
        {
            configurarEditorContenido(id_editor_contenido_descripcion_quienes_somos);
            configurarEditorContenido(id_editor_contenido_equipo_semana_del_arbol);

            if (validarPerfilAdministrador(_usuarioLogueado) || validarPerfilEditor(_usuarioLogueado))
            {
                $("#btnGuardar").click(function () { guardarContenido_QuienesSomos(); });
                $("#pnlGuardar").css({ "display": "inline-block" }).fadeIn();
            }
        }
        function guardarContenido_QuienesSomos()
        {
            var contenido_descripcion_quienes_somos = obtenerValorEditorContenido(id_editor_contenido_descripcion_quienes_somos);
            var contenido_equipo_semana_del_arbol   = obtenerValorEditorContenido(id_editor_contenido_equipo_semana_del_arbol);
            guardarSeccionContenido(_secciones.QuienesSomos, "Descripcion_Quienes_Somos", contenido_descripcion_quienes_somos, null);
            guardarSeccionContenido(_secciones.QuienesSomos, "Equipo_Semana_Del_Arbol", contenido_equipo_semana_del_arbol, null);
        }
    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >
                
            <div id="pnlGuardar" style="display:none; width:100%" >
                <span style="float:right" >
                    <input id="btnGuardar" type="button" class="button btnGuardar" value="Guardar" style="width:100px;" />
                </span>
            </div>

            <!-- ¿Quiénes Somos? -->
            <div id="pnlQuienesSomos" >
                <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                    <legend class="ui-widget-header" align="center" >¿Quiénes somos?</legend>
                    <textarea id="contenido_Descripcion_Quienes_Somos" runat="server" style="display:none;" ></textarea>
                </fieldset>
            </div>
            <!-- Fin de [¿Quiénes Somos?] -->

            <!-- Equipo de trabajo -->
            <div id="equipoTrabajo" style="padding:10px 0px 10px 0px;" >
                <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                    <legend class="ui-widget-header" align="center" style="font-size:18px; padding:5px;" >Equipo de la Semana del Árbol</legend>
                    <textarea id="contenido_Equipo_Semana_Del_Arbol" runat="server" style="display:none;height:100%" ></textarea>
                </fieldset>
            </div>
            <!-- Fin de [Equipo de trabajo] -->

        </div>

        
    </div>

</asp:Content>