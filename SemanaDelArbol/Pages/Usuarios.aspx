﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Usuarios.aspx.cs" Inherits="SemanaDelArbol.Pages.Usuarios" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idTabla = "tblUsuarios";
        var totalUsuarios = 0;
        var paginaActual = 1;
        var cantidadUsuarios = 10;
        $(document).ready(function ()
        {
            paginaActual = getQuerystring("PaginaActual") != '' ? getQuerystring("PaginaActual") : 1;            
            obtenerUsuarioLogueado(cargarPagina_Usuarios);
        });
        function cargarPagina_Usuarios()
        {
            obtenerUsuarios_Admin();

            $("#btnAgregarUsuario").click(function () {
                abrirPagina(_secciones.Registrarse);
            });
        }
        function obtenerUsuarios_Admin()
        {
            var params = new Object();
                params.periodo = -1;
                params.pais = _filtroActual.Pais.Nombre;
                params.pagina = paginaActual,
                params.cantidad = cantidadUsuarios
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoUsuarios").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerUsuarios_Paginado", jsonParams, obtenerUsuarios_Admin_Respuesta);
            Helpers.AJAXHelper.doAjaxCall("obtenerTotalUsuarios", jsonParams, actualizarTotalUsuarios);
        }
        function obtenerUsuarios_Admin_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var usuariosInfo = respuestaInfo.Valor;
                cargarUsuarios(usuariosInfo);
            }
            $("#pnlCargandoUsuarios").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function cargarUsuarios(usuariosInfo)
        {
            Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
            for (var i = 0; i < usuariosInfo.length; i++)
            {
                crearFilaUsuarios(usuariosInfo[i].Valor);
            }            
        }
        function crearFilaUsuarios(usuarioInfo) {
            var idUsuario = usuarioInfo.Id;

            var btnEditar   = Helpers.HtmlHelper.Button.create("btnEditarUsuario_{0}".format(idUsuario), "Editar", "btnEditar");
            var btnEliminar = Helpers.HtmlHelper.Button.create("btnEliminarUsuario_{0}".format(idUsuario), "Eliminar", "btnEliminar");

            var fila = '<td>' + btnEditar + btnEliminar + '</td>';
                fila += '<td>' + usuarioInfo.NombreUsuario + '</td>';
                fila += '<td>' + usuarioInfo.Nombre + '</td>';
                fila += '<td>' + usuarioInfo.Apellido + '</td>';
                fila += '<td>' + usuarioInfo.Tipo + '</td>';
                fila += '<td>' + usuarioInfo.Perfil + '</td>';
            Helpers.HtmlHelper.Table.addNewRow(idTabla, fila, "idUsuario", idUsuario, false);

            var btnEditar = $("#btnEditarUsuario_{0}".format(idUsuario));
                btnEditar.click(function () { editarUsuario(idUsuario); });
                btnEditar.button();

            var btnEliminar = $("#btnEliminarUsuario_{0}".format(idUsuario));
                btnEliminar.click(function () { alert('Esta función no está disponible aún') });
                btnEliminar.button();
        }
        function editarUsuario(idUsuario)
        {
            abrirPagina(_secciones.DetalleUsuario, "idUsuario={0}".format(idUsuario));
        }
        function actualizarTotalUsuarios(datos) {
            totalUsuarios = jQuery.parseJSON(datos.d).Valor;
            var totalPaginas = ~~(totalUsuarios / cantidadUsuarios);
            if ((totalUsuarios % cantidadUsuarios) != 0)
                totalPaginas++;
            if (totalPaginas > 1)
                Helpers.HtmlHelper.Table.crearPaginador(paginaActual, totalPaginas, 'paginador', 'Usuarios.aspx');
            $("#lblTotalUsuarios").html("Total: <b>{0}</b> usuarios(s) en <b>{1}</b>".format(totalUsuarios, _filtroActual.Pais.Nombre));
        }

    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" >
                    Administración de Usuarios
                </div>
            </div>

            <input type="button" class="button btnAgregar" id="btnAgregarUsuario" value="Agregar" style="width:100px" />
            <label id="lblTotalUsuarios" >Total 0 usuarios</label>

            <span id="pnlCargandoUsuarios" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando usuarios.." />
                <label>Cargando..</label>
            </span>

            <table id="tblUsuarios" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="25%" /> <!-- Acción -->
                    <col width="15%" /> <!-- NombreUsuario -->
                    <col width="20%" /> <!-- Nombre -->
                    <col width="20%" /> <!-- Apellido -->
                    <col width="10%" /> <!-- Tipo -->
                    <col width="10%" /> <!-- Perfil -->
                </colgroup>
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>Nombre de Usuario</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Tipo</th>
                        <th>Perfil</th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>

            <div id="paginador"></div>
        </div>

    </div>

</asp:Content>