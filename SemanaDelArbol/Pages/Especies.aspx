﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Especies.aspx.cs" Inherits="SemanaDelArbol.Pages.Especies" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idTabla = "tblEspecies";
        var idTablaLocalizaciones = "tblEspecieLocalizaciones";
        var totalEspecies = 0;
        var totalEspecieLocalizaciones = 0;
        $(document).ready(function ()
        {
            obtenerUsuarioLogueado(cargarPagina_Especies);
        });
        function cargarPagina_Especies()
        {
            obtenerListadoEspecies();

            cargarListasGeograficas("selEspecieLocalizacionInfo_Pais", null, -1, null);

            inicializarDialogEspecieInfo();
            inicializarDialogEspecieLocalizacionInfo();
            inicializarDialogEspecieLocalizacionesInfo();

            $("#btnAgregarEspecie").click(function ()
            {
                limpiarDatosEspecieInfoDialog();
                abrirEspecieInfoDialog("Agregar especie");
            });

            $("#btnAgregarEspecieLocalizacion").click(function ()
            {
                limpiarDatosEspecieLocalizacionInfoDialog();
                abrirEspecieLocalizacionInfoDialog("Agregar localización");
            });
        }
        function crearFilaEspecie(especieInfo, insertFirst)
        {
            var idEspecie   = especieInfo.Id;
            
            var btnEditar       = Helpers.HtmlHelper.Button.create("btnEditarEspecie_{0}".format(idEspecie), "Editar", "btnEditar");
            var btnEliminar     = Helpers.HtmlHelper.Button.create("btnEliminarEspecie_{0}".format(idEspecie), "Eliminar", "btnEliminar");
            var btnLocalizar    = Helpers.HtmlHelper.Button.create("btnLocalizarEspecie_{0}".format(idEspecie), "Localizar", "btnLocalizar");

            var fila  = '<td>' + btnEditar + btnEliminar + btnLocalizar + '</td>';
                fila += '<td>' + especieInfo.Nombre + '</td>';
                fila += '<td>' + especieInfo.NombreCientifico + '</td>';
                fila += '<td>' + especieInfo.FactorCO2 + '</td>';
                Helpers.HtmlHelper.Table.addNewRow(idTabla, fila, "idEspecie", idEspecie, insertFirst);

            var btnEditar = $("#btnEditarEspecie_{0}".format(idEspecie));
                btnEditar.click(function () { editarEspecie(idEspecie); });
                btnEditar.button();

            var btnEliminar = $("#btnEliminarEspecie_{0}".format(idEspecie));
                btnEliminar.click(function () { eliminarEspecie(idEspecie, especieInfo.Nombre) });
                btnEliminar.button();

            var btnLocalizar = $("#btnLocalizarEspecie_{0}".format(idEspecie));
                btnLocalizar.click(function () { editarEspecieLocalizaciones(idEspecie, especieInfo.Nombre) });
                btnLocalizar.button();

            actualizarTotalEspecies(+1);
        }
        function crearFilaEspecieLocalizacion(localizacionInfo, insertFirst)
        {
            var idLocalizacion = localizacionInfo.Id;

            var btnEditar   = Helpers.HtmlHelper.Button.create("btnEditarEspecieLocalizacion_{0}".format(idLocalizacion), "Editar", "btnEditar");
            var btnEliminar = Helpers.HtmlHelper.Button.create("btnEliminarEspecieLocalizacion_{0}".format(idLocalizacion), "Eliminar", "btnEliminar");

            var fila = '<td>' + btnEditar + btnEliminar + '</td>';
                fila += '<td>' + localizacionInfo.Pais + '</td>';
                fila += '<td>' + localizacionInfo.Nombre + '</td>';
            Helpers.HtmlHelper.Table.addNewRow(idTablaLocalizaciones, fila, "idLocalizacion", idLocalizacion, insertFirst);

            var btnEditar = $("#btnEditarEspecieLocalizacion_{0}".format(idLocalizacion));
                btnEditar.click(function () { editarEspecieLocalizacion(idLocalizacion); });
                btnEditar.button();

            var btnEliminar = $("#btnEliminarEspecieLocalizacion_{0}".format(idLocalizacion));
                btnEliminar.click(function () { eliminarEspecieLocalizacion(idLocalizacion) });
                btnEliminar.button();

            actualizarTotalEspecieLocalizaciones(+1);
        }
        function actualizarTotalEspecies(incremento)
        {
            totalEspecies = totalEspecies + incremento;
            $("#lblTotalEspecies").html("Total: <b>{0}</b> especie(s)".format(totalEspecies));
        }
        function actualizarTotalEspecieLocalizaciones(incremento)
        {
            totalEspecieLocalizaciones = totalEspecieLocalizaciones + incremento;
            $("#lblTotalEspecieLocalizaciones").html("Total: <b>{0}</b> localizacion(es)".format(totalEspecieLocalizaciones));
        }
        function actualizarFilaEspecie(especieInfo)
        {
            var fila = findElementByAttribute("tr", "idEspecie", especieInfo.Id);
            if (fila.length == 0)
                crearFilaEspecie(especieInfo, true);
            else
                $(fila.find("td")[1]).html(especieInfo.Nombre);
            fila = findElementByAttribute("tr", "idEspecie", especieInfo.Id);
            scrollTo(fila, 100, 1000);
        }
        function actualizarFilaEspecieLocalizacion(localizacionInfo)
        {
            var fila = findElementByAttribute("tr", "idLocalizacion", localizacionInfo.Id);
            if (fila.length == 0)
            {
                crearFilaEspecieLocalizacion(localizacionInfo, true);
            }
            else
            {
                $(fila.find("td")[1]).html(localizacionInfo.Pais);
                $(fila.find("td")[2]).html(localizacionInfo.Nombre);
            }
            fila = findElementByAttribute("tr", "idLocalizacion", localizacionInfo.Id);
        }
        function removeEstadoActivoBotones(idEspecie)
        {
            $("#btnEditarEspecie_{0}".format(idEspecie)).removeClass(UI_States.Focus);
            $("#btnEliminarEspecie_{0}".format(idEspecie)).removeClass(UI_States.Focus);
            $("#btnLocalizarEspecie_{0}".format(idEspecie)).removeClass(UI_States.Focus);
        }
        function removeEstadoActivoBotonesLocalizacion(idEspecieLocalizacion)
        {
            $("#btnEditarEspecieLocalizacion_{0}".format(idEspecieLocalizacion)).removeClass(UI_States.Focus);
            $("#btnEliminarEspecieLocalizacion_{0}".format(idEspecieLocalizacion)).removeClass(UI_States.Focus);
        }
    </script>

    <!-- Dialog EspecieInfo -->
    <script type="text/javascript" >
        function inicializarDialogEspecieInfo()
        {
            var especieInfoOptions =
            {
                title: "Especie",
                width: _dialogos.EspecieInfo.Width,
                height: _dialogos.EspecieInfo.Height,
                buttons:
                [
                    { id: "btnCancelEspecieInfoDialog", text: "Cancelar", click: cerrarEspecieInfoDialog },
                    { id: "btnConfirmEspecieInfoDialog", text: "Aceptar", click: guardarEspecie }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.EspecieInfo.Id, especieInfoOptions);

            $("#txtEspecieInfo_FactorCO2").spinner({ step: 0.01, min: 0, numberFormat: "n" });
        }
        function abrirEspecieInfoDialog(titulo)
        {
            Helpers.DialogHelper.openDialog(_dialogos.EspecieInfo.Id);
            Helpers.DialogHelper.setTitle(_dialogos.EspecieInfo.Id, titulo);
        }
        function cerrarEspecieInfoDialog()
        {
            Helpers.DialogHelper.closeDialog(_dialogos.EspecieInfo.Id);
        }
        function limpiarDatosEspecieInfoDialog()
        {
            $("#hdnEspecieInfo_Id").setValue(null);
            $("#txtEspecieInfo_Nombre").setValue(null);
            $("#txtEspecieInfo_NombreCientifico").setValue(null);
            $("#txtEspecieInfo_FactorCO2").setValue("0");

            desactivarMensajesDeError("#txtEspecieInfo_Nombre");
        }
    </script>
    <!-- Fin de [Dialog EspecieInfo] -->

    <!-- Dialog EspecieLocalizacionesInfo -->
    <script type="text/javascript" >
        function inicializarDialogEspecieLocalizacionesInfo()
        {
            var especieLocalizacionesInfoOptions =
            {
                title: "Localización Especie",
                width: _dialogos.EspecieLocalizacionesInfo.Width,
                height: _dialogos.EspecieLocalizacionesInfo.Height
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.EspecieLocalizacionesInfo.Id, especieLocalizacionesInfoOptions);
        }
        function abrirEspecieLocalizacionesInfoDialog(titulo)
        {
            Helpers.DialogHelper.openDialog(_dialogos.EspecieLocalizacionesInfo.Id);
            Helpers.DialogHelper.setTitle(_dialogos.EspecieLocalizacionesInfo.Id, titulo);
        }
        function limpiarDatosEspecieLocalizacionesInfoDialog()
        {
            totalEspecieLocalizaciones = 0;
            actualizarTotalEspecieLocalizaciones(0);
            $("#hdnEspecieLocalizacionesInfo_IdEspecie").setValue(null);
        }
    </script>
    <!-- Fin de [Dialog EspecieLocalizacionesInfo] -->

    <!-- Dialog EspecieLocalizacionInfo -->
    <script type="text/javascript" >
        function inicializarDialogEspecieLocalizacionInfo()
        {
            var especieLocalizacionInfoOptions =
            {
                title: "Localización especie",
                width: _dialogos.EspecieLocalizacionInfo.Width,
                height: _dialogos.EspecieLocalizacionInfo.Height,
                buttons:
                [
                    { id: "btnCancelEspecieLocalizacionInfoDialog", text: "Cancelar", click: cerrarEspecieLocalizacionInfoDialog },
                    { id: "btnConfirmEspecieLocalizacionInfoDialog", text: "Aceptar", click: guardarEspecieLocalizacion }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.EspecieLocalizacionInfo.Id, especieLocalizacionInfoOptions);
        }
        function abrirEspecieLocalizacionInfoDialog(titulo)
        {
            Helpers.DialogHelper.openDialog(_dialogos.EspecieLocalizacionInfo.Id);
            Helpers.DialogHelper.setTitle(_dialogos.EspecieLocalizacionInfo.Id, titulo);
        }
        function cerrarEspecieLocalizacionInfoDialog()
        {
            Helpers.DialogHelper.closeDialog(_dialogos.EspecieLocalizacionInfo.Id);
        }
        function limpiarDatosEspecieLocalizacionInfoDialog()
        {
            $("#hdnEspecieLocalizacionInfo_Id").setValue(null);
            $("#selEspecieLocalizacionInfo_Pais").setValue(null);
            $("#txtEspecieLocalizacionInfo_Nombre").setValue(null);
            desactivarMensajesDeError("#selEspecieLocalizacionInfo_Pais, #txtEspecieLocalizacionInfo_Nombre");
        }
    </script>
    <!-- Fin de [Dialog EspecieLocalizacionInfo] -->

    <!-- CRUD Especie -->
    <script type="text/javascript" >
        function obtenerListadoEspecies()
        {
            var params = new Object();
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoEspecies").fadeIn();
            Helpers.CRUDHelper.Get("obtenerListadoEspecies", jsonParams, obtenerListadoEspecies_Respuesta);
        }
        function obtenerListadoEspecies_Respuesta(responseInfo)
        {
            var especiesInfo = responseInfo.Valor;
            if (!isEmpty(especiesInfo))
            {
                Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
                for (var i = 0; i < especiesInfo.length; i++)
                {
                    crearFilaEspecie(especiesInfo[i]);
                }
            }
            $("#pnlCargandoEspecies").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function editarEspecie(idEspecie)
        {
            limpiarDatosEspecieInfoDialog();
            Helpers.CRUDHelper.Get("../Default.aspx/obtenerEspecie", "{'id':'{0}' }".format(idEspecie), editarEspecie_Response);
            abrirEspecieInfoDialog("Editar especie");
        }
        function editarEspecie_Response(responseInfo)
        {
            var especieInfo = responseInfo.Valor;
            if (!isEmpty(especieInfo))
            {
                $("#hdnEspecieInfo_Id").setValue(especieInfo.Id);
                $("#txtEspecieInfo_Nombre").setValue(especieInfo.Nombre);
                $("#txtEspecieInfo_NombreCientifico").setValue(especieInfo.NombreCientifico);
                $("#txtEspecieInfo_FactorCO2").setValue(especieInfo.FactorCO2);
                $("#chkEspecie_DesdeWiki").setValue(especieInfo.DesdeWiki);
                $("#chkEspecie_Aprobado").setValue(especieInfo.Aprobado);

                removeEstadoActivoBotones(especieInfo.Id);
            }
        }
        function guardarEspecie()
        {
            var infoValidacion =
                [
                    { campo: "txtEspecieInfo_Nombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre." }] },
                    //{ campo: "txtEspecieInfo_Nombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre científico." }] }
                    { campo: "txtEspecieInfo_FactorCO2", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un factor de CO<sub>2." }] }
                ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            cerrarEspecieInfoDialog();

            var id = $("#hdnEspecieInfo_Id").getValue();
            var especieInfo = new Object();
                especieInfo.id                  = isEmpty(id) ? -1 : id;
                especieInfo.nombre              = $("#txtEspecieInfo_Nombre").getValue();
                especieInfo.nombreCientifico    = $("#txtEspecieInfo_NombreCientifico").getValue();
                especieInfo.factorCO2           = $("#txtEspecieInfo_FactorCO2").getValue();
                especieInfo.aprobado            = $("#chkEspecie_Aprobado").getValue();
            var jsonParams = JSON.stringify(especieInfo);

            Helpers.CRUDHelper.Save("../Default.aspx/guardarEspecie", jsonParams, guardarEspecie_Response, null, "Guardar datos", "¿Desea guardar los datos?");
        }
        function guardarEspecie_Response(responseInfo)
        {
            var especieInfo = responseInfo.Valor;
            actualizarFilaEspecie(especieInfo);
            Helpers.NotificationHelper.showMessage(responseInfo.Mensaje);
        }
        function eliminarEspecie(idEspecie)
        {
            Helpers.CRUDHelper.Delete("../Default.aspx/eliminarEspecie", idEspecie, eliminarEspecie_Response, null, "Eliminar especie", "¿Desea eliminar esta especie?");
            removeEstadoActivoBotones(idEspecie);
        }
        function eliminarEspecie_Response(responseInfo)
        {
            var especieInfo = responseInfo.Valor;
            Helpers.HtmlHelper.Table.deleteRow(idTabla, "idEspecie", especieInfo.Id);
            actualizarTotalEspecies(-1);
            Helpers.NotificationHelper.showMessage(responseInfo.Mensaje);
        }
    </script>
    <!-- Fin de [CRUD] -->

    <!-- CRUD Especie Localizaciones -->
    <script type="text/javascript" >
        function editarEspecieLocalizaciones(idEspecie, especie)
        {
            limpiarDatosEspecieLocalizacionesInfoDialog();
            abrirEspecieLocalizacionesInfoDialog("Localización especie <u>{0}<u>".format(especie));
            obtenerListadoEspecieLocalizaciones(idEspecie);
        }
        function obtenerListadoEspecieLocalizaciones(idEspecie)
        {
            var params = new Object();
                params.idEspecie = idEspecie;
            var jsonParams = JSON.stringify(params);

            $("#hdnEspecieLocalizacionesInfo_IdEspecie").setValue(idEspecie);

            $("#pnlCargandoEspecieLocalizaciones").fadeIn();
            Helpers.CRUDHelper.Get("obtenerListadoEspecieLocalizaciones", jsonParams, obtenerListadoEspecieLocalizaciones_Respuesta);
        }
        function obtenerListadoEspecieLocalizaciones_Respuesta(responseInfo)
        {
            Helpers.HtmlHelper.Table.deleteAllRows(idTablaLocalizaciones);
            var localizacionesInfo = responseInfo.Valor;
            if (!isEmpty(localizacionesInfo))
            {
                for (var i = 0; i < localizacionesInfo.length; i++)
                {
                    crearFilaEspecieLocalizacion(localizacionesInfo[i]);
                }
            }
            $("#pnlCargandoEspecieLocalizaciones").fadeOut('400', function () { $("#" + idTablaLocalizaciones).slideDown() });
        }
    </script>
    <!-- Fin de [CRUD Especie Localizaciones] -->

    <!-- CRUD Especie Localización -->
    <script type="text/javascript" >
        function editarEspecieLocalizacion(idLocalizacion) {
            limpiarDatosEspecieLocalizacionInfoDialog();
            abrirEspecieLocalizacionInfoDialog("Editar localización");
            obtenerEspecieLocalizacion(idLocalizacion);
        }
        function obtenerEspecieLocalizacion(idLocalizacion) {
            var params = new Object();
                params.id = idLocalizacion;
            var jsonParams = JSON.stringify(params);

            Helpers.CRUDHelper.Get("obtenerEspecieLocalizacion", jsonParams, obtenerEspecieLocalizacion_Respuesta);
        }
        function obtenerEspecieLocalizacion_Respuesta(responseInfo) {
            var localizacionInfo = responseInfo.Valor;
            if (!isEmpty(localizacionInfo)) {
                $("#hdnEspecieLocalizacionInfo_Id").setValue(localizacionInfo.Id);
                $("#selEspecieLocalizacionInfo_Pais").setValue(localizacionInfo.IdPais);
                $("#txtEspecieLocalizacionInfo_Nombre").setValue(localizacionInfo.Nombre);
            }
        }
        function guardarEspecieLocalizacion() {
            var infoValidacion =
                [
                    { campo: "selEspecieLocalizacionInfo_Pais", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar un país." }] },
                    { campo: "txtEspecieLocalizacionInfo_Nombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre." }] }
                ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            cerrarEspecieLocalizacionInfoDialog();

            var id = $("#hdnEspecieLocalizacionInfo_Id").getValue();
            var localizacionInfo = new Object();
                localizacionInfo.id         = isEmpty(id) ? -1 : id;
                localizacionInfo.idEspecie  = $("#hdnEspecieLocalizacionesInfo_IdEspecie").getValue();
                localizacionInfo.idPais     = $("#selEspecieLocalizacionInfo_Pais").getValue();
                localizacionInfo.nombre     = $("#txtEspecieLocalizacionInfo_Nombre").getValue();
            var jsonParams = JSON.stringify(localizacionInfo);

            Helpers.CRUDHelper.Save("../Default.aspx/guardarEspecieLocalizacion", jsonParams, guardarEspecieLocalizacion_Response, null, "Guardar datos", "¿Desea guardar los datos?");
        }
        function guardarEspecieLocalizacion_Response(responseInfo) {
            var localizacionInfo = responseInfo.Valor;
            actualizarFilaEspecieLocalizacion(localizacionInfo);
            Helpers.NotificationHelper.showMessage(responseInfo.Mensaje);
        }
        function eliminarEspecieLocalizacion(idLocalizacion)
        {
            Helpers.CRUDHelper.Delete("../Default.aspx/eliminarEspecieLocalizacion", idLocalizacion, eliminarEspecieLocalizacion_Response, null, "Eliminar localización", "¿Desea eliminar esta localización?");
            removeEstadoActivoBotones(idLocalizacion);
        }
        function eliminarEspecieLocalizacion_Response(responseInfo) {
            var localizacionInfo = responseInfo.Valor;
            Helpers.HtmlHelper.Table.deleteRow(idTablaLocalizaciones, "idLocalizacion", localizacionInfo.Id);
            actualizarTotalEspecieLocalizaciones(-1);
            Helpers.NotificationHelper.showMessage(responseInfo.Mensaje);
        }
    </script>
    <!-- Fin de [CRUD Especie Localización] -->

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" >
                    Administración de Especies
                </div>
            </div>

            <input type="button" class="button btnAgregar" id="btnAgregarEspecie" value="Agregar" style="width:100px" />
            <label id="lblTotalEspecies" >Total 0 especies</label>

            <span id="pnlCargandoEspecies" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando especies.." />
                <label>Cargando..</label>
            </span>

            <table id="tblEspecies" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="36%" /> <!-- Acción -->
                    <col width="27%" /> <!-- Nombre -->
                    <col width="27%" /> <!-- Nombre Científico -->
                    <col width="10%" /> <!-- Factor CO2 -->
                </colgroup>
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>Nombre</th>
                        <th>Nombre Científico</th>
                        <th>Factor CO<sub>2</sub></th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>

        </div>

        <!-- Dialogo Especie Info -->
        <div id="dialogEspecieInfo" style="display:none" >
            <input type="hidden" id="hdnEspecieInfo_Id" />
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="35%" />
                    <col width="65%" />               
                </colgroup>
                <tr>
                    <td>
                        <label>Nombre:</label>
                    </td>
                    <td> 
                        <input id="txtEspecieInfo_Nombre" type="text" value="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Nombre científico:</label>
                    </td>
                    <td> 
                        <input id="txtEspecieInfo_NombreCientifico" type="text" value="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Factor CO<sub>2</sub></label>
                    </td>
                    <td>
                        <input id="txtEspecieInfo_FactorCO2" type="text" value="" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Desde Wiki</label>
                    </td>
                    <td> 
                        <input type="checkbox" id="chkEspecie_DesdeWiki" class="btn" checked="checked" disabled="disabled" />
                    </td>
                </tr>       
                <tr>
                    <td>
                        <label>Aprobado</label>
                    </td>
                    <td> 
                        <input type="checkbox" id="chkEspecie_Aprobado" class="btn" checked="checked" />
                    </td>
                </tr>           
            </table>
        </div>

        <!-- Dialogo Especie Localizaciones Info -->
        <div id="dialogEspecieLocalizacionesInfo" style="display:none" >

            <input type="hidden" id="hdnEspecieLocalizacionesInfo_IdEspecie" />

            <input type="button" class="button btnAgregar" id="btnAgregarEspecieLocalizacion" value="Agregar" style="width:100px" />
            <label id="lblTotalEspecieLocalizaciones" >Total 0 localizaciones</label>

            <span id="pnlCargandoEspecieLocalizaciones" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando localizaciones.." />
                <label>Cargando..</label>
            </span>

            <table id="tblEspecieLocalizaciones" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="30%" /> <!-- Acción -->
                    <col width="30%" /> <!-- País -->
                    <col width="30%" /> <!-- Nombre -->
                </colgroup>
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>País</th>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>
        </div>

        <!-- Dialogo Especie Localizacion Info -->
        <div id="dialogEspecieLocalizacionInfo" style="display:none" >
            <input type="hidden" id="hdnEspecieLocalizacionInfo_Id" />
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="25%" />
                    <col width="65%" />               
                </colgroup>
                <tr>
                    <td>
                        <label>País:</label>
                    </td>
                    <td> 
                        <select id="selEspecieLocalizacionInfo_Pais" ></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Nombre:</label>
                    </td>
                    <td> 
                        <input id="txtEspecieLocalizacionInfo_Nombre" type="text" value="" />
                    </td>
                </tr>                     
            </table>
        </div>

    </div>

</asp:Content>