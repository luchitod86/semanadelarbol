﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SemanaDelArbol.Helpers;
using Comun;
using Entidades.jSON.Sistema;
using Entidades;
using Controladores;

namespace SemanaDelArbol.Pages
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        private Noticia _noticia;
        private bool _noticiaLoaded = false;
        private string _serverURL = "http://www.semanadelarbol.org";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.GetLocalization();            
        }

        #region Methods

        private void GetLocalization()
        {
            var localizedStrings = Helpers.LocalizationHelper.GetLocalizedStrings();
            this.hdnLocalization.Value = Comun.Formateador.jSON.convertToJson(localizedStrings);
        }

        public string GetSocialTitle()
        {
            if (Request["idNoticia"] != null)
            {
                if (!_noticiaLoaded)
                {
                    try
                    {
                        var idNoticia = Convert.ToInt32(Request["idNoticia"]);
                        _noticia = new NoticiaControlador().ObtenerPorId(idNoticia);
                        _noticiaLoaded = true;
                        if (_noticia != null)
                            return String.Format("{0} - Semana del Árbol", _noticia.Titulo);
                    }
                    catch (FormatException e) {
                        _noticiaLoaded = true;
                        _noticia = null;
                    }
                    
                }
                else 
                {
                    if (_noticia != null)
                    {
                        return String.Format("{0} - Semana del Árbol", _noticia.Titulo);
                    }                
                }
            }
            return "Semana del Árbol";
        }

        public string GetSocialImage()
        {
            if (Request["idNoticia"] != null)
            {
                if (!_noticiaLoaded)
                {
                    var idNoticia = Convert.ToInt32(Request["idNoticia"]);
                    _noticia = new NoticiaControlador().ObtenerPorId(idNoticia);
                    _noticiaLoaded = true;
                    if (_noticia != null && _noticia.Imagenes.Count > 0)
                        return _serverURL + _noticia.Imagenes[0].URLImagen;
                }
                else
                {
                    if (_noticia != null && _noticia.Imagenes.Count > 0)
                    {
                        return _serverURL + _noticia.Imagenes[0].URLImagen;
                    }
                }
            }

            return "http://www.semanadelarbol.org/Images/Logo-fb.png";
        }

        public string GetSocialURL() {
            return Request.Url.AbsoluteUri;
        }

        public string GetSocialDescription()
        {
            if (Request["idNoticia"] != null)
            {
                if (!_noticiaLoaded)
                {
                    var idNoticia = Convert.ToInt32(Request["idNoticia"]);
                    _noticia = new NoticiaControlador().ObtenerPorId(idNoticia);
                    _noticiaLoaded = true;
                    if (_noticia != null )
                        return _noticia.Resumen;
                }
                else
                {
                    if (_noticia != null)
                    {
                        return _noticia.Resumen;;
                    }
                }
            }
            return "Del 24 al 31 de Agosto, participá activamente, y creémos entre todos la SEMANA DEL ÁRBOL. ¡Plantando, adoptando o donando árboles nativos en todo el país!";
        }

        #endregion

        #region Properties

        public string PaisActual(String pais)
        {
            if ( !String.IsNullOrEmpty(pais) )
                this.hdnPaisActual.Value = pais;
            return this.hdnPaisActual.Value;
        }

        public string PaisActual(int idPais)
        {
            if (idPais != -1)
                this.hdnPaisActualId.Value = idPais.ToString();
            return this.hdnPaisActualId.Value;
        }

        public string PeriodoActual(Nullable<Int32> periodo)
        {
            if ( periodo.HasValue )
                this.hdnPeriodoActual.Value = periodo.Value.ToString();
            return this.hdnPeriodoActual.Value;
        }

        public string UsuarioPerfiles(String usuarioPerfiles)
        {
            if (!String.IsNullOrEmpty(usuarioPerfiles))
                this.hdnUsuarioPerfiles.Value = usuarioPerfiles;
            return this.hdnUsuarioPerfiles.Value;
        }

        public string UsuarioTipos(String usuarioTipos)
        {
            if (!String.IsNullOrEmpty(usuarioTipos))
                this.hdnUsuarioTipos.Value = usuarioTipos;
            return this.hdnUsuarioTipos.Value;
        }

        public string SponsorsTipos(String sponsorsTipos)
        {
            if (!String.IsNullOrEmpty(sponsorsTipos))
                this.hdnSponsorsTipos.Value = sponsorsTipos;
            return this.hdnSponsorsTipos.Value;
        }


        public string PreferenciasUsuario(String preferenciasUsuario)
        {
            if (!String.IsNullOrEmpty(preferenciasUsuario))
                this.hdnPreferenciasUsuario.Value = preferenciasUsuario;
            return this.hdnPreferenciasUsuario.Value;
        }
        #endregion

    }
}