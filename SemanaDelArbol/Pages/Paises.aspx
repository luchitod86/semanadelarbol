﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Paises.aspx.cs" Inherits="SemanaDelArbol.Pages.Paises" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idTabla = "tblPaises";
        $(document).ready(function ()
        {
            obtenerUsuarioLogueado(cargarPagina_Paises);
        });
        function cargarPagina_Paises()
        {
            $("#btnGuardarPaises").click(function () { guardarPaises(); });

            $("#chkPaises_CheckAll").change(function ()
            {
                var checked = $(this).getValue();
                $(".chkPaises_Admin").setValue(checked);
            });

            obtenerPaises_Admin();
        }
        function obtenerPaises_Admin()
        {
            var params = new Object();
                params.incluirInactivos = true;
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoPaises").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerPaises", jsonParams, obtenerPaises_Admin_Respuesta);
        }
        function obtenerPaises_Admin_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var paisesInfo = respuestaInfo.Valor;
                cargarPaises(paisesInfo);
            }
            $("#pnlCargandoPaises").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function cargarPaises(paisesInfo)
        {
            Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
            for (var i = 0; i < paisesInfo.length; i++)
            {
                crearFilaPaises( paisesInfo[i] );
            }
            var botonesAdminProvincias = $(".btnAdministrarProvincias");
                botonesAdminProvincias.button();
                botonesAdminProvincias.click
                    (
                        function ()
                        {
                            var idPais = $(this).attr("idPais");
                            abrirPagina(_secciones.Provincias, "idPais=" + idPais);
                        }
                    );
        }
        function crearFilaPaises(paisInfo)
        {
            var checked = paisInfo.Activo ? ' checked="checked" ' : '';
            var fila  = '<td><input type="checkbox"' + checked + ' class="chkPaises_Admin" /></td>';
                fila += '<td><img src="../Images/Banderas/' + paisInfo.Continente + '/32x32/' + paisInfo.Bandera + '" width="16" height="16" />&nbsp;' + paisInfo.Nombre + '</td>';
                fila += '<td><input type="button" idPais="' + paisInfo.Id + '" class="button btnEditar btnAdministrarProvincias" value="Administrar" style="width:120px"  ></td>';

                Helpers.HtmlHelper.Table.addNewRow(idTabla, fila, "idPais", paisInfo.Id);
        }
        function guardarPaises()
        {
            Helpers.showLoadingMessage();

            var paises = new Array();
            var filas = Helpers.HtmlHelper.Table.getAllRows(idTabla);
            for (var i = 0; i < filas.length; i++)
            {
                var fila   = $(filas[i]);
                var idPais = fila.attr("idPais");
                var activo = $(fila.find("td")[0]).find("input").getValue();

                var pais        = new Object();
                    pais.Id     = idPais;
                    pais.Activo = activo;

                paises.push( pais );
            }

            Helpers.AJAXHelper.doAjaxCall("guardarPaises", "{'paisInfoItems':'" + JSON.stringify(paises) + "'}", guardarPaises_Respuesta);
        }
        function guardarPaises_Respuesta(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                //var respuestaInfo = respuestaInfo.Valor;
            }
        }
    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" >
                    Administración de Países
                </div>
            </div>

            <input type="button" class="button btnEditar" id="btnGuardarPaises" value="Actualizar" style="width:105px" />
            <span id="pnlCargandoPaises" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando países.." />
                <label>Cargando..</label>
            </span>

            <table id="tblPaises" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="5%" /> <!-- Activo -->
                    <col width="50%" /> <!-- País -->
                    <col width="45%" /> <!-- País -->
                </colgroup>
                <thead>
                    <tr>
                        <th style="padding-left: 4px;"><input id="chkPaises_CheckAll" type="checkbox" /></th>
                        <th>País</th>
                        <th>Provincias</th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>

        </div>

    </div>

</asp:Content>