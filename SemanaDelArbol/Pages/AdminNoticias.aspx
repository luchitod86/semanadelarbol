﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master"  CodeBehind="AdminNoticias.aspx.cs" Inherits="SemanaDelArbol.Pages.AdminNoticias" %>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idTabla = "tblNoticias";
        var totalNoticias = 0;
        var paginaActual = 1;
        var cantidadNoticias = 10;
        $(document).ready(function ()
        {
            paginaActual = getQuerystring("PaginaActual") != '' ? getQuerystring("PaginaActual") : 1;            
            obtenerUsuarioLogueado(cargarPagina_Noticias);
        });
        function cargarPagina_Noticias()
        {
            obtenerNoticias_Admin();

            $("#btnAgregarNoticia").click(function () {
                abrirPagina(_secciones.AgregarNoticia);
            });
        }
        function obtenerNoticias_Admin()
        {
            var params = new Object();
                params.pais = _filtroActual.Pais.Nombre;
                params.pagina = paginaActual,
                params.cantidad = cantidadNoticias
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoNoticias").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerNoticias_Paginado", jsonParams, obtenerNoticias_Admin_Respuesta);
            Helpers.AJAXHelper.doAjaxCall("obtenerTotalNoticias", jsonParams, actualizarTotalNoticias);
        }
        function obtenerNoticias_Admin_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var noticiasInfo = respuestaInfo.Valor;
                cargarNoticias(noticiasInfo);
            }
            $("#pnlCargandoNoticias").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function cargarNoticias(noticiasInfo)
        {
            Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
            for (var i = 0; i < noticiasInfo.length; i++)
            {
                crearFilaNoticias(noticiasInfo[i]);
            }            
        }
        function crearFilaNoticias(noticiaInfo) {
            var idNoticia = noticiaInfo.Id;

            var btnEditar   = Helpers.HtmlHelper.Button.create("btnEditarNoticia_{0}".format(idNoticia), "Editar", "btnEditar");
            var btnEliminar = Helpers.HtmlHelper.Button.create("btnEliminarNoticia_{0}".format(idNoticia), "Eliminar", "btnEliminar");

            var fila  = '<td>' + btnEditar + btnEliminar + '</td>';
                fila += '<td>' + noticiaInfo.Titulo + '</td>';
                fila += '<td>' + noticiaInfo.Fecha + '</td>';
            Helpers.HtmlHelper.Table.addNewRow(idTabla, fila, "idNoticia", idNoticia, false);

            var btnEditar = $("#btnEditarNoticia_{0}".format(idNoticia));
                btnEditar.click(function () { editarNoticia(idNoticia); });
                btnEditar.button();

            var btnEliminar = $("#btnEliminarNoticia_{0}".format(idNoticia));            
                btnEliminar.click(function () { eliminarNoticia(idNoticia) });
                btnEliminar.button();
        }
        function editarNoticia(idNoticia)
        {
            abrirPagina(_secciones.DetalleNoticia, "idNoticia={0}".format(idNoticia)); noticias
        }

        function eliminarNoticia(idNoticia) {
            Helpers.CRUDHelper.Delete("../Default.aspx/eliminarNoticia", idNoticia, eliminarNoticia_Response, null, "Eliminar noticia", "¿Desea eliminar esta noticia?");
            removeEstadoActivoBotones(idNoticia);
        }
        function eliminarNoticia_Response(responseInfo) {
            var noticiaInfo = responseInfo.Valor;
            Helpers.HtmlHelper.Table.deleteRow(idTabla, "idNoticia", noticiaInfo.Id);
            actualizarTotalNoticiasIncremento(-1);
            Helpers.NotificationHelper.showMessage(responseInfo.Mensaje);
        }

        function actualizarTotalNoticiasIncremento(incremento) {
            totalNoticias = totalNoticias + incremento;
            var totalPaginas = ~~(totalNoticias / cantidadNoticias);            
            if ((totalNoticias % cantidadNoticias) != 0)
                totalPaginas++;

            if (totalPaginas > 1)
                Helpers.HtmlHelper.Table.crearPaginador(paginaActual, totalPaginas, 'paginador', 'AdminNoticias.aspx');
            $("#lblTotalNoticias").html("Total: <b>{0}</b> noticia(s)".format(totalNoticias));
        }

        function actualizarTotalNoticias(datos) {
            totalNoticias = jQuery.parseJSON(datos.d).Valor;
            var totalPaginas = ~~(totalNoticias / cantidadNoticias);
            if ((totalNoticias % cantidadNoticias) != 0)
                totalPaginas++;
            if(totalPaginas > 1)
                Helpers.HtmlHelper.Table.crearPaginador(paginaActual, totalPaginas, 'paginador', 'AdminNoticias.aspx');
            $("#lblTotalNoticias").html("Total: <b>{0}</b> noticias(s) en <b>{1}</b>".format(totalNoticias, _filtroActual.Pais.Nombre));
        }     
    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" >
                    Administración de Noticias
                </div>
            </div>

            <input type="button" class="button btnAgregar" id="btnAgregarNoticia" value="Agregar" style="width:100px" />
            <label id="lblTotalNoticias" >Total 0 noticias</label>

            <span id="pnlCargandoNoticias" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando noticias.." />
                <label>Cargando..</label>
            </span>

            <table id="tblNoticias" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="25%" /> <!-- Acción -->
                    <col width="50%" /> <!-- Titulo -->
                    <col width="25%" /> <!-- Fecha -->
                </colgroup>
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>Titulo</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>

            <div id="paginador"></div>
        </div>

    </div>

</asp:Content>
