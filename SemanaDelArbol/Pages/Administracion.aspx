﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Administracion.aspx.cs" Inherits="SemanaDelArbol.Pages.Administracion" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
    
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Administracion);
        });
        function cargarPagina_Administracion()
        {
            $("#btnAdministrarNoticias").click(function () { abrirPagina(_secciones.AdminNoticias); });
            $("#btnAdministrarSponsors").click(function () { abrirPagina(_secciones.Sponsors); });
            $("#btnAdministrarViveros").click(function () { abrirPagina(_secciones.Viveros); });
            $("#btnAdministrarPaises").click(function () { abrirPagina(_secciones.Paises); });
            $("#btnReportes").click(function () { abrirPagina(_secciones.Reportes); });
            $("#btnConfigurarEmail").click(function () { abrirPagina(_secciones.ConfiguracionEmail); });
            $("#btnMetaTags").click(function () { abrirPagina(_secciones.MetaTags); });
            $("#btnEspecies").click(function () { abrirPagina(_secciones.Especies); });
            $("#btnUsuarios").click(function () { abrirPagina(_secciones.Usuarios); });
            $("#btnLocalizacion").click(function () { abrirPagina(_secciones.Localizacion); });            

            $("#tblAdministracion").slideDown();
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" >
                    Panel de Administración
                </div>
            </div>

            <table id="tblAdministracion" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="20%" /> <!-- Sección -->
                    <col width="62%" /> <!-- Descripción -->
                    <col width="18%" /> <!-- Acción -->
                </colgroup>
                <thead>
                    <tr>
                        <th style="font-weight:bold" >Sección</th>
                        <th style="font-weight:bold" >Descripción</th>
                        <th style="font-weight:bold" >Acción</th>
                    </tr>
                </thead>
                <tbody>
                     <tr>
                        <td>
                            Noticias
                        </td>
                        <td>
                            Últimas noticias vinculadas al evento y a diferentes eventos de Agua y Juventud Argentina.
                        </td>
                        <td>
                            <input id="btnAdministrarNoticias" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sponsors
                        </td>
                        <td>
                            Entidades que organizan, apoyan y acompañan la campaña.
                        </td>
                        <td>
                            <input id="btnAdministrarSponsors" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Viveros
                        </td>
                        <td>
                            Usuarios del sistema que se publican como viveros.
                        </td>
                        <td>
                            <input id="btnAdministrarViveros" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Países
                        </td>
                        <td>
                            Países registrados en el sistema (activos e inactivos), y sus Provincias asociadas.
                        </td>
                        <td>
                            <input id="btnAdministrarPaises" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Especies
                        </td>
                        <td>
                            Especies de árboles registradas en el sistema.
                        </td>
                        <td>
                            <input id="btnEspecies" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Usuarios
                        </td>
                        <td>
                            Usuarios registrados en el sistema
                        </td>
                        <td>
                            <input id="btnUsuarios" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Meta-Tags
                        </td>
                        <td>
                            Información sobre las páginas del sitio, destinada para el indexado de los buscadores.
                        </td>
                        <td>
                            <input id="btnMetaTags" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td>
                            Localización
                        </td>
                        <td>
                            Traducciones de textos parametrizables del sistema.
                        </td>
                        <td>
                            <input id="btnLocalizacion" type="button" class="button btnEditar centrado" value="Administrar" style="width:140px" />
                        </td>
                    </tr>
                    -->
                    <tr>
                        <td>
                            Reportes
                        </td>
                        <td>
                            Reportes dinámicos generados con la base de datos del sistema.
                        </td>
                        <td>
                            <input id="btnReportes" type="button" class="button btnReportes centrado" value="Generar" style="width:140px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email
                        </td>
                        <td>
                            Configuración de casilla de e-mail del sistema.
                        </td>
                        <td>
                            <input id="btnConfigurarEmail" type="button" class="button btnEditar centrado" value="Configurar" style="width:140px" />
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>

</asp:Content>