﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Donar.aspx.cs" Inherits="SemanaDelArbol.Pages.Donar" %>

<%@ Register Src="~/CustomControls/MisDatos.ascx" TagName="MisDatos" TagPrefix="MisDatos" %>
<%@ Register Src="~/CustomControls/Mapa.ascx" TagName="Mapa" TagPrefix="Mapa" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >
    
    <!-- Styles -->
    <style type="text/css">
        #pnlDonaciones
        {
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Donar);
        });
        function cargarPagina_Donar()
        {
            var configMapa =
                {
                    IdMapa: "mapa",
                    TituloMapa: "Ubicación del donante",
                    TituloBuscador: "", //"Localice en el mapa la ubicación aproximada",
                    MostrarMapa: true,
                    MostrarBuscador: true,
                    MostrarFiltroMarcadores: false,
                    MostrarListaMarcadores: false,
                    MostrarBotonMiPosicionActual: true,
                    OnMapLoadComplete: onMapLoadComplete_Donar,
                    Zoom: _googleMap.NivelZoom,
                    Direccion: _filtroActual.Pais.Nombre,
                    ImagenMarcador: _googleMap.Marcadores[_googleMap.TiposMarcador.Donante].Icono,
                    DeshabilitarMarcadores: false,
                    OnClickMap: onClickToMap_Donar,
                    OnBuscarDireccion: null
                };
            inicializarMapa(configMapa);
            inicializarDialogoDonacionInfo();
            inicializarDialogoUbicacionDonacionInfo();
            obtenerEspecies("selDonacionInfoEspecie");
            configurarMenuMisDatos("Puede ubicar la zona de <b>donación</b> ingresando una dirección, y luego navegar el mapa hasta encontrar el punto exacto que está buscando.");
        }
        function onMapLoadComplete_Donar(idMapa)
        {
            if ( verificarUsuarioLogueado() )
            {
                verificarDatosUbicacionDonante(_usuarioLogueado.Latitud, _usuarioLogueado.Longitud);
                obtenerDonacionesUsuario(_usuarioLogueado.Id, "tblDonaciones", null, onObtenerDonacionesUsuario);
            }
        }
        function agregarMarcadorDonante(latitud, longitud)
        {
            var onUserClick = 'javascript:verInfoUsuario(' + _usuarioLogueado.Id + ')';

            var burbuja  = _googleMap.Marcadores[_googleMap.TiposMarcador.Donante].Burbuja;
            var ancho    = burbuja.Width;
            var alto     = burbuja.Height;
                alto    += 25;

            var map      = Helpers.GoogleMapHelper.getMap();
            var position = Helpers.GoogleMapHelper.createPosition(latitud, longitud);
            var title    = "Click para más info.";

            var content = 'Esta es su <b>ubicación actual</b>.';
                content += '<br /><br />',
                content += 'Si desea <b>modificarla</b>, haga <b>click en el mapa</b> sobre la posición que corresponda.';

            content = crearContenedorBurbujaMarcador(content, alto, ancho);

            var idMarker = _usuarioLogueado.Id;

            //elimino el marcador de la ubicación previa (en caso de existir)..
            Helpers.GoogleMapHelper.removeMarker( String.format("{0}_{1}", _googleMap.TiposMarcador.Donante, idMarker), true );
            //agrego la ubicación en el mapa..

            //var atributoMarcadorDonante = { Name: "DoNotRemove", Value: idMarker };
            //Helpers.GoogleMapHelper.addMarker(map, position, idMarker, _googleMap.TiposMarcador.Donante, _googleMap.Marcadores[_googleMap.TiposMarcador.Donante].Icono, title, false, "Mi ubicación", content, null, null, atributoMarcadorDonante);
            Helpers.GoogleMapHelper.addMarker(map, position, idMarker, _googleMap.TiposMarcador.Donante, _googleMap.Marcadores[_googleMap.TiposMarcador.Donante].Icono, title, false, "Mi ubicación", content, null, null);
        }
        function verificarDatosUbicacionDonante(latitud, longitud)
        {
            if (latitud != 0 && longitud != 0)
            {
                $("#notificacionPosicionFaltante").hide();
                agregarMarcadorDonante(latitud, longitud);
            }
            else
            {
                $("#notificacionPosicionFaltante").show();
            }
        }
        function onObtenerDonacionesUsuario(donacionesInfo)
        {
            _misDonaciones = donacionesInfo;
            if ( isEmpty(_misDonaciones) || _misDonaciones.length == 0 )
                $("#notificacionMinimoDonaciones").fadeIn();
        }
        function verAyudaDonacion()
        {
            var colapsado = $("#ayudaDonacion").hasClass("colapsado");
            if ( colapsado )
            {
                $("#ayudaDonacion").removeClass("colapsado");
                $("#imgAyudaDonacion").attr("src", "../Images/Comun/16x16/Colapsar.png");
                $("#ayudaDonacion").slideDown();
            }
            else
            {
                $("#ayudaDonacion").addClass("colapsado");
                $("#imgAyudaDonacion").attr("src", "../Images/Comun/16x16/Expandir.png");
                $("#ayudaDonacion").slideUp();
            }
        }
        function onClickToMap_Donar(map, position, icon, title, idMarker)
        {
            //abrirUbicacionDonacionInfoDialog();
            $("#hdnUbicacionDonacionInfoLatitud").val(position.lat());
            $("#hdnUbicacionDonacionInfoLongitud").val(position.lng());

            latLng = String.format("{0}, {1}", position.lat(), position.lng());
            Helpers.GoogleMapHelper.getPositionFromLatLng(latLng, "mapa", onGetPositionFromLatLng_Donar_Response);
        }
        function onGetPositionFromLatLng_Donar_Response(address_components)
        {
            var direccion = parseAddressComponents(address_components);
            var pais = direccion.pais;

            abrirUbicacionDonacionInfoDialog();

            mostrarPanelAdvertenciaUbicacionDistinta(_dialogos.UbicacionDonacionInfo.Id, pais);

            var dialogo_UbicacionDonacionInfo_Height = $("#" + _dialogos.UbicacionDonacionInfo.Id).parent().outerHeight();
            $("#pnlUbicacionDonacion_AdvertenciaMinimoDonaciones").hide();
            if (_misDonaciones.length == 0)
            {
                $("#pnlUbicacionDonacion_AdvertenciaMinimoDonaciones").show();
                dialogo_UbicacionDonacionInfo_Height += 60;
            }
            Helpers.DialogHelper.resizeDialog(_dialogos.UbicacionDonacionInfo.Id, null, dialogo_UbicacionDonacionInfo_Height);
        }
        function cancelarUbicacionDonacionInfo()
        {
            cerrarUbicacionDonacionInfoDialog();
        }
        function guardarUbicacionDonacionInfo()
        {
            cerrarUbicacionDonacionInfoDialog();

            var ubicacionInfo = new Object();
                ubicacionInfo.Latitud = $("#hdnUbicacionDonacionInfoLatitud").val();
                ubicacionInfo.Longitud = $("#hdnUbicacionDonacionInfoLongitud").val();

            agregarUbicacionDonacionInfo(ubicacionInfo.Latitud, ubicacionInfo.Longitud);
        }
        function agregarUbicacionDonacionInfo(latitud, longitud)
        {
            if (latitud != 0 && longitud != 0) {
                $("#notificacionPosicionFaltante").hide();
            }
            agregarMarcadorDonante(latitud, longitud);
            guardarUbicacionDonante(latitud, longitud);
        }
        function guardarUbicacionDonante(latitud, longitud)
        {
            var ubicacion = new Object();
                ubicacion.idUsuario = _usuarioLogueado.Id;
                ubicacion.latitud = latitud;
                ubicacion.longitud = longitud;

            var parametros = JSON.stringify(ubicacion);
            var url = "guardarUbicacionDonante";
            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall(url, parametros, guardarUbicacionDonante_Response);
        }
        function guardarUbicacionDonante_Response(datos)
        {
            Helpers.hideLoadingMessage();
            var respuesta = jQuery.parseJSON(datos.d);
            if (respuesta.Resultado)
                Helpers.NotificationHelper.showMessage("Su ubicación fue guardada exitosamente.");
        }
    </script>

    <!-- Guardar Donaciones -->
    <script type="text/javascript">
        function cantidadDonaciones_Change()
        {
            var cantidad = $("#txtDonacionInfoCantidad").val();
            var adoptados = $("#txtDonacionInfoAdoptados").val();
            var disponibles = 0;

            cantidad = (isEmpty(cantidad) ? 0 : cantidad);
            adoptados = (isEmpty(adoptados) ? 0 : adoptados);

            if (!isNaN(cantidad))
                disponibles = (cantidad - adoptados);
            $("#txtDonacionInfoDisponibles").val(disponibles);
        }
        function cancelarDonacionInfo()
        {
            cerrarDonacionInfoDialog();
        }
        function guardarDonacionInfo()
        {
            var infoValidacion =
                [
                    { campo: "selDonacionInfoPais", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar un país." }] },
                    { campo: "selDonacionInfoEspecie", seleccion: "#selDonacionInfoEspecie + button", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar una especie." }] },
                    { campo: "txtDonacionInfoCantidad", validaciones: [{ operacion: _validaciones.Valor.MayorA, valor: 0, mensaje: "Debe ingresar una cantidad mayor a cero." }] }
                ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            cerrarDonacionInfoDialog();

            var donacionInfo = new Object();
                //donacionInfo.Id           = $("#hdnDonacionInfoId").val();
                donacionInfo.Id = -1;
                donacionInfo.IdDonante = _usuarioLogueado.Id;
                donacionInfo.Donante = _usuarioLogueado.NombreUsuario;
                donacionInfo.IdEspecie = Helpers.MultiSelectHelper.getFirstSelectedValue("selDonacionInfoEspecie");
                donacionInfo.Especie = Helpers.MultiSelectHelper.getFirstSelectedText("selDonacionInfoEspecie");
                donacionInfo.Cantidad = $("#txtDonacionInfoCantidad").val();
                donacionInfo.Adoptados = $("#txtDonacionInfoAdoptados").val();
                donacionInfo.Disponibles = $("#txtDonacionInfoDisponibles").val();
                donacionInfo.IdPais = Helpers.SelectListHelper.getSelectedValueFromSelectList("selDonacionInfoPais");

            var parametros = JSON.stringify(donacionInfo);
            var url = "guardarDonacion";

            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall(url, parametros, guardarDonacionInfo_Response);
        }
        function guardarDonacionInfo_Response(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var donacionInfo = respuestaInfo.Valor;
                agregarDonacionInfo(donacionInfo, "tblDonaciones", null);
                generarContenidoSocial_Donacion(donacionInfo);
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, "", "", null, true);
            }
            else {
                Helpers.NotificationHelper.showWarning(respuestaInfo.Mensaje, '');
            }
        }
    </script>

    <!-- Diálogos -->
    <script type="text/javascript" >
        function inicializarDialogoDonacionInfo()
        {
            var donacionInfoOptions =
            {
                title: "Donación",
                width: _dialogos.DonacionInfo.Width,
                height: _dialogos.DonacionInfo.Height,
                buttons:
                [
                    { id: "btnCancelDonacionInfoDialog", text: "Cancelar", click: cancelarDonacionInfo },
                    { id: "btnConfirmDonacionInfoDialog", text: "Aceptar", click: guardarDonacionInfo }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.DonacionInfo.Id, donacionInfoOptions);
        }
        function abrirDonacionInfoDialog()
        {
          if (_usuarioLogueado.Latitud != 0 && _usuarioLogueado.Longitud != 0){
                limpiarDatosDonacionInfoDialog();
                Helpers.SelectListHelper.setSelectedValue("selDonacionInfoPais", _filtroActual.Pais.Id);
                Helpers.DialogHelper.openDialog( _dialogos.DonacionInfo.Id );
            }
            else{
                Helpers.NotificationHelper.showWarning("Debe cargar su ubicación.", "Atención");
            }
        }
        function cerrarDonacionInfoDialog()
        {
            Helpers.DialogHelper.closeDialog( _dialogos.DonacionInfo.Id );
        }
        function limpiarDatosDonacionInfoDialog()
        {
            Helpers.SelectListHelper.setSelectedIndex("selDonacionInfoPais", -1);
            Helpers.MultiSelectHelper.setAll( "selDonacionInfoEspecie", false);
            $("#txtDonacionInfoCantidad").val("0");
            $("#txtDonacionInfoAdoptados").val("0");
            $("#txtDonacionInfoDisponibles").val("0");

            desactivarMensajesDeError("#selDonacionInfoPais, #selDonacionInfoEspecie + button, #txtDonacionInfoCantidad");
        }
        function inicializarDialogoUbicacionDonacionInfo()
        {
            var ubicacionDonacionInfoOptions =
            {
                title: "Ubicación geográfica del donante",
                width: _dialogos.UbicacionDonacionInfo.Width,
                height: _dialogos.UbicacionDonacionInfo.Height,
                buttons:
                [
                    { id: "btnCancelUbicacionDonacionInfoDialog", text: "Cancelar", click: cancelarUbicacionDonacionInfo },
                    { id: "btnConfirmUbicacionDonacionInfoDialog", text: "Aceptar", click: guardarUbicacionDonacionInfo }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.UbicacionDonacionInfo.Id, ubicacionDonacionInfoOptions);
        }
        function abrirUbicacionDonacionInfoDialog()
        {
            //limpiarDatosUbicacionDonacionInfoDialog();
            Helpers.DialogHelper.openDialog(_dialogos.UbicacionDonacionInfo.Id);
        }
        function cerrarUbicacionDonacionInfoDialog()
        {
            Helpers.DialogHelper.closeDialog(_dialogos.UbicacionDonacionInfo.Id);
        }
        function limpiarDatosUbicacionDonacionInfoDialog()
        {
            $("#hdnUbicacionDonacionInfoLatitud").val("");
            $("#hdnUbicacionDonacionInfoLongitud").val("");
        }
    </script>
    <!-- Fin de [Diálogos] -->

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <!-- Notificacion -->
            <div id="pnlNotificacion" style="padding:5px 0px 5px 0px;" >

                <div id="notificacionDonacion" class="ui-state-highlight" >
                    <div onclick="javascript:verAyudaDonacion();" class="link" >
                        <img id="imgAyudaDonacion" src="../Images/Comun/16x16/Colapsar.png" alt="Mostrar más" class="link" />¿Cómo realizar donaciones de árboles?
                    </div>
                </div>
                <div id="ayudaDonacion" class="panelAyuda" >
                    <label><b><u>Pasos</u></b></label>
                    <ol>
                        <li>Llene los campos y marque su <b>posición</b> como donante en el mapa.</li>
                        <li>Indique la <b>especie</b> del árbol y la <b>cantidad</b> disponible.</li>
                    </ol>
                    <div style="padding: 5px 0px 5px 0px" >
                        <label>Recuerde que se pueden realizar varias donaciones, y ¡<u>no olvide guardar los datos</u>!</label>
                    </div>
                </div>
            </div>

            <div id="pnlUbicacionDonante" >

                <!-- Mapa -->
                <div>
                    <Mapa:Mapa ID="Mapa" runat="server" />
                </div>
                <!-- Fin de [Mapa] -->
  
            </div>

            <!-- Donaciones -->
            <div id="pnlDonaciones" >
                <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                    <legend class="ui-widget-header" align="center" >Mis donaciones</legend>
                    <table id="tblDonaciones" class="tblInteractiva" border="0" cellspacing="0" cellpadding="0" width="100%" >
                        <colgroup>
                            <col width="25%" />
                            <col width="25%" />               
                            <col width="25%" />
                            <col width="25%" />
                        </colgroup>
                        <thead>
                            <tr class="rowHeader" >
                                <th class="invisible" >
                                    <label>Id</label>
                                </th>
                                <th class="invisible" >
                                    <label>IdEspecie</label>
                                </th>
                                <th>
                                    <label>Especie</label>
                                </th>
                                <th>
                                    <label>Cantidad</label>
                                </th>
                                <th>
                                    <label>Adoptados</label>
                                </th>
                                <th>
                                    <label>Disponibles</label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div class="rightCorner" >
                        <input type="button" class="button btnAgregar" value="Agregar" onclick="javascript:abrirDonacionInfoDialog();" style="width:100px" />
                    </div>
                </fieldset>
            </div>
            
            <div id="notificacionPosicionFaltante" class="ui-state-error" style="padding:5px !important; margin:2px 0px 5px 0px; display:none;" >
                    <div onclick="javascript:void(0);" >
                        <img id="imgDatosFaltantes" src="../Images/Comun/16x16/Advertencia.png" alt="Atención" />
                        <u><label style="vertical-align:middle" >Atención</label></u>:&nbsp;<label style="vertical-align:middle" >Aún no indicó su posición en el mapa</label>
                    </div>
            </div>
            <div id="notificacionMinimoDonaciones" class="ui-state-error" style="padding:5px !important; margin:2px 0px 5px 0px; display:none;" >
                    <div onclick="javascript:void(0);" >
                        <img id="imgMinimoDonaciones" src="../Images/Comun/16x16/Advertencia.png" alt="Atención" />
                        <u><label style="vertical-align:middle" >Advertencia</label></u>:&nbsp;<label style="vertical-align:middle" >Su donación/vivero no aparecerá en el mapa hasta que no disponga de al menos 1 árbol en donación.</label>
                    </div>
            </div>
            

        </div>

        <!-- Donacion Info -->
        <div id="dialogDonacionInfo" style="display:none" >
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="35%" /> 
                    <col width="65%" />                
                </colgroup>
                <tr>
                    <td>
                        <label>País:</label>&nbsp;
                        <label id="lblDonacionInfoAdvertencia" class="tooltipLeft ui-state-highlight" title="Asegúrese de seleccionar el país en donde se realiza la donación." style="float:right; margin-right:5px; height:14px; cursor:pointer;" >*</label>
                    </td>
                    <td>
                        <select id="selDonacionInfoPais" ></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Especie:</label>
                    </td>
                    <td>
                        <select id="selDonacionInfoEspecie" ></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Cantidad:</label>
                    </td>
                    <td>
                        <input id="txtDonacionInfoCantidad" type="text" style="width:50px" value="" onchange="javascript:cantidadDonaciones_Change();" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Adoptados:</label>
                    </td>
                    <td>
                        <input id="txtDonacionInfoAdoptados" type="text" style="width:50px" value="" disabled="disabled" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Disponibles:</label>
                    </td>
                    <td>
                        <input id="txtDonacionInfoDisponibles" type="text" style="width:50px" value="" disabled="disabled" readonly="readonly" />
                    </td>
                </tr>
            </table>
        </div>

        <!-- Ubicacion Donacion Info -->
        <div id="dialogUbicacionDonacionInfo" style="display:none" >
            <div id="pnlUbicacionDonacion_AdvertenciaMinimoDonaciones" class="ui-state-error" style="display:none; margin: 2px 0px 5px; padding: 5px !important;" >
                <img src="../Images/Comun/16x16/Advertencia.png" alt="Atención" />
                <u><label style="vertical-align:middle">Advertencia</label></u>:&nbsp;<label id="lblAdvertenciaMinimoDonaciones" style="vertical-align:middle">Su donación/vivero no aparecerá en el mapa hasta que no disponga de al menos 1 árbol en donación.</label>
            </div>
            <input type="hidden" id="hdnUbicacionDonacionInfoLatitud" />
            <input type="hidden" id="hdnUbicacionDonacionInfoLongitud" />
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="35%" /> 
                    <col width="65%" />                
                </colgroup>
                <tr>
                    <td colspan="2" >
                        <label>¿Desea guardar esta posición como su ubicación actual?</label>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <!-- Menú Mis Datos -->
    <div>
        <MisDatos:MisDatos ID="MisDatos" runat="server" />
    </div>
    <!-- Fin de [Menú Mis Datos] -->

</asp:Content>