﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="NuevaActividad.aspx.cs" Inherits="SemanaDelArbol.Pages.NuevaActividad" %>

<%@ Register Src="~/CustomControls/MisDatos.ascx" TagName="MisDatos" TagPrefix="MisDatos" %>
<%@ Register Src="~/CustomControls/Mapa.ascx" TagName="Mapa" TagPrefix="Mapa" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function () {
            obtenerUsuarioLogueado(cargarPagina_Actividades);
        });
        function cargarPagina_Actividades() {
            if (verificarUsuarioLogueado())
                $("#acciones").show();

            cargarListasGeograficas("selActividadInfo_Pais", "selActividadInfo_Provincia");

            var configMapa =
                {
                    IdMapa: "mapa",
                    TituloMapa: "Actividades",
                    MostrarMapa: true,
                    MostrarBuscador: true,
                    MostrarFiltroMarcadores: false,
                    MostrarListaMarcadores: false,
                    MostrarBotonMiPosicionActual: true,
                    OnMapLoadComplete: onMapLoadComplete_NuevaActividad,
                    Zoom: _googleMap.NivelZoom,
                    Direccion: _filtroActual.Pais.Nombre,
                    ImagenMarcador: _googleMap.Marcadores[_googleMap.TiposMarcador.Actividad].Icono,
                    DeshabilitarMarcadores: false,
                    OnClickMap: onClickToMap_Actividad,
                    OnBuscarDireccion: null
                };
            inicializarMapa(configMapa);

            configurarMenuMisDatos("Recuerde que para <b>agregar</b> o <b>editar</b> sus <b>actividades</b> hay que estar logueado en el sistema.");
        }
        function onMapLoadComplete_NuevaActividad(idMapa) {
            obtenerActividades();
        }
        function verAyudaActividad() {
            var colapsado = $("#ayudaActividad").hasClass("colapsado");
            if (colapsado) {
                $("#ayudaActividad").removeClass("colapsado");
                $("#imgAyudaActividad").attr("src", "../Images/Comun/16x16/Colapsar.png");
                $("#ayudaActividad").slideDown();
            }
            else {
                $("#ayudaActividad").addClass("colapsado");
                $("#imgAyudaActividad").attr("src", "../Images/Comun/16x16/Expandir.png");
                $("#ayudaActividad").slideUp();
            }
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <!-- Notificacion -->
            <div id="pnlNotificacion" style="padding:5px 0px 5px 0px;" >
                <div id="notificacionActividad" class="ui-state-highlight" >
                    <div onclick="javascript:verAyudaActividad();" class="link" >
                        <img id="imgAyudaActividad" src="../Images/Comun/16x16/Expandir.png" alt="Mostrar más" class="link" />¿Cómo agendar una actividad?
                    </div>
                </div>
                <div id="ayudaActividad" class="panelAyuda colapsado" style="display:none" >
                    <label><b><u>Pasos</u></b></label>
                    <ol>
                        <li><b>Ubique el área</b> aproximada en el mapa.</li>
                        <li>Haga <b>click sobre el mapa</b> cuando encuentre el punto donde realizará la actividad.</li>
                        <li><b>Complete los datos</b>, ¡y listo!.</li>
                    </ol>
                    <div style="padding: 5px 0px 5px 0px" >
                        <label>Recuerde que se pueden agendar todas las actividades que desee.</label>
                    </div>
                </div>
            </div>

            <!-- Mapa -->
            <div>
                <Mapa:Mapa ID="Mapa" runat="server" />
            </div>
            <!-- Fin de [Mapa] -->

        </div>

    </div>

    <!-- Menú Mis Datos -->
    <div>
        <MisDatos:MisDatos ID="MisDatos" runat="server" />
    </div>
    <!-- Fin de [Menú Mis Datos] -->

    
    <!-- Dialogo Actividad Info -->
    <div id="dialogActividadInfo" style="display:none" >
        <input type="hidden" id="hdnActividadInfo_Id" />
        <input type="hidden" id="hdnActividadInfo_Latitud" />
        <input type="hidden" id="hdnActividadInfo_Longitud" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="45%" /> 
                <col width="55%" />                
            </colgroup>
            <tr>
                <td>
                    <label>Usuario:</label>
                </td>
                <td> 
                    <label id="lblActividadInfo_Usuario" ></label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>País:</label>
                    &nbsp;
                    <label id="lblActividadInfoAdvertencia" class="tooltipLeft ui-state-highlight" style="float:right; margin-right:5px; height:14px; cursor:pointer;" title="Asegúrese de seleccionar el país en donde se realiza la Actividad." >*</label>
                </td>
                <td>
                    <select id="selActividadInfo_Pais" ></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Provincia / Departamento / Estado:</label>
                </td>
                <td>
                    <select id="selActividadInfo_Provincia" ></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Fecha:</label>
                </td>
                <td> 
                    <input id="txtActividadInfo_FechaYHora" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Descripción:</label>
                </td>
                <td> 
                    <input id="txtActividadInfo_Descripcion" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Comentarios:</label>
                </td>
                <td style="vertical-align:top;" >
                    <div style="padding:5px 0px 5px 0px" >
                        <textarea id="txtActividadInfo_Comentarios" class="noResize" rows="4" cols="1" ></textarea>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right" >
                    <label for="chkActividadInfo_PermitirColaboracion" >Permitir colaboración de los Usuarios</label>&nbsp;<input id="chkActividadInfo_PermitirColaboracion" type="checkbox" />
                </td>
            </tr>
        </table>
    </div>
    <!-- Fin de [Dialogo Actividad Info] -->
</asp:Content>