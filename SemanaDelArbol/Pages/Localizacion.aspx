﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Localizacion.aspx.cs" Inherits="SemanaDelArbol.Pages.Localizacion" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idTabla = "tblRecursos";
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Localizacion);
        });
        function cargarPagina_Localizacion()
        {
            obtenerListadoRecursos();
            //$("#btnAdministrarSponsors").click(function () { abrirPagina(_secciones.Sponsors); });
            //$("#tblLocalizacion").slideDown();
        }
        function obtenerListadoRecursos()
        {
            var params = new Object();
                params.soloDefaults = true;
                params.recurso = "";
            var jsonParams = JSON.stringify(params);
            
            $("#pnlCargandoRecursos").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerListadoRecursos", jsonParams, obtenerListadoRecursos_Respuesta);
        }
        function obtenerListadoRecursos_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var recursosInfo = respuestaInfo.Valor;
                if ( !isEmpty(recursosInfo) )
                {
                    cargarRecursos(recursosInfo);
                }
            }
        }
        function cargarRecursos(recursosInfo)
        {
            Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
            for (var i = 0; i < recursosInfo.length; i++)
            {
                crearFilaRecurso(recursosInfo[i]);
            }
            $("#pnlCargandoRecursos").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function crearFilaRecurso(recursoInfo, insertFirst)
        {
            var idRecurso = recursoInfo.Id;

            var btnEditar   = Helpers.HtmlHelper.Button.create("btnEditarRecurso_{0}".format(idRecurso), "Editar", "btnEditar");
            var btnEliminar = Helpers.HtmlHelper.Button.create("btnEliminarRecurso_{0}".format(idRecurso), "Eliminar", "btnEliminar");

            var fila  = '<td>' + btnEditar + btnEliminar + '</td>';
                fila += '<td>' + recursoInfo.Nombre + '</td>';
            Helpers.HtmlHelper.Table.addNewRow(idTabla, fila, "idRecurso", idRecurso, insertFirst);

            var btnEditar = $("#btnEditarRecurso_{0}".format(idRecurso));
                btnEditar.click(function () { eliminarRecurso(idRecurso); });
                btnEditar.button();

            var btnEliminar = $("#btnEliminarRecurso_{0}".format(idRecurso));
                btnEliminar.click(function () { eliminarProvincia(idRecurso) });
                btnEliminar.button();
        }
        function obtenerRecurso(recurso)
        {
            Helpers.showLoadingMessage();

            var params = new Object();
                params.recurso = recurso;
            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("obtenerRecurso", jsonParams, obtenerRecurso_Respuesta);
        }
        function obtenerRecurso_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var recursosInfo = respuestaInfo.Valor;
                if (!isEmpty(recursosInfo))
                {
                    //cargarRecursos(recursosInfo);
                }
            }
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" >
                    Administración de Localización
                </div>
            </div>

            <span id="pnlCargandoRecursos" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando recursos.." />
                <label>Cargando..</label>
            </span>

            <table id="tblRecursos" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="25%" /> <!-- Acción -->
                    <col width="75%" /> <!-- Nombre -->
                </colgroup>
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>Nombre archivo de recursos</th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>

        </div>

    </div>

</asp:Content>