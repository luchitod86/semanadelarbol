﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Entidades.aspx.cs" Inherits="SemanaDelArbol.Pages._Entidades" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!-- Estilos Footer.ascx -->
    <style type="text/css" >
        .entidad
        {
            float: left;
            margin: 4px;            
            border: 1px solid #ccc;
        }
        .entidad :hover
        {
            background-color: #4CED9A;
            -webkit-box-shadow: 7px 7px 5px rgba(50, 50, 50, 0.75);
            -moz-box-shadow: 7px 7px 5px rgba(50, 50, 50, 0.75);
            box-shadow: 7px 7px 5px rgba(50, 50, 50, 0.75);
            border: 1px solid #63C794;
            /*
            text-decoration: underline;
            border: 1px solid #63C794;
            background-color: #4CED9A;
            color: #fff;
            cursor: pointer;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            */
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Entidades);
        });
        function cargarPagina_Entidades()
        {
            obtenerEntidades();
        }
        function obtenerEntidades()
        {
            var params          = new Object();
                params.periodo  = _filtroActual.Periodo;
                params.pais     = _filtroActual.Pais.Nombre;
                params.idPais   = _filtroActual.Pais.Id;
            var jsonParams      = JSON.stringify(params); 

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerSponsors", jsonParams, obtenerEntidades_Respuesta);

            $("#pnlCargandoEntidades").fadeIn();
        }
        function obtenerEntidades_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var lista = $("#listaEntidades");
                var sponsorsInfo = respuestaInfo.Valor;
                for(var i=0; i < sponsorsInfo.length; i++)
                {
                    var sponsorInfo = sponsorsInfo[i];
                    if ( !isEmpty(sponsorInfo.Imagen) )
                    {
                        var imgDirectory = String.format("Images/Sponsors/{0}s/{1}/", sponsorInfo.Tipo, _filtroActual.Pais.Id);
                        var currentPage = getCurrentPageName();
                        if (currentPage != _secciones.Default)
                            imgDirectory = "../" + imgDirectory;

                        var id      = i + 1;
                        var img = $("<img />", { src: imgDirectory + sponsorInfo.Imagen });
                            img.addClass("imgSponsor");
                        var item = $("<div />", { id: "entidad_" + id });
                            item.addClass("entidad sombreado");
                        var anchor  = $("<a />", { href: String.format("http://{0}", sponsorInfo.Web.replace("http://", "")), target: "_blank" });
                        img.appendTo(item);
                        item.appendTo(anchor);
                        anchor.appendTo(lista);
                    }
                }
            }
            $("#pnlCargandoEntidades").fadeOut();
        }
    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >
            <div id="listaEntidades" >
                <div>
                    <label class="tituloSeccion" >Entidades que apoyan la campaña</label>&nbsp;
                    <span id="pnlCargandoEntidades" style="display:none" >
                        <img src="../Images/Procesando.gif" alt="Cargando entidades.." />
                        <label>Cargando..</label>
                    </span>
                </div>
            </div>
        </div>

    </div>


</asp:Content>