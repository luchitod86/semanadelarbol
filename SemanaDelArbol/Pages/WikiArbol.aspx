﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master"  CodeBehind="WikiArbol.aspx.cs" Inherits="SemanaDelArbol.Pages.WikiArbol" %>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <!--$(document).ready-->  
    <script type="text/javascript" >
      //  var _paisesWiki = [];
        var idEspecie = null;
        var idTabla = "tblEspeciesWiki";
        $(document).ready(function ()
        {
            paginaActual = getQuerystring("PaginaActual") != '' ? getQuerystring("PaginaActual") : 1;            
            obtenerEspeciesWiki();
            obtenerUsuarioLogueado(cargarPagina_WikiArbol);            
            crearNuevoPanelLateral();

            inicializarDialogEspecieWikiInfo();
            inicializarDialogAgregarNombre();
            inicializarDialogAgregarPais();
            cargarPaises();
        });
        function cargarPagina_WikiArbol()
        {
            $("#btnAgregarEspecie").click(function () {
                if (!verificarUsuarioLogueado()) {
                    Helpers.NotificationHelper.showWarning("Debe loguearse para realizar esta acción.", "Acceso restringido");
                    return;
                }
                limpiarDatosEspecieInfoDialog();
                abrirEspecieInfoDialog("Agregar especie");
            });            
        }      

        function obtenerEspeciesWiki()
        {            
            var params = new Object();
            var jsonParams = JSON.stringify(params);
            
            $("#pnlCargandoEspecies").fadeIn();
            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("obtenerEspeciesWiki", jsonParams, obtenerEspeciesWiki_Respuesta);
        }
        function obtenerEspeciesWiki_Respuesta(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var especiesInfo = respuestaInfo.Valor;
                cargarEspeciesWiki(especiesInfo);
            }
            $("#pnlCargandoEspecies").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function cargarEspeciesWiki(especiesInfo)
        {
            $("#tblEspeciesWiki").empty();
            //  Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
            if (especiesInfo.length == 0)
                $("#tblEspeciesWiki").append('<p>No se han encontrado especies.</p>');
            for (var i = 0; i < especiesInfo.length; i++)
            {
                crearFilaEspecies(especiesInfo[i]);
                if (i == 0)
                    $('#tblEspeciesWiki .entrada').addClass('selected');
            }

            if (especiesInfo.length > 0)
                obtenerEspeciePorId(especiesInfo[0].Id);

            $('#txtSearch').keyup(function () {
                var valThis = $(this).val().toLowerCase();
                if (valThis == "") {
                    $('#tblEspeciesWiki > div').show();
                } else {
                    $('#tblEspeciesWiki > div').each(function () {
                        var textNombre = $(this).find('h2').text().toLowerCase();
                        var textNombreCientifico = $(this).find('p').text().toLowerCase();
                        ((textNombre.indexOf(valThis) >= 0) || (textNombreCientifico.indexOf(valThis) >= 0))  ? $(this).show() : $(this).hide();
                    });
                };
            });

            $('.entrada').click(function () {
                var selected = $('.selected');
                selected.removeClass('selected');
                $(this).addClass('selected');
                var idEspecie = $(this).find('h2').attr('data-value');
                obtenerEspeciePorId(idEspecie);
            });
        }
        function crearFilaEspecies(especieInfo) {
            idEspecie = especieInfo.Id;

            var especieWiki = '<div class="entrada">' +
                                   '<h2 data-value="' + idEspecie + '">' + especieInfo.NombreCientifico + '</h2>'; 
                                        var nombres = especieInfo.Nombre.split('***');
                                        $.each(nombres, function (index, value) {
                                            especieWiki += '<p>' + value + '</p>';
                                        });
                                  especieWiki +='</div>';

                 $("#tblEspeciesWiki").append(especieWiki);

        }       
  
        function obtenerEspeciePorId(idEspecie) {

            var params = new Object();
            params.id = idEspecie;
            var jsonParams = JSON.stringify(params);
            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerEspecie", jsonParams, obtenerEspecie_Respuesta);            
        }

        function obtenerEspecie_Respuesta( datos) {
            Helpers.hideLoadingMessage();
            var container = $("#pnlDetallesEspecie");
            container.empty();

            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var especieInfo = respuestaInfo.Valor;
                verEspecieWiki(especieInfo);
            }
        }

        function verEspecieWiki(especieInfo) {
            idEspecie = especieInfo.Id;
            
            var especieWiki = '<div id="pnlWikiTitulo">' +
                               '<h2>' + especieInfo.NombreCientifico + '</h2>';

            var nombres = especieInfo.Nombre.split('***');
            $.each(nombres, function (index, value) {
                especieWiki += '<p>' + value + '</p>';
            });                
            
            especieWiki += '</div>';
            especieWiki += '<div id="pnlWikiIndicadores">';
            if (especieInfo.AptoPlaza)
                especieWiki += '<div class="icon-indicador" Title="Apto para Plazas"><img src="../Images/Middle/IconPlazas.png"></img><p style="color:#1E4E3C;"> - </p></div>';
            if (especieInfo.AptoVereda)
                especieWiki += '<div class="icon-indicador" Title="Apto para Veredas"><img src="../Images/Middle/IconVeredas.png"></img><p>10m<sup>2</sup></p></div>';
            if (especieInfo.AptoCantero)
                especieWiki += '<div class="icon-indicador" Title="Apto para Canteros"><img src="../Images/Middle/IconCanteros.png"></img><p style="color:#1E4E3C;"> - </p></div>';
            especieWiki += '</div>';
            especieWiki += '<div id="pnlWikiInfo"><div id="pnlIzqWikiInfo"><div id="pnlFotos"><ul id="sliderFotos">';
 
            if (!isEmpty(especieInfo.Foto1))
                especieWiki += "<li><img src='.." + especieInfo.Foto1 + "'/></li>";
            else
                especieWiki += "<li><img src='../Images/Logo.gif'>";
            if (!isEmpty(especieInfo.Foto2))
                especieWiki += "<li><img src='.." + especieInfo.Foto2 + "' /></li>";
            if (!isEmpty(especieInfo.Foto3))
                especieWiki += "<li><img src='.." + especieInfo.Foto3 + "'  /></li>";
            if (!isEmpty(especieInfo.Foto4))
                especieWiki += "<li><img src='.." + especieInfo.Foto4 + "' /></li>";
            if (!isEmpty(especieInfo.Foto5))
                especieWiki += "<li><img src='.." + especieInfo.Foto5 + "' /></li>";
            if (!isEmpty(especieInfo.Foto6))
                especieWiki += "<li><img src='.." + especieInfo.Foto6 + "' /></li>";

            especieWiki += '</ul></div></div><div id="pnlDerWikiInfo">';
            if (especieInfo.EsNativo)
                especieWiki += '<img class="icon-tipo-arbol" src="../Images/Middle/IconNativo.png"></img>';
            else if (especieInfo.EsExotico)
                especieWiki += '<img class="icon-tipo-arbol" src="../Images/Middle/IconExotico.png"></img>';

            especieWiki += "  <ul style='margin-top: 50px;  max-height: 280px;'> "
                    + "<li><font  class='label' color='#A2CC44'>Origen: </font>" + especieInfo.Origen + "</li>"
                    + "<li><font  class='label' color='#A2CC44'>Pais/es: </font><span id='spanPaises'></span></li>"
                    + "<li><font  class='label' color='#A2CC44'>Ecorregión: </font>" + especieInfo.Ecorregion + "</li>"
                    + "<li><font class='label' color='#A2CC44'>Altura: </font>" + especieInfo.Altura + "</li>"
                    + "<li><font class='label' color='#A2CC44'>Follaje: </font>" + especieInfo.Follaje + "</li>"
                    + "<li><font class='label' color='#A2CC44'>Hojas: </font>" + especieInfo.Hojas + "</li>"
                    + "<li><font class='label' color='#A2CC44'>Flores: </font>" + especieInfo.Flores + "</li>"
                    + "<li><font class='label' color='#A2CC44'>Frutos: </font>" + especieInfo.Frutos + "</li>"
                    + "</ul>"
                    + "<div class='mas-info'><div class='icon-mas-info'></div>" + especieInfo.MasInfo + "</div>";

                especieWiki += "</div></div><div class='aportes'><p>Aportes De:</p><ul id='aportes'>";

                especieWiki += "</ul><div><p class='btnEditar' id='btnEditarEspecie'>Sugerir Cambios/Agregar Info</p></div></div>";

                var params = new Object();
                params.idEspecie = idEspecie;
                var jsonParams = JSON.stringify(params);
                Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPaisesWiki", jsonParams, obtenerPaises_Respuesta);
                Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerUsuariosAporteWiki", jsonParams, obtenerUsuarios_Respuesta);

            $("#pnlDetallesEspecie").append(especieWiki);

            if(!isEmpty(especieInfo.Foto2)){
                var slider = $("#sliderFotos").pgwSlider({
                    displayControls: 'true',
                    touchControls: true,
                    verticalCentering: true
                });
                
            }
            else{
                $("#sliderFotos").addClass("unaFoto");
            }

            $("#btnEditarEspecie").click(function () {
                if (!verificarUsuarioLogueado()) {
                    Helpers.NotificationHelper.showWarning("Debe loguearse para realizar esta acción.", "Acceso restringido");
                    return;
                }
                editarEspecie(idEspecie); 
            });
        }

        function obtenerUsuarios_Respuesta(datos) {
            var usuarioshtml = '';
            var usuariosInfo = jQuery.parseJSON(datos.d);            
            if (usuariosInfo.Resultado) {
                var usuarios = usuariosInfo.Valor;
                $.each(usuarios, function (index, value) {
                    if(value.Tipo === 'Individuo')
                        usuarioshtml += '<li class="aporte-usuario">' + value.Nombre + ' ' + value.Apellido  + '</li>';
                    else
                        usuarioshtml += '<li class="aporte-escuela">' + value.Nombre + '</li>';
                });

            }           

            $('#aportes').append(usuarioshtml);
        }

        function obtenerPaises_Respuesta(datos) {
            var paiseshtml = '';
            var paisesInfo = jQuery.parseJSON(datos.d);
            if (paisesInfo.Resultado) {
                var paises = paisesInfo.Valor;
                $.each(paises, function (index, value) {
                    if (paises.length == 1 || paises.length == index + 1)
                        paiseshtml += value.Nombre;
                    else
                        paiseshtml += value.Nombre + ", ";
                });
            }

            $('#spanPaises').append(paiseshtml);
        }

        function crearNuevoPanelLateral() {
            $('#tituloAccionesMenu').remove();
            $('#accionesMenu').remove();


            var pnlLateralWiki = '<div>' +
                    '<div class="titulo-wiki" ></div>' +
                    '<p class="text">¡Bienvenido a la <u>primera enciclopedia virtual interactiva</u> de árboles nativos!</p>' +
                    '<p  class="colabora">Colabora en su edición, sumándole información y conoce las especies que deben plantarse en cada región.</p>' +
                    '<ul><li>- BUSCANDO UNA ESPECIE</li><li>- SUMANDO INFORMACIÓN A LA FICHA DE UNA ESPECIE</li><li>- AGREGANDO UNA FICHA DE UNA NUEVA ESPECIE</li><li>- COMPARTIENDO LAS FICHAS EN REDES SOCIALES</li></ul>' +
            '</div>';

            $('#divPnlLateral').html(pnlLateralWiki);
        }
            
     
       function inicializarDialogEspecieWikiInfo()
        {
            var especieInfoOptions =
            {
                title: "Especie",
                width: _dialogos.EspecieWikiInfo.Width,
                height: _dialogos.EspecieWikiInfo.Height,
                buttons:
                [
                    { id: "btnCancelEspecieInfoDialog", text: "Cancelar", click: cerrarEspecieInfoDialog },
                    { id: "btnConfirmEspecieInfoDialog", text: "Aceptar", click: guardarEspecieWiki }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.EspecieWikiInfo.Id, especieInfoOptions);

            inicializarFileUpload("imgEspecieFoto1", "imgEspecieFoto1");
            inicializarFileUpload("imgEspecieFoto2", "imgEspecieFoto2");
            inicializarFileUpload("imgEspecieFoto3", "imgEspecieFoto3");
            inicializarFileUpload("imgEspecieFoto4", "imgEspecieFoto4");
            inicializarFileUpload("imgEspecieFoto5", "imgEspecieFoto5");
            inicializarFileUpload("imgEspecieFoto6", "imgEspecieFoto6");
        }
        function abrirEspecieInfoDialog(titulo)
        {
            Helpers.DialogHelper.openDialog(_dialogos.EspecieWikiInfo.Id);
            Helpers.DialogHelper.setTitle(_dialogos.EspecieWikiInfo.Id, titulo);
        }
        function cerrarEspecieInfoDialog()
        {
            Helpers.DialogHelper.closeDialog(_dialogos.EspecieWikiInfo.Id);
        }
        function limpiarDatosEspecieInfoDialog()
        {
            $("#hdnEspecieInfo_Id").setValue(null);
            $("#hdnCantidadNombres").setValue(0);
            $("#txtEspecieInfo_Nombre").setValue(null);
            $("#txtEspecieInfo_NombreCientifico").setValue(null);           
            $('#tdNombres').html('');
            $('#tdPaises').html('');
            $("#txtEspecieInfo_Origen").val(null);
            $("#txtEspecieInfo_Ecorregion").val(null);
            $("#chkEspecieInfo_EsNativo").prop('checked', false);
            $("#chkEspecieInfo_EsExotico").prop('checked', false);
            $("#txtEspecieInfo_Altura").val(null);
            $("#txtEspecieInfo_Follaje").val(null);
            $("#txtEspecieInfo_Hojas").val(null);
            $("#txtEspecieInfo_Flores").val(null);
            $("#txtEspecieInfo_Frutos").val(null);
            $("#txtEspecieInfo_MasInfo").val(null);
            $("#chkEspecieInfo_AptoPlaza").prop('checked', false);
            $("#chkEspecieInfo_AptoCantero").prop('checked', false);
            $("#chkEspecieInfo_AptoVereda").prop('checked', false);

            $("#imgEspecieFoto1").attr("src", '');
            $("#imgEspecieFoto2").attr("src", '');
            $("#imgEspecieFoto3").attr("src", '');
            $("#imgEspecieFoto4").attr("src", '');
            $("#imgEspecieFoto5").attr("src", '');
            $("#imgEspecieFoto6").attr("src", '');

            desactivarMensajesDeError("#txtEspecieInfo_NombreCientifico");
        }

        function editarEspecie(idEspecie) {
            limpiarDatosEspecieInfoDialog();
            Helpers.CRUDHelper.Get("../Default.aspx/obtenerEspecie", "{'id':'{0}' }".format(idEspecie), obtenerDatosEspecie_Response);
            var params = new Object();
            params.idEspecie = idEspecie;
            var jsonParams = JSON.stringify(params);
            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPaisesWiki", jsonParams, obtenerPaisesEspecie_Respuesta);
            abrirEspecieInfoDialog("Editar especie");
            
        }

        function obtenerDatosEspecie_Response(responseInfo) {
          //  Helpers.hideLoadingMessage();
            var especieInfo = responseInfo.Valor;
            if (!isEmpty(especieInfo))
            {
                $("#hdnEspecieInfo_Id").setValue(especieInfo.Id);

                if (especieInfo.Nombre != ''){
                    var nombres = especieInfo.Nombre.split('***');
                    $.each(nombres, function (index, value) {
                        $('#tdNombres').append('<p>' + value + '</p> - ');
                    });
                    $("#hdnCantidadNombres").setValue(nombres.length);

                    $('#tdNombres p').click(function () {
                        $(this).remove();
                        var cantNombres = $("#hdnCantidadNombres").getValue();
                        cantNombres = parseInt(cantNombres) - 1;
                        $("#hdnCantidadNombres").setValue(cantNombres);
                    });
                }
                else
                    $("#hdnCantidadNombres").setValue(0);

                $("#txtEspecieInfo_NombreCientifico").val(especieInfo.NombreCientifico);
                $("#txtEspecieInfo_Origen").val(especieInfo.Origen);
                $("#txtEspecieInfo_Ecorregion").val(especieInfo.Ecorregion);
                $("#chkEspecieInfo_EsNativo").prop('checked', especieInfo.EsNativo);
                $("#chkEspecieInfo_EsExotico").prop('checked', especieInfo.EsExotico);
                $("#txtEspecieInfo_Altura").val(especieInfo.Altura);
                $("#txtEspecieInfo_Follaje").val(especieInfo.Follaje);
                $("#txtEspecieInfo_Hojas").val(especieInfo.Hojas);
                $("#txtEspecieInfo_Flores").val(especieInfo.Flores);
                $("#txtEspecieInfo_Frutos").val(especieInfo.Frutos);
                $("#txtEspecieInfo_MasInfo").val(especieInfo.MasInfo);
                $("#chkEspecieInfo_AptoPlaza").prop('checked', especieInfo.AptoPlaza);
                $("#chkEspecieInfo_AptoCantero").prop('checked', especieInfo.AptoCantero);
                $("#chkEspecieInfo_AptoVereda").prop('checked', especieInfo.AptoVereda);

                if (especieInfo.Foto1 != null && especieInfo.Foto1 != '')
                    $("#imgEspecieFoto1").attr("src", '..' + especieInfo.Foto1);
                if (especieInfo.Foto2 != null && especieInfo.Foto2 != '')
                    $("#imgEspecieFoto2").attr("src", '..' + especieInfo.Foto2);
                if (especieInfo.Foto3 != null && especieInfo.Foto3 != '')
                    $("#imgEspecieFoto3").attr("src", '..' + especieInfo.Foto3);
                if (especieInfo.Foto4 != null && especieInfo.Foto4 != '')
                    $("#imgEspecieFoto4").attr("src", '..' + especieInfo.Foto4);
                if (especieInfo.Foto5 != null && especieInfo.Foto5 != '')
                    $("#imgEspecieFoto5").attr("src", '..' + especieInfo.Foto5);
                if (especieInfo.Foto6 != null && especieInfo.Foto6 != '')
                    $("#imgEspecieFoto6").attr("src", '..' + especieInfo.Foto6);
            }
        }

        function obtenerPaisesEspecie_Respuesta(datos) {
            $('#tdPaises').html('');
            var paiseshtml = '';
            var paisesInfo = jQuery.parseJSON(datos.d);
            if (paisesInfo.Resultado) {
                var paises = paisesInfo.Valor;
                $.each(paises, function (index, value) {
                    $('#tdPaises').append('<p>' + value.Nombre + '</p> - ');
                });

                $('#tdPaises p').click(function () {
                    $(this).remove();
                 /*   $("#tdPaises").children().each(function () {
                        $(this).html($(this).html().replace(/ - - /g, " - "));
                    });*/
                });
            }

            $('#tdPaises').append(paiseshtml);            
        }

        function guardarEspecieWiki() {
            var infoValidacion =
                 [
                    { campo: "txtEspecieInfo_NombreCientifico", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre Cientifico." }] },
                    { campo: "hdnCantidadNombres", seleccion: '#tdNombres', validaciones: [{ operacion: _validaciones.Valor.MayorA, valor: 0, mensaje: "Debe ingresar al menos un Nombre." }] }
                 ];

            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            Helpers.showLoadingMessage();

            var nombre = '';
            var nombres = $('#tdNombres').find('p');
            $.each(nombres, function (index, value) {
                if (index === 0)
                    nombre = value.innerHTML;
                else
                    nombre += '***' + value.innerHTML;
            });

            var id = $("#hdnEspecieInfo_Id").getValue();
            var especieInfo = new Object();
            especieInfo.Id = isEmpty(id) ? 0 : id;
            especieInfo.Nombre = nombre;
            especieInfo.NombreCientifico = $("#txtEspecieInfo_NombreCientifico").getValue();
            especieInfo.Origen = $("#txtEspecieInfo_Origen").getValue();
            especieInfo.IdPais = Helpers.SelectListHelper.getSelectedValueFromSelectList("selEspecieInfo_Pais");
            especieInfo.Ecorregion = $("#txtEspecieInfo_Ecorregion").getValue();
            especieInfo.EsNativo = $("#chkEspecieInfo_EsNativo").getValue();
            especieInfo.EsExotico = $("#chkEspecieInfo_EsExotico").getValue();           
            especieInfo.AptoPlaza = $("#chkEspecieInfo_AptoPlaza").getValue();
            especieInfo.AptoCantero = $("#chkEspecieInfo_AptoCantero").getValue();
            especieInfo.AptoVereda = $("#chkEspecieInfo_AptoVereda").getValue();

            especieInfo.Foto1 = getBase64Image("imgEspecieFoto1");
            especieInfo.Foto2 = getBase64Image("imgEspecieFoto2");
            especieInfo.Foto3 = getBase64Image("imgEspecieFoto3");
            especieInfo.Foto4 = getBase64Image("imgEspecieFoto4");
            especieInfo.Foto5 = getBase64Image("imgEspecieFoto5");
            especieInfo.Foto6 = getBase64Image("imgEspecieFoto6");

            var masInfo = $("#txtEspecieInfo_MasInfo").getValue();
            var altura = $("#txtEspecieInfo_Altura").getValue();
            var follaje = $("#txtEspecieInfo_Follaje").getValue();
            var hojas = $("#txtEspecieInfo_Hojas").getValue();
            var flores = $("#txtEspecieInfo_Flores").getValue();
            var frutos = $("#txtEspecieInfo_Frutos").getValue();

            var pais = '';
            var paises = $('#tdPaises').find('p');
            $.each(paises, function (index, value) {
                if (index === 0)
                    pais = value.innerHTML;
                else
                    pais += '***' + value.innerHTML;
            });

            var jsonParams = JSON.stringify(especieInfo);
            Helpers.AJAXHelper.doAjaxCall("guardarEspecieWiki", "{'especieInfo':'" + jsonParams + "', 'idUsuario': '" + _usuarioLogueado.Id + "', 'paises': '" + pais + "', 'altura': '" + altura + "', 'follaje': '" + follaje + "', 'hojas': '" + hojas + "', 'frutos': '" + frutos + "', 'flores': '" + flores + "', 'masInfo': '" + masInfo + "'}", crearEspecie_Respuesta);
        }

        function crearEspecie_Respuesta(datos) {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var especieInfo = respuestaInfo.Valor;
                var onConfirmarEspecieCreada = overloadFunction(confirmarEspecieCreada, especieInfo);
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, 'Mensaje', null, onConfirmarEspecieCreada);
            }
            else {
                Helpers.NotificationHelper.showWarning(respuestaInfo.Mensaje);
            }
        }
        function confirmarEspecieCreada(especieInfo) {
            abrirPagina(_secciones.WikiArbol);
        }
 
        function inicializarDialogAgregarNombre() {
            var agregarNombreOptions =
            {
                title: "Agregar Nombre",
                width: _dialogos.AgregarNombre.Width,
                height: _dialogos.AgregarNombre.Height,
                buttons:
                [
                    { id: "btnConfirmAdopcionInfoDialog", text: "Aceptar", click: confirmarAgregarNombre }
                ]
            };            
            Helpers.DialogHelper.initializeDialog(_dialogos.AgregarNombre.Id, agregarNombreOptions);
        }

        function abrirAgregarNombreDialog() {
            $("#txtNombre").val('');
            Helpers.DialogHelper.openDialog(_dialogos.AgregarNombre.Id);
        }

        function confirmarAgregarNombre() {
            var nombre = $("#txtNombre").val();
            var infoValidacion =
               [
                   { campo: "txtNombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre Cientifico." }] }
               ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            $('#tdNombres').append('<p>' + nombre + '</p> - ');

            var cantNombres = $("#hdnCantidadNombres").getValue();
            cantNombres = parseInt(cantNombres) + 1;
            $("#hdnCantidadNombres").setValue(cantNombres);

            $('#tdNombres p').click(function () {
                $(this).remove();
                var cantNombres = $("#hdnCantidadNombres").getValue();
                cantNombres = parseInt(cantNombres) - 1;
                $("#hdnCantidadNombres").setValue(cantNombres);
            });
            Helpers.DialogHelper.closeDialog(_dialogos.AgregarNombre.Id);
        }

        function inicializarDialogAgregarPais() {
            var agregarPaisOptions =
            {
                title: "Agregar Pais",
                width: _dialogos.AgregarPais.Width,
                height: _dialogos.AgregarPais.Height,
                buttons:
                [
                    { id: "btnConfirmAgregarPaisDialog", text: "Aceptar", click: confirmarAgregarpais }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.AgregarPais.Id, agregarPaisOptions);
        }

        function abrirAgregarPaisDialog() {
            Helpers.SelectListHelper.setSelectedValue("selEspecieInfo_Pais", 0);
            Helpers.DialogHelper.openDialog(_dialogos.AgregarPais.Id);
        }

        function confirmarAgregarpais() {
            var pais = Helpers.SelectListHelper.getSelectedTextFromSelectList("selEspecieInfo_Pais");
            var infoValidacion =
               [
                   { campo: "selEspecieInfo_Pais", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: 0, mensaje: "Debe seleccionar un país." }] }
               ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            $('#tdPaises').append('<p>' + pais + '</p> - ');           

            $('#tdPaises p').click(function () {
                $(this).remove();
            });
            Helpers.DialogHelper.closeDialog(_dialogos.AgregarPais.Id);
        }

        function cargarPaises() {
            var data = new Object();
            data.IdListaPaises = 'selEspecieInfo_Pais';
            data.IdListaProvincia = null;
            data.IdPaisActual = -1;
            data.IdProvinciaActual = -1;
            data.PropiedadValor = "Id";
            data.PropiedadTexto = "Nombre";

            var params = new Object();
            params.incluirInactivos = false;
            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPaises", jsonParams, obtenerPaisesSelect_Response, data);

        }

        function obtenerPaisesSelect_Response(data, datos) {
            var response = jQuery.parseJSON(datos.d);
            if (response.Resultado) {
                var paisesInfo = response.Valor;

                if (_ListaPaises == null)
                    _ListaPaises = paisesInfo;

                obtenerPaises_Procesar(paisesInfo, data);
            }
        }

        function obtenerPaises_Procesar(paisesInfo, data) {
            var paises = Helpers.SelectListHelper.formatForSelectList(paisesInfo, data.PropiedadValor, data.PropiedadTexto);
            Helpers.SelectListHelper.fillSelectList(data.IdListaPaises, paises);

            var htmlSelect = $("#" + data.IdListaPaises);
            htmlSelect.unbind('change');
            htmlSelect.change(function (event) {
                data.IdPaisActual = Helpers.SelectListHelper.getSelectedValueFromSelectList(data.IdListaPaises);
                obtenerProvincias(data);
            });
            $('#selEspecieInfo_Pais').prepend('<option value="0">Seleccione pais...</option>');
            if (data.IdPaisActual == -1) //obtiene el País seleccionado..
                data.IdPaisActual = Helpers.SelectListHelper.getSelectedValueFromSelectList(data.IdListaPaises);
            else //establece el país seleccionado..
                Helpers.SelectListHelper.setSelectedValue(data.IdListaPaises, data.IdPaisActual);

        }

    </script>

    <!--HTML-->
    <div id="contentWiki" >
        <div id="pnlDetallesEspecie" >
        </div>
        <div id="pnlEspecies" >

            <div >
                <input type="search" id="txtSearch" placeholder="Buscar especie..." />
            </div>
           
            <span id="pnlCargandoEspecies" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando especies.." />
                <label>Cargando..</label>
            </span>

            <div id="tblEspeciesWiki" class="listado-especies" style="display:none" >
                
            </div>
            <div class="pnlAgregar">
                <p class="button btnAgregar" id="btnAgregarEspecie" >+</p>
            </div> 
        </div>
    </div>

    <!-- Dialogo Especie Info -->
        <div id="dialogEspecieWikiInfo" style="display:none" >
            <input type="hidden" id="hdnEspecieInfo_Id" />
            <input type="hidden" id="hdnCantidadNombres" />

            <div class="agregarEspecieHeader">
                <div class="ficha-titulo">WIKIÁRBOL</div>
                <div class="ficha-desc">Ficha para nueva especie</div>
                <input class="btnGuardar" type="button" value="Enviar / Aceptar" onclick="guardarEspecieWiki();" />
            </div>

            <div class="agregarEspecieContent">
                <ul class="border-right">
                    <li>
                        <label>Nombre científico:</label>
                        <input id="txtEspecieInfo_NombreCientifico" type="text" value="" maxlength="100"/>
                    </li>
                    <li>
                        <label style="display:inline-block;">Nombres vulgares:</label>
                        <input style="display:inline-block; float:right;" type="button" value="+" onclick="abrirAgregarNombreDialog();" />
                        <div id="tdNombres">
                        </div>                      
                    </li>
                    <li>
                         <label>Origen:</label>
                         <input id="txtEspecieInfo_Origen" type="text" value="" maxlength="255" style="width:180px;float:right; margin-right:3px;"/>
                    </li>
                    <li>
                         <label>Pais/es:</label>
                        <input style="display:inline-block; float:right;" type="button" value="+" onclick="abrirAgregarPaisDialog();" />
                        <div id="tdPaises">
                        </div>                              
                    </li>
                    <li>
                         <label>Ecorregión/es:</label>
                         <input id="txtEspecieInfo_Ecorregion" type="text" value="" maxlength="255"/>
                    </li>
                    <li>
                         <label>Altura(M):</label>
                         <input id="txtEspecieInfo_Altura" type="text" value="" style="width:170px;float:right; margin-right:3px;"/>
                    </li>
                    <li>
                        <label>Clase:</label>
                         <input id="chkEspecieInfo_EsNativo" class="wiki-check wiki-check-nativo" type="checkbox" title="Es Nativo"/>
                         <input id="chkEspecieInfo_EsExotico" class="wiki-check wiki-check-exotico" type="checkbox" title="Es Exótico"/>                                          
                    </li>
                    <li>
                        <label>Plantación:</label>
                         <input id="chkEspecieInfo_AptoVereda" class="wiki-check-apto wiki-check-apto-vereda" type="checkbox" title="Apto Vereda"/>
                         <input id="chkEspecieInfo_AptoCantero" class="wiki-check-apto wiki-check-apto-cantero" type="checkbox" title="Apto Cantero"/>
                         <input id="chkEspecieInfo_AptoPlaza" class="wiki-check-apto wiki-check-apto-plaza" type="checkbox" title="Apto Plaza"/>
                    </li>

                    <li>   
                                <label>Imágenes:</label>                         
                                <div class="fotos">
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto1" src="" width="70px" height="50px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto1').attr('src', '');" style="display:block;width:70px;background-image:none;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto2" src="" width="70px" height="50px" class="foto" />
                                        <input type="button" value="Eliminar"  onclick="$('#imgEspecieFoto2').attr('src', '');" style="display:block;width:70px;background-image:none;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto3" src="" width="70px" height="50px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto3').attr('src', '');" style="display:block;width:70px;background-image:none;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto4" src="" width="70px" height="50px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto4').attr('src', '');" style="display:block;width:70px;background-image:none;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto5" src="" width="70px" height="50px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto5').attr('src', '');" style="display:block;width:70px;background-image:none;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto6" src="" width="70px" height="50px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto6').attr('src', '');" style="display:block;width:70px;background-image:none;" />
                                    </div>
                                 </div>
                    </li>
                </ul>
                <ul>
                    <li>
                       <label>Follaje:</label>
                       <textarea id="txtEspecieInfo_Follaje" rows="2" maxlength="255"></textarea>
                    </li>
                    <li>
                       <label>Hojas:</label>
                       <textarea id="txtEspecieInfo_Hojas" rows="2" maxlength="255"></textarea>                     
                    </li>
                    <li>
                       <label>Flores:</label>
                       <textarea id="txtEspecieInfo_Flores" rows="2" maxlength="255"></textarea>
                    </li>
                    <li>
                       <label>Frutos:</label>
                       <textarea id="txtEspecieInfo_Frutos" rows="2" maxlength="255"></textarea>
                    </li>
                    <li>
                       <label>Más Información:</label>
                       <textarea id="txtEspecieInfo_MasInfo" rows="6" ></textarea>
                    </li>
                </ul>  
            </div>

         </div>

        <!-- Dialogo Agregar Nombre -->
        <div id="dialogAgregarnombre" style="display:none" >   
            <!-- Donaciones -->
            <div id="pnlAgregarNombre" >
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <colgroup>
                        <col width="50%" /> 
                        <col width="50%" />                
                    </colgroup>
                    <tr>
                        <td>
                            <label>Nombre:</label>
                        </td>
                        <td>
                            <input id="txtNombre" type="text" style="width:100%" value="" maxlength="50"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <!-- Fin de [Dialogo Agregar Nombre] -->

    <!-- Dialogo Agregar Pais -->
        <div id="dialogAgregarPais" style="display:none" >   
            <!-- Paises -->
            <div id="pnlAgregarpais" >
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <colgroup>
                        <col width="50%" /> 
                        <col width="50%" />                
                    </colgroup>
                    <tr>
                        <td>
                            <label>Pais:</label>
                        </td>
                        <td>
                            <select id="selEspecieInfo_Pais" style="width: 185px;float: right; margin-right:3px;" ></select>   
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <!-- Fin de [Dialogo Agregar Pais] -->
</asp:Content>
