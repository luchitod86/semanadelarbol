﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="EditarEspecie.aspx.cs" Inherits="SemanaDelArbol.Pages.EditarEspecie" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

     <style type="text/css" >
        .panelAgregarEspecie
        {
            width: 65%;
            margin: 5px;
        }

        .panelAgregarEspecie label
        {
            color: #204234;
        }

       #tdNombres p{
            color: #204234;
            display: inline;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idEspecie;

        $(document).ready(function () {
            obtenerUsuarioLogueado(cargarPagina_AgregarEspecie);
            inicializarDialogAgregarNombre();            
            cargarPaises();
        });

        function cargarPagina_AgregarEspecie()
        {
            idEspecie = getQuerystring("idEspecie");

            obtenerDatosEspecie(idEspecie);

            $("#btnGuardarEspecie").click(function () { crearEspecie(); });

            inicializarFileUpload("imgEspecieFoto1", "imgEspecieFoto1");
            inicializarFileUpload("imgEspecieFoto2", "imgEspecieFoto2");
            inicializarFileUpload("imgEspecieFoto3", "imgEspecieFoto3");
            inicializarFileUpload("imgEspecieFoto4", "imgEspecieFoto4");
            inicializarFileUpload("imgEspecieFoto5", "imgEspecieFoto5");
            inicializarFileUpload("imgEspecieFoto6", "imgEspecieFoto6");
        }

        function obtenerDatosEspecie(idEspecie) {
            Helpers.showLoadingMessage();
            var params = new Object();
            params.id = idEspecie;
            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerEspecie", jsonParams, obtenerDatosEspecie_Response);
        }
        function obtenerDatosEspecie_Response(datos) {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var especieInfo = respuestaInfo.Valor;

                var nombres = especieInfo.Nombre.split('***');
                $.each(nombres, function (index, value) {
                    $('#tdNombres').append('<p>' + value + '</p> - ');
                });


                $("#txtEspecieInfo_NombreCientifico").val(especieInfo.NombreCientifico);
                $("#txtEspecieInfo_Origen").val(especieInfo.Origen);
                Helpers.SelectListHelper.setSelectedValue('selEspecieInfo_Pais', especieInfo.IdPais);
                $("#txtEspecieInfo_Ecorregion").val(especieInfo.Ecorregion);
                $("#chkEspecieInfo_EsNativo").prop('checked', especieInfo.EsNativo);
                $("#chkEspecieInfo_EsExotico").prop('checked', especieInfo.EsExotico);
                $("#txtEspecieInfo_Altura").val(especieInfo.Altura);
                $("#txtEspecieInfo_Follaje").val(especieInfo.Follaje);
                $("#txtEspecieInfo_Hojas").val(especieInfo.Hojas);
                $("#txtEspecieInfo_Flores").val(especieInfo.Flores);
                $("#txtEspecieInfo_Frutos").val(especieInfo.Frutos);
                $("#txtEspecieInfo_MasInfo").val(especieInfo.MasInfo);
                $("#chkEspecieInfo_AptoPlaza").prop('checked', especieInfo.AptoPlaza);
                $("#chkEspecieInfo_AptoCantero").prop('checked', especieInfo.AptoCantero);
                $("#chkEspecieInfo_AptoVereda").prop('checked', especieInfo.AptoVereda);

                if (especieInfo.Foto1 != null && especieInfo.Foto1 != '')
                    $("#imgEspecieFoto1").attr("src", '..' + especieInfo.Foto1);
                if (especieInfo.Foto2 != null && especieInfo.Foto2 != '')
                    $("#imgEspecieFoto2").attr("src", '..' + especieInfo.Foto2);
                if (especieInfo.Foto3 != null && especieInfo.Foto3 != '')
                    $("#imgEspecieFoto3").attr("src", '..' + especieInfo.Foto3);
                if (especieInfo.Foto4 != null && especieInfo.Foto4 != '')
                    $("#imgEspecieFoto4").attr("src", '..' + especieInfo.Foto4);
                if (especieInfo.Foto5 != null && especieInfo.Foto5 != '')
                    $("#imgEspecieFoto5").attr("src", '..' + especieInfo.Foto5);
                if (especieInfo.Foto6 != null && especieInfo.Foto6 != '')
                    $("#imgEspecieFoto6").attr("src", '..' + especieInfo.Foto6);
            }
        }

        function inicializarDialogAgregarNombre() {
            var agregarNombreOptions =
            {
                title: "Agregar Nombre",
                width: _dialogos.AgregarNombre.Width,
                height: _dialogos.AgregarNombre.Height,
                buttons:
                [
                    { id: "btnConfirmAdopcionInfoDialog", text: "Aceptar", click: confirmarAgregarNombre }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.AgregarNombre.Id, agregarNombreOptions);
        }

        function abrirAgregarNombreDialog() {
            $("#txtNombre").val('');            
            Helpers.DialogHelper.openDialog(_dialogos.AgregarNombre.Id);
        }

        function confirmarAgregarNombre() {
            var nombre =$("#txtNombre").val();
            var infoValidacion =
               [
                   { campo: "txtNombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre Cientifico." }] }
               ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            $('#tdNombres').append('<p>' + nombre + '</p> - ');

            Helpers.DialogHelper.closeDialog(_dialogos.AgregarNombre.Id);
        }
      
    </script>

    <!-- Crear especie -->
    <script type="text/javascript">
        function crearEspecie()
        {
            var infoValidacion =
                 [
                    { campo: "txtEspecieInfo_NombreCientifico", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre Cientifico." }] }
                 ];
            
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false; 

            Helpers.showLoadingMessage();

            var nombre = '';
            var nombres = $('#tdNombres').find('p');
            $.each(nombres, function (index, value) {
                if (index === 0)
                    nombre = value.innerHTML;
                else
                    nombre += '***' + value.innerHTML;
            });
                
            var especieInfo = new Object();
            especieInfo.Id = idEspecie;
            especieInfo.Nombre = nombre;
            especieInfo.NombreCientifico = $("#txtEspecieInfo_NombreCientifico").getValue();
            especieInfo.Origen = $("#txtEspecieInfo_Origen").getValue();
            especieInfo.IdPais = Helpers.SelectListHelper.getSelectedValueFromSelectList("selEspecieInfo_Pais");
            especieInfo.Ecorregion = $("#txtEspecieInfo_Ecorregion").getValue();
            especieInfo.EsNativo = $("#chkEspecieInfo_EsNativo").getValue();
            especieInfo.EsExotico = $("#chkEspecieInfo_EsExotico").getValue();
            especieInfo.Altura = $("#txtEspecieInfo_Altura").getValue();
            especieInfo.Follaje = $("#txtEspecieInfo_Follaje").getValue();
            especieInfo.Hojas = $("#txtEspecieInfo_Hojas").getValue();
            especieInfo.Flores = $("#txtEspecieInfo_Flores").getValue();
            especieInfo.Frutos = $("#txtEspecieInfo_Frutos").getValue();
            especieInfo.MasInfo = $("#txtEspecieInfo_MasInfo").getValue();
            especieInfo.AptoPlaza = $("#chkEspecieInfo_AptoPlaza").getValue();
            especieInfo.AptoCantero = $("#chkEspecieInfo_AptoCantero").getValue();
            especieInfo.AptoVereda = $("#chkEspecieInfo_AptoVereda").getValue();
            
            especieInfo.Foto1 = getBase64Image("imgEspecieFoto1");
            especieInfo.Foto2 = getBase64Image("imgEspecieFoto2");
            especieInfo.Foto3 = getBase64Image("imgEspecieFoto3");
            especieInfo.Foto4 = getBase64Image("imgEspecieFoto4");
            especieInfo.Foto5 = getBase64Image("imgEspecieFoto5");
            especieInfo.Foto6 = getBase64Image("imgEspecieFoto6");

            var jsonParams = JSON.stringify(especieInfo).replace(/'/g, "\\'");
            Helpers.AJAXHelper.doAjaxCall("guardarEspecieWiki", "{'especieInfo':'" + jsonParams + "', 'idUsuario': '"+_usuarioLogueado.Id +"'}", crearEspecie_Respuesta);
        }
       
        function crearEspecie_Respuesta(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var especieInfo = respuestaInfo.Valor;
                var onConfirmarEspecieCreada = overloadFunction(confirmarEspecieCreada, especieInfo);
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, 'Mensaje', null, onConfirmarEspecieCreada);
            }
            else
            {
                Helpers.NotificationHelper.showWarning(respuestaInfo.Mensaje);
            }
        }
        function confirmarEspecieCreada(especieInfo)
        {
            abrirPagina(_secciones.WikiArbol);
        }
    
        function cargarPaises()
        {
            var data = new Object();
            data.IdListaPaises = 'selEspecieInfo_Pais';
            data.IdListaProvincia = null;
            data.IdPaisActual =  -1;
            data.IdProvinciaActual = -1;
            data.PropiedadValor = "Id";
            data.PropiedadTexto = "Nombre";

            var params = new Object();
            params.incluirInactivos = false;
            var jsonParams = JSON.stringify(params);            

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerPaises", jsonParams, obtenerPaisesSelect_Response, data);
            
        }

        function obtenerPaisesSelect_Response(data, datos)
        {
            var response = jQuery.parseJSON(datos.d);
            if ( response.Resultado )
            {
                var paisesInfo = response.Valor;
                
                if ( _ListaPaises == null )
                    _ListaPaises = paisesInfo;

                obtenerPaises_Procesar(paisesInfo, data);
            }
        }

      function obtenerPaises_Procesar(paisesInfo , data) {
          var paises = Helpers.SelectListHelper.formatForSelectList(paisesInfo, data.PropiedadValor, data.PropiedadTexto);
          Helpers.SelectListHelper.fillSelectList(data.IdListaPaises, paises);

          var htmlSelect = $("#" + data.IdListaPaises);
          htmlSelect.unbind('change');
          htmlSelect.change(function (event)
          {
              data.IdPaisActual = Helpers.SelectListHelper.getSelectedValueFromSelectList(data.IdListaPaises);
              obtenerProvincias(data);
          });
          $('#selEspecieInfo_Pais').prepend('<option value="0">Seleccione pais...</option>');
          if (data.IdPaisActual == -1) //obtiene el País seleccionado..
              data.IdPaisActual = Helpers.SelectListHelper.getSelectedValueFromSelectList(data.IdListaPaises);
          else //establece el país seleccionado..
              Helpers.SelectListHelper.setSelectedValue(data.IdListaPaises, data.IdPaisActual);
            
        }
       
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >            
            <div class="seccion" >
                <div class="titulo" >
                    Datos de la Especie
                </div>
                <div id="pnlAgregarEspecie" class="panelAgregarEspecie" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="40%" />
                            <col width="60%" />
                        </colgroup>
                        <tr>
                            <td>
                                <label>Nombre científico:</label>
                            </td>
                            <td> 
                                <input id="txtEspecieInfo_NombreCientifico" type="text" value="" maxlength="100"/>
                            </td>
                        </tr>                 
                        <tr>
                            <td>
                                <label>Nombres:</label>
                            </td>
                            <td id="tdNombres">
                                
                            </td>
                            <td> 
                                <input type="button" value="Agregar Nombre" onclick="abrirAgregarNombreDialog();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Origen:</label>
                            </td>
                            <td>
                                <input id="txtEspecieInfo_Origen" type="text" value="" maxlength="255"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Pais:</label>
                            </td>
                            <td>
                                <select id="selEspecieInfo_Pais" ></select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Ecorregión:</label>
                            </td>
                            <td>
                                <input id="txtEspecieInfo_Ecorregion" type="text" value="" maxlength="255"/>
                            </td>
                        </tr>
                        <tr>
                            <td>                                
                            </td>
                            <td>
                                <label for="chkEspecieInfo_EsNativo" >Es Nativo:</label>&nbsp;<input id="chkEspecieInfo_EsNativo" type="checkbox"/>
                                <label for="chkEspecieInfo_EsExotico" >Es Exotico:</label>&nbsp;<input id="chkEspecieInfo_EsExotico" type="checkbox"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Altura:</label>
                            </td>
                            <td>
                                <input id="txtEspecieInfo_Altura" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Follaje:</label>
                            </td>
                            <td>
                                <input id="txtEspecieInfo_Follaje" type="text" value="" maxlength="255"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Hojas:</label>
                            </td>
                            <td>
                                <input id="txtEspecieInfo_Hojas" type="text" value="" maxlength="255"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Flores:</label>
                            </td>
                            <td>
                                <input id="txtEspecieInfo_Flores" type="text" value="" maxlength="255"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Frutos:</label>
                            </td>
                            <td>
                                <input id="txtEspecieInfo_Frutos" type="text" value="" maxlength="255"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Más Info:</label>
                            </td>
                            <td>
                                <textarea id="txtEspecieInfo_MasInfo" rows="6" ></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>                                
                            </td>
                            <td>
                                <label for="chkEspecieInfo_AptoVereda" >Apto Vereda:</label>&nbsp;<input id="chkEspecieInfo_AptoVereda" type="checkbox"/>
                                <label for="chkEspecieInfo_AptoCantero" >Apto Cantero:</label>&nbsp;<input id="chkEspecieInfo_AptoCantero" type="checkbox"/>
                                <label for="chkEspecieInfo_AptoPlaza" >Apto Plaza:</label>&nbsp;<input id="chkEspecieInfo_AptoPlaza" t type="checkbox"/>
                            </td>
                        </tr>
                        <tr>
                                <td>   
                                    <label>Imágenes:</label>                         
                                </td>
                                <td class="fotos">
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto1" src="" width="100px" height="80px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto1').attr('src', '');" style="display:block;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto2" src="" width="100px" height="80px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto2').attr('src', '');" style="display:block;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto3" src="" width="100px" height="80px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto3').attr('src', '');" style="display:block;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto4" src="" width="100px" height="80px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto4').attr('src', '');" style="display:block;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto5" src="" width="100px" height="80px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto5').attr('src', '');" style="display:block;" />
                                    </div>
                                    <div style="display:inline-block;">
                                        <img id="imgEspecieFoto6" src="" width="100px" height="80px" class="foto" />
                                        <input type="button" value="Eliminar" onclick="$('#imgEspecieFoto6').attr('src', '');" style="display:block;" />
                                    </div>
                                 </td>
                            </tr>
                    </table>
                </div>
            </div>      
            <input type="button" id="btnGuardarEspecie" value="Guardar Especie" class="button button btnAgregar" style="width:145px" />
        </div>     
     
    </div>
    <!-- Dialogo Agregar Nombre -->
<div id="dialogAgregarnombre" style="display:none" >   
    <!-- Donaciones -->
    <div id="pnlAgregarNombre" >
        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="50%" /> 
                <col width="50%" />                
            </colgroup>
            <tr>
                <td>
                    <label>Nombre:</label>
                </td>
                <td>
                    <input id="txtNombre" type="text" style="width:100%" value="" />
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- Fin de [Dialogo Agregar Nombre] -->
</asp:Content>