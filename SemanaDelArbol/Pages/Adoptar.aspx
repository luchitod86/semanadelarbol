﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Adoptar.aspx.cs" Inherits="SemanaDelArbol.Pages.Adoptar" %>

<%@ Register Src="~/CustomControls/MisDatos.ascx" TagName="MisDatos" TagPrefix="MisDatos" %>
<%@ Register Src="~/CustomControls/Mapa.ascx" TagName="Mapa" TagPrefix="Mapa" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
        #totalDonantes
        {
            border: 1px solid transparent;
            padding: 4px;
        }

        #resultadoBusqueda{
            color: #204234;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Adoptar);
        });
        function cargarPagina_Adoptar()
        {
            var configMapa =
                {
                    IdMapa: "mapa",
                    TituloMapa: "Mapa de donantes",
                    TituloBuscador: "", //"Localice en el mapa la ubicación aproximada",
                    MostrarMapa: true,
                    MostrarBuscador: true,
                    MostrarFiltroMarcadores: false,
                    MostrarListaMarcadores: false,
                    MostrarBotonMiPosicionActual: true,
                    OnMapLoadComplete: onMapLoadComplete_Adoptar,
                    Zoom: _googleMap.NivelZoom,
                    Direccion: _filtroActual.Pais.Nombre,
                    ImagenMarcador: _googleMap.Marcadores[_googleMap.TiposMarcador.Donante].Icono,
                    DeshabilitarMarcadores: false,
                    OnClickMap: null, //onClickToMap_Adoptar,
                    OnBuscarDireccion: buscarDonantes
                };
            inicializarMapa(configMapa);
            configurarMenuMisDatos("Puede ubicar la zona de <b>adopción</b> ingresando una dirección, y luego navegar el mapa hasta encontrar el punto exacto que está buscando.");
        }
        function onMapLoadComplete_Adoptar(idMapa)
        {
            var idPais      = Helpers.SelectListHelper.getSelectedValueFromSelectList("selPaises");
            var pais        = Helpers.SelectListHelper.getSelectedTextFromSelectList("selPaises");
            var idProvincia = -1;
            var provincia   = "";
            var ciudad      = "";

            obtenerDonantes(idPais, pais, idProvincia, provincia, ciudad, onObtenerDonantes_Respuesta);
        }
        function verAyudaAdopcion()
        {
            var colapsado = $("#ayudaAdopcion").hasClass("colapsado");
            if ( colapsado )
            {
                $("#ayudaAdopcion").removeClass("colapsado");
                $("#imgAyudaAdopcion").attr("src", "../Images/Comun/16x16/Colapsar.png");
                $("#ayudaAdopcion").slideDown();
            }
            else
            {
                $("#ayudaAdopcion").addClass("colapsado");
                $("#imgAyudaAdopcion").attr("src", "../Images/Comun/16x16/Expandir.png");
                $("#ayudaAdopcion").slideUp();
            }
        }
        function buscarDonantes()
        {
            var idPais      = Helpers.SelectListHelper.getSelectedValueFromSelectList("selPaises");
            var pais        = Helpers.SelectListHelper.getSelectedTextFromSelectList("selPaises");
            var idProvincia = Helpers.SelectListHelper.getSelectedValueFromSelectList("selProvincias");
            var provincia   = Helpers.SelectListHelper.getSelectedTextFromSelectList("selProvincias");
            var ciudad      = $("#txtMapa_Buscador_Ciudad").val();

            idPais          = !isEmpty(idPais)      ? idPais : -1;
            idProvincia     = !isEmpty(idProvincia) ? idProvincia : -1;
            
            obtenerDonantes(idPais, pais, idProvincia, provincia, ciudad, onObtenerDonantes_Respuesta);
        }
        function onObtenerDonantes_Respuesta(resultadoBusqueda)
        {
            var cantidad    = resultadoBusqueda.cantidad;
            var pais        = String.format("<u><b>{0}</b></u>", resultadoBusqueda.pais);
            var provincia   = !isEmpty(resultadoBusqueda.provincia) ? String.format(", <u><b>{0}</b></u>", resultadoBusqueda.provincia) : "";
            var ciudad      = !isEmpty(resultadoBusqueda.ciudad) ? String.format(", <u><b>{0}</b></u>", resultadoBusqueda.ciudad) : "";
            var texto       = String.format("Donantes encontrados en {0}{1}{2}: <b>{3}</b>", pais, provincia, ciudad, cantidad);

            highlight("#totalDonantes", 4000, "recuadroVerde");
            $("#resultadoBusqueda").html(texto);
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <!-- Notificacion -->
            <div id="pnlNotificacion" style="padding:5px 0px 5px 0px;" >
                <div id="notificacionAdopcion" class="ui-state-highlight" >
                    <div onclick="javascript:verAyudaAdopcion();" class="link" >
                        <img id="imgAyudaAdopcion" src="../Images/Comun/16x16/Colapsar.png" alt="Mostrar más" class="link" />¿Cómo realizar la adopción de árboles?
                    </div>
                </div>
                <div id="ayudaAdopcion" class="panelAyuda" >
                    <label><b><u>Pasos</u></b></label>
                    <ol>
                        <li><b>Ubique en el mapa</b> llenando los campos.</li>
                        <li>Haga <b>click sobre un donante</b> para ver sus donaciones</li>
                        <li>Indique la <b>especie</b> y <b>cantidad</b> a adoptar, ¡y listo!</li>
                    </ol>
                </div>
            </div>

            <!-- Resultado búsqueda Donantes -->
            <div>
                <img src="../Images/Marcadores/Donante.png" alt="Donantes" />
                <span id="totalDonantes" >
                    <span id="resultadoBusqueda" ></span>
                </span>
            </div>
            <!-- Fin de [Resultado búsqueda Donantes] -->

            <!-- Mapa -->
            <div>
                <Mapa:Mapa ID="Mapa" runat="server" />
            </div>
            <!-- Fin de [Mapa] -->

        </div>

    </div>

    <!-- Menú Mis Datos -->
    <div>
        <MisDatos:MisDatos ID="MisDatos" runat="server" />
    </div>
    <!-- Fin de [Menú Mis Datos] -->

</asp:Content>