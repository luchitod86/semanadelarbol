﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Registrarse.aspx.cs" Inherits="SemanaDelArbol.Pages.Registrarse" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
        .panelRegistrar
        {
            width: 65%;
            margin: 5px;
        }
        .panelRegistrar label
        {
            color: #204234;
        }
        #lnkRegistrarConFacebook,
        #lnkRegistrarConTwitter,
        #lnkRegistrarConGooglePlus
        {
            font-size: 16px;
            vertical-align: middle;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        $(document).ready(function () {
            obtenerUsuarioLogueado(cargarPagina_Registrarse);
        });
        function cargarPagina_Registrarse()
        {
            cargarListasGeograficas("selRegistrar_Pais", "selRegistrar_Provincia", _filtroActual.Pais.Id);

            cargarListaTiposUsuario("selRegistrar_TipoUsuario", onTipoUsuarioChanged);
            cargarListaPerfilesUsuario("selRegistrar_PerfilUsuario");

            $("#lnkRegistrarConTwitter, #lnkRegistrarConGooglePlus").removeClass("link");
            sideMenu("mnuRegistrarConRedesSociales", "right", 230, 100, 120, false);

            enlazarRegistrarConRedesSociales();

            $("#btnCrearCuenta").click( function() { crearCuenta(); } );
        }
        function enlazarRegistrarConRedesSociales()
        {
            $("#lnkRegistrarConFacebook").click(function () { registrarConFacebook(); });
            //$("#lnkRegistrarConTwitter").click(function () { registrarConTwitter(); });
            //$("#lnkRegistrarConGooglePlus").click(function () { registrarConGooglePlus(); });
        }
        function onTipoUsuarioChanged()
        {
            var apellido = $("#trRegistrar_Apellido");
            var web = $("#trRegistrar_Web");
            if (esIndividuo())
            {
                apellido.show();
                web.hide();
            }
            else
            {
                apellido.hide();
                web.show();
            }
        }
    </script>

    <!-- Crear cuenta -->
    <script type="text/javascript">
        function crearCuenta()
        {
            //$("#selRegistrar_TipoUsuario + div.chosen-container > a")seleccion: "#selPlantacionInfoEspecie + button"
            var infoValidacion =
                [
                    //{ campo: "selRegistrar_TipoUsuario", seleccion: "#selRegistrar_TipoUsuario + div.chosen-container > a" , validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: -1, mensaje: "Debe seleccionar cómo registrarse." }] },
                    { campo: "txtRegistrar_NombreUsuario", validaciones: [ { operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre de usuario." } ] },
                    { campo: "txtRegistrar_Password", seleccion: "#txtRegistrar_Password, #txtRegistrar_ConfirmarPassword", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 4, mensaje: "Debe ingresar una contraseña de al menos 5 caracteres." }, { operacion: _validaciones.Valor.IgualA, valor: $("#txtRegistrar_ConfirmarPassword").val(), mensaje: "Las contraseñas deben coincidir" }] },
                    { campo: "txtRegistrar_Nombre",  validaciones: [ { operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre." } ] },
                    //{ campo: "txtRegistrar_Apellido", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un apellido." }] },
                    { campo: "txtRegistrar_Email", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un e-mail." }] }
                    //{ campo: "selRegistrar_Pais", seleccion: "#selRegistrar_Pais + div.chosen-container > a", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: -1, mensaje: "Debe seleccionar un país." }] }
                ];
            var validacionApellido = { campo: "txtRegistrar_Apellido", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un apellido." }] };

            if ( esIndividuo() )
                infoValidacion.push(validacionApellido);

            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            var cuenta = new Object();

                cuenta.IdTipo = $("#selRegistrar_TipoUsuario").getValue();
                cuenta.IdPerfil = $("#selRegistrar_PerfilUsuario").getValue();

                cuenta.NombreUsuario = $("#txtRegistrar_NombreUsuario").getValue();
                cuenta.Password = $("#txtRegistrar_Password").getValue();
                cuenta.NombreUsuarioFacebook = $("#txtRegistrar_NombreUsuario_Facebook").getValue();
                cuenta.NombreUsuarioTwitter = $("#txtRegistrar_NombreUsuario_Twitter").getValue();
                cuenta.NombreUsuarioGooglePlus = $("#txtRegistrar_NombreUsuario_GooglePlus").getValue();

                cuenta.Nombre = $("#txtRegistrar_Nombre").getValue();
                cuenta.Apellido = $("#txtRegistrar_Apellido").getValue();
                cuenta.Telefono = $("#txtRegistrar_Telefono").getValue();
                cuenta.Email = $("#txtRegistrar_Email").getValue();
                cuenta.Web = $("#txtRegistrar_Web").getValue();

                cuenta.IdPais = $("#selRegistrar_Pais").getValue();
                cuenta.IdProvincia = $("#selRegistrar_Provincia").getValue();
                if (cuenta.IdProvincia == null)
                    cuenta.IdProvincia = -1;
                cuenta.Ciudad = $("#txtRegistrar_Ciudad").getValue();

            var jsonParams = JSON.stringify(cuenta).replace(/'/g, "\\'");

            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("Pages/Registrarse.aspx/registrar", "{'infoUsuario':'" + jsonParams + "'}", crearCuenta_Respuesta);
        }
        function registrarConRedSocial_Respuesta(redSocial, exito, infoUsuarioRedSocial)
        {
            if (!exito)
                return;

            var userId      = infoUsuarioRedSocial.Id;
            var userName    = infoUsuarioRedSocial.UserName;
            var firstName   = infoUsuarioRedSocial.FirstName;
            var lastName    = infoUsuarioRedSocial.LastName;
            var address     = infoUsuarioRedSocial.Address != null ? infoUsuarioRedSocial.Address.split(',') : null;

            var mensaje = String.format("Se ha podido recuperar de su cuenta de {0}:", redSocial);

            var campos = "";
                campos += String.format("<li><b>Nombre</b>: {0}</li>", firstName);
                campos += String.format("<li><b>Apellido</b>: {0}</li>", lastName);
                campos += String.format("<li><b>Nombre de usuario</b>: {0}</li>", userName);
            if ( !isEmpty(address) )
                campos += String.format("<li><b>Ubicación</b>: {0}</li>", address);
                campos = "<ul>" + campos + "</ul>";

            Helpers.NotificationHelper.showMessage(mensaje + campos);

            var txtRegistrar_NombreUsuario_RedSocial;
            switch (redSocial)
            {
                case _redesSociales.Facebook:
                    txtRegistrar_NombreUsuario_RedSocial = $("#txtRegistrar_NombreUsuario_Facebook");
                    break;
                case _redesSociales.Twitter:
                    txtRegistrar_NombreUsuario_RedSocial = $("#txtRegistrar_NombreUsuario_Twitter");
                    break;
                case _redesSociales.GooglePlus:
                    txtRegistrar_NombreUsuario_RedSocial = $("#txtRegistrar_NombreUsuario_GooglePlus");
                    break;
            }
            $("#txtRegistrar_Nombre").setValue(firstName);
            $("#txtRegistrar_Apellido").setValue(lastName);
            $("#txtRegistrar_NombreUsuario").setValue(userName);
            txtRegistrar_NombreUsuario_RedSocial.setValue(userId);

            if (address[1] != null)
            {
                var pais = address[1];
                var idPais = Helpers.SelectListHelper.findOptionByText("selRegistrar_Pais", pais.trim());
                    idPais = idPais != null ? idPais.value : null;
            }

            if (address[0] != null)
            {
                var provincia = address[0];
                var idProvincia = Helpers.SelectListHelper.findOptionByText("selRegistrar_Provincia", provincia.trim());
                    idProvincia = idProvincia != null ? idProvincia.value : null;
            }
  
            if ( !isEmpty(idPais) )
                Helpers.SelectListHelper.setSelectedValue("selRegistrar_Pais", idPais);
            if ( !isEmpty(idProvincia) )
                Helpers.SelectListHelper.setSelectedValue("selRegistrar_Provincia", idProvincia);
        }
        function crearCuenta_Respuesta(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var usuarioInfo = respuestaInfo.Valor;
                var onConfirmarCuentaCreada = overloadFunction(confirmarCuentaCreada, usuarioInfo);
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, 'Mensaje', null, onConfirmarCuentaCreada);
            }
            else
            {
                Helpers.NotificationHelper.showWarning(respuestaInfo.Mensaje);
            }
        }
        function confirmarCuentaCreada(usuarioInfo)
        {
            //actualizarDatosSesion(usuarioInfo);
            abrirPagina(_secciones.Default);
        }
        function esIndividuo()
        {
            var idTipoUsuario = Helpers.SelectListHelper.getSelectedValueFromSelectList( "selRegistrar_TipoUsuario" );
            if (idTipoUsuario == _usuarioTipos.Individuo.Id)
                return true;
            else
                return false;
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >
            <div class="seccion" >
                <div class="titulo" >
                    Datos del registrante
                </div>
                <div id="pnlRegistrar_DatosRegistrante" class="panelRegistrar" >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="40%" />
                            <col width="60%" />
                        </colgroup>
                        <tr>
                            <td align="left" >
                                <label style="font-weight:bold; text-decoration:underline" >Registrarse como:</label>
                            </td>
                            <td align="center" >
                                <select id="selRegistrar_TipoUsuario" ></select>
                                <select id="selRegistrar_PerfilUsuario" style="display:none" ></select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Nombre:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_Nombre" type="text" value="" />
                            </td>
                        </tr>
                        <tr id="trRegistrar_Apellido" >
                            <td>
                                <label>Apellido:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_Apellido" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Teléfono:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_Telefono" type="text"  value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>E-mail:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_Email" type="text" value="" />
                            </td>
                        </tr>
                        <tr id="trRegistrar_Web" style="display:none" >
                            <td>
                                <label>Web:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_Web" type="text" value="" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="seccion" >
                <div class="titulo" >
                    Datos del sitio web
                </div>
                <div id="pnlRegistrar_DatosWeb" class="panelRegistrar" >
                    <input id="txtRegistrar_NombreUsuario_Facebook" type="hidden" />
                    <input id="txtRegistrar_NombreUsuario_Twitter" type="hidden" />
                    <input id="txtRegistrar_NombreUsuario_GooglePlus" type="hidden" />
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="40%" /> 
                            <col width="60%" />                
                        </colgroup>
                        <tr>
                            <td>
                                <label>Nombre de usuario:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_NombreUsuario" type="text" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Contraseña:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_Password" type="text"  value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Confirmar contraseña:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_ConfirmarPassword" type="text" value="" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="seccion" >
                <div class="titulo" >
                    Datos de ubicación geográfica
                </div>
                <div id="pnlRegistrar_DatosUbicacion" class="panelRegistrar"  >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="40%" /> 
                            <col width="60%" />                
                        </colgroup>
                        <tr>
                            <td>
                                <label>País:</label>
                            </td>
                            <td>
                                <select id="selRegistrar_Pais" ></select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Provincia / Departamento / Estado:</label>
                            </td>
                            <td>
                                <select id="selRegistrar_Provincia" ></select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Localidad/Ciudad/Pueblo:</label>
                            </td>
                            <td>
                                <input id="txtRegistrar_Ciudad" type="text" value="" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <input type="button" id="btnCrearCuenta" value="Registrarse" class="button button btnAgregar" style="width:115px" />
            
        </div>

        <!-- Menú Registrar con Redes Sociales -->
        <div id="mnuRegistrarConRedesSociales" class="sideMenu right" >
            <div class="options" style="padding:10px;" >
                <div class="title" >
                    <img src="../Images/Iconos/Facebook.png" alt="Registrar con Facebook" class="icono24x24" />
                    <label id="lnkRegistrarConFacebook" class="link" >Registrar con Facebook</label>
                </div>
                <div class="title notAvailable" >
                    <img src="../Images/Iconos/Twitter.png" alt="Registrar con Twitter" class="icono24x24" />
                    <label id="lnkRegistrarConTwitter" >Registrar con Twitter</label>
                </div>
                <div class="title notAvailable" >
                    <img src="../Images/Iconos/GooglePlus.png" alt="Registrar con Google+" class="icono24x24" />
                    <label id="lnkRegistrarConGooglePlus" >Registrar con Google+</label>
                </div>
            </div>
        </div>

        <!-- Dialogo Asociar cuentas -->
        <div id="dialogAsociarCuentas" style="display:none" >
            <fieldset class="ui-widget ui-widget-content" >
                <legend class="ui-widget-header" align="center" >Datos de cuenta de Red Social</legend>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                    <colgroup>
                        <col width="40%" /> 
                        <col width="60%" />                
                    </colgroup>
                    <tr>
                        <td>
                            <label>Nombre Usuario:</label>
                        </td>
                        <td>
                            <input id="txtAsociarCuentas_NombreUsuario" type="text" class="tooltipRight" title="Este es el nombre de usuario con el que se identificará en nuestro sistema." value="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre:</label>
                        </td>
                        <td>
                            <input id="txtAsociarCuentas_Nombre" type="text" class="tooltipRight" title="Este es el nombre obtenido de su red social, pero puede modificarlo si lo prefiere." value="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Apellido:</label>
                        </td>
                        <td>
                            <input id="txtAsociarCuentas_Apellido" type="text" class="tooltipRight" title="Este es el apellido obtenido de su red social, pero puede modificarlo si lo prefiere." value="" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Usuario&nbsp;<label id="lblAsociarCuentas_UsuarioRedSocial" >:</label>
                        </td>
                        <td>
                            <input id="txtAsociarCuentas_UsuarioFacebook" type="text" class="disabled grisado" style="display:none;" value="" readonly="readonly" />
                            <input id="txtAsociarCuentas_UsuarioTwitter" type="text" class="disabled grisado" style="display:none;" value="" readonly="readonly" />
                            <input id="txtAsociarCuentas_UsuarioGooglePlus" type="text" class="disabled grisado" style="display:none;" value="" readonly="readonly" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>

    </div>

</asp:Content>