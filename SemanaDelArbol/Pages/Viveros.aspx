﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Viveros.aspx.cs" Inherits="SemanaDelArbol.Pages.Viveros" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Viveros);
        });
        function cargarPagina_Viveros()
        {
            if ( verificarUsuarioLogueado() )
            {
                if (validarPerfilAdministrador(_usuarioLogueado))
                {
                    $("#btnAgregarVivero").click( function() { agregarVivero(); } );
                    $("#btnAgregarVivero").fadeIn();
                }
            }
            
            obtenerViveros();
            cargarListaUsuarios("selViveroInfo_Usuario", _filtroActual.Pais.Nombre);
            inicializarFileUpload("btnViveroInfoSubirImagen", "imgViveroInfo_Imagen");
            inicializarDialogoViveroInfo();
        }
        function obtenerViveros()
        {
            var params  = new Object();
                params.periodo  = _filtroActual.Periodo;
                params.pais     = _filtroActual.Pais.Nombre;
                params.idPais   = _filtroActual.Pais.Id;

            var parametros = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("obtenerViveros", parametros, obtenerViveros_Respuesta);
        }
        function obtenerViveros_Respuesta(datos)
        {
            var respuesta = jQuery.parseJSON(datos.d);
            if ( respuesta.Resultado )
            {
                var viverosInfo = respuesta.Valor;
                for(var i=0; i < viverosInfo.length; i++)
                {
                    agregarFichaViveroInfo(viverosInfo[i].Valor);
                }
            }
            else
            {
                Helpers.NotificationHelper.showWarning(respuesta.Mensaje);
            }
        }
    </script>

    <!-- Diálogo Vivero -->
    <script type="text/javascript">
        function inicializarDialogoViveroInfo() {
            var viveroInfoOptions =
            {
                title: "Agregar Vivero",
                width: _dialogos.ViveroInfo.Width,
                height: _dialogos.ViveroInfo.Height,
                buttons:
                [
                    { id: "btnCancelViveroInfoDialog", text: "Cancelar", click: cerrarViveroInfoDialog },
                    { id: "btnConfirmViveroInfoDialog", text: "Aceptar", click: guardarViveroInfo }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.ViveroInfo.Id, viveroInfoOptions);
        }
        function limpiarDatosViveroInfoDialog()
        {
            Helpers.MultiSelectHelper.setAll("selViveroInfo_Usuario", false);
            $("#txtViveroInfo_Direccion").setValue(null);
            $("#txtViveroInfo_Telefono").setValue(null);
            $("#txtViveroInfo_Email").setValue(null);
            $("#txtViveroInfo_Web").setValue(null);
            $("#imgViveroInfo_Imagen").attr("src", "");

            desactivarMensajesDeError("#selViveroInfo_Usuario + button, #txtViveroInfo_Direccion, #txtViveroInfo_Telefono, #txtViveroInfo_Email, #txtViveroInfo_Web");
        }
        function abrirViveroInfoDialog(viveroInfo)
        {
            limpiarDatosViveroInfoDialog();
            if (!isEmpty(viveroInfo))
            {
                $("#hdnViveroInfo_Id").val(viveroInfo.Id);
                Helpers.MultiSelectHelper.setSelected("selViveroInfo_Usuario", viveroInfo.IdUsuario);
                $("#txtViveroInfo_Direccion").val(viveroInfo.Direccion);
                $("#txtViveroInfo_Telefono").val(viveroInfo.Telefono);
                $("#txtViveroInfo_Email").val(viveroInfo.Email);
                $("#txtViveroInfo_Web").val(viveroInfo.Web);
                setBase64Image("imgViveroInfo_Imagen", viveroInfo.Imagen);
            }
            Helpers.DialogHelper.openDialog(_dialogos.ViveroInfo.Id);
        }
        function cerrarViveroInfoDialog()
        {
            Helpers.DialogHelper.closeDialog(_dialogos.ViveroInfo.Id);
        }
    </script>
    <!-- Fin de [Diálogo Viveros] -->

    <!-- Vivero -->
    <script type="text/javascript">
        function obtenerViveroInfo(id, onResponseFunction)
        {
            var params = new Object();
                params.id = id;
            var jsonParams = JSON.stringify(params);

            var data = new Object();
                data.onResponseFunction = onResponseFunction;

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerVivero", jsonParams, obtenerViveroInfo_Respuesta, data);
        }
        function obtenerViveroInfo_Respuesta(data, datos) {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var viveroInfo = respuestaInfo.Valor;
                if (!isEmpty(data.onResponseFunction))
                    data.onResponseFunction.call(this, viveroInfo);
            }
            else {
                Helpers.NotificationHelper.showError(respuestaInfo.Mensaje, "Error");
            }
        }
        function guardarViveroInfo()
        {
            var infoValidacion =
                [
                    { campo: "selViveroInfo_Usuario", seleccion: "#selViveroInfo_Usuario + button", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar un usuario." }] },
                    { campo: "txtViveroInfo_Direccion", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una dirección." }] },
                    { campo: "txtViveroInfo_Telefono", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un teléfono." }] },
                    { campo: "txtViveroInfo_Email", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un e-mail." }] },
                    { campo: "txtViveroInfo_Web", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una web." }] }
                ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            cerrarViveroInfoDialog();

            var id = $("#hdnViveroInfo_Id").val();

            var viveroInfo = new Object();
                viveroInfo.Id = (id.length == 0 ? -1 : id);
                viveroInfo.IdUsuario = Helpers.MultiSelectHelper.getFirstSelectedValue("selViveroInfo_Usuario");
                viveroInfo.Direccion = $("#txtViveroInfo_Direccion").val();
                viveroInfo.Telefono = $("#txtViveroInfo_Telefono").val();
                viveroInfo.Email = $("#txtViveroInfo_Email").val();
                viveroInfo.Web = $("#txtViveroInfo_Web").val();
                viveroInfo.Imagen = getBase64Image("imgViveroInfo_Imagen");

            var parametros = JSON.stringify(viveroInfo);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/guardarVivero", parametros, guardarViveroInfo_Respuesta);
        }
        function guardarViveroInfo_Respuesta(datos) {
            var respuesta = jQuery.parseJSON(datos.d);
            if (respuesta.Resultado) {
                var viveroInfo = respuesta.Valor;
                Helpers.NotificationHelper.showMessage(respuesta.Mensaje, "", "", null);
                agregarFichaViveroInfo(viveroInfo);
            }
            else {
                Helpers.NotificationHelper.showWarning(respuesta.Mensaje);
            }
        }
        function agregarFichaViveroInfo(viveroInfo) {
            var id = viveroInfo.Id;

            var btnEditar = '<input id="btnEditarVivero_' + id + '" type="button" class="button btnEditar" value="Editar" style="display:none;" >';
            var btnEliminar = '<input id="btnEliminarVivero_' + id + '" type="button" class="button btnEliminar" value="Eliminar" style="display:none;" >';

            var ficha = $("#viveroId_" + id);
            if (ficha.length > 0) {
                ficha.empty();
            }
            else {
                ficha = $("<div />").attr("id", "viveroId_" + id).css("margin-bottom", "15px");
                $("#listadoViveros").append(ficha);
            }

            var contenido = '';
                contenido += '  <fieldset class="ui-widget ui-widget-content ui-corner-all" style="padding:20px" >';
                contenido += '      <table border="0" cellpadding="0" cellspacing="0" width="100%" >';
                contenido += '          <colgroup>';
                contenido += '              <col width="25%" />';
                contenido += '              <col width="40%" />';
                contenido += '              <col width="25%" />';
                contenido += '              <col width="10%" />';
                contenido += '          </colgroup>';
                contenido += '          <tr>';
                contenido += '              <td colspan="2" >';
                contenido += '                  <label style="font-weight:bold; text-decoration:underline; text-transform:uppercase" >' + viveroInfo.Usuario + '</label>';
                contenido += '              </td>';
                contenido += '              <td rowspan="8" >';
                contenido += '                  <img id="imgViveroId_' + id + '" src="" alt="' + viveroInfo.Usuario + '" width="200px" height="150px" style="border:1px solid #CCE2C4;" />';
                contenido += '              </td>';
                contenido += '              <td rowspan="8" style="padding-left:x15px;" >';
                contenido +=                    btnEditar + btnEliminar;
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td colspan="2" >';
                contenido += '                  &nbsp;';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td>';
                contenido += '                  <label>Provincia / Departamento / Estado:</label>';
                contenido += '              </td>';
                contenido += '              <td>';
                contenido += '                  <label>' + viveroInfo.Provincia + '</label>';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td>';
                contenido += '                  <label>Dirección:</label>';
                contenido += '              </td>';
                contenido += '              <td>';
                contenido += '                  <label>' + viveroInfo.Direccion + '</label>';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td>';
                contenido += '                  <label>Teléfono:</label>';
                contenido += '              </td>';
                contenido += '              <td>';
                contenido += '                  <label>' + viveroInfo.Telefono + '</label>';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td>';
                contenido += '                  <label>E-mail:</label>';
                contenido += '              </td>';
                contenido += '              <td>';
                contenido += '                  <a href="mailto:' + viveroInfo.Email + '" >' + viveroInfo.Email + '</a>';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td>';
                contenido += '                  <label>Web:</label>';
                contenido += '              </td>';
                contenido += '              <td>';
                contenido += '                  <a href="' + parseUrlAddress(viveroInfo.Web) + '" target="_blank" >' + viveroInfo.Web + '</a>';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '      </table>';
                contenido += '  </fieldset>';

            ficha.append(contenido);

            if (!isEmpty(viveroInfo.Imagen))
                setBase64Image("imgViveroId_" + id, viveroInfo.Imagen);
            else
                $("#imgViveroId_" + id).attr("src", "../Images/NoImage.gif");

            if (verificarUsuarioLogueado()) {
                if (validarPerfilAdministrador(_usuarioLogueado)) {
                    $("input[type=button]").button();
                    $("#btnEditarVivero_" + id)
                        .click(function () { editarVivero(id); })
                        .fadeIn();
                    $("#btnEliminarVivero_" + id)
                        .click(function () { eliminarVivero(id); })
                        .fadeIn();
                }
            }
        }
        function eliminarFichaViveroInfo(viveroInfo) {
            var ficha = $("#viveroId_" + viveroInfo.Id);
                ficha.remove();
        }
        function agregarVivero() {
            abrirViveroInfoDialog();
        }
        function editarVivero(idVivero) {
            obtenerViveroInfo(idVivero, editarVivero_Respuesta);
        }
        function editarVivero_Respuesta(viveroInfo) {
            if (!isEmpty(viveroInfo))
                abrirViveroInfoDialog(viveroInfo);
        }
        function eliminarVivero(idVivero) {
            var onEliminarVivero_Confirm = overloadFunction(onEliminarVivero_Confirmacion, idVivero);
            Helpers.NotificationHelper.showConfirm("¿Desea eliminar este Vivero?", "Eliminar", null, onEliminarVivero_Confirm);
        }
        function onEliminarVivero_Confirmacion(idVivero) {
            var params = new Object();
                params.id = idVivero;
            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/eliminarVivero", jsonParams, onEliminarVivero_Response);
        }
        function onEliminarVivero_Response(datos) {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var viveroInfo = respuestaInfo.Valor;
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, "Mensaje");
                eliminarFichaViveroInfo(viveroInfo);
            }
            else {
                Helpers.NotificationHelper.showError(respuestaInfo.Mensaje, "Error");
            }
        }
    </script>
    <!-- Fin de [Viveros] -->

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >
            <div style="width:15%;" >
                <input id="btnAgregarVivero" type="button" class="button btnAgregar" value="Agregar" style="margin-bottom:10px; width:100px; display:none" />
            </div>
            <div id="listadoViveros" ></div>
        </div>

    </div>

    <!-- Dialogo Vivero Info -->
    <div id="dialogViveroInfo" style="display:none" >
        <input type="hidden" id="hdnViveroInfo_Id" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="30%" /> 
                <col width="70%" />                
            </colgroup>
            <tr>
                <td>
                    <label>Usuario:</label>
                </td>
                <td> 
                    <select id="selViveroInfo_Usuario" ></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Dirección:</label>
                </td>
                <td> 
                    <input id="txtViveroInfo_Direccion" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Teléfono:</label>
                </td>
                <td> 
                    <input id="txtViveroInfo_Telefono" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>E-mail:</label>
                </td>
                <td>
                    <input id="txtViveroInfo_Email" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Web:</label>
                </td>
                <td>
                    <input id="txtViveroInfo_Web" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Imagen:</label>
                </td>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="30%" /> 
                            <col width="70%" />                
                        </colgroup>
                        <tr>
                            <td>
                                <input id="btnViveroInfoSubirImagen" class="button btnSubirImagen" type="button" value="Subir" /> 
                            </td>
                            <td>
                                <img id="imgViveroInfo_Imagen" src="" width="50px" height="40px" class="foto" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <!-- Fin de [Dialogo Vivero Info] -->

</asp:Content>