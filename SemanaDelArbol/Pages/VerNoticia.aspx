﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="VerNoticia.aspx.cs" Inherits="SemanaDelArbol.Pages.VerNoticia" %>
 
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >
    
    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idNoticia;
        $(document).ready(function () {
            obtenerUsuarioLogueado(cargarPagina_DetalleNoticia);

            var currentPage = $(location).attr('href');
            $('#twitterButton').attr("data-url", currentPage);
            $('#gplusButton').attr("data-href", currentPage);
            $('#fbButton').attr("data-href", currentPage);
        });
        function cargarPagina_DetalleNoticia() {
            idNoticia = getQuerystring("idNoticia");
            obtenerDatosNoticia(idNoticia);
        }

        function obtenerDatosNoticia(idNoticia) {
            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("obtenerNoticia", "{'idNoticia':'" + idNoticia + "'}", obtenerDatosNoticia_Response);
        }
        function obtenerDatosNoticia_Response(datos) {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado) {
                var noticiaInfo = respuestaInfo.Valor;

                var panelNoticia = $("#pnlNoticia");
                
                var encabezado = "<div class='encabezado'>" +
                                    "<h1>" + noticiaInfo.Titulo + "</h1>" +
                                    "<p class='fecha'> " + noticiaInfo.Fecha + "</p>" +
                                    "<p>" + noticiaInfo.Resumen + "</p>";
                                    "</div>";                
                
                var divImagenes = "<div class='imagenes' style='height: 270px;'><ul class='pgwSliderImagenes'>";
                if (!isEmpty(noticiaInfo.Foto1))
                    divImagenes += "<li><img src='.." + noticiaInfo.Foto1 + "'></li>";
                else
                    divImagenes += "<li><img src='../Images/Logo.gif'></li>";
                if (!isEmpty(noticiaInfo.Foto2))
                    divImagenes += "<li><img src='.." + noticiaInfo.Foto2 + "'></li>";
                if (!isEmpty(noticiaInfo.Foto3))
                    divImagenes += "<li><img src='.." + noticiaInfo.Foto3 + "'></li>";
                if (!isEmpty(noticiaInfo.Foto4))
                    divImagenes += "<li><img src='.." + noticiaInfo.Foto4 + "'></li>";
                if (!isEmpty(noticiaInfo.Foto5))
                    divImagenes += "<li><img src='.." + noticiaInfo.Foto5 + "'></li>";
                if (!isEmpty(noticiaInfo.Foto6))
                    divImagenes += "<li><img src='.." + noticiaInfo.Foto6 + "'></li>";
                divImagenes += "</ul></div>";
                              

                var divTexto = "<div class='texto-noticia'><span>" + noticiaInfo.Texto + "</span></div>";

                panelNoticia.append(encabezado);
                panelNoticia.append(divImagenes);
                panelNoticia.append(divTexto);

                $(".pgwSliderImagenes").pgwSlider({
                    displayList: false,
                    displayControls: true,
                    maxHeight: 270,
                    touchControls: true,
                    verticalCentering: true
                });

                generarContenidoSocial_Noticia(noticiaInfo);
            }
        }

    

    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >
            <div class="seccion" >
                <div class="titulo" >
                    Noticia
                </div>
                <div id="pnlNoticia" class="content-noticia" >                   
         
                     <script>
                                 (function (d, s, id) {
                                     var js, fjs = d.getElementsByTagName(s)[0];
                                     if (d.getElementById(id)) return;
                                     js = d.createElement(s); js.id = id;
                                     js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.4";
                                     fjs.parentNode.insertBefore(js, fjs);
                                 }(document, 'script', 'facebook-jssdk'));

                    </script>                
                    <div id="fb-root"></div>                                
                    <div id="fbButton" class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button"></div>
                    <div style='display:inline; width:80px;' ><a id='twitterButton' href='https://twitter.com/share' class='twitter-share-button' data-url="window.location.href" data-count='none' data-via='semanadelarbol' data-lang='en' >Twitear</a></div>    
                    <div id="gplusButton" class="g-plus" data-action="share" data-annotation="none" data-height="20" data-href="http://www.semanadelarbol.org" ></div>
                </div>                    
              </div>
            </div>      
        </div>     
</asp:Content>