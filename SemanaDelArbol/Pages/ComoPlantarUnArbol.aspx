﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="ComoPlantarUnArbol.aspx.cs" Inherits="SemanaDelArbol.Pages.ComoPlantarUnArbol" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!-- ComoPlantarUnArbol Styles -->
    <style type="text/css" >
        .pasoCirculo
        {
            width:                  65px;
            height:                 65px;

            border-radius:          99px;
            -moz-border-radius:     99px;
            -webkit-border-radius:  99px;
            -khtml-border-radius:   99px;
            background:             #686970;
            
            /*border:                 6px solid #959AC2; */ /* Violeta */
            border:                 6px solid #f6a828; /* Naranja */
            
            -webkit-box-shadow:     #A1A1A1 6px 6px 6px;
            -moz-box-shadow:        #A1A1A1 6px 6px 6px;
            box-shadow:             #A1A1A1 6px 6px 6px;
        }
        .pasoNumero
        {
            text-align:     center;
            font-family:    Tahoma;
            font-size:      50px;
            font-weight:    bold;
            color:          #fff;
            margin-top: -13px;
            text-shadow:    1px 1px 1px #111;
            filter:         dropshadow(color=#111, offx=1, offy=1);
        }
        .pasoTexto
        {
            font-size:      15px;
            font-family:    Tahoma;
            color:          #686970;
        }
        .pasoImagen
        {
            padding:                0px 0px 0px 0px;
            -webkit-box-shadow:     #A1A1A1 6px 6px 6px;
            -moz-box-shadow:        #A1A1A1 6px 6px 6px;
            box-shadow:             #A1A1A1 6px 6px 6px;            

            /*
            width: 130px;
            height: 120px;
            */
            width: 100px;
            height: 90px;

            /*
            border-radius:          45px;
            -moz-border-radius:     45px;
            -webkit-border-radius:  45px;
            -khtml-border-radius:   45px;
            */
            background:             #686970;
            
            /*border:                 6px solid #959AC2; */ /* Violeta */
            border:                 6px solid #f6a828; /* Naranja */

            -webkit-box-shadow:     #A1A1A1 6px 6px 6px;
            -moz-box-shadow:        #A1A1A1 6px 6px 6px;
            box-shadow:             #A1A1A1 6px 6px 6px;
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_ComoPlantarUnArbol);
        });
        function cargarPagina_ComoPlantarUnArbol()
        {
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <!-- Como Plantar Un Árbol -->
            <div>
                <div style="float:left; width:100%" >
                    <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                        <legend class="ui-widget-header" style="font-size:18px; padding:5px;" align="center" >Pasos para plantar un árbol</legend>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                            <colgroup>
                                <col width="9%" /> 
                                <col width="28%" />                
                                <col width="13%" /> 
                                <col width="9%" /> 
                                <col width="28%" />                
                                <col width="13%" /> 
                            </colgroup>
                            <tr>
                                <td align="left" >
                                    <div class="pasoCirculo" >
                                        <div class="pasoNumero" >1</div>
                                    </div>
                                </td>
                                <td>
                                    <div style="padding:0px 10px 0px 10px" >
                                        <label class="pasoTexto" >Aflojar la tierra en una superficie de 40x40 centímetros de profundidad.</label>
                                    </div>
                                </td>
                                <td>
                                    <img src="../Images/PasosPlantacion/1.png" alt="Paso 1" class="pasoImagen" width="165" height="145" />
                                </td>
                                <td align="center" >
                                    <div class="pasoCirculo" >
                                        <div class="pasoNumero" >2</div>
                                    </div>
                                </td>
                                <td>
                                    <div style="padding:0px 10px 0px 10px" >
                                        <label class="pasoTexto" >En el centro abrir un hoyo tan ancho y profundo como la raíz de la planta.</label>
                                    </div>
                                </td>
                                <td>
                                    <img src="../Images/PasosPlantacion/2.png" alt="Paso 2" class="pasoImagen" width="165" height="145" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" >
                                    <div class="pasoCirculo" >
                                        <div class="pasoNumero" >3</div>
                                    </div>
                                </td>
                                <td>
                                    <div style="padding:0px 10px 0px 10px" >
                                        <label class="pasoTexto" >Quitar la bolsa y tomar la planta de la parte más baja del tallo con dos dedos.<br />No maltratarla ni tocar la raíz.</label>
                                        <br />
                                        <label class="pasoTexto" style="font-size:12px; font-style:italic" ><u>Consejo:</u></label>
                                        <label class="pasoTexto" style="font-size:12px; font-style:italic" >Después de cavar el pozo conviene llenarlo de agua y dejar que se infiltre totalmente, si es posible 2 veces; ya que esto favorece el desarrollo de raíces y un enraizamiento mas rápido.</label>
                                    </div>
                                </td>
                                <td>
                                    <img src="../Images/PasosPlantacion/3.png" alt="Paso 3" class="pasoImagen" width="165" height="145" />
                                </td>
                                <td align="center" >
                                    <div class="pasoCirculo" >
                                        <div class="pasoNumero" >4</div>
                                    </div>
                                </td>
                                <td>
                                    <div style="padding:0px 10px 0px 10px" >
                                        <label class="pasoTexto" >Poner en el hoyo, al ras del suelo.</label>
                                    </div>
                                </td>
                                <td>
                                    <img src="../Images/PasosPlantacion/4.png" alt="Paso 4" class="pasoImagen" width="165" height="145" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" >
                                    <div class="pasoCirculo" >
                                        <div class="pasoNumero" >5</div>
                                    </div>
                                </td>
                                <td>
                                    <div style="padding:0px 10px 0px 10px" >
                                        <label class="pasoTexto" >Cubrir la raíz de la planta con la tierra.</label>
                                    </div>
                                </td>
                                <td>
                                    <img src="../Images/PasosPlantacion/5.png" alt="Paso 5" class="pasoImagen" width="165" height="145" />
                                </td>
                                <td align="center" >
                                    <div class="pasoCirculo" >
                                        <div class="pasoNumero" >6</div>
                                    </div>
                                </td>
                                <td>
                                    <div style="padding:0px 10px 0px 10px" >
                                        <label class="pasoTexto" >Compactar la tierra con las manos. No debe quedar muy apretada ni muy floja.</label>
                                    </div>
                                </td>
                                <td>
                                    <img src="../Images/PasosPlantacion/6.png" alt="Paso 6" class="pasoImagen" width="165" height="145" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" >
                                    <div class="pasoCirculo" >
                                        <div class="pasoNumero" >7</div>
                                    </div>
                                </td>
                                <td>
                                    <label class="pasoTexto" >Compactar la tierra con las manos. No debe quedar muy apretada ni muy floja.</label>
                                </td>
                                <td>
                                    <img src="../Images/PasosPlantacion/7.png" alt="Paso 7" class="pasoImagen" width="165" height="145" />
                                </td>
                                <td colspan="3" align="center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
            <!-- Fin de [Como Plantar Un Árbol] -->

        </div>
       
    </div>

</asp:Content>