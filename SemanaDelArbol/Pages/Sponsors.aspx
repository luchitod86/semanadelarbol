﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Sponsors.aspx.cs" Inherits="SemanaDelArbol.Pages.Sponsors" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Sponsors);
        });
        function cargarPagina_Sponsors()
        {
            if ( verificarUsuarioLogueado() )
            {
                if (validarPerfilAdministrador(_usuarioLogueado))
                {
                    $("#btnAgregarSponsor").click( function() { agregarSponsor(); } );
                    $("#btnAgregarSponsor").fadeIn();
                }
            }
            
            obtenerListaSponsors();

            inicializarDialogoSponsorInfo();
            inicializarFileUpload("btnSponsorInfoSubirImagen", "imgSponsorInfo_Imagen");
        }
        function agregarSponsor()
        {
            abrirSponsorInfoDialog();
        }
        function obtenerListaSponsors()
        {
            var params = new Object();
                params.periodo  = _filtroActual.Periodo;
                params.pais     = _filtroActual.Pais.Nombre;
                params.idPais   = _filtroActual.Pais.Id;
            var jsonParams = JSON.stringify(params);
            Helpers.AJAXHelper.doAjaxCall("obtenerSponsors", jsonParams, obtenerListaSponsors_Respuesta);

            $("#pnlCargandoSponsors").fadeIn();
        }
        function obtenerListaSponsors_Respuesta(datos)
        {
            var respuesta = jQuery.parseJSON(datos.d);
            if ( respuesta.Resultado )
            {
                var sponsorsInfo = respuesta.Valor;
                for(var i=0; i < sponsorsInfo.length; i++)
                {
                    agregarFichaSponsorInfo( sponsorsInfo[i] );
                }
            }
            $("#pnlCargandoSponsors").fadeOut();
        }
    </script>

    <!-- Diálogo Sponsor -->
    <script type="text/javascript">
        function inicializarDialogoSponsorInfo()
        {
            var sponsorInfoOptions =
            {
                title: "Agregar Sponsor",
                width: _dialogos.SponsorInfo.Width,
                height: _dialogos.SponsorInfo.Height,
                buttons:
                [
                    { id: "btnCancelSponsorInfoDialog", text: "Cancelar", click: cerrarSponsorInfoDialog },
                    { id: "btnConfirmSponsorInfoDialog", text: "Aceptar", click: guardarSponsorInfo }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.SponsorInfo.Id, sponsorInfoOptions);

            var tipos = getObjectKeys(_sponsorTipos);
            for (var i = 0; i < tipos.length; i++) {
                var sponsorTipo = _sponsorTipos[tipos[i]];
                Helpers.SelectListHelper.appendOption("selSponsorInfo_Tipo", sponsorTipo.Id, sponsorTipo.Nombre);
            }
        }
        function limpiarDatosSponsorInfoDialog()
        {
            $("#selSponsorInfo_Tipo").setValue(null);
            $("#hdnSponsorInfo_Id").setValue(null);
            $("#txtSponsorInfo_Nombre").setValue(null);
            $("#txtSponsorInfo_Web").setValue(null);
            $("#imgSponsorInfo_Imagen").attr("src", "");

            desactivarMensajesDeError("#selSponsorInfo_Tipo, #txtSponsorInfo_Nombre, #txtSponsorInfo_Web");

            Helpers.SelectListHelper.disableOptionsFromSelectList("selSponsorInfo_Tipo", [1], false);
        }
        function abrirSponsorInfoDialog(sponsorInfo)
        {
            limpiarDatosSponsorInfoDialog();
            if (!isEmpty(sponsorInfo))
            {
                $("#hdnSponsorInfo_Id").setValue(sponsorInfo.Id);
                $("#selSponsorInfo_Tipo").setValue(sponsorInfo.IdTipo);
                $("#txtSponsorInfo_Nombre").setValue(sponsorInfo.Nombre);
                $("#txtSponsorInfo_Web").setValue(sponsorInfo.Web);
                var imgSrc = "";
                if (!isEmpty(sponsorInfo.Imagen))
                    imgSrc = String.format("../Images/Sponsors/{0}s/{1}/", sponsorInfo.Tipo, _filtroActual.Pais.Id, sponsorInfo.Imagen);
                $("#imgSponsorInfo_Imagen").attr("src", imgSrc);
            }
            Helpers.DialogHelper.openDialog(_dialogos.SponsorInfo.Id);
        }
        function cerrarSponsorInfoDialog()
        {
            Helpers.DialogHelper.closeDialog(_dialogos.SponsorInfo.Id);
        }
        function obtenerSponsorInfo(id, onResponseFunction)
        {
            var params = new Object();
                params.id = id;
            var jsonParams = JSON.stringify(params);

            var data = new Object();
                data.onResponseFunction = onResponseFunction;

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/obtenerSponsor", jsonParams, obtenerSponsorInfo_Respuesta, data);
        }
        function obtenerSponsorInfo_Respuesta(data, datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var sponsorInfo = respuestaInfo.Valor;
                if (!isEmpty(data.onResponseFunction))
                    data.onResponseFunction.call(this, sponsorInfo);
            }
            else
            {
                var excepcion = crearObjetoException(respuestaInfo.Mensaje, "obtenerSponsor", null);
                Helpers.NotificationHelper.showError(excepcion, "Error");
            }
        }
        function guardarSponsorInfo()
        {
            var infoValidacion =
                [
                    { campo: "selSponsorInfo_Tipo", validaciones: [{ operacion: _validaciones.Valor.DistintoDe, valor: null, mensaje: "Debe seleccionar un tipo." }] },
                    { campo: "txtSponsorInfo_Nombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre." }] },
                    { campo: "txtSponsorInfo_Web", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar una web." }] }
                ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            cerrarSponsorInfoDialog();

            var id = $("#hdnSponsorInfo_Id").val();

            var sponsorInfo = new Object();
                sponsorInfo.id = (id.length == 0 ? -1 : id);
                sponsorInfo.idTipo = $("#selSponsorInfo_Tipo").getValue();
                sponsorInfo.nombre = $("#txtSponsorInfo_Nombre").getValue();
                sponsorInfo.web = $("#txtSponsorInfo_Web").getValue();
                sponsorInfo.idPais = _filtroActual.Pais.Id;
                sponsorInfo.imagen = getBase64Image("imgSponsorInfo_Imagen");

            var parametros = JSON.stringify(sponsorInfo);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/guardarSponsor", parametros, guardarSponsorInfo_Respuesta);
        }
        function guardarSponsorInfo_Respuesta(datos)
        {
            var respuesta = jQuery.parseJSON(datos.d);
            if (respuesta.Resultado)
            {
                var sponsorInfo = respuesta.Valor;
                Helpers.NotificationHelper.showMessage(respuesta.Mensaje, "", "", null);
                agregarFichaSponsorInfo(sponsorInfo);
            }
            else
            {
                Helpers.NotificationHelper.showWarning(respuesta.Mensaje);
            }
        }
        function agregarFichaSponsorInfo(sponsorInfo)
        {
            var id = sponsorInfo.Id;

            var btnEditar = '<input id="btnEditarSponsor_' + id + '" type="button" class="button btnEditar" value="Editar" style="display:none;" >';
            var btnEliminar = '<input id="btnEliminarSponsor_' + id + '" type="button" class="button btnEliminar" value="Eliminar" style="display:none;" >';

            var ficha = $("#sponsorId_" + id);
            if (ficha.length > 0)
            {
                ficha.empty();
            }
            else
            {
                ficha = $("<div />").attr("id", "sponsorId_" + id).css("margin-bottom", "15px");
                $("#listadoSponsors").append(ficha);
            }

            var contenido = '';
                contenido += '  <fieldset class="ui-widget ui-widget-content ui-corner-all" style="padding:20px" >';
                contenido += '      <table border="0" cellpadding="0" cellspacing="0" width="100%" >';
                contenido += '          <colgroup>';
                contenido += '              <col width="15%" />';
                contenido += '              <col width="40%" />';
                contenido += '              <col width="30%" />';
                contenido += '              <col width="15%" />';
                contenido += '          </colgroup>';
                contenido += '          <tr>';
                contenido += '              <td colspan="2" >';
                contenido += '                  <label style="font-weight:bold; text-decoration:underline; text-transform:uppercase" >' + sponsorInfo.Nombre + '</label>';
                contenido += '              </td>';
                contenido += '              <td rowspan="8" >';
                contenido += '                  <img id="imgSponsorId_' + id + '" class="imgSponsor" src="" alt="' + sponsorInfo.Usuario + '" style="border: 1px solid #CCE2C4;" />';
                contenido += '              </td>';
                contenido += '              <td rowspan="8" >';
                contenido += btnEditar + btnEliminar;
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td colspan="2" >';
                contenido += '                  &nbsp;';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '          <tr>';
                contenido += '              <td>';
                contenido += '                  <label>Web:</label>';
                contenido += '              </td>';
                contenido += '              <td>';
                contenido += '                  <a href="' + parseUrlAddress(sponsorInfo.Web) + '" target="_blank" >' + sponsorInfo.Web + '</a>';
                contenido += '              </td>';
                contenido += '          </tr>';
                contenido += '      </table>';
                contenido += '  </fieldset>';

            ficha.append(contenido);

            var imgSrc = "../Images/NoImage.gif";
            if (!isEmpty(sponsorInfo.Imagen))
                imgSrc = String.format("../Images/Sponsors/{0}s/{1}/{2}", sponsorInfo.Tipo, sponsorInfo.IdPais, sponsorInfo.Imagen);
            $("#imgSponsorId_" + id).attr("src", imgSrc);

            if (verificarUsuarioLogueado())
            {
                if (validarPerfilAdministrador(_usuarioLogueado))
                {
                    $("input[type=button]").button();
                    $("#btnEditarSponsor_" + id)
                        .click(function () { editarSponsor(id); })
                        .fadeIn();
                    $("#btnEliminarSponsor_" + id)
                        .click(function () { eliminarSponsor(id); })
                        .fadeIn();
                }
            }
        }
        function eliminarFichaSponsorInfo(sponsorInfo)
        {
            var ficha = $("#sponsorId_" + sponsorInfo.Id);
                ficha.remove();
        }
        function editarSponsor(idSponsor)
        {
            obtenerSponsorInfo(idSponsor, editarSponsor_Respuesta);
        }
        function editarSponsor_Respuesta(sponsorInfo)
        {
            if (!isEmpty(sponsorInfo))
                abrirSponsorInfoDialog(sponsorInfo);
        }
        function eliminarSponsor(idSponsor)
        {
            var onEliminarSponsor_Confirm = overloadFunction(onEliminarSponsor_Confirmacion, idSponsor);
            Helpers.NotificationHelper.showConfirm("¿Desea eliminar este Sponsor?", "Eliminar", null, onEliminarSponsor_Confirm);
        }
        function onEliminarSponsor_Confirmacion(idSponsor)
        {
            var params = new Object();
                params.id = idSponsor;
            var jsonParams = JSON.stringify(params);

            Helpers.AJAXHelper.doAjaxCall("../Default.aspx/eliminarSponsor", jsonParams, onEliminarSponsor_Response);
        }
        function onEliminarSponsor_Response(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var sponsorInfo = respuestaInfo.Valor;
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje, "Mensaje");
                eliminarFichaSponsorInfo(sponsorInfo);
            }
            else
            {
                var excepcion = crearObjetoException(respuestaInfo.Mensaje, "eliminarSponsor", null);
                Helpers.NotificationHelper.showError(excepcion, "Error");
            }
        }
    </script>
    <!-- Fin de [ Diálogo Sponsors] -->

    <!--HTML-->
    <div id="content" >

        <div id="pnlPrincipal" >

            <div style="width:15%;" >
                <input id="btnAgregarSponsor" type="button" class="button btnAgregar" value="Agregar" style="display:none" />
            </div>
            <br />

            <div id="listadoSponsors" >
                <div>
                    <label class="tituloSeccion" >Sponsors</label>&nbsp;
                    <span id="pnlCargandoSponsors" style="display:none" >
                        <img src="../Images/Procesando.gif" alt="Cargando sponsors.." />
                        <label>Cargando..</label>
                    </span>
                </div>
                <br />
            </div>
        </div>

    </div>

    <!-- Dialogo Sponsors Info -->
    <div id="dialogSponsorInfo" style="display:none" >
        <input type="hidden" id="hdnSponsorInfo_Id" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
            <colgroup>
                <col width="30%" /> 
                <col width="70%" />                
            </colgroup>
            <tr>
                <td>
                    <label>Tipo:</label>
                </td>
                <td>
                    <select id="selSponsorInfo_Tipo" ></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Nombre:</label>
                </td>
                <td>
                    <input id="txtSponsorInfo_Nombre" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Web:</label>
                </td>
                <td>
                    <input id="txtSponsorInfo_Web" type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Imagen:</label>
                </td>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="30%" /> 
                            <col width="70%" />                
                        </colgroup>
                        <tr>
                            <td>
                                <input id="btnSponsorInfoSubirImagen" class="button btnSubirImagen" type="button" value="Subir" /> 
                            </td>
                            <td>
                                <img id="imgSponsorInfo_Imagen" src="" width="50px" height="40px" class="foto" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <!-- Fin de [Dialogo Sponsors Info] -->

</asp:Content>