﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Participar.aspx.cs" Inherits="SemanaDelArbol.Pages.Participar" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >



        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Participar);
            //$(".participarTip").hover( function() { $(this).addClass("ui-state-highlight"); }, function() { $(this).removeClass("ui-state-highlight");  } )
        });
        function cargarPagina_Participar()
        {
          /* configurarEditorContenido(id_editor_contenido_descripcion_participar);

            if (validarPerfilAdministrador(_usuarioLogueado) || validarPerfilEditor(_usuarioLogueado))
            {
                $("#btnGuardar").click(function () { guardarContenido_Participar(); });
                $("#pnlGuardar").css({ "display": "inline-block" }).fadeIn();
            }*/
        }
        function guardarContenido_Participar()
        {
            var contenido_descripcion_participar = obtenerValorEditorContenido(id_editor_contenido_descripcion_participar);
            guardarSeccionContenido(_secciones.Participar, "Descripcion_Participar", contenido_descripcion_participar, null);
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlGuardar" style="display:none; width:100%" >
            <span style="float:right" >
                <input id="btnGuardar" type="button" class="button btnGuardar" value="Guardar" style="width:100px;" />
            </span>
        </div>

        <div id="pnlPrincipal" class="page-participar" >
            <!-- ¿Cómo participar? -->
            <div id="pnlParticipar" >
                    <h1>¿Cómo participar?</h1>
                    <h3>¿Cómo son las formas de participar en el portal y en Semana del Árbol?</h3>
                    <div class="row">
                        <div class="bloque-participar col-sm-6">
                            <img src="../Images/ComoParticipar-Individuo.png"/>
                            <h4>Como individuo</h4>
                            <span class="texto">Plantando y/o adoptando un árbol, para esto es necesario acercarse al sitio más próximo de tu localidad o ciudad que se puede ubicar en <span class="resaltado">el mapa interactivo de la página de la Semana del Árbol.</span> Después de sembrarlo puedes indicar la dirección, especie exacta y subir una foto en el mapa del portal.</span>
                        </div>
                        <div class="bloque-participar col-sm-6">
                            <img src="../Images/ComoParticipar-Icon-Donante.png"/>
                            <h4>Como donante</h4>
                            <span class="texto">Si posees un vivero o eres una persona independiente que produce o tiene plantas nativas para donar, tus datos y disponibilidad de árboles pueden aparecer en la página de la Semana del Árbol para que las personas se acerquen donde te encuentras y retiren la donación.</span>
                        </div>
                    </div>
                    <div class="row">                        
                        <div class="bloque-participar col-sm-6">
                            <img src="../Images/ComoParticipar-institucion.png"/>
                            <h4>Como institución</h4>
                            <span class="texto">Si formas parte de <span class="resaltado">un municipio, una escuela, una ONG, un club, una empresa u otra institución,</span> puedes donar árboles, organizar una jornada de plantación abierta o adoptar árboles disponibles en la campaña. Todo esto lo puedes registrar utilizando el portal de la Semana del Árbol indicando el nombre de la institución, actividad y cantidad de especies plantadas, georreferenciándolas en el mapa con su respectivas fotos.</span>
                        </div>
                        <div class="bloque-participar col-sm-6">
                            <h4>Apoyando y difundiendo </h4>
                            <span class="texto">Si sos una organización o medio de comunicación, podés ayudar a difundir la Semana del Árbol a través de tus medios y contactos, o enviarnos un mail a info@semanadelarbol.org, para participar más activamente</span>
                        </div>
                    </div>                                       
                </div>          
            <!-- Fin de [¿Cómo participar?] -->
        </div>
    </div>
</asp:Content>