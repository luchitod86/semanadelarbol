﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Provincias.aspx.cs" Inherits="SemanaDelArbol.Pages.Provincias" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idPais = null;
        var idTabla = "tblProvincias";
        var totalProvincias = 0;
        $(document).ready(function ()
        {
            obtenerUsuarioLogueado(cargarPagina_Provincias);            
        });
        function cargarPagina_Provincias()
        {
            idPais = getQuerystring("idPais");
            obtenerProvincias_Admin(idPais);

            inicializarDialogProvinciaInfo();

            $("#btnAgregarProvincia").click(function()
            {
                limpiarDatosProvinciaInfoDialog();
                abrirProvinciaInfoDialog("Agregar provincia");
            });
        }
        function obtenerProvincias_Admin(idPais)
        {
            var params = new Object();
                params.idPais = idPais;
            var jsonParams = JSON.stringify(params);

            $("#pnlCargandoProvincias").fadeIn();
            Helpers.AJAXHelper.doAjaxCall("obtenerProvincias", jsonParams, obtenerProvincias_Admin_Respuesta);
        }
        function obtenerProvincias_Admin_Respuesta(datos)
        {
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                var provinciasInfo = respuestaInfo.Valor;
                if ( !isEmpty(provinciasInfo) )
                {
                    $("#tituloSeccion").html(String.format("Administración de Provincia(s) de <b>{0}</b>", provinciasInfo[0].Pais));
                    cargarProvincias(provinciasInfo);
                }
            }
            $("#pnlCargandoProvincias").fadeOut('400', function () { $("#" + idTabla).slideDown(); });
        }
        function cargarProvincias(provinciasInfo)
        {
            Helpers.HtmlHelper.Table.deleteAllRows(idTabla);
            for (var i = 0; i < provinciasInfo.length; i++)
            {
                crearFilaProvincia(provinciasInfo[i]);
            }
        }
        function crearFilaProvincia(provinciaInfo, insertFirst)
        {
            var idProvincia = provinciaInfo.Id;

            var btnEditar   = Helpers.HtmlHelper.Button.create("btnEditarProvincia_{0}".format(idProvincia), "Editar", "btnEditar")
            var btnEliminar = Helpers.HtmlHelper.Button.create("btnEliminarProvincia_{0}".format(idProvincia), "Eliminar", "btnEliminar");

            var fila  = '<td>' + btnEditar + btnEliminar + '</td>';
                fila += '<td>' + provinciaInfo.Nombre + '</td>';
                Helpers.HtmlHelper.Table.addNewRow(idTabla, fila, "idProvincia", idProvincia, insertFirst);

            var btnEditar = $("#btnEditarProvincia_{0}".format(idProvincia));
                btnEditar.click(function() { editarProvincia(idProvincia) });
                btnEditar.button();

            var btnEliminar = $("#btnEliminarProvincia_{0}".format(idProvincia));
                btnEliminar.click(function () { eliminarProvincia(idProvincia) });
                btnEliminar.button();

            actualizarTotalProvincias(+1);
        }
        function actualizarTotalProvincias(incremento)
        {
            totalProvincias = totalProvincias + incremento;
            $("#lblTotalProvincias").html("Total: <b>{0}</b> provincias".format(totalProvincias));
        }
        function actualizarFilaProvincia(provinciaInfo)
        {
            var fila = findElementByAttribute("tr", "idprovincia", provinciaInfo.Id);
            if (fila.length == 0)
                crearFilaProvincia(provinciaInfo, true);
            else
                $(fila.find("td")[1]).html(provinciaInfo.Nombre);
            fila = findElementByAttribute("tr", "idprovincia", provinciaInfo.Id);
            scrollTo(fila, 100, 1000);
        }
        function removeEstadoActivoBotones(idProvincia)
        {
            $("#btnEditarProvincia_{0}".format(idProvincia)).removeClass(UI_States.Focus);
            $("#btnEliminarProvincia_{0}".format(idProvincia)).removeClass(UI_States.Focus);
        }
    </script>
    
    <!-- CRUD -->
    <script type="text/javascript" >
        function editarProvincia(idProvincia)
        {
            limpiarDatosProvinciaInfoDialog();
            Helpers.CRUDHelper.Get("../Default.aspx/obtenerProvincia", "{'idProvincia':'{0}' }".format(idProvincia), editarProvincia_Response);
            abrirProvinciaInfoDialog("Editar provincia");
        }
        function editarProvincia_Response(responseInfo)
        {
            var provinciaInfo = responseInfo.Valor;
            $("#hdnProvinciaInfo_Id").setValue(provinciaInfo.Id);
            $("#txtProvinciaInfo_Nombre").setValue(provinciaInfo.Nombre);
            removeEstadoActivoBotones(provinciaInfo.Id);
        }
        function guardarProvincia()
        {
            var infoValidacion =
                [
                    { campo: "txtProvinciaInfo_Nombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre." }] }
                ];
            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            cerrarProvinciaInfoDialog();

            var id = $("#hdnProvinciaInfo_Id").getValue();
            var provinciaInfo           = new Object();
                provinciaInfo.id        = isEmpty(id) ? -1 : id;
                provinciaInfo.idPais    = idPais;
                provinciaInfo.nombre    = $("#txtProvinciaInfo_Nombre").getValue();
            var jsonParams = JSON.stringify(provinciaInfo);

            Helpers.CRUDHelper.Save("../Default.aspx/guardarProvincia", jsonParams, guardarProvincia_Response, null, "Guardar datos", "¿Desea guardar los datos?");
        }
        function guardarProvincia_Response(responseInfo)
        {
            var provinciaInfo = responseInfo.Valor;
            actualizarFilaProvincia(provinciaInfo);
            Helpers.NotificationHelper.showMessage(responseInfo.Mensaje);
        }
        function eliminarProvincia(idProvincia)
        {
            Helpers.CRUDHelper.Delete("../Default.aspx/eliminarProvincia", idProvincia, eliminarProvincia_Response, null, "Eliminar provincia", "¿Desea eliminar esta provincia?");
            removeEstadoActivoBotones(idProvincia);
        }
        function eliminarProvincia_Response(responseInfo)
        {
            var provinciaInfo = responseInfo.Valor;
            Helpers.HtmlHelper.Table.deleteRow(idTabla, "idProvincia", provinciaInfo.Id);
            actualizarTotalProvincias(-1);
            Helpers.NotificationHelper.showMessage(responseInfo.Mensaje);
        }
    </script>
    <!-- Fin de [CRUD] -->

    <!-- Dialog Provincia Info -->
    <script type="text/javascript" >
        function inicializarDialogProvinciaInfo()
        {
            var provinciaInfoOptions =
            {
                title: "Provincia",
                width: _dialogos.ProvinciaInfo.Width,
                height: _dialogos.ProvinciaInfo.Height,
                buttons:
                [
                    { id: "btnCancelProvinciaInfoDialog", text: "Cancelar", click: cerrarProvinciaInfoDialog },
                    { id: "btnConfirmProvinciaInfoDialog", text: "Aceptar", click: guardarProvincia }
                ]
            };
            Helpers.DialogHelper.initializeDialog(_dialogos.ProvinciaInfo.Id, provinciaInfoOptions);
        }
        function abrirProvinciaInfoDialog(titulo)
        {
            Helpers.DialogHelper.openDialog(_dialogos.ProvinciaInfo.Id);
            Helpers.DialogHelper.setTitle(_dialogos.ProvinciaInfo.Id, titulo);
        }
        function cerrarProvinciaInfoDialog()
        {
            Helpers.DialogHelper.closeDialog(_dialogos.ProvinciaInfo.Id);
        }
        function limpiarDatosProvinciaInfoDialog()
        {
            $("#hdnProvinciaInfo_Id").setValue(null);
            $("#txtProvinciaInfo_Nombre").setValue(null);
            desactivarMensajesDeError("#txtProvinciaInfo_Nombre");
        }
    </script>
    <!-- Fin de [Dialog ProvinciaInfo] -->

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <div class="seccion" >
                <div class="titulo" id="tituloSeccion" >
                    Administración de Provincias
                </div>
            </div>

            <input type="button" id="btnAgregarProvincia" class="button btnAgregar" value="Agregar" />
            <label id="lblTotalProvincias" >Total 0 provincias</label>

            <span id="pnlCargandoProvincias" style="display:none" >
                <img src="../Images/Procesando.gif" alt="Cargando provincias.." />
                <label>Cargando..</label>
            </span>

            <table id="tblProvincias" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none" >
                <colgroup>
                    <col width="25%" /> <!-- Acción -->
                    <col width="75%" /> <!-- Nombre -->
                </colgroup>
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>Nombre</th>
                    </tr>
                </thead>
                <!--
                <tbody>
                </tbody>
                -->
            </table>

        </div>

        <!-- Dialogo Provincia Info -->
        <div id="dialogProvinciaInfo" style="display:none" >
            <input type="hidden" id="hdnProvinciaInfo_Id" />
            <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                <colgroup>
                    <col width="25%" />
                    <col width="65%" />               
                </colgroup>
                <tr>
                    <td>
                        <label>Nombre:</label>
                    </td>
                    <td> 
                        <input id="txtProvinciaInfo_Nombre" type="text" value="" />
                    </td>
                </tr>
            </table>
        </div>

    </div>

</asp:Content>