﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="Reportes.aspx.cs" Inherits="SemanaDelArbol.Pages.Reportes" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
        .outer
        {
            display: table;
            height: 100%;
            width: 100%;
        }
        .middle
        {
            display: table-cell;
            vertical-align: middle;
        }
        .inner
        {
            margin-left: auto;
            margin-right: auto; 
            width: 50%;
            text-align: center;
            background-color: white;
            border: 1px dashed #ccc
        }
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        var idPanelCargandoReporte      = "pnlCargandoReporte";

        var idGraficoReporte            = "chartReportes";
        var idTablaReporteUsuarios      = "tblReporteUsuarios";
        var idTablaReportePlantaciones  = "tblReportePlantaciones";
        var idTablaReporteDonaciones    = "tblReporteDonaciones";
        var idTablaReporteAdopciones    = "tblReporteAdopciones";
        var idTablaReporteActividades   = "tblReporteActividades";
        var idTablaReporteTotales       = "tblReporteTotales";

        var infoReportes  = [];
        var graficoActual = new Object();

        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Reportes);
        });
        function cargarPagina_Reportes()
        {
            Helpers.SelectListHelper.bindListOnChangeEvent("selReporte_Agrupamiento", onReporteAgrupamiento_Changed);
            Helpers.SelectListHelper.bindListOnChangeEvent("selReporte_Formato", onReporteFormato_Changed);

            infoReportes =
                [
                    { tipo: "Usuarios",     idTabla: idTablaReporteUsuarios,        url: "obtenerReporteUsuarios",      onResponse: crearFilaReporteUsuario },
                    { tipo: "Plantaciones", idTabla: idTablaReportePlantaciones,    url: "obtenerReportePlantaciones",  onResponse: crearFilaReportePlantacion },
                    { tipo: "Donaciones",   idTabla: idTablaReporteDonaciones,      url: "obtenerReporteDonaciones",    onResponse: crearFilaReporteDonacion },
                    { tipo: "Adopciones",   idTabla: idTablaReporteAdopciones,      url: "obtenerReporteAdopciones",    onResponse: crearFilaReporteAdopcion },
                    { tipo: "Actividades",  idTabla: idTablaReporteActividades,     url: "obtenerReporteActividades",   onResponse: crearFilaReporteActividad },
                    { tipo: "Totales",      idTabla: idTablaReporteTotales,         url: "obtenerReporteTotales",       onResponse: crearFilaReporteTotal }
                ];

            for (var i = 0; i < infoReportes.length; i++)
            {
                $("#btnReporte{0}".format(infoReportes[i].tipo)).click(function ()
                {
                    var id = $(this)[0].id;
                    var tipo = id.replace("btnReporte", "");
                    obtenerReporte(tipo, infoReportes);

                    var data = getElementInArray(infoReportes, "tipo", tipo);
                    Helpers.HtmlHelper.Table.addExportPanel(data.idTabla);

                    obtenerGrafico(tipo, infoReportes);
                });
            }
        }
        function obtenerGrafico(tipo, infoReportes)
        {
            var data = getElementInArray(infoReportes, "tipo", tipo);
            establecerOpcionesGraficoFormato();
            establecerOpcionesGraficoAgrupamiento(tipo);
            obtenerDatosGrafico(data);
        }
        function establecerOpcionesGraficoFormato()
        {
            var idLista = "selReporte_Formato";
            Helpers.SelectListHelper.removeAllOptions(idLista);
            Helpers.SelectListHelper.appendOption(idLista, "0", "Valor");
            Helpers.SelectListHelper.appendOption(idLista, "1", "Porcentaje");
        }
        function establecerOpcionesGraficoAgrupamiento(tipo)
        {
            var options = new Array();
            switch (tipo )
            {
                case "Usuarios":
                    options.push(["tipoUsuario", "Tipo de Usuario"]);
                    options.push(["provincia", "Provincia"]);
                    break;
                case "Plantaciones":
                    options.push(["tipoUsuario", "Tipo de Usuario"]);
                    options.push(["especie", "Especie"]);
                    break;
                case "Donaciones":
                    options.push(["tipoDonante", "Tipo de Donante"]);
                    options.push(["especie", "Especie"]);
                    break;
                case "Adopciones":
                    options.push(["tipoUsuario", "Tipo de Usuario"]);
                    options.push(["tipoDonante", "Tipo de Donante"]);
                    options.push(["especie", "Especie"]);
                    break;
                case "Actividades":
                    options.push(["tipoUsuario", "Tipo de Usuario"]);
                    options.push(["provincia", "Provincia"]);
                    break;
                case "Totales":
                    options.push(["pais", "País"]);
                    options.push(["usuarios", "Usuarios"]);
                    options.push(["plantaciones", "Plantaciones"]);
                    options.push(["adopciones", "Adopciones"]);
                    options.push(["donaciones", "Donaciones"]);
                    options.push(["donantes", "Donantes"]);
                    options.push(["actividades", "Actividades"]);
                    break;
            }

            var idLista = "selReporte_Agrupamiento"
            Helpers.SelectListHelper.removeAllOptions(idLista);
            for(var i=0; i < options.length; i++)
            {
                Helpers.SelectListHelper.appendOption(idLista, options[i][0], options[i][1]);
            }
        }
        function onReporteAgrupamiento_Changed(valor, texto)
        {
            var data = getElementInArray(infoReportes, "tipo", graficoActual.tipo);
            obtenerDatosGrafico(data);
        }
        function onReporteFormato_Changed(valor, texto)
        {
            graficoActual.porcentaje = valor;
            visualizarGrafico();
        }
        function obtenerDatosGrafico(data)
        {
            $("#{0}".format(idGraficoReporte)).empty();

            _showInlineLoadingMessage(idGraficoReporte, "Cargando gráfico..");

            graficoActual.agrupamiento  = $("#selReporte_Agrupamiento").getValue();
            graficoActual.tipo          = data.tipo;

            var params = new Object();
                params.idPais       = _filtroActual.Pais.Id;
                params.pais         = _filtroActual.Pais.Nombre;
                params.periodo      = _filtroActual.Periodo;
                params.tipo         = graficoActual.tipo.toLowerCase();
                params.agrupamiento = graficoActual.agrupamiento;
            var jsonParams = JSON.stringify(params);

            var onResponse_Fn = overloadFunction(obtenerDatosGrafico_Respuesta, data);

            Helpers.CRUDHelper.Get("Pages/Reportes.aspx/{0}".format("obtenerDatosGrafico"), jsonParams, onResponse_Fn);
        }
        function obtenerDatosGrafico_Respuesta(data, respuestaInfo)
        {
            if (respuestaInfo.Resultado)
            {
                var datos  = respuestaInfo.Valor.Datos;
                var data = new Array();
                for (var i = 0; i < datos.length; i++)
                {
                    data.push( [datos[i].Texto, datos[i].Valor] );
                }
                
                //graficoActual.tipo          = data.tipo;
                graficoActual.datos         = JSON.parse(JSON.stringify(data));
                graficoActual.titulo        = respuestaInfo.Valor.Titulo;
                graficoActual.porcentaje    = $("#selReporte_Formato").getValue() == 1 ? true : false;

                visualizarGrafico();
            }

            _hideInlineLoadingMessage(idGraficoReporte);
        }
        function visualizarGrafico()
        {
            initializeChart(idGraficoReporte, graficoActual.datos, graficoActual.porcentaje, graficoActual.titulo);
        }
    </script>

    <!-- Reportes -->
    <script type="text/javascript" >
        function obtenerReporte(tipo, datos)
        {
            var params = new Object();
                params.idPais   = _filtroActual.Pais.Id;
                params.periodo  = _filtroActual.Periodo;
            var jsonParams = JSON.stringify(params);

            var data = getElementInArray(infoReportes, "tipo", tipo);

            $("#{0}".format(idPanelCargandoReporte)).fadeIn();

            $("#lblTituloReporte").text("Reporte de {0}".format(tipo));

            $(".panelReporte").fadeOut();

            var onResponse_Fn = overloadFunction(obtenerReporte_Respuesta, data);

            Helpers.CRUDHelper.Get("Pages/Reportes.aspx/{0}".format(data.url), jsonParams, onResponse_Fn);
        }
        function obtenerReporte_Respuesta(data, respuestaInfo)
        {
            if (respuestaInfo.Resultado)
            {
                var datos = respuestaInfo.Valor;
                Helpers.HtmlHelper.Table.deleteAllRows(data.idTabla);
                for (var i = 0; i < datos.length; i++)
                {
                    data.onResponse(datos[i]);
                }

                if ( data.idTabla == idTablaReporteTotales )
                    crearFilaTotal_Footer(datos);
            }
            initializeTable(data.idTabla);

            $("#{0}".format(idPanelCargandoReporte)).fadeOut("slow", function () { $("#pnlReporte{0}".format(data.tipo)).fadeIn(); });
        }
        function crearFilaReporteUsuario(datos)
        {
            var tr = Helpers.HtmlHelper.Table.insertNewRow(idTablaReporteUsuarios, datos.Id);
            Helpers.HtmlHelper.Table.addCellData(tr, isEmpty(datos.Apellido) ? datos.Nombre : String.format("{0} {1}", datos.Nombre, datos.Apellido));
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Email, "email");
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Provincia == "Ciudad Autónoma de Buenos Aires" ? "C.A.B.A." : datos.Provincia);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Ciudad);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Tipo);
        }
        function crearFilaReportePlantacion(datos)
        {
            var tr = Helpers.HtmlHelper.Table.insertNewRow(idTablaReportePlantaciones, datos.Id);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Usuario);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.TipoUsuario);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Especie);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Cantidad);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Fecha, "", "data-sort-value", datos.Fecha);
        }
        function crearFilaReporteAdopcion(datos)
        {
            var tr = Helpers.HtmlHelper.Table.insertNewRow(idTablaReporteAdopciones, datos.Id);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Usuario);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.TipoUsuario);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Donante);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.TipoDonante);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Arbol);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Cantidad);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Fecha);
        }
        function crearFilaReporteDonacion(datos)
        {
            var tr = Helpers.HtmlHelper.Table.insertNewRow(idTablaReporteDonaciones, datos.Id);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Donante);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.TipoDonante);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Especie);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Cantidad);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Adoptados);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Disponibles);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Fecha);
        }
        function crearFilaReporteActividad(datos)
        {
            var tr = Helpers.HtmlHelper.Table.insertNewRow(idTablaReporteActividades, datos.Id);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Usuario);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.TipoUsuario);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Provincia == "Ciudad Autónoma de Buenos Aires" ? "C.A.B.A." : datos.Provincia);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Descripcion);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.FechaYHora);
        }
        function crearFilaReporteTotal(datos)
        {
            var tr = Helpers.HtmlHelper.Table.insertNewRow(idTablaReporteTotales, datos.Id);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Pais);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Usuarios);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Plantaciones);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Adopciones);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Donaciones);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Donantes);
            Helpers.HtmlHelper.Table.addCellData(tr, datos.Actividades);
        }
        function crearFilaTotal_Footer(datos)
        {
            var usuarios        = 0;
            var plantaciones    = 0;
            var adopciones      = 0;
            var donaciones      = 0;
            var donantes        = 0;
            var actividades     = 0;
            for (var i = 0; i < datos.length; i++)
            {
                usuarios        += datos[i].Usuarios;
                plantaciones    += datos[i].Plantaciones;
                adopciones      += datos[i].Adopciones;
                donaciones      += datos[i].Donaciones;
                donantes        += datos[i].Donantes;
                actividades     += datos[i].Actividades;
            }
            Helpers.HtmlHelper.Table.deleteFooterRow(idTablaReporteTotales);
            var tr = Helpers.HtmlHelper.Table.insertFooterRow(idTablaReporteTotales, datos.Id);
            Helpers.HtmlHelper.Table.addCellData(tr, "<b>TOTAL</b>");
            Helpers.HtmlHelper.Table.addCellData(tr, "<b>{0}</b>".format(usuarios));
            Helpers.HtmlHelper.Table.addCellData(tr, "<b>{0}</b>".format(plantaciones));
            Helpers.HtmlHelper.Table.addCellData(tr, "<b>{0}</b>".format(adopciones));
            Helpers.HtmlHelper.Table.addCellData(tr, "<b>{0}</b>".format(donaciones));
            Helpers.HtmlHelper.Table.addCellData(tr, "<b>{0}</b>".format(donantes));
            Helpers.HtmlHelper.Table.addCellData(tr, "<b>{0}</b>".format(actividades));
        }
    </script>

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" >

            <label class="titulo" >Seleccionar el tipo de reporte:</label><br />

            <div style="float:left; width:100%; margin-top:10px; margin-bottom:10px;"></div>

            <div style="float:left; width:100%;" >
                <div id="panelBotones" style="float:left; width:15%;" >
                    <input id="btnReporteUsuarios" type="button" class="button btnReporteUsuarios" value="Usuarios" style="width:140px" /><br />
                    <input id="btnReportePlantaciones" type="button" class="button btnReportePlantaciones" value="Plantaciones" style="width:140px" /><br />
                    <input id="btnReporteAdopciones" type="button" class="button btnReporteAdopciones" value="Adopciones" style="width:140px" /><br />
                    <input id="btnReporteDonaciones" type="button" class="button btnReporteDonaciones" value="Donaciones" style="width:140px" /><br />
                    <input id="btnReporteActividades" type="button" class="button btnReporteActividades" value="Actividades" style="width:140px" /><br />
                    <input id="btnReporteTotales" type="button" class="button" value="Estadísticas Totales" style="width:140px" /><br />
                </div>
                <div style="float:left; width:85%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="25%" />
                            <col width="75%" />
                        </colgroup>
                        <tr>
                            <td style="vertical-align:top; padding-left:20px; padding-right: 20px;" >
                                <label>Agrupar por:</label>
                                <select id="selReporte_Agrupamiento" >
                                </select>
                                <br /><br />
                                <label>Formato:</label>
                                <select id="selReporte_Formato" >
                                </select>
                            </td>
                            <td>
                                <div id="chartReportes" class="disabled" style="height:250px;width:100%;" ></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div style="float:left; width:100%; margin-top:10px; margin-bottom:10px;"></div>

            <div style="float:left; width:100%;">
                <label id="lblTituloReporte" class="titulo" ></label>&nbsp;
                <span id="pnlCargandoReporte" style="display:none" >
                    <img src="../Images/Procesando.gif" alt="Cargando reporte.." />
                    <label>Cargando reporte..</label>
                </span>
            </div>

            <div style="float:left; width:100%; margin-top:10px; margin-bottom:10px;"></div>

            <div class="panel" >

                <div id="pnlReporteTotales" class="panelReporte" style="display:none" >
                    <table id="tblReporteTotales" class="table" border="0" cellpadding="0" cellspacing="0" width="100%" >
                        <colgroup>
                            <col width="28%" /> <!-- País -->
                            <col width="12%" /> <!-- Usuarios -->
                            <col width="12%" /> <!-- Plantaciones -->
                            <col width="12%" /> <!-- Adopciones -->
                            <col width="12%" /> <!-- Donaciones -->
                            <col width="12%" /> <!-- Donantes -->
                            <col width="12%" /> <!-- Actividades -->
                        </colgroup>
                        <thead>
                            <tr>
                                <th data-sort="string-ins" >País</th>
                                <th data-sort="int" >Usuarios</th>
                                <th data-sort="int" >Plantaciones</th>
                                <th data-sort="int" >Adopciones</th>
                                <th data-sort="int" >Donaciones</th>
                                <th data-sort="int" >Donantes</th>
                                <th data-sort="int" >Actividades</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div id="pnlReporteUsuarios" class="panelReporte" style="display:none;" >
                    <table id="tblReporteUsuarios" class="table" border="0" cellpadding="0" cellspacing="0" width="100%"  >
                        <colgroup>
                            <col width="30%" /> <!-- Nombre -->
                            <col width="18%" /> <!-- Email -->
                            <col width="20%" /> <!-- Provincia -->
                            <col width="22%" /> <!-- Ciudad -->
                            <col width="10%" /> <!-- Tipo -->
                        </colgroup>
                        <thead>
                            <tr>
                                <th data-sort="string-ins" >Nombre</th>
                                <th data-sort="string-ins" >Email</th>
                                <th data-sort="string-ins" >Provincia</th>
                                <th data-sort="string-ins" >Ciudad</th>
                                <th data-sort="string-ins" >Tipo</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div id="pnlReportePlantaciones" class="panelReporte" style="display:none" >
                    <table id="tblReportePlantaciones" class="table" border="0" cellpadding="0" cellspacing="0" width="100%"  >
                        <colgroup>
                            <col width="25%" /> <!-- Usuario -->
                            <col width="20%" /> <!-- Tipo -->
                            <col width="15%" /> <!-- Especie -->
                            <col width="10%" /> <!-- Cantidad -->
                            <col width="10%" /> <!-- Fecha -->
                        </colgroup>
                        <thead>
                            <tr>
                                <th data-sort="string-ins" >Usuario</th>
                                <th data-sort="string-ins" >Tipo</th>
                                <th data-sort="string-ins" >Especie</th>
                                <th data-sort="int" >Cantidad</th>
                                <th data-sort="date" >Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div id="pnlReporteAdopciones" class="panelReporte" style="display:none" >
                    <table id="tblReporteAdopciones" class="table" border="0" cellpadding="0" cellspacing="0" width="100%"  >
                        <colgroup>
                            <col width="20%" /> <!-- Usuario -->
                            <col width="10%" /> <!-- Tipo -->
                            <col width="20%" /> <!-- Donante -->
                            <col width="10%" /> <!-- Tipo -->
                            <col width="20%" /> <!-- Especie -->
                            <col width="10%" /> <!-- Cantidad -->
                            <col width="10%" /> <!-- Fecha -->
                        </colgroup>
                        <thead>
                            <tr>
                                <th data-sort="string-ins" >Usuario</th>
                                <th data-sort="string-ins" >Tipo</th>
                                <th data-sort="string-ins" >Donante</th>
                                <th data-sort="string-ins" >Tipo</th>
                                <th data-sort="string-ins" >Especie</th>
                                <th data-sort="int" >Cantidad</th>
                                <th data-sort="date" >Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div id="pnlReporteDonaciones" class="panelReporte" style="display:none" >
                    <table id="tblReporteDonaciones" class="table" border="0" cellpadding="0" cellspacing="0" width="100%"  >
                        <colgroup>
                            <col width="25%" /> <!-- Donante -->
                            <col width="10%" /> <!-- Tipo -->
                            <col width="25%" /> <!-- Especie -->
                            <col width="10%" /> <!-- Cantidad -->
                            <col width="10%" /> <!-- Adoptados -->
                            <col width="10%" /> <!-- Disponibles -->
                            <col width="15%" /> <!-- Fecha -->
                        </colgroup>
                        <thead>
                            <tr>
                                <th data-sort="string-ins" >Donante</th>
                                <th data-sort="string-ins" >Tipo</th>
                                <th data-sort="string-ins" >Especie</th>
                                <th data-sort="int" >Cantidad</th>
                                <th data-sort="int" >Adoptados</th>
                                <th data-sort="int" >Disponibles</th>
                                <th data-sort="date" >Fecha</th>
                            </tr>
                        </thead>
                    </table>
                </div>

                <div id="pnlReporteActividades" class="panelReporte" style="display:none" >
                    <table id="tblReporteActividades" class="table" border="0" cellpadding="0" cellspacing="0" width="100%"  >
                        <colgroup>
                            <col width="25%" /> <!-- Usuario -->
                            <col width="10%" /> <!-- Tipo Usuario -->
                            <col width="20%" /> <!-- Provincia -->
                            <col width="27%" /> <!-- Descripción -->
                            <col width="18%" /> <!-- Fecha y Hora -->
                        </colgroup>
                        <thead>
                            <tr>
                                <th data-sort="string-ins" >Usuario</th>
                                <th data-sort="string-ins" >Tipo</th>
                                <th data-sort="string-ins" >Provincia</th>
                                <th data-sort="string-ins" >Descripcion</th>
                                <th data-sort="date" >Fecha y Hora</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
            
        </div>

    </div>

</asp:Content>