﻿<%@ Page Language="C#" ValidateRequest="false" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master"  CodeBehind="Contacto.aspx.cs" Inherits="SemanaDelArbol.Pages.Contacto" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <style type="text/css" >
    </style>

    <!--$(document).ready-->  
    <script type="text/javascript" >

        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_Contacto);
        });
        function cargarPagina_Contacto()
        {
           
        }
        
    </script>

    <!--HTML-->
    <div id="content" >

        <div id="pnlContacto" class="container-fluid">
                
           <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSubmit">
                <div class ="row">
                    <div class ="col-sm-3" style="color:#fff;" >
                        Nombre:
                    </div>
                    <div class ="col-sm-9">
                        <asp:TextBox ID="YourName" runat="server" Width="100%" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                    ControlToValidate="YourName" ValidationGroup="save" />
                        
                    </div>
                </div>
               
               <div class ="row">
                    <div class ="col-sm-3" style="color:#fff;">
                        E-mail:
                    </div>
                    <div class ="col-sm-9">
                        <asp:TextBox ID="YourEmail" runat="server" Width="100%" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                    ControlToValidate="YourEmail" ValidationGroup="save" />
                        <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator23"
                    SetFocusOnError="true" Text="Example: username@gmail.com" ControlToValidate="YourEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                    ValidationGroup="save" />
                        
                    </div>
                </div>
               
                <div class ="row">
                    <div class ="col-sm-3" style="color:#fff;">
                        Asunto:
                    </div>
                    <div class ="col-sm-9">
                        <asp:TextBox ID="YourSubject" runat="server" Width="100%" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="YourSubject" ValidationGroup="save" />
                        
                    </div>
                </div>
               
               <div class ="row">
                    <div class ="col-sm-3" style="color:#fff;">
                        Comentarios:
                    </div>
                    <div class ="col-sm-9">
                        <asp:TextBox ID="Comments" runat="server" TextMode="MultiLine" Rows="10" Width="100%" />
                    </div>
                </div>
               <div class ="row">
                   <div class ="col-sm-9">
                    </div>
                   <div class ="col-sm-3"  style="text-align: right;">
                       <asp:Button ID="btnSubmit" runat="server" Text="Enviar" 
                            OnClick="Button1_Click" ValidationGroup="save" />
                    </div>
                </div>
            
        </asp:Panel>
        <div class="row">
            <asp:Label ID="DisplayMessage" runat="server" Visible="false" />
        </div>  

        </div>
        
    </div>

</asp:Content>