﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Pages/MasterPage.Master" CodeBehind="DetalleUsuario.aspx.cs" Inherits="SemanaDelArbol.Pages.DetalleUsuario" %>

<%@ Register Src="~/CustomControls/MisDatos.ascx" TagName="MisDatos" TagPrefix="MisDatos" %>
<%@ Register Src="~/CustomControls/Mapa.ascx" TagName="Mapa" TagPrefix="Mapa" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="Server" >

    <!--$(document).ready-->  
    <script type="text/javascript" >
        var idUsuario;
        $(document).ready(function()
        {
            obtenerUsuarioLogueado(cargarPagina_DetalleUsuario);
        });
        function cargarPagina_DetalleUsuario()
        {
            cargarListaTiposUsuario("selUsuario_Tipo", onTipoUsuarioChanged);
            cargarListaPerfilesUsuario("selUsuario_Perfil");
            
            var configMapa =
                {
                    IdMapa: "mapa",
                    TituloMapa: "Plantaciones",
                    TituloBuscador: null, //"Localice en el mapa la ubicación aproximada",
                    MostrarMapa: true,
                    MostrarBuscador: false,
                    MostrarFiltroMarcadores: false,
                    MostrarListaMarcadores: false,
                    MostrarBotonMiPosicionActual: false,
                    OnMapLoadComplete: onMapLoadComplete_DetalleUsuario,
                    Zoom: _googleMap.NivelZoom,
                    Direccion: _filtroActual.Pais.Nombre,
                    ImagenMarcador: _googleMap.Marcadores[_googleMap.TiposMarcador.Plantacion].Icono,
                    DeshabilitarMarcadores: false,
                    OnClickMap: onClickToMap_DetalleUsuario,
                    OnBuscarDireccion: null
                };
            inicializarMapa(configMapa);

            idUsuario = getQuerystring("idUsuario");
            obtenerDatosUsuario(idUsuario);
            obtenerAdopcionesUsuario(idUsuario, "tblAdopcionesUsuario");
            obtenerDonacionesUsuario(idUsuario, "tblDonacionesUsuario");

            $("#selUsuario_Tipo, #txtUsuario_NombreUsuario, #txtUsuario_Nombre, #txtUsuario_Apellido, #txtUsuario_Email, #txtUsuario_Telefono, #txtUsuario_Web, #selUsuario_Pais, #selUsuario_Provincia, #txtUsuario_Ciudad").setEnabled(false);
            if ( verificarUsuarioLogueado() )
            {
                if ( _usuarioLogueado.Id == idUsuario || validarPerfilAdministrador(_usuarioLogueado) )
                {
                    if (validarPerfilAdministrador(_usuarioLogueado))
                        $("#trUsuario_Perfil").show();
                    
                    configurarMenuMisDatos("Puede <b>editar</b> sus datos, además de ver sus <b>plantaciones</b>, <b>donaciones</b>, y <b>adopciones</b>.");
                    $("#selUsuario_Tipo, #txtUsuario_NombreUsuario, #txtUsuario_Nombre, #txtUsuario_Apellido, #txtUsuario_Email, #txtUsuario_Telefono, #txtUsuario_Web, #selUsuario_Pais, #selUsuario_Provincia, #txtUsuario_Ciudad").setEnabled(true);
                    $("#trUsuario_Password").show();

                    $("#btnGuardarUsuario").show().click(function () { guardarUsuario(); });
                }
            }
        }
        function onMapLoadComplete_DetalleUsuario(idMapa)
        {
            idUsuario = getQuerystring("idUsuario");
            if ( !isEmpty(idUsuario) )
            {
                obtenerPlantacionesUsuario(idUsuario);
            }
        }
        function onClickToMap_DetalleUsuario(map, position, icon, title, idMarker)
        {
            //..
        }
        function obtenerDatosUsuario(idUsuario)
        {
            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("obtenerUsuario", "{'idUsuario':'" + idUsuario + "'}", obtenerDatosUsuario_Response);
        }
        function obtenerDatosUsuario_Response(datos)
        {
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if ( respuestaInfo.Resultado )
            {
                var usuarioInfo = respuestaInfo.Valor;

                $("#txtUsuario_Id").val(usuarioInfo.Id);
                $("#txtUsuario_NombreUsuario").val(usuarioInfo.NombreUsuario);
                $("#txtUsuario_Password").val(usuarioInfo.Password);
                $("#txtUsuario_Nombre").val(usuarioInfo.Nombre);
                $("#txtUsuario_Apellido").val(usuarioInfo.Apellido);
                $("#txtUsuario_Email").val(usuarioInfo.Email);
                $("#txtUsuario_Telefono").val(usuarioInfo.Telefono);
                $("#txtUsuario_Ciudad").val(usuarioInfo.Ciudad);
                $("#txtUsuario_Web").val(usuarioInfo.Web);
                cargarListasGeograficas("selUsuario_Pais", "selUsuario_Provincia", usuarioInfo.IdPais, usuarioInfo.IdProvincia);
                Helpers.SelectListHelper.setSelectedValue("selUsuario_Tipo", usuarioInfo.IdTipo);
                Helpers.SelectListHelper.setSelectedValue("selUsuario_Perfil", usuarioInfo.IdPerfil);
            }
        }
        function esIndividuo()
        {
            var idTipoUsuario = Helpers.SelectListHelper.getSelectedValueFromSelectList("selUsuario_Tipo");
            if (idTipoUsuario == _usuarioTipos.Individuo.Id)
                return true;
            else
                return false;
        }
        function onTipoUsuarioChanged()
        {
            var apellido = $("#trUsuario_Apellido");
            var web = $("#trUsuario_Web");
            if (esIndividuo())
            {
                apellido.show();
                web.hide();
            }
            else
            {
                apellido.hide();
                web.show();
            }
        }
    </script>

    <!-- Guardar Usuario-->
    <script type="text/javascript">
        function guardarUsuario()
        {
            var infoValidacion =
                [
                    { campo: "txtUsuario_NombreUsuario", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre de usuario." }] },
                    { campo: "txtUsuario_Password", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 4, mensaje: "Debe ingresar una contraseña de al menos 5 caracteres." }] },
                    { campo: "txtUsuario_Nombre", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un nombre." }] },
                    //{ campo: "txtUsuario_Apellido", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un apellido." }] },
                    { campo: "txtUsuario_Email", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un e-mail." }] },
                    { campo: "selUsuario_Pais", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe seleccionar un país." }] }
                ];

            var validacionApellido = { campo: "txtUsuario_Apellido", validaciones: [{ operacion: _validaciones.Longitud.MayorA, valor: 0, mensaje: "Debe ingresar un apellido." }] };

            if (esIndividuo())
                infoValidacion.push(validacionApellido);

            var arrErrores = validarFormulario(infoValidacion);
            if (arrErrores.length > 0)
                return false;

            var cuenta = new Object();

                cuenta.Id = $("#txtUsuario_Id").val();

                cuenta.IdTipo = $("#selUsuario_Tipo").getValue();
                cuenta.IdPerfil = $("#selUsuario_Perfil").getValue();

                cuenta.NombreUsuario = $("#txtUsuario_NombreUsuario").getValue();
                cuenta.Password = $("#txtUsuario_Password").getValue();

                cuenta.Nombre = $("#txtUsuario_Nombre").getValue();
                cuenta.Apellido = $("#txtUsuario_Apellido").getValue();
                cuenta.Telefono = $("#txtUsuario_Telefono").getValue();
                cuenta.Email = $("#txtUsuario_Email").getValue();
                cuenta.Web = $("#txtUsuario_Web").getValue();

                cuenta.IdPais = $("#selUsuario_Pais").getValue();
                cuenta.IdProvincia = $("#selUsuario_Provincia").getValue();
            if (cuenta.IdProvincia == null)
                cuenta.IdProvincia = -1;
                cuenta.Ciudad = $("#txtUsuario_Ciudad").getValue();

            var jsonParams = JSON.stringify(cuenta).replace(/'/g, "\\'");
   
            Helpers.showLoadingMessage();
            Helpers.AJAXHelper.doAjaxCall("guardarUsuario", "{'infoUsuario':'" + jsonParams + "'}", guardarUsuario_Respuesta);
        }
        function guardarUsuario_Respuesta(datos)
        {
            debugger;
            Helpers.hideLoadingMessage();
            var respuestaInfo = jQuery.parseJSON(datos.d);
            if (respuestaInfo.Resultado)
            {
                Helpers.NotificationHelper.showMessage(respuestaInfo.Mensaje);
            }
        }
    </script>
    <!-- Fin de [Guardar Usuario] -->

    <!--HTML-->
    <div id="content"  >

        <div id="pnlPrincipal" style="width:75%;" >

            <!-- Datos Usuario -->
            <div id="pnlDatosUsuario" >
                <input type="button" id="btnGuardarUsuario" value="Guardar" class="button button btnGuardar" style="width:100px; display:none" />
                <div id="pnlDatosUsuario_DatosWeb" class="panel" >
                    <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                        <legend class="ui-widget-header" align="center" >Datos de la web</legend>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                            <colgroup>
                                <col width="40%" /> 
                                <col width="60%" />                
                            </colgroup>
                            <tr>
                                <td>
                                    <label>Nombre de usuario:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_NombreUsuario" type="text" value="" />
                                </td>
                            </tr>
                            <tr id="trUsuario_Password" style="display:none" >
                                <td>
                                    <label>Contraseña:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_Password" type="text" value="" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div id="pnlDatosUsuario_DatosPersonales" class="panel" >
                    <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                        <legend class="ui-widget-header" align="center" >Datos personales</legend>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                            <colgroup>
                                <col width="40%" /> 
                                <col width="60%" />                
                            </colgroup>
                            <tr id="trUsuario_Id" style="display:none">
                                <td>
                                    <label>Id:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_Id" type="text" value="" />
                                </td>
                            </tr>
                            <tr id="trUsuario_Perfil" style="display:none" >
                                <td>
                                    <label>Perfil:</label>
                                </td>
                                <td>
                                    <select id="selUsuario_Perfil" ></select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Tipo:</label>
                                </td>
                                <td>
                                    <select id="selUsuario_Tipo" ></select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Nombre:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_Nombre" type="text" value="" />
                                </td>
                            </tr>
                            <tr id="trUsuario_Apellido" >
                                <td>
                                    <label>Apellido:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_Apellido" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Teléfono:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_Telefono" type="text" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>E-mail:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_Email" type="text" value="" />
                                </td>
                            </tr>
                            <tr id="trUsuario_Web" >
                                <td>
                                    <label>Web:</label>
                                </td>
                                <td>
                                    <input type="text" id="txtUsuario_Web" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <br />
                <div id="pnlDatosUsuario_DatosUbicacion" class="panel" >
                    <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                        <legend class="ui-widget-header" align="center" >Datos de ubicación</legend>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" >
                            <colgroup>
                                <col width="40%" /> 
                                <col width="60%" />                
                            </colgroup>
                            <tr>
                                <td>
                                    <label>País:</label>
                                </td>
                                <td>
                                    <select id="selUsuario_Pais" ></select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Provincia / Departamento / Estado:</label>
                                </td>
                                <td>
                                    <select id="selUsuario_Provincia" ></select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Localidad/Ciudad/Pueblo:</label>
                                </td>
                                <td>
                                    <input id="txtUsuario_Ciudad" type="text" value="" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>             
                </div>
            </div>
            <!-- Fin de [Datos Usuario] -->
                
            <!-- Plantaciones -->
            <div id="plantacionesUsuario" class="panel" >

                <!-- Mapa -->
                <div>
                    <Mapa:Mapa ID="Mapa" runat="server" />
                </div>
                <!-- Fin de [Mapa] -->
                
            </div>
            <!-- Fin de [Plantaciones] -->

            <br />

            <!-- Adopciones -->
            <div id="adopcionesUsuario" class="panel" >
                <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                    <legend class="ui-widget-header" align="center" >Adopciones</legend>
                    <table id="tblAdopcionesUsuario" class="tblInteractiva" border="0" cellspacing="0" cellpadding="0" width="100%" >
                        <colgroup>
                            <col width="0%" />
                            <col width="0%" />
                            <col width="33%" />
                            <col width="0%" />               
                            <col width="33%" />
                            <col width="33%" />
                        </colgroup>
                        <tr class="rowHeader" >
                            <td class="invisible" >
                                <label>Id</label>
                            </td>
                            <td class="invisible" >
                                <label>IdEspecie</label>
                            </td>
                            <td>
                                <label>Especie</label>
                            </td>
                            <td class="invisible" >
                                <label>IdDonante</label>
                            </td>
                            <td>
                                <label>Donante</label>
                            </td>
                            <td>
                                <label>Cantidad</label>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <!-- Fin de [Adopciones] -->

            <br />

            <!-- Donaciones -->
            <div id="donacionesUsuario" class="panel" >
                <fieldset class="ui-widget ui-widget-content ui-corner-all" >
                    <legend class="ui-widget-header" align="center" >Donaciones</legend>
                    <table id="tblDonacionesUsuario" class="tblInteractiva" border="0" cellspacing="0" cellpadding="0" width="100%" >
                        <colgroup>
                            <col width="0%" />
                            <col width="0%" />
                            <col width="25%" />
                            <col width="25%" />
                            <col width="25%" />
                            <col width="25%" />
                        </colgroup>
                        <tr class="rowHeader" >
                            <td class="invisible" >
                                <label>Id</label>
                            </td>
                            <td class="invisible" >
                                <label>IdEspecie</label>
                            </td>
                            <td>
                                <label>Especie</label>
                            </td>
                            <td>
                                <label>Cantidad</label>
                            </td>
                            <td>
                                <label>Adoptados</label>
                            </td>
                            <td>
                                <label>Disponibles</label>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <!-- Fin de [Donaciones] -->

        </div>

        <!-- Menú Mis Datos -->
        <div>
            <MisDatos:MisDatos ID="MisDatos" runat="server" />
        </div>
        <!-- Fin de [Menú Mis Datos] -->

    </div>

</asp:Content>