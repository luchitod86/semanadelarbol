﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Controladores;
using Entidades.jSON.Sistema;
using Entidades.jSON;
using Entidades;
using SemanaDelArbol.Helpers;

namespace SemanaDelArbol.Pages
{
    public partial class Reportes : PaginaBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);
        }

        #region Web Methods

        #region Private Methods

        #region Grafico - Usuarios
        private static GraficoInfo ObtenerGraficoUsuarios_AgrupadoPorTipo(int idPais, string pais, int periodo)
        {
            var uc = new UsuarioControlador();
            int totalIndividuos     = uc.ObtenerTotalUsuarios(idPais, periodo, -1, Comun.Constantes.UsuarioTipos.Individuo);
            int totalInstituciones  = uc.ObtenerTotalUsuarios(idPais, periodo, -1, Comun.Constantes.UsuarioTipos.Institucion);
            int totalMunicipios     = uc.ObtenerTotalUsuarios(idPais, periodo, -1, Comun.Constantes.UsuarioTipos.Municipio);

            var graficoInfo = new GraficoInfo(String.Format("Usuarios de {0} ({1}) agrupados por tipo", pais, periodo));
                graficoInfo.Datos.Add(new GraficoInfo.Item(Comun.Constantes.UsuarioTipos.Individuo, totalIndividuos));
                graficoInfo.Datos.Add(new GraficoInfo.Item(Comun.Constantes.UsuarioTipos.Institucion, totalInstituciones));
                graficoInfo.Datos.Add(new GraficoInfo.Item(Comun.Constantes.UsuarioTipos.Municipio, totalMunicipios));

            return graficoInfo;
        }
        private static GraficoInfo ObtenerGraficoUsuarios_AgrupadoPorProvincia(int idPais, string pais, int periodo)
        {
            var usuarios = new UsuarioControlador().ObtenerTodos(periodo, idPais);
            var items = new List<GraficoInfo.Item>();
            if (usuarios.IsNotNull() && usuarios.Count > 0)
            {
                var usuarioSinProvincia  = usuarios.Where(x => x.Provincia.IsNull()).ToList();
                var usuariosConProvincia = usuarios.Except(usuarioSinProvincia).ToList();

                items = usuariosConProvincia.GroupBy(x => x.Provincia.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();

                if ( usuarioSinProvincia.Count > 0 )
                    items.Add(new GraficoInfo.Item() { Texto = "SIN PROVINCIA", Valor = usuarioSinProvincia.Count() });
            }
            var graficoInfo = new GraficoInfo(String.Format("Usuarios de {0} ({1}) agrupados por provincia", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        #endregion

        #region Grafico - Plantaciones
        private static GraficoInfo ObtenerGraficoPlantaciones_AgrupadoPorTipo(int idPais, string pais, int periodo)
        {
            var plantaciones = new PlantacionControlador().ObtenerTodas(-1, idPais, periodo, false);

            var items = new List<GraficoInfo.Item>();
            if (plantaciones.IsNotNull() && plantaciones.Count > 0)
            {
                items = plantaciones.GroupBy(x => x.Usuario.Tipo.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Plantaciones de {0} ({1}) agrupadas por tipo de usuario", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        private static GraficoInfo ObtenerGraficoPlantaciones_AgrupadoPorEspecie(int idPais, string pais, int periodo)
        {
            var plantaciones = new PlantacionControlador().ObtenerTodas(-1, idPais, periodo, false);

            var items = new List<GraficoInfo.Item>();
            if (plantaciones.IsNotNull() && plantaciones.Count > 0)
            {
                items = plantaciones.GroupBy(x => x.Arbol.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Plantaciones de {0} ({1}) agrupadas por especie", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        #endregion

        #region Grafico - Adopciones
        private static GraficoInfo ObtenerGraficoAdopciones_AgrupadoPorTipoUsuario(int idPais, string pais, int periodo)
        {
            var adopciones = new AdopcionControlador().ObtenerTodas(-1, -1, idPais, periodo);

            var items = new List<GraficoInfo.Item>();
            if (adopciones.IsNotNull() && adopciones.Count > 0)
            {
                items = adopciones.GroupBy(x => x.Usuario.Tipo.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Adopciones de {0} ({1}) agrupadas por tipo de usuario", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        private static GraficoInfo ObtenerGraficoAdopciones_AgrupadoPorTipoDonante(int idPais, string pais, int periodo)
        {
            var adopciones = new AdopcionControlador().ObtenerTodas(-1, -1, idPais, periodo);

            var items = new List<GraficoInfo.Item>();
            if (adopciones.IsNotNull() && adopciones.Count > 0)
            {
                items = adopciones.GroupBy(x => x.Donante.Tipo.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Adopciones de {0} ({1}) agrupadas por tipo de donante", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        private static GraficoInfo ObtenerGraficoAdopciones_AgrupadoPorEspecie(int idPais, string pais, int periodo)
        {
            var adopciones = new AdopcionControlador().ObtenerTodas(-1, -1, idPais, periodo);

            var items = new List<GraficoInfo.Item>();
            if (adopciones.IsNotNull() && adopciones.Count > 0)
            {
                items = adopciones.GroupBy(x => x.Arbol.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Adopciones de {0} ({1}) agrupadas por especie", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        #endregion

        #region Grafico - Donaciones
        private static GraficoInfo ObtenerGraficoDonaciones_AgrupadoPorTipo(int idPais, string pais, int periodo)
        {
            var donaciones = new DonacionControlador().ObtenerTodas(-1, idPais, periodo);

            var items = new List<GraficoInfo.Item>();
            if (donaciones.IsNotNull() && donaciones.Count > 0)
            {
                items = donaciones.GroupBy(x => x.Donante.Tipo.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Donaciones de {0} ({1}) agrupadas por tipo de donante", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        private static GraficoInfo ObtenerGraficoDonaciones_AgrupadoPorEspecie(int idPais, string pais, int periodo)
        {
            var donaciones = new DonacionControlador().ObtenerTodas(-1, idPais, periodo);

            var items = new List<GraficoInfo.Item>();
            if (donaciones.IsNotNull() && donaciones.Count > 0)
            {
                items = donaciones.GroupBy(x => x.Arbol.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Donaciones de {0} ({1}) agrupadas por especie", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        #endregion

        #region Grafico - Actividades
        private static GraficoInfo ObtenerGraficoActividades_AgrupadoPorTipo(int idPais, string pais, int periodo)
        {
            var actividades = new ActividadControlador().ObtenerTodas(idPais, periodo, -1, -1);

            var items = new List<GraficoInfo.Item>();
            if (actividades.IsNotNull() && actividades.Count > 0)
            {
                items = actividades.GroupBy(x => x.Usuario.Tipo.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();
            }
            var graficoInfo = new GraficoInfo(String.Format("Actividades de {0} ({1}) agrupadas por tipo de usuario", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }
        private static GraficoInfo ObtenerGraficoActividades_AgrupadoPorProvincia(int idPais, string pais, int periodo)
        {
            var actividades = new ActividadControlador().ObtenerTodas(idPais, periodo, -1, -1);
            var items = new List<GraficoInfo.Item>();
            if (actividades.IsNotNull() && actividades.Count > 0)
            {
                var actividadesSinProvincia = actividades.Where(x => x.Provincia.IsNull()).ToList();
                var actividadesConProvincia = actividades.Except(actividadesSinProvincia).ToList();

                items = actividadesConProvincia.GroupBy(x => x.Provincia.Nombre).Select(n => new GraficoInfo.Item() { Texto = n.Key, Valor = n.Count() }).ToList();

                if (actividadesSinProvincia.Count > 0)
                    items.Add(new GraficoInfo.Item() { Texto = "SIN PROVINCIA", Valor = actividadesSinProvincia.Count() });
            }
            var graficoInfo = new GraficoInfo(String.Format("Actividades de {0} ({1}) agrupadas por provincia", pais, periodo));
                graficoInfo.Datos = items;

            return graficoInfo;
        }

        #endregion

        #region Grafico - Totales

        private static GraficoInfo ObtenerGraficoTotales_AgrupadoPorPaises(int idPais, string pais, int periodo)
        {
            int totalUsuarios       = new UsuarioControlador().ObtenerTotalUsuarios(idPais, periodo, -1, "");
            int totalPlantaciones   = new PlantacionControlador().ObtenerTotalPlantaciones(idPais, periodo, -1);
            int totalAdopciones     = new AdopcionControlador().ObtenerTotalAdopciones(idPais, periodo, -1, -1, -1);
            int totalDonaciones     = new DonacionControlador().ObtenerTotalDonaciones(idPais, periodo, -1);
            int totalDonantes       = new DonacionControlador().ObtenerTotalDonantes(idPais, periodo);
            int totalActividades    = new ActividadControlador().ObtenerTotalActividades(idPais, periodo);

            var graficoInfo = new GraficoInfo(String.Format("Totales de {0} ({1})", pais, periodo));
                graficoInfo.Datos.Add(new GraficoInfo.Item("Usuarios", totalUsuarios));
                graficoInfo.Datos.Add(new GraficoInfo.Item("Plantaciones", totalPlantaciones));
                graficoInfo.Datos.Add(new GraficoInfo.Item("Adopciones", totalAdopciones));
                graficoInfo.Datos.Add(new GraficoInfo.Item("Donaciones", totalDonaciones));
                graficoInfo.Datos.Add(new GraficoInfo.Item("Donantes", totalDonantes));
                graficoInfo.Datos.Add(new GraficoInfo.Item("Actividades", totalActividades));

            return graficoInfo;
        }

        private static GraficoInfo ObtenerGraficoTotales_AgrupadoPorUsuarios(int periodo)
        {
            var graficoInfo = new GraficoInfo(String.Format("Totales de usuarios por país ({0})", periodo));
            var paises = new PaisControlador().ObtenerTodos();
            foreach (var pais in paises)
            {
                int totalUsuarios = new UsuarioControlador().ObtenerTotalUsuarios(pais.Id, periodo, -1, "");
                graficoInfo.Datos.Add(new GraficoInfo.Item(pais.Nombre, totalUsuarios));
            }
            return graficoInfo;
        }

        private static GraficoInfo ObtenerGraficoTotales_AgrupadoPorPlantaciones(int periodo)
        {
            var graficoInfo = new GraficoInfo(String.Format("Totales de plantaciones por país ({0})", periodo));
            var paises = new PaisControlador().ObtenerTodos();
            foreach (var pais in paises)
            {
                int totalPlantaciones = new PlantacionControlador().ObtenerTotalPlantaciones(pais.Id, periodo, -1);
                graficoInfo.Datos.Add(new GraficoInfo.Item(pais.Nombre, totalPlantaciones));
            }
            return graficoInfo;
        }

        private static GraficoInfo ObtenerGraficoTotales_AgrupadoPorAdopciones(int periodo)
        {
            var graficoInfo = new GraficoInfo(String.Format("Totales de adopciones por país ({0})", periodo));
            var paises = new PaisControlador().ObtenerTodos();
            foreach (var pais in paises)
            {
                int totalAdopciones = new AdopcionControlador().ObtenerTotalAdopciones(pais.Id, periodo, -1, -1, -1);
                graficoInfo.Datos.Add(new GraficoInfo.Item(pais.Nombre, totalAdopciones));
            }
            return graficoInfo;
        }

        private static GraficoInfo ObtenerGraficoTotales_AgrupadoPorDonaciones(int periodo)
        {
            var graficoInfo = new GraficoInfo(String.Format("Totales de donaciones por país ({0})", periodo));
            var paises = new PaisControlador().ObtenerTodos();
            foreach (var pais in paises)
            {
                int totalDonaciones = new DonacionControlador().ObtenerTotalDonaciones(pais.Id, periodo, -1);
                graficoInfo.Datos.Add(new GraficoInfo.Item(pais.Nombre, totalDonaciones));
            }
            return graficoInfo;
        }

        private static GraficoInfo ObtenerGraficoTotales_AgrupadoPorDonantes(int periodo)
        {
            var graficoInfo = new GraficoInfo(String.Format("Totales de donantes por país ({0})", periodo));
            var paises = new PaisControlador().ObtenerTodos();
            foreach (var pais in paises)
            {
                int totalDonantes = new DonacionControlador().ObtenerTotalDonantes(pais.Id, periodo);
                graficoInfo.Datos.Add(new GraficoInfo.Item(pais.Nombre, totalDonantes));
            }
            return graficoInfo;
        }

        private static GraficoInfo ObtenerGraficoTotales_AgrupadoPorActividades(int periodo)
        {
            var graficoInfo = new GraficoInfo(String.Format("Totales de actividades por país ({0})", periodo));
            var paises = new PaisControlador().ObtenerTodos();
            foreach (var pais in paises)
            {
                int totalActividades = new ActividadControlador().ObtenerTotalActividades(pais.Id, periodo);
                graficoInfo.Datos.Add(new GraficoInfo.Item(pais.Nombre, totalActividades));
            }
            return graficoInfo;
        }

        #endregion

        #endregion

        #region Reportes

        [WebMethod]
        public static string obtenerReporteTotales()
        {
            string mensaje  = Resources.Global.Obtener_Mensaje_Error;
            bool resultado  = false;
            object valor    = null;
            try
            {
                var pc = new PaisControlador();

                var usuarioControlador      = new UsuarioControlador();
                var plantacionesControlador = new PlantacionControlador();
                var adopcionesControlador   = new AdopcionControlador();
                var donacionesControlador   = new DonacionControlador();
                var actividadesControlador  = new ActividadControlador();

                var filtroInfo = SessionHelper.ObtenerFiltroActual();
                int periodo = filtroInfo.Periodo;
                var estadisticas = new List<EstadisticasInfo>();

                var paises = pc.ObtenerTodos();
                foreach (Pais pais in paises)
                {
                    var idPais = pais.Id;

                    var estadisticasInfo                = new EstadisticasInfo();
                        estadisticasInfo.Pais           = pais.Nombre;
                        estadisticasInfo.Adopciones     = adopcionesControlador.ObtenerTotalAdopciones(idPais, periodo, -1, -1, -1);
                        estadisticasInfo.Donaciones     = donacionesControlador.ObtenerTotalDonaciones(idPais, periodo, -1);
                        estadisticasInfo.Donantes       = donacionesControlador.ObtenerTotalDonantes(idPais, periodo);
                        estadisticasInfo.Plantaciones   = plantacionesControlador.ObtenerTotalPlantaciones(idPais, periodo, -1);
                        estadisticasInfo.Usuarios       = usuarioControlador.ObtenerTotalUsuarios(idPais, periodo, -1, "");
                        estadisticasInfo.Actividades    = actividadesControlador.ObtenerTotalActividades(idPais, periodo);

                    estadisticas.Add(estadisticasInfo);
                }

                mensaje     = Resources.Global.Obtener_Mensaje_Exito;
                resultado   = true;
                valor       = estadisticas;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            return Helpers.SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerReporteUsuarios(int idPais, int periodo)
        {
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            bool resultado = false;
            object valor = null;
            try
            {
                var usuariosInfo = new List<UsuarioInfo>();
                var usuarios = new UsuarioControlador().ObtenerTodos(periodo, idPais);
                if (usuarios.IsNotNull())
                {
                    foreach (var usuario in usuarios)
                    {
                        usuariosInfo.Add(new UsuarioInfo(usuario));
                    }
                }
                valor = usuariosInfo;
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerReportePlantaciones(int idPais, int periodo)
        {
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            bool resultado = false;
            object valor = null;
            try
            {
                var plantacionesInfo = new List<PlantacionInfo>();
                var plantaciones = new PlantacionControlador().ObtenerTodas(-1, idPais, periodo);
                if (plantaciones.IsNotNull())
                {
                    foreach (var plantacion in plantaciones)
                    {
                        plantacionesInfo.Add(new PlantacionInfo(plantacion));
                    }
                }
                valor = plantacionesInfo;
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerReporteDonaciones(int idPais, int periodo)
        {
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            bool resultado = false;
            object valor = null;
            try
            {
                var donacionesInfo = new List<DonacionInfo>();
                var donaciones = new DonacionControlador().ObtenerTodas(-1, idPais, periodo);
                if (donaciones.IsNotNull())
                {
                    foreach (var donacion in donaciones)
                    {
                        donacionesInfo.Add(new DonacionInfo(donacion));
                    }
                }
                valor = donacionesInfo;
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerReporteAdopciones(int idPais, int periodo)
        {
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            bool resultado = false;
            object valor = null;
            try
            {
                var adopcionesInfo = new List<AdopcionInfo>();
                var adopciones = new AdopcionControlador().ObtenerTodas(-1, -1, idPais, periodo);
                if (adopciones.IsNotNull())
                {
                    foreach (var adopcion in adopciones)
                    {
                        adopcionesInfo.Add(new AdopcionInfo(adopcion));
                    }
                }
                valor = adopcionesInfo;
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        [WebMethod]
        public static string obtenerReporteActividades(int idPais, int periodo)
        {
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            bool resultado = false;
            object valor = null;
            try
            {
                var actividadesInfo = new List<ActividadInfo>();
                var actividades = new ActividadControlador().ObtenerTodas(idPais, periodo, -1, -1);
                if (actividades.IsNotNull())
                {
                    foreach (var actividad in actividades)
                    {
                        actividadesInfo.Add(new ActividadInfo(actividad));
                    }
                }
                valor = actividadesInfo;
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #region Gráficos

        [WebMethod]
        public static string obtenerDatosGrafico(int idPais, string pais, int periodo, string tipo, string agrupamiento)
        {
            string mensaje = Resources.Global.Obtener_Mensaje_Error;
            bool resultado = false;
            object valor = null;
            try
            {
                GraficoInfo graficoInfo = null;
                switch (tipo)
                {
                    case "usuarios":
                        #region "Usuarios"
                        switch (agrupamiento)
                        {
                            case "tipoUsuario":
                                graficoInfo = ObtenerGraficoUsuarios_AgrupadoPorTipo(idPais, pais, periodo);
                                break;
                            case "provincia":
                                graficoInfo = ObtenerGraficoUsuarios_AgrupadoPorProvincia(idPais, pais, periodo);
                                break;
                        }
                        #endregion
                        break;
                    case "plantaciones":
                        #region "Plantaciones"
                        switch (agrupamiento)
                        {
                            case "tipoUsuario":
                                graficoInfo = ObtenerGraficoPlantaciones_AgrupadoPorTipo(idPais, pais, periodo);
                                break;
                            case "especie":
                                graficoInfo = ObtenerGraficoPlantaciones_AgrupadoPorEspecie(idPais, pais, periodo);
                                break;
                        }
                        #endregion
                        break;
                    case "adopciones":
                        #region Adopciones"
                        switch (agrupamiento)
                        {
                            case "tipoUsuario":
                                graficoInfo = ObtenerGraficoAdopciones_AgrupadoPorTipoUsuario(idPais, pais, periodo);
                                break;
                            case "tipoDonante":
                                graficoInfo = ObtenerGraficoAdopciones_AgrupadoPorTipoDonante(idPais, pais, periodo);
                                break;
                            case "especie":
                                graficoInfo = ObtenerGraficoAdopciones_AgrupadoPorEspecie(idPais, pais, periodo);
                                break;
                        }
                        #endregion
                        break;
                    case "donaciones":
                        #region Donaciones"
                        switch (agrupamiento)
                        {
                            case "tipoDonante":
                                graficoInfo = ObtenerGraficoDonaciones_AgrupadoPorTipo(idPais, pais, periodo);
                                break;
                            case "especie":
                                graficoInfo = ObtenerGraficoDonaciones_AgrupadoPorEspecie(idPais, pais, periodo);
                                break;
                        }
                        #endregion
                        break;
                    case "actividades":
                        #region Actividades"
                        switch (agrupamiento)
                        {
                            case "tipoUsuario":
                                graficoInfo = ObtenerGraficoActividades_AgrupadoPorTipo(idPais, pais, periodo);
                                break;
                            case "provincia":
                                graficoInfo = ObtenerGraficoActividades_AgrupadoPorProvincia(idPais, pais, periodo);
                                break;
                        }
                        #endregion
                        break;
                    case "totales":
                        #region Totales
                        switch (agrupamiento)
                        {
                            case "pais":
                                graficoInfo = ObtenerGraficoTotales_AgrupadoPorPaises(idPais, pais, periodo);
                                break;
                            case "usuarios":
                                graficoInfo = ObtenerGraficoTotales_AgrupadoPorUsuarios(periodo);
                                break;
                            case "plantaciones":
                                graficoInfo = ObtenerGraficoTotales_AgrupadoPorPlantaciones(periodo);
                                break;
                            case "adopciones":
                                graficoInfo = ObtenerGraficoTotales_AgrupadoPorAdopciones(periodo);
                                break;
                            case "donaciones":
                                graficoInfo = ObtenerGraficoTotales_AgrupadoPorDonaciones(periodo);
                                break;
                            case "donantes":
                                graficoInfo = ObtenerGraficoTotales_AgrupadoPorDonantes(periodo);
                                break;
                            case "actividades":
                                graficoInfo = ObtenerGraficoTotales_AgrupadoPorActividades(periodo);
                                break;
                        }
                        #endregion
                        break;
                }
                valor = graficoInfo;
                resultado = true;
                mensaje = Resources.Global.Obtener_Mensaje_Exito;
            }
            catch (Exception ex)
            {
                HandleException(ex);
                throw;
            }
            return SessionHelper.BuildResponse(mensaje, resultado, valor);
        }

        #endregion

        #endregion
    }
}