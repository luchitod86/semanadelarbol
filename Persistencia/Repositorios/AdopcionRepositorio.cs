﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class AdopcionRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AdopcionRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve todas las Adopciones
        /// </summary>
        /// <param name="idDonante"></param>
        /// <param name="idEspecie"></param>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioAdopcion> ObtenerTodas(int idDonante = -1, int idEspecie = -1, int idPais = Comun.Constantes.Defaults.Pais.ID, int periodo = -1)
        {
            var adopciones = new List<UsuarioAdopcion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioAdopcion> query = _session.Query<UsuarioAdopcion>();

                    if (idDonante != -1)
                        query = query.Where(x => x.Donante.Id == idDonante);

                    if (idEspecie != -1)
                        query = query.Where(x => x.Arbol.Id == idEspecie);

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    if (periodo != -1)
                        query = query.Where(x => x.Fecha.Year == periodo);

                    adopciones = query.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return adopciones;
        }

        /// <summary>
        /// Devuelve la cantidad total de Adopciones
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idDonante"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idEspecie"></param>
        /// <returns></returns>
        public int ObtenerTotalAdopciones(int idPais = -1, int periodo = -1, int idDonante = -1, int idUsuario = -1, int idEspecie = -1)
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioAdopcion> query = _session.Query<UsuarioAdopcion>();

                    if (idDonante != -1)
                        query = query.Where(x => x.Donante.Id == idDonante);

                    if (idUsuario != -1)
                        query = query.Where(x => x.Usuario.Id == idUsuario);

                    if (idEspecie != -1)
                        query = query.Where(x => x.Arbol.Id == idEspecie);

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    if (periodo != -1)
                        query = query.Where(x => x.Fecha.Year == periodo);

                    total = query.Count();
                    if (total > 0)
                        total = query.Sum(x => x.Cantidad);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return total;
        }

        /// <summary>
        /// Devuelve las últimas Adopciones, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioAdopcion> ObtenerUltimasAdopciones(int cantidad, string pais = "", int periodo = -1)
        {
            List<UsuarioAdopcion> adopciones = new List<UsuarioAdopcion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioAdopcion> query = _session.Query<UsuarioAdopcion>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if ( periodo != -1 )
                        query = query.Where( x => x.Fecha.Year == periodo );

                    adopciones = query
                                    .OrderByDescending(x => x.Id)
                                    .Take(cantidad)
                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return adopciones;
        }

        #endregion
    }
}
