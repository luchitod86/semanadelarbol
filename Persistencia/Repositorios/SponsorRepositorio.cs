﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using Entidades;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class SponsorRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SponsorRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve un Sponsor según un Id
        /// </summary>
        /// <param name="Id_Provincia"></param>
        /// <returns></returns>
        public Sponsor Obtener(int idSponsor)
        {
            Sponsor provincia = new Sponsor();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    provincia = _session.Query<Sponsor>()
                        .Where(x => x.Id == idSponsor)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return provincia;
        }

        /// <summary>
        /// Devuelve todos los Sponsors
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="idPais"></param>
        /// <returns></returns>
        public List<Sponsor> ObtenerTodos(int periodo = -1, string pais = "", int idPais = -1)
        {
            List<Sponsor> sponsors = new List<Sponsor>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Sponsor> query = _session.Query<Sponsor>();

                    //if (!String.IsNullOrEmpty(pais))
                    //    query = query.Where(x => x.Pais.Nombre.ToUpper() == pais.ToUpper());

                    if ( idPais != -1 )
                        query = query.Where(x => x.Pais.Id == idPais);

                    sponsors = query
                                .OrderBy(x => x.Id)
                                .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sponsors;
        }

        /// <summary>
        /// Guardar un Sponsor
        /// </summary>
        /// <param name="sponsor"></param>
        /// <returns></returns>
        public bool Guardar(Sponsor sponsor)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(sponsor);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        /// <summary>
        /// Elimina un Sponsor
        /// </summary>
        /// <param name="sponsor"></param>
        /// <returns></returns>
        public bool Eliminar(Sponsor sponsor)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.Delete(sponsor);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion
    }
}
