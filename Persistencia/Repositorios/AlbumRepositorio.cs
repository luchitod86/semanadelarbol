﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using Entidades;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class AlbumRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AlbumRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve un Album según un Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Album Obtener(int id)
        {
            Album album = new Album();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    album = _session.Query<Album>()
                        .Where(x => x.Id == id)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return album;
        }

        /// <summary>
        /// Devuelve todos los Albums de una entidad
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <param name="idEntidad"></param>
        /// <returns></returns>
        public List<Album> ObtenerTodos(int periodo = -1, string pais = "", string idEntidad = "")
        {
            List<Album> albums = new List<Album>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Album> query = _session.Query<Album>();

                    if ( periodo != -1 )
                        query = query.Where( x => x.Fecha.Year == periodo );

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if ( !string.IsNullOrEmpty(idEntidad) )
                        query = query.Where( x => x.IdEntidad == idEntidad );

                    albums = query
                                .OrderBy(x => x.Id)
                                .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return albums;
        }

        /// <summary>
        /// Guardar un Album
        /// </summary>
        /// <param name="album"></param>
        /// <returns></returns>
        public bool Guardar(Album album)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(album);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        /// <summary>
        /// Elimina un Album
        /// </summary>
        /// <param name="sponsor"></param>
        /// <returns></returns>
        public bool Eliminar(Album album)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.Delete(album);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion
    }
}
