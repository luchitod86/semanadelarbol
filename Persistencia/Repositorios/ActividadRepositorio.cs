﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class ActividadRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ActividadRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve una Actividad según su Id
        /// </summary>
        /// <param name="Id_Actividad"></param>
        /// <returns></returns>
        public UsuarioActividad Obtener(int Id_Actividad)
        {
            UsuarioActividad actividad = new UsuarioActividad();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    actividad = _session.Query<UsuarioActividad>()
                        .Where(x => x.Id == Id_Actividad)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actividad;
        }


        /// <summary>
        /// Devuelve todas las Actividades dada fecha
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<UsuarioActividad> ObtenerTodasPorFecha(DateTime fecha, string pais = "")
        {
            List<UsuarioActividad> actividades = new List<UsuarioActividad>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioActividad> query = _session.Query<UsuarioActividad>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );
                    
                    query = query.Where
                        (
                            x => x.FechaYHora.Year == fecha.Year &&
                            x.FechaYHora.Month == fecha.Month &&
                            x.FechaYHora.Day == fecha.Day
                        );
                    
                    actividades = query.OrderByDescending( x => x.FechaYHora ).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actividades;
        }

        /// <summary>
        /// Devuelve todas las Actividades
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="mes"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        public List<UsuarioActividad> ObtenerTodas(int idPais = -1, int periodo = -1, int mes = -1, int cantidad = -1)
        {
            var actividades = new List<UsuarioActividad>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioActividad> query = _session.Query<UsuarioActividad>();

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    if ( periodo != -1 )
                        query = query.Where( x => x.FechaYHora.Year == periodo );
                    if (mes != -1)
                        query = query.Where(x => x.FechaYHora.Month == mes);

                    query = query.OrderBy(x => x.FechaYHora);

                    if ( cantidad != -1 )
                        query = query.Take(cantidad);

                    actividades = query.ToList();                                
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actividades;
        }

        /// <summary>
        /// Devuelve el total de las Actividades
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public int ObtenerTotalActividades(int idPais = -1, int periodo = -1)
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioActividad> query = _session.Query<UsuarioActividad>();

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    if (periodo != -1)
                        query = query.Where(x => x.FechaYHora.Year == periodo);

                    total = query.Count();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }

        /// <summary>
        /// Devuelve las últimas Actividades, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioActividad> ObtenerUltimasActividades(int cantidad, string pais = "", int periodo = -1)
        {
            List<UsuarioActividad> actividades = new List<UsuarioActividad>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioActividad> query = _session.Query<UsuarioActividad>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if ( periodo != -1 )
                        query = query.Where( x => x.FechaYHora.Year == periodo );

                    actividades = query
                                    .OrderByDescending(x => x.Id)
                                    .Take(cantidad)
                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return actividades;
        }

        #endregion
    }
}
