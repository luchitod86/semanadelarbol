﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class LogRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public LogRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve un Log según un Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Log Obtener(int id)
        {
            var log = new Log();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    log = _session.Query<Log>()
                        .Where(x => x.Id == id)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return log;
        }

        /// <summary>
        /// Devuelve un Tipo de Log, según su Nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public LogTipo ObtenerTipo(string nombre)
        {
            var tipo = new LogTipo();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    tipo = _session.Query<LogTipo>()
                        .Where(x => x.Nombre.ToLower() == nombre.ToLower())
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tipo;
        }

        /// <summary>
        /// Devuelve todos los Logs
        /// </summary>
        /// <param name="fechaDesde"></param>
        /// <param name="fechaHasta"></param>
        /// <param name="idTipo"></param>
        /// <param name="seccion"></param>
        /// <returns></returns>
        public List<Log> ObtenerTodos(Nullable<DateTime> fechaDesde, Nullable<DateTime> fechaHasta, Nullable<int> idTipo, string seccion = null)
        {
            List<Log> logs = new List<Log>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Log> query = _session.Query<Log>();

                    if ( !String.IsNullOrEmpty(seccion) )
                        query = query.Where(x => x.Seccion.Nombre.ToLower() == seccion.ToLower());

                    if ( idTipo.HasValue )
                        query = query.Where(x => x.Tipo.Id == idTipo.Value);

                    if (fechaDesde.HasValue)
                        query = query.Where(x => x.FechaYHora >= fechaDesde.Value);

                    if (fechaHasta.HasValue)
                        query = query.Where(x => x.FechaYHora <= fechaHasta.Value);

                    logs = query.OrderBy(x => x.FechaYHora).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return logs;
        }

        /// <summary>
        /// Guardar un Log
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public bool Guardar(Log log)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(log);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion
    }
}