﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using Entidades;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class ViveroRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ViveroRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve un Vivero según un Id
        /// </summary>
        /// <param name="Id_Provincia"></param>
        /// <returns></returns>
        public Vivero Obtener(int idVivero)
        {
            Vivero provincia = new Vivero();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    provincia = _session.Query<Vivero>()
                        .Where(x => x.Id == idVivero)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return provincia;
        }

        /// <summary>
        /// Devuelve todos los Viveros
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<Vivero> ObtenerTodos(int periodo = -1, string pais = "")
        {
            List<Vivero> viveros = new List<Vivero>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Vivero> query = _session.Query<Vivero>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Usuario.Pais.Nombre == pais );

                    viveros = query
                                .OrderBy(x => x.Id)
                                .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return viveros;
        }

        /// <summary>
        /// Guardar un Vivero
        /// </summary>
        /// <param name="vivero"></param>
        /// <returns></returns>
        public bool Guardar(Vivero vivero)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(vivero);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        /// <summary>
        /// Elimina un Vivero
        /// </summary>
        /// <param name="vivero"></param>
        /// <returns></returns>
        public bool Eliminar(Vivero vivero)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.Delete(vivero);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion
    }
}
