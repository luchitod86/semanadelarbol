﻿using System;
using System.Linq;
using System.Linq.Dynamic;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Linq;
using Entidades;

namespace Persistencia.Repositorios
{
    public class RepositorioBase<T> where T : Entidad
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public RepositorioBase()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Constructors

        #region Metodos

        /// <summary>
        /// Devuelve todas las Entidades
        /// </summary>
        /// <returns></returns>
        public List<T> ObtenerEntidades()
        {
            List<T> secciones = new List<T>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    secciones = _session.Query<T>().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return secciones;
        }

        /// <summary>
        /// Devuelve todas las Entidades, según un filtro
        /// </summary>
        /// <returns></returns>
        public List<T> ObtenerEntidades(string filtro)
        {
            List<T> secciones = new List<T>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    secciones = _session.Query<T>()
                                    .Where(filtro)
                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return secciones;
        }

        /// <summary>
        /// Devuelve una Entidad, dado su Id (PK)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T ObtenerEntidad(int id)
        {
            T entidad = null;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    entidad = _session.Query<T>().Where(x => x.Id == id).FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return entidad;
        }

        /// <summary>
        /// Devuelve una Entidad, según un filtro
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public T ObtenerEntidad(string filtro)
        {
            T entidad = null;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    entidad = _session.Query<T>()
                        .Where(filtro)
                        .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return entidad;
        }

        /// <summary>
        /// Guarda una Entidad
        /// </summary>
        /// <param name="entidad"></param>
        /// <returns></returns>
        public bool GuardarEntidad(Entidad entidad)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(entidad);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resultado;
        }

        /// <summary>
        /// Elimina una Entidad
        /// </summary>
        /// <param name="entidad"></param>
        /// <returns></returns>
        public bool EliminarEntidad(Entidad entidad)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.Delete(entidad);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resultado;
        }

        #endregion Metodos
    }
}