﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class PaisRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public PaisRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve un Pais según un Id
        /// </summary>
        /// <param name="Id_Pais"></param>
        /// <returns></returns>
        public Pais Obtener(int Id_Pais)
        {
            Pais pais = new Pais();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    pais = _session.Query<Pais>()
                        .Where(x => x.Id == Id_Pais)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return pais;
        }

        /// <summary>
        /// Devuelve un Pais según un Nombre
        /// </summary>
        /// <param name="Nombre_Pais"></param>
        /// <returns></returns>
        public Pais ObtenerPorNombre(string Nombre_Pais)
        {
            Pais pais = new Pais();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    pais = _session.Query<Pais>()
                        .Where(x => x.Nombre == Nombre_Pais)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return pais;
        }

        /// <summary>
        /// Devuelve todos los Países
        /// </summary>
        /// <param name="incluirInactivos"></param>
        /// <returns></returns>
        public List<Pais> ObtenerTodos(bool incluirInactivos = false)
        {
            List<Pais> paises = new List<Pais>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Pais> query = _session.Query<Pais>();

                    if ( !incluirInactivos )
                        query = query.Where( x => x.Activo == true );

                    paises = query.OrderBy(x => x.Nombre).ToList();;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return paises;
        }

        #endregion

    }
}
