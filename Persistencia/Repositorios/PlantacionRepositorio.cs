﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class PlantacionRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public PlantacionRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve una plantación, según un Id
        /// </summary>
        /// <param name="Id_Plantacion"></param>
        /// <returns></returns>
        public UsuarioPlantacion Obtener(int Id_Plantacion)
        {
            UsuarioPlantacion plantacion = new UsuarioPlantacion();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    plantacion = _session.Query<UsuarioPlantacion>()
                        .Where(x => x.Id == Id_Plantacion)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return plantacion;
        }

        /// <summary>
        /// Devuelve el total de las plantaciones de usuarios
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public int ObtenerTotalPlantaciones(int idPais = -1, int periodo = -1, int idUsuario = -1)
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioPlantacion> query = _session.Query<UsuarioPlantacion>();

                    if ( idUsuario != -1 )
                        query = query.Where( x => x.Usuario.Id == idUsuario );

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    if ( periodo != -1 )
                        query = query.Where( x => x.Fecha.Year == periodo );

                    total = query.Count();
                    if ( total > 0 )
                        total = query.Sum(x => x.Cantidad);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }

        /// <summary>
        /// Devuelve todas las plantaciones de usuario
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <param name="soloConFoto"></param>
        /// <returns></returns>
        public List<UsuarioPlantacion> ObtenerTodas(int idUsuario = -1, string pais = "", int periodo = -1, bool soloConFoto = false)
        {
            List<UsuarioPlantacion> plantaciones = new List<UsuarioPlantacion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioPlantacion> query = _session.Query<UsuarioPlantacion>();

                    if ( idUsuario != -1 )
                        query = query.Where( x => x.Usuario.Id == idUsuario );

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if ( periodo != -1 )
                        query = query.Where( x => x.Fecha.Year == periodo );

                    if ( soloConFoto )
                        query = query.Where( x => x.TieneFoto );

                    plantaciones = query.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return plantaciones;
        }

        public List<UsuarioPlantacion> ObtenerTodas(int idUsuario = -1, int idPais = Comun.Constantes.Defaults.Pais.ID, int periodo = -1, bool soloConFoto = false)
        {
            var plantaciones = new List<UsuarioPlantacion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioPlantacion> query = _session.Query<UsuarioPlantacion>();

                    if (idUsuario != -1)
                        query = query.Where(x => x.Usuario.Id == idUsuario);

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    if (periodo != -1)
                        query = query.Where(x => x.Fecha.Year == periodo);

                    if (soloConFoto)
                        query = query.Where(x => x.TieneFoto);

                    plantaciones = query.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return plantaciones;
        }

        /// <summary>
        /// Devuelve las últimas Plantaciones, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioPlantacion> ObtenerUltimasPlantaciones(int cantidad, string pais = "", int periodo = -1)
        {
            List<UsuarioPlantacion> plantaciones = new List<UsuarioPlantacion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioPlantacion> query = _session.Query<UsuarioPlantacion>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if ( periodo != -1 )
                        query = query.Where( x => x.Fecha.Year == periodo );

                    plantaciones = query
                                    .OrderByDescending(x => x.Id)
                                    .Take(cantidad)
                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return plantaciones;
        }

        #endregion
    }
}
