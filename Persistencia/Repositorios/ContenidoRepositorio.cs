﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class ContenidoRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ContenidoRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Métodos

        #region SeccionContenido

        /// <summary>
        /// Devuelve una SeccionContenido, según un Id
        /// </summary>
        /// <param name="idSeccionContenido"></param>
        /// <returns></returns>
        public SeccionContenido ObtenerSeccionContenido(int idSeccionContenido)
        {
            SeccionContenido seccionContenido = new SeccionContenido();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    seccionContenido = _session.Query<SeccionContenido>()
                                        .Where(x => x.Id == idSeccionContenido)
										.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return seccionContenido;
        }

		/// <summary>
		/// Devuelve una SeccionContenido de un Campo, correspondiente a una Sección, de un País
		/// </summary>
        /// <param name="idPais"></param>
		/// <param name="seccion"></param>
        /// <param name="campo"></param>
		/// <returns></returns>
        public SeccionContenido ObtenerSeccionContenido(int idPais, string seccion, string campo)
        {
            SeccionContenido seccionContenido = new SeccionContenido();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    seccionContenido = _session.Query<SeccionContenido>()
                                        .Where(x => x.Pais.Id == idPais && x.Seccion.Nombre.ToLower() == seccion.ToLower() && x.Campo.ToLower() == campo.ToLower() )
                                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return seccionContenido;
        }

        /// <summary>
        /// Devuelve todos los Contenidos de una Sección, de un País
        /// </summary>
        /// <returns></returns>
        public List<SeccionContenido> ObtenerTodosLosSeccionContenido(int idPais = -1)
        {
            List<SeccionContenido> paises = new List<SeccionContenido>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<SeccionContenido> query = _session.Query<SeccionContenido>();

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    paises = query.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return paises;
        }

        /// <summary>
        /// Guardar un SeccionContenido
        /// </summary>
        /// <param name="contenido"></param>
        /// <returns></returns>
        public bool GuardarSeccionContenido(SeccionContenido contenido)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(contenido);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion

        #region Seccion

        /// <summary>
        /// Devuelve una Seccion, según un Id
        /// </summary>
        /// <param name="idSeccion"></param>
        /// <returns></returns>
        public Seccion ObtenerSeccion(int idSeccion)
        {
            Seccion seccionContenido = new Seccion();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    seccionContenido = _session.Query<Seccion>()
                                        .Where(x => x.Id == idSeccion)
                                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return seccionContenido;
        }

        /// <summary>
        /// Devuelve una Seccion, según su Nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        public Seccion ObtenerSeccion(string nombre)
        {
            Seccion seccion = new Seccion();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    seccion = _session.Query<Seccion>()
                                        .Where(x => x.Nombre.ToLower() == nombre.ToLower())
                                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return seccion;
        }

        #endregion

        #endregion

    }
}
