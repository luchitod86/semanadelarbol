﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class NoticiaRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public NoticiaRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve una plantación, según un Id
        /// </summary>
        /// <param name="Id_Plantacion"></param>
        /// <returns></returns>
        public Noticia Obtener(int Id_Noticia)
        {
            Noticia noticia = new Noticia();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    noticia = _session.Query<Noticia>()
                        .Where(x => x.Id == Id_Noticia)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return noticia;
        }

        /// <summary>
        /// Devuelve el total de las noticias de pais
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public int ObtenerTotalNoticias(string pais = "")
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Noticia> query = _session.Query<Noticia>();

                    if (pais != "")
                        query = query.Where(x => x.Pais.Nombre == pais);

                    total = query.Count();                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }

        /// <summary>
        /// Devuelve todas las noticias
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <param name="soloConFoto"></param>
        /// <returns></returns>
        public List<Noticia> ObtenerTodas(string pais = "")
        {
            List<Noticia> noticias = new List<Noticia>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Noticia> query = _session.Query<Noticia>();
                    
                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where(x => x.Pais.Nombre.ToUpper() == pais.ToUpper()).OrderByDescending(x => x.Id);

                    noticias = query.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return noticias;
        }

        /// <summary>
        /// Devuelve las últimas Noticias, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<Noticia> ObtenerUltimasNoticias(int cantidad, string pais = "")
        {
            List<Noticia> plantaciones = new List<Noticia>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Noticia> query = _session.Query<Noticia>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );                    

                    plantaciones = query
                                    .OrderByDescending(x => x.Id)
                                    .Take(cantidad)
                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return plantaciones;
        }

        /// <summary>
        /// Devuelve las últimas Noticias en una lista paginada
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="pagina"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        public List<Noticia> ObtenerTodas_Paginado(string pais = "", int pagina = 1, int cantidad = 10)
        {
            List<Noticia> noticias = new List<Noticia>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Noticia> query = _session.Query<Noticia>();

                    if (!string.IsNullOrEmpty(pais))
                        query = query.Where(x => x.Pais.Nombre.ToUpper() == pais.ToUpper()).OrderByDescending(x => x.Id) ;

                    noticias = query
                        .Skip((pagina - 1) * cantidad).Take(cantidad)
                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return noticias;
        }

        /// <summary>
        /// Guardar una Noticia
        /// </summary>
        /// <param name="noticia"></param>
        /// <returns></returns>
        public bool Guardar(Noticia noticia)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(noticia);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resultado;
        }

        #endregion
    }
}
