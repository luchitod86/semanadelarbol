﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;
using Comun;

namespace Persistencia
{
    public class UsuarioRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve un Usuario según un Id
        /// </summary>
        /// <param name="Id_Usuario"></param>
        /// <returns></returns>
        public Usuario Obtener(int Id_Usuario)
        {
            Usuario usuario = new Usuario();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    usuario = _session.Query<Usuario>()
                        .Where(x => x.Id == Id_Usuario)
                        .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuario;
        }

        /// <summary>
        /// Devuelve el Tipo de Usuario
        /// </summary>
        /// <param name="Id_Tipo"></param>
        /// <returns></returns>
        public UsuarioTipo ObtenerTipoUsuario(int Id_Tipo)
        {
            UsuarioTipo usuarioTipo = new UsuarioTipo();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    usuarioTipo = _session.Query<UsuarioTipo>()
                                    .Where(x => x.Id == Id_Tipo)
                                    .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuarioTipo;
        }

        /// <summary>
        /// Devuelve el Perfil de Usuario
        /// </summary>
        /// <param name="Id_Perfil"></param>
        /// <returns></returns>
        public UsuarioPerfil ObtenerPerfilUsuario(int Id_Perfil)
        {
            UsuarioPerfil usuarioPerfil = new UsuarioPerfil();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    usuarioPerfil = _session.Query<UsuarioPerfil>()
                                    .Where(x => x.Id == Id_Perfil)
                                    .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuarioPerfil;
        }

        #region Totales

        /// <summary>
        /// Devuelve el total de Usuarios
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public int ObtenerTotal(string filtro)
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    if (filtro.Length == 0)
                        filtro = "1 == 1";

                    total = _session.Query<Usuario>()
                        .Where(filtro)
                        .Count();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return total;
        }

        /// <summary>
        /// Devuelve la cantidad de Usuarios, según un Tipo
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idUsuario"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public int ObtenerTotalUsuarios(int idPais = -1, int periodo = -1, int idUsuario = -1, string tipo = "")
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();

                    if ( idUsuario != -1 )
                        query = query.Where( x => x.Id == idUsuario );

                    if ( idPais != -1)
                        query = query.Where( x => x.Pais.Id == idPais);

                    if ( periodo != -1 )
                        query = query.Where( x => x.FechaCreacion.Year == periodo);

                    if ( !string.IsNullOrEmpty(tipo) )
                    {
                        switch ( tipo.ToLower() )
                        {
                            case "individuo":
                                query = query.Where(x => x.Tipo.Nombre.ToLower() == "individuo" );
                                break;
                            case "institucion":
                                query = query.Where(x => x.Tipo.Nombre.ToLower() == "institución");
                                break;
                            case "municipio":
                                query = query.Where(x => x.Tipo.Nombre.ToLower() == "municipio");
                                break;
                        }
                    }

                    total = query.Count();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return total;
        }

        /// <summary>
        /// Devuelve la cantidad de Usuarios, según un pais
        /// </summary>
        /// <param name="pais"></param>
        /// <returns></returns>
        public int ObtenerTotalUsuarios(string pais = "")
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();

                    if (pais != "")
                        query = query.Where(x => x.Pais.Nombre.ToUpper() == pais.ToUpper());                   

                    total = query.Count();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return total;
        }

        #endregion

        /// <summary>
        /// Devuelve todos los Donantes
        /// </summary>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<Usuario> ObtenerDonantes(string pais = "", int periodo = -1)
        {
            List<Usuario> donantes = new List<Usuario>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();

                    query = query.Where(x => x.Latitud != 0 && x.Longitud != 0 );
                    //query = query.Where( x => x.Donaciones.Count > 0 );
                    query = query.Where( x => x.Donaciones.Any(y => y.Cantidad > 0 && y.Fecha.Year == periodo ) );

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    //query = query.OrderByDescending(x => x.Id)
                    query = query.OrderBy(x => x.Id);

                    donantes = query.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return donantes;
        }

        /// <summary>
        /// Devuelve todos los Donantes, según un Pais y Provincia
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="idPais"></param>
        /// <param name="idProvincia"></param>
        /// <param name="ciudad"></param>
        /// <returns></returns>
        public List<Usuario> ObtenerDonantes(int periodo, int idPais, int idProvincia, string ciudad)
        {
            List<Usuario> donantes = new List<Usuario>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();
                                        
                    query = query.Where(x => x.Latitud != 0 && x.Longitud != 0 );
                    //query = query.Where( x => x.Donaciones != null && x.Donaciones.Count > 0 );
                    query = query.Where(x => x.Donaciones.Count > 0);

                    if ( idPais != null && idPais != -1 )
                        query = query.Where(x => x.Pais.Id == idPais);

                    if ( periodo != null && periodo != -1 )
                        query = query.Where(x => x.Donaciones.Where(y => y.Fecha.Year == periodo).Count() > 0 );

                    if ( idProvincia != null && idProvincia != -1 )
                        query = query.Where(x => x.Provincia.Id == idProvincia);

                    if ( ciudad != null && ciudad != string.Empty )
                        query = query.Where(x => x.Ciudad == ciudad);

                    //query = query.OrderByDescending(x => x.Id)
                    query = query.OrderBy(x => x.Id);

                    donantes = query.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return donantes;
        }

        /// <summary>
        /// Devuelve todos los Usuarios
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="pais"></param>
        /// <returns></returns>
        public List<Usuario> ObtenerTodos(int periodo = -1, string pais = "")
        {
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if (periodo != -1)
                        query = query.Where(x => x.FechaCreacion.Year == periodo);

                    usuarios = query
                        .OrderBy(x => x.Nombre)
                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuarios;
        }

        public List<Usuario> ObtenerTodos(int periodo = -1, int idPais = Comun.Constantes.Defaults.Pais.ID)
        {
            var usuarios = new List<Usuario>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();

                    query = query.Where(x => x.Pais.Id == idPais);

                    if (periodo != -1)
                        query = query.Where(x => x.FechaCreacion.Year == periodo);

                    usuarios = query
                        .OrderBy(x => x.Nombre)
                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuarios;
        }

        public List<Usuario> ObtenerTodos_Paginado(int periodo = -1, string pais = "", int pagina = 1, int cantidad = 10)
        {
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();

                    if (!string.IsNullOrEmpty(pais))
                        query = query.Where(x => x.Pais.Nombre.ToUpper() == pais.ToUpper()).OrderBy(x => x.NombreUsuario);

                    if (periodo != -1)
                        query = query.Where(x => x.FechaCreacion.Year == periodo);

                    usuarios = query
                        .Skip((pagina - 1) * cantidad).Take(cantidad)                        
                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuarios;
        }

        /// <summary>
        /// Devuelve todos los Usuarios
        /// </summary>
        /// <returns></returns>
        public List<Usuario> ObtenerTodos()
        {
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    usuarios = _session.Query<Usuario>()
                        .OrderBy(x => x.Nombre)
                        .ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuarios;
        }

        /// <summary>
        /// Obtiene un Usuario según un Nombre de Usuario y Password
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Usuario ObtenerPorUsuarioYPassword(string nombreUsuario, string password)
        {
            Usuario usuario = new Usuario();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    usuario = _session.Query<Usuario>()
                        .Where(x => x.NombreUsuario.ToLower() == nombreUsuario.ToLower())
                        .Where(x => x.Password.ToLower() == password.ToLower())
                        .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuario;
        }

        /// <summary>
        /// Obtiene un Usuario según un Nombre de Usuario
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <returns></returns>
        public Usuario ObtenerPorUsuario(string nombreUsuario)
        {
            Usuario usuario = new Usuario();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    usuario = _session.Query<Usuario>()
                        .Where(x => x.NombreUsuario.ToLower() == nombreUsuario.ToLower())
                        .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuario;
        }

        /// <summary>
        /// Obtiene un Usuario según un Nombre de Usuario y Red Social
        /// </summary>
        /// <param name="redSocial"></param>
        /// <param name="nombreUsuario"></param>
        /// <returns></returns>
        public Usuario ObtenerPorUsuarioRedSocial(string redSocial, string nombreUsuario)
        {
            Usuario usuario = new Usuario();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    switch ( redSocial )
                    {
                        case Constantes.RedSocial.FACEBOOK:
                            usuario = _session.Query<Usuario>()
                                .Where(x => x.NombreUsuarioFacebook.ToLower() == nombreUsuario.ToLower())
                                .FirstOrDefault();
                            break;
                        case Constantes.RedSocial.TWITTER:
                            usuario = _session.Query<Usuario>()
                                .Where(x => x.NombreUsuarioTwitter.ToLower() == nombreUsuario.ToLower())
                                .FirstOrDefault();
                            break;
                        case Constantes.RedSocial.GOOGLE_PLUS:
                            usuario = _session.Query<Usuario>()
                                .Where(x => x.NombreUsuarioGooglePlus.ToLower() == nombreUsuario.ToLower())
                                .FirstOrDefault();
                            break;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuario;
        }

        /// <summary>
        /// Obtiene un Usuario según un Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Usuario ObtenerPorEmail(string email)
        {
            Usuario usuario = new Usuario();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    usuario = _session.Query<Usuario>()
                        .Where(x => x.Email == email.Trim().ToLower())
                        .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return usuario;
        }

        /// <summary>
        /// Guardar un Usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool Guardar(Usuario usuario)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(usuario);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resultado;
        }

        /// <summary>
        /// Elimina un Usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool Eliminar(Usuario usuario)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.Delete(usuario);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resultado;
        }

        #endregion
    }
}
