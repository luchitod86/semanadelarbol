﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class ConfiguracionRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ConfiguracionRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        
        /// <summary>
        /// Devuelve la Configuracion del sistema
        /// </summary>
        /// <returns></returns>
        public Configuracion ObtenerConfiguracion()
        {
            Configuracion configuracion = new Configuracion();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    configuracion = _session.Query<Configuracion>().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return configuracion;
        }

        /// <summary>
        /// Devuelve la Configuración de Email
        /// </summary>
        /// <returns></returns>
        public ConfiguracionEmail ObtenerConfiguracionEmail()
        {
            ConfiguracionEmail configuracionEmail = new ConfiguracionEmail();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    configuracionEmail = _session.Query<ConfiguracionEmail>().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return configuracionEmail;
        }

        /// <summary>
        /// Guarda una Configuracion
        /// </summary>
        /// <param name="configuracion"></param>
        /// <returns></returns>
        public bool Guardar(Configuracion configuracion)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(configuracion);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }
    }
}
