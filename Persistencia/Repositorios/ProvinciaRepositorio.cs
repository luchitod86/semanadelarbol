﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class ProvinciaRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ProvinciaRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve una Provincia según un Id
        /// </summary>
        /// <param name="Id_Provincia"></param>
        /// <returns></returns>
        public Provincia Obtener(int Id_Provincia)
        {
            Provincia provincia = new Provincia();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    provincia = _session.Query<Provincia>()
                        .Where(x => x.Id == Id_Provincia)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return provincia;
        }

        /// <summary>
        /// Devuelve todas las Provincias de un País
        /// </summary>
        /// <param name="Id_Pais"></param>
        /// <returns></returns>
        public List<Provincia> ObtenerTodas(int Id_Pais)
        {
            List<Provincia> provincias = new List<Provincia>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    provincias = _session.Query<Provincia>()
                        .Where(x => x.Pais.Id == Id_Pais)
                        .OrderBy(x => x.Nombre)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return provincias;
        }

        #endregion
    }
}
