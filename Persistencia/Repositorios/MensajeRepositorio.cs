﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class MensajeRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public MensajeRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve un Mensaje según un Id
        /// </summary>
        /// <param name="Id_Mensaje"></param>
        /// <returns></returns>
        public Mensaje Obtener(int Id_Mensaje)
        {
            Mensaje Mensaje = new Mensaje();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    Mensaje = _session.Query<Mensaje>()
                        .Where(x => x.Id == Id_Mensaje)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Mensaje;
        }

        /// <summary>
        /// Devuelve todos los Mensajes
        /// </summary>
        /// <returns></returns>
        public List<Mensaje> ObtenerTodos()
        {
            List<Mensaje> mensajes = new List<Mensaje>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    mensajes = _session.Query<Mensaje>()
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mensajes;
        }

        /// <summary>
        /// Devuelve el total de Mensajes
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public int ObtenerTotal(string filtro)
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    if (filtro.Length == 0)
                        filtro = "1 == 1";

                    total = _session.Query<Mensaje>()
                        .Where(filtro)
                        .Count();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }

        /// <summary>
        /// Guardar un Mensaje
        /// </summary>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        public bool Guardar(Mensaje mensaje)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(mensaje);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        /// <summary>
        /// Elimina un Mensaje
        /// </summary>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        public bool Eliminar(Mensaje mensaje)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.Delete(mensaje);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultado;
        }

        #endregion
    }

}
