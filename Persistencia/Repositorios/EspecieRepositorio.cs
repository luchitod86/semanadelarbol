﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class EspecieRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspecieRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve una especie de Arbol según un Id
        /// </summary>
        /// <param name="Id_Especie"></param>
        /// <returns></returns>
        public Especie Obtener(int Id_Especie)
        {
            Especie pais = new Especie();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    pais = _session.Query<Especie>()
                        .Where(x => x.Id == Id_Especie)
                        .FirstOrDefault();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return pais;
        }

        /// <summary>
        /// Devuelve todas las Especies
        /// </summary>
        /// <returns></returns>
        public List<Especie> ObtenerTodas()
        {
            List<Especie> especies = new List<Especie>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    especies = _session.Query<Especie>().OrderBy(x => x.Nombre).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return especies;
        }

        /// <summary>
        /// Devuelve todas las Especies
        /// </summary>
        /// <returns></returns>
        public List<Especie> ObtenerTodasPorPais(int idPais)
        {
            List<Especie> especies = new List<Especie>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    bool? nullableTrue = true;
                    bool? nullableFalse = false;
                    especies = _session.Query<Especie>().Where(x => (x.DesdeWiki == nullableFalse || x.Aprobado == nullableTrue) &&(x.Pais == null || x.Pais.Id == idPais)).OrderBy(x => x.Nombre).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return especies;
        }

        /// <summary>
        /// Devuelve todas las Localizaciones de una Especie
        /// </summary>
        /// <returns></returns>
        public List<EspeciePais> ObtenerLocalizaciones(int idEspecie)
        {
            var localizaciones = new List<EspeciePais>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    localizaciones = _session.Query<EspeciePais>()
                        .Where(x => x.Especie.Id == idEspecie)
                        .OrderBy(x => x.Nombre).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return localizaciones;
        }

        /// <summary>
        /// Devuelve todas las Especies dadas de alta desde la Wiki
        /// </summary>
        /// <returns></returns>
        public List<Especie> ObtenerTodasWiki()
        {
            List<Especie> especies = new List<Especie>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    bool? nullableTrue = true;
                    especies = _session.Query<Especie>()
                        .Where(x => x.DesdeWiki == nullableTrue)
                        .OrderBy(x => x.NombreCientifico).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return especies;
        }

        /// <summary>
        /// Devuelve el total de las especies dadas de alta desde la wiki
        /// </summary>       
        /// <returns></returns>
        public int ObtenerTotalEspeciesWiki()
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Especie> query = _session.Query<Especie>();

                    bool? nullableTrue = true;
                    total = query.Where(x => x.DesdeWiki == nullableTrue).Count();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return total;
        }

        /// <summary>
        /// Devuelve todas los usuarios que aportaron en la info de una Especie
        /// </summary>
        /// <returns></returns>
        public List<Usuario> ObtenerUsuariosAporteWiki(int idEspecie)
        {
            List<EspecieUsuario> usuarios = new List<EspecieUsuario>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                     usuarios = _session.Query<EspecieUsuario>()
                        .Where(x => x.Especie.Id == idEspecie).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            List<Usuario> ret = usuarios.Select(x => x.Usuario).ToList();

            return ret;
        }

        /// <summary>
        /// Devuelve todas los paises asociados a una Especie
        /// </summary>
        /// <returns></returns>
        public List<Pais> ObtenerPaisesEspecieWiki(int idEspecie)
        {
            List<EspeciePaisWiki> paises = new List<EspeciePaisWiki>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    paises = _session.Query<EspeciePaisWiki>()
                       .Where(x => x.Especie.Id == idEspecie).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
            List<Pais> ret = paises.Select(x => x.Pais).ToList();

            return ret;
        }

        public bool Guardar(Especie especie)
        {
            bool resultado = false;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    _session.SaveOrUpdate(especie);
                    _session.Flush();
                    transaction.Commit();
                    resultado = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return resultado;
        }

        #endregion
    }
}
