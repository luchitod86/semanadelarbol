﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic;

namespace Persistencia.Repositorios
{
    public class DonacionRepositorio
    {
        private readonly ISession _session;

        #region Constructors

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public DonacionRepositorio()
        {
            try
            {
                _session = NHibernateSessionHelper.GetSession();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Devuelve una donación, según un Id
        /// </summary>
        /// <param name="Id_Donacion"></param>
        /// <returns></returns>
        public UsuarioDonacion Obtener(int Id_Donacion)
        {
            UsuarioDonacion donacion = new UsuarioDonacion();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    donacion = _session.Query<UsuarioDonacion>()
                        .Where(x => x.Id == Id_Donacion)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return donacion;
        }

        /// <summary>
        /// Devuelve todas las donaciones de usuario
        /// </summary>
        /// <param name="idDonante"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioDonacion> ObtenerTodas(int idDonante = -1, string pais = "", int periodo = -1)
        {
            var donaciones = new List<UsuarioDonacion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioDonacion> query = _session.Query<UsuarioDonacion>();
                    
                    if ( idDonante != -1 )
                        query = query.Where( x => x.Donante.Id == idDonante );

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if ( periodo != -1 )
                        query = query.Where( x => x.Fecha.Year == periodo );

                    donaciones = query.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return donaciones;
        }

        /// <summary>
        /// Devuelve todas una lista de Donaciones
        /// </summary>
        /// <param name="idDonante"></param>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="soloConFoto"></param>
        /// <returns></returns>
        public List<UsuarioDonacion> ObtenerTodas(int idDonante = -1, int idPais = Comun.Constantes.Defaults.Pais.ID, int periodo = -1)
        {
            var donaciones = new List<UsuarioDonacion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioDonacion> query = _session.Query<UsuarioDonacion>();

                    if (idDonante != -1)
                        query = query.Where(x => x.Donante.Id == idDonante);

                    if ( idPais != -1 )
                        query = query.Where(x => x.Pais.Id == idPais);

                    if (periodo != -1)
                        query = query.Where(x => x.Fecha.Year == periodo);

                    donaciones = query.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return donaciones;
        }

        /// <summary>
        /// Devuelve la cantidad total de Donaciones
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public int ObtenerTotalDonaciones(int idPais = -1, int periodo = -1, int idUsuario = -1)
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioDonacion> query = _session.Query<UsuarioDonacion>();

                    if (idUsuario > 0)
                        query = query.Where(x => x.Donante.Id == idUsuario);

                    if (idPais != -1)
                        query = query.Where(x => x.Pais.Id == idPais);

                    if (periodo != -1)
                        query = query.Where(x => x.Fecha.Year == periodo);

                    total = query.Count();
                    if (total > 0)
                        total = query.Sum(x => x.Cantidad);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return total;
        }

        /// <summary>
        /// Devuelve el total de todos los Donantes
        /// </summary>
        /// <param name="idPais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public int ObtenerTotalDonantes(int idPais = -1, int periodo = -1)
        {
            int total = 0;
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<Usuario> query = _session.Query<Usuario>();

                    query = query.Where(x => x.Latitud != 0 && x.Longitud != 0);

                    if ( periodo != -1 )
                        query = query.Where(x => x.Donaciones.Any(y => y.Cantidad > 0 && y.Fecha.Year == periodo));

                    if ( idPais != -1 )
                        query = query.Where(x => x.Pais.Id == idPais);

                    total = query.Count();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return total;
        }

        /// <summary>
        /// Devuelve las últimas Donaciones, según una cantidad
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="pais"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public List<UsuarioDonacion> ObtenerUltimasDonaciones(int cantidad, string pais = "", int periodo = -1)
        {
            List<UsuarioDonacion> donaciones = new List<UsuarioDonacion>();
            try
            {
                using (ITransaction transaction = _session.BeginTransaction())
                {
                    IQueryable<UsuarioDonacion> query = _session.Query<UsuarioDonacion>();

                    if ( !string.IsNullOrEmpty(pais) )
                        query = query.Where( x => x.Pais.Nombre.ToUpper() == pais.ToUpper() );

                    if ( periodo != -1 )
                        query = query.Where( x => x.Fecha.Year == periodo );

                    donaciones = query
                                    .OrderByDescending(x => x.Id)
                                    .Take(cantidad)
                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return donaciones;
        }

        #endregion
    }
}
