﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Web.Configuration;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using System.Threading;

namespace Persistencia
{
    /// <summary>
    /// Helper class to handle nHibernate Session creation and access..
    /// </summary>
    public class NHibernateSessionHelper
    {
        private static ISessionFactory _sessionFactory = null;

        private static object lockObject = new object();

        /// <summary>
        /// Returns current nHibernate Session
        /// If never created before, it uses a SessionFactory to create one..
        /// </summary>
        /// <returns></returns>
        public static ISession GetSession()
        {
            ISessionFactory sessionFactory = null;
            ISession session = null;
            try
            {
                sessionFactory = GetSessionFactory();
                session = sessionFactory.OpenSession();
                if (session == null)
                {
                    throw new InvalidOperationException("Cannot open nHibernate session.");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return session;
        }

        /// <summary>
        /// Creates (and returns) a nHibernate Session, including database connection and mappings
        /// </summary>
        /// <returns></returns>
        private static ISessionFactory GetSessionFactory()
        {
            /*
                The lock lets the first thread come in and create the static session factory.
                The second thread needs to wait until the first is leaving the lock scope.
                When the second comes in, the session factory had been created and it just takes it.
                Without the lock, the session factory is created multiple times in parallel.
            */
            lock (lockObject)
            {
                try
                {
                    if (_sessionFactory == null)
                    {
                        #region > Fluent Configuration
                        //string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
                        string connString = ConfigurationManager.AppSettings.GetValues("myConnectionString")[0];

                        string MappingsDomain = "Persistencia"; //Project name (assembly) where mappings classes are located..
                        System.Reflection.Assembly asm = System.Reflection.Assembly.Load(MappingsDomain);

                        _sessionFactory = Fluently.Configure()
                            .Database(MsSqlConfiguration.MsSql2008.ConnectionString(connString).ShowSql())
                            .Mappings(m => m.FluentMappings.AddFromAssembly(asm))
                            .BuildSessionFactory();
                        #endregion

                        if (_sessionFactory == null)
                        {
                            throw new InvalidOperationException("Could not build SessionFactory");
                        }
                    }
                }
                catch (ThreadAbortException)
                {
                    Thread.ResetAbort();
                }
                catch (Exception)
                {
                    throw;
                }
                return _sessionFactory;
            }
        }
    }
}
