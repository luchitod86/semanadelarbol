﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Entidades;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase NoticiaImagen
    /// </summary>
    public class NoticiaImagenMap : ClassMap<NoticiaImagen>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public NoticiaImagenMap()
        {
            Table("NOTICIA_IMAGEN");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Noticia).Column("IdNoticia");
            Map(x => x.URLImagen).Column("URLImagen");
        }
    }
}
