﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase UsuarioAdopcion
    /// </summary>
    public class UsuarioAdopcionMap : ClassMap<UsuarioAdopcion>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioAdopcionMap()
        {
            Table("USUARIO_ADOPCION");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Usuario).Column("IdUsuario");
            References(x => x.Donante).Column("IdDonante");
            References(x => x.Arbol).Column("IdArbol");
            Map(x => x.Cantidad).Column("Cantidad");
            Map(x => x.Fecha).Column("Fecha");
            References(x => x.Pais).Column("IdPais");
        }
    }
}
