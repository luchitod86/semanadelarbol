﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase UsuarioTipo
    /// </summary>
    public class UsuarioTipoMap : ClassMap<UsuarioTipo>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioTipoMap()
        {
            Table("USUARIO_TIPO");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Descripcion).Column("Descripcion");
        }
    }
}