﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Usuario
    /// </summary>
    public class UsuarioMap : ClassMap<Usuario>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioMap()
        {
            Table("USUARIO");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.NombreUsuario).Column("NombreUsuario");
            Map(x => x.Password).Column("Password");
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Apellido).Column("Apellido");
            Map(x => x.Telefono).Column("Telefono");
            Map(x => x.Email).Column("Email");
            Map(x => x.Web).Column("Web");
            References(x => x.Tipo).Column("IdTipo");
            References(x => x.Perfil).Column("IdPerfil");
            References(x => x.Pais).Column("IdPais");
            References(x => x.Provincia).Column("IdProvincia");
            Map(x => x.Ciudad).Column("Ciudad");
            Map(x => x.Latitud).Column("Latitud");
            Map(x => x.Longitud).Column("Longitud");
            Map(x => x.NombreUsuarioFacebook).Column("NombreUsuarioFacebook");
            Map(x => x.NombreUsuarioTwitter).Column("NombreUsuarioTwitter");
            Map(x => x.NombreUsuarioGooglePlus).Column("NombreUsuarioGooglePlus");
            Map(x => x.FechaCreacion).Column("FechaCreacion");
            HasMany<UsuarioPlantacion>(x => x.Plantaciones)
                .Table("USUARIO_PLANTACION")
                .Inverse()
                .KeyColumn("IdUsuario")
                .Cascade.AllDeleteOrphan()
                .Not.LazyLoad();
            HasMany<UsuarioDonacion>(x => x.Donaciones)
                .Table("USUARIO_DONACION")
                .Inverse()
                .KeyColumn("IdDonante")
                .Cascade.AllDeleteOrphan()
                .Not.LazyLoad();
            HasMany<UsuarioAdopcion>(x => x.Adopciones)
                .Table("USUARIO_ADOPCION")
                .Inverse()
                .KeyColumn("IdUsuario")
                .Cascade.AllDeleteOrphan()
                .Not.LazyLoad();
            HasMany<UsuarioActividad>(x => x.Actividades)
                .Table("USUARIO_ACTIVIDAD")
                .Inverse()
                .KeyColumn("IdUsuario")
                .Cascade.AllDeleteOrphan()
                .Not.LazyLoad();
        }
    }
}
