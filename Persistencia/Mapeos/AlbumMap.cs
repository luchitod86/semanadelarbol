﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Album
    /// </summary>
    public class AlbumMap : ClassMap<Album>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public AlbumMap()
        {
            Table("ALBUM");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.IdEntidad).Column("IdEntidad");
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.IdAlbum).Column("IdAlbum");
            Map(x => x.Url).Column("Url");
            Map(x => x.Fecha).Column("Fecha");
            References(x => x.Pais).Column("IdPais");
        }
    }
}
