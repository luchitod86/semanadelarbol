﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Vivero
    /// </summary>
    public class ViveroMap : ClassMap<Vivero>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ViveroMap()
        {
            Table("VIVERO");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Usuario).Column("IdUsuario");
            Map(x => x.Direccion).Column("Direccion");
            Map(x => x.Telefono).Column("Telefono");
            Map(x => x.Email).Column("Email");
            Map(x => x.Web).Column("Web");
            Map(x => x.Imagen).Column("Imagen").CustomType("BinaryBlob").Length(int.MaxValue).Nullable();
        }
    }
}
