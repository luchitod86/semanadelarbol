﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Configuracion
    /// </summary>
    public class ConfiguracionMap : ClassMap<Configuracion>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ConfiguracionMap()
        {
            Table("CONFIGURACION");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Email).Column("IdEmail");
            References(x => x.Social).Column("IdSocial");
        }
    }
}
