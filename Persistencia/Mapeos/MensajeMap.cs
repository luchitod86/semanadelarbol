﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Entidades;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Mensaje
    /// </summary>
    public class MensajeMap : ClassMap<Mensaje>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public MensajeMap()
        {
            Table("MENSAJE");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Texto).Column("Texto");
        }
    }
}
