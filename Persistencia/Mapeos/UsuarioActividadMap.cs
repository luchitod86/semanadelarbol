﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Entidades;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Actividad
    /// </summary>
    public class UsuarioActividadMap : ClassMap<UsuarioActividad>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioActividadMap()
        {
            Table("USUARIO_ACTIVIDAD");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Usuario).Column("IdUsuario");
            Map(x => x.FechaYHora).Column("FechaYHora");
            Map(x => x.Descripcion).Column("Descripcion");
            Map(x => x.Comentarios).Column("Comentarios");
            Map(x => x.Latitud).Column("Latitud");
            Map(x => x.Longitud).Column("Longitud");
            References(x => x.Pais).Column("IdPais");
            References(x => x.Provincia).Column("IdProvincia");
            Map(x => x.EsPublica).Column("EsPublica");
        }
    }
}
