﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Configuracion de Sociales
    /// </summary>
    public class ConfiguracionSocialMap : ClassMap<ConfiguracionSocial>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ConfiguracionSocialMap()
        {
            Table("CONFIGURACION_SOCIAL");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Facebook).Column("Facebook");
            Map(x => x.Twitter).Column("Twitter");
            Map(x => x.YouTube).Column("YouTube");
        }
    }
}
