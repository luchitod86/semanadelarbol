﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase EspeciePais
    /// </summary>
    public class EspeciePaisMap : ClassMap<EspeciePais>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspeciePaisMap()
        {
            Table("ESPECIE_PAIS");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Especie).Column("IdEspecie");
            References(x => x.Pais).Column("IdPais");
            Map(x => x.Nombre).Column("Nombre");
        }
    }
}
