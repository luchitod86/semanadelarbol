﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Especie
    /// </summary>
    public class EspecieMap : ClassMap<Especie>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspecieMap()
        {
            Table("ESPECIE");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.NombreCientifico).Column("NombreCientifico");
            Map(x => x.FactorCO2).Column("FactorCO2");
            Map(x => x.DesdeWiki).Column("DesdeWiki");
            Map(x => x.Aprobado).Column("Aprobado");
            Map(x => x.MasInfo).Column("MasInfo");
            Map(x => x.Origen).Column("Origen");
            Map(x => x.Ecorregion).Column("Ecorregion");
            Map(x => x.Altura).Column("Altura");
            Map(x => x.Follaje).Column("Follaje");
            Map(x => x.Hojas).Column("Hojas");
            Map(x => x.Flores).Column("Flores");
            Map(x => x.Frutos).Column("Frutos");
            Map(x => x.AptoVereda).Column("AptoVereda");
            Map(x => x.AptoCantero).Column("AptoCantero");
            Map(x => x.AptoPlaza).Column("AptoPlaza");
            Map(x => x.EsNativo).Column("EsNativo");
            Map(x => x.EsExotico).Column("EsExotico");
            References(x => x.Pais).Column("IdPais");
            HasMany<EspeciePais>(x => x.Localizaciones)
                .Table("ESPECIE_PAIS")
                .Inverse()
                .KeyColumn("IdEspecie")
                .Cascade.AllDeleteOrphan()
                //.Not.LazyLoad();
                .LazyLoad();
            HasMany<EspecieImagen>(x => x.Imagenes)
               .Table("ESPECIE_IMAGEN")
               .Inverse()
               .KeyColumn("IdEspecie")
               .Cascade.AllDeleteOrphan()
               .Not.LazyLoad();
            HasMany<EspecieUsuario>(x => x.Usuarios)
               .Table("ESPECIE_USUARIO")
               .Inverse()
               .KeyColumn("IdEspecie")
               .Cascade.AllDeleteOrphan()
               .Not.LazyLoad();
            HasMany<EspeciePaisWiki>(x => x.Paises)
                .Table("ESPECIE_PAIS_WIKI")
                .Inverse()
                .KeyColumn("IdEspecie")
                .Cascade.AllDeleteOrphan()
                //.Not.LazyLoad();
                .LazyLoad();
        }
    }
}
