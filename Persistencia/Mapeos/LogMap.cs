﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Error
    /// </summary>
    public class LogMap : ClassMap<Log>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public LogMap()
        {
            Table("LOG");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Tipo).Column("IdTipo");
            References(x => x.Seccion).Column("IdSeccion");
            Map(x => x.FechaYHora).Column("FechaYHora");
            Map(x => x.Texto).Column("Texto").CustomType("StringClob").CustomSqlType("varchar(max)");
            Map(x => x.Detalle).Column("Detalle").CustomType("StringClob").CustomSqlType("varchar(max)");
        }
    }
}
