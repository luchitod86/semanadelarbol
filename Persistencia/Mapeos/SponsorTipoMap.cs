﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase SponsorTipo
    /// </summary>
    public class SponsorTipoMap : ClassMap<SponsorTipo>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SponsorTipoMap()
        {
            Table("SPONSOR_TIPO");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
        }
    }
}
