﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Email
    /// </summary>
    public class EmailMap : ClassMap<Email>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EmailMap()
        {
            Table("EMAIL");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Mail).Column("Email");
            Map(x => x.Comentarios).Column("Comentarios");
            Map(x => x.Fecha).Column("Fecha");
        }
    }
}
