﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
   /// <summary>
    /// Mapeo para la clase Configuracion de Email
    /// </summary>
    public class ConfiguracionEmailMap : ClassMap<ConfiguracionEmail>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ConfiguracionEmailMap()
        {
            Table("CONFIGURACION_EMAIL");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.EmisorAlias).Column("Emisor_Alias");
            Map(x => x.EmisorDireccion).Column("Emisor_Direccion");
            Map(x => x.EmisorUsuario).Column("Emisor_Usuario");
            Map(x => x.EmisorPassword).Column("Emisor_Password");
            Map(x => x.SMTPServidor).Column("SMTP_Servidor");
            Map(x => x.SMTPPuerto).Column("SMTP_Puerto");
            Map(x => x.SMTPHabilitarSSL).Column("SMTP_HabilitarSSL");
        }
    }
}
