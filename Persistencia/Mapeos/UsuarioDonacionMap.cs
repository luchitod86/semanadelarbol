﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Entidades;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase UsuarioDonacion
    /// </summary>
    public class UsuarioDonacionMap : ClassMap<UsuarioDonacion>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioDonacionMap()
        {
            Table("USUARIO_DONACION");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Donante).Column("IdDonante");
            References(x => x.Arbol).Column("IdArbol");
            Map(x => x.Cantidad).Column("Cantidad");
            Map(x => x.Disponibles).Column("Disponibles");
            Map(x => x.Adoptados).Column("Adoptados");
            Map(x => x.Fecha).Column("Fecha");
            References(x => x.Pais).Column("IdPais");
        }
    }
}
