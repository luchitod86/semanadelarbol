﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Pais
    /// </summary>
    public class PaisMap : ClassMap<Pais>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public PaisMap()
        {
            Table("PAIS");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Bandera).Column("Bandera");
            Map(x => x.Lenguaje).Column("Lenguaje");
            Map(x => x.Activo).Column("Activo");
            References(x => x.Continente).Column("IdContinente");
        }
    }
}
