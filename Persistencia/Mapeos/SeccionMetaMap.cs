﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Mapeos
{
    class SeccionMetaMap : ClassMap<SeccionMeta>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SeccionMetaMap()
        {
            Table("SECCION_META");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Seccion).Column("IdSeccion");
            Map(x => x.Keywords).Column("Keywords");
            Map(x => x.Description).Column("Description");
        }
    }
}
