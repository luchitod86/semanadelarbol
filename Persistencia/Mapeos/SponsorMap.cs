﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Sponsor
    /// </summary>
    public class SponsorMap : ClassMap<Sponsor>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SponsorMap()
        {
            Table("SPONSOR");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Web).Column("Web");
            References(x => x.Tipo).Column("IdTipo");
            References(x => x.Pais).Column("IdPais");
            //Map(x => x.Imagen).Column("Imagen");
        }
    }
}
