﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase UsuarioEspecie
    /// </summary>
    public class EspecieUsuarioMap : ClassMap<EspecieUsuario>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspecieUsuarioMap()
        {
            Table("ESPECIE_USUARIO");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Usuario).Column("IdUsuario");
            References(x => x.Especie).Column("IdEspecie");
        }
    }
}
