﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Seccion
    /// </summary>
    public class SeccionMap : ClassMap<Seccion>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SeccionMap()
        {
            Table("SECCION");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Descripcion).Column("Descripcion");
            References(x => x.Tipo).Column("IdTipo");
            Map(x => x.Privada).Column("Privada");
            HasMany<SeccionContenido>(x => x.Contenidos)
                .Table("SECCION_CONTENIDO")
                .Inverse()
                .KeyColumn("IdSeccion")
                .Cascade.AllDeleteOrphan()
                //.Not.LazyLoad();
                .LazyLoad();
        }
    }
}
