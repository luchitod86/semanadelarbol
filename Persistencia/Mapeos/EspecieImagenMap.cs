﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Entidades;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase EspecieImagen
    /// </summary>
    public class EspecieImagenMap : ClassMap<EspecieImagen>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspecieImagenMap()
        {
            Table("ESPECIE_IMAGEN");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Especie).Column("IdEspecie");
            Map(x => x.URLImagen).Column("URLImagen");
        }
    }
}
