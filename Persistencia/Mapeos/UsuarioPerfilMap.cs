﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase UsuarioPerfil
    /// </summary>
    public class UsuarioPerfilMap : ClassMap<UsuarioPerfil>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioPerfilMap()
        {
            Table("USUARIO_PERFIL");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Descripcion).Column("Descripcion");
        }
    }
}
