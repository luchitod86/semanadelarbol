﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Entidades;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase UsuarioPlantacion
    /// </summary>
    public class UsuarioPlantacionMap : ClassMap<UsuarioPlantacion>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public UsuarioPlantacionMap()
        {
            Table("USUARIO_PLANTACION");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Usuario).Column("IdUsuario");
            References(x => x.Arbol).Column("IdArbol");
            Map(x => x.Latitud).Column("Latitud");
            Map(x => x.Longitud).Column("Longitud");
            Map(x => x.Cantidad).Column("Cantidad");
            Map(x => x.Foto).Column("Foto").CustomType("BinaryBlob").Length(int.MaxValue).Nullable();
            Map(x => x.TieneFoto).Column("TieneFoto");
            Map(x => x.Fecha).Column("Fecha");
            References(x => x.Pais).Column("IdPais");
        }
    }
}
