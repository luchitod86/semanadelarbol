﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase EspeciePaisWiki
    /// </summary>
    public class EspeciePaisWikiMap : ClassMap<EspeciePaisWiki>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public EspeciePaisWikiMap()
        {
            Table("ESPECIE_PAIS_WIKI");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Especie).Column("IdEspecie");
            References(x => x.Pais).Column("IdPais");
        }
    }
}
