﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase LogTipo
    /// </summary>
    public class LogTipoMap : ClassMap<LogTipo>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public LogTipoMap()
        {
            Table("LOG_TIPO");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Descripcion).Column("Descripcion");
        }
    }
}
