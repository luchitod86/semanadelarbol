﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Entidades;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Noticia
    /// </summary>
    public class NoticiaMap : ClassMap<Noticia>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public NoticiaMap()
        {
            Table("NOTICIA");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Titulo).Column("Titulo");
            Map(x => x.Resumen).Column("Resumen");
            Map(x => x.Texto).Column("Texto").Length(Int32.MaxValue);
         //   Map(x => x.Foto).Column("Foto").CustomType("BinaryBlob").Length(int.MaxValue).Nullable();
            Map(x => x.Fecha).Column("Fecha");
            References(x => x.Pais).Column("IdPais");
            HasMany<NoticiaImagen>(x => x.Imagenes)
               .Table("NOTICIA_IMAGEN")
               .Inverse()
               .KeyColumn("IdNoticia")
               .Cascade.AllDeleteOrphan()
               .Not.LazyLoad();
        }
    }
}
