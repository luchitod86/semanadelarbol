﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase SeccionTipo
    /// </summary>
    public class SeccionTipoMap : ClassMap<SeccionTipo>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SeccionTipoMap()
        {
            Table("SECCION_TIPO");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Descripcion).Column("Descripcion");
        }
    }
}
