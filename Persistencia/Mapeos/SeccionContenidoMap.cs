﻿using Entidades;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase SeccionContenido
    /// </summary>
    public class SeccionContenidoMap : ClassMap<SeccionContenido>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public SeccionContenidoMap()
        {
            Table("SECCION_CONTENIDO");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.Seccion).Column("IdSeccion");
            References(x => x.Pais).Column("IdPais");
            Map(x => x.Campo).Column("Campo");
            Map(x => x.Contenido).Column("Contenido").CustomType("StringClob").CustomSqlType("varchar(max)");
            Map(x => x.Valor).Column("Valor");
        }
    }
}
