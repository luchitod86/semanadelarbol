﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entidades;
using FluentNHibernate.Mapping;

namespace Persistencia.Mapeos
{
    /// <summary>
    /// Mapeo para la clase Continente
    /// </summary>
    public class ContinenteMap : ClassMap<Continente>
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public ContinenteMap()
        {
            Table("CONTINENTE");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Nombre).Column("Nombre");
            Map(x => x.Descripcion).Column("Descripcion");
        }
    }
}
